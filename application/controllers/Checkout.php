<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends CI_Controller {
	private $sess_id = 0;
	private $langId;
	private $lang_code;
	public function __construct()
	{
		parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
		$this->load->model("checkout_model");
		$this->load->model('user_model');
		$this->load->model('product_model');
		$this->load->model('wishlist_model');
		$this->load->library('form_validation');
		$this->load->library('cart');
		if($this->session->userdata('log_in')== false){redirect('/');}
		else
		{
			$sess_val = $this->session->userdata('log_in');
			$this->sess_id = $sess_val["user_id"];
		}
	}
	
	public function index(){
		$logged_in = $this->session->userdata('log_in');
		$user_id = $logged_in['user_id'];
		$user = $this->user_model->userdeatails($user_id);
		foreach($user as $u){
			$data['user'] = $u;	
		}
		//print_r($this->cart->contents());
		//die("checkout/index");
		foreach($this->cart->contents() as $con){
			if($con["type"] != "cash_gift")
			{
				if($con['qty'] > $con['stock_quantity']){
					$flashmsg = 'Products marked with *** are not available in the desired quantity or not in stock!';
					$this->session->set_flashdata("errorMsg",$flashmsg);
					redirect('cart/view'); 	
				}
				else{
			
					if($con['wishlist_id']!=0){
						$con['address'] = $this->checkout_model->getAddressByWid($con['wishlist_id']);
						$address[] = $con['address'];
					}else{
						
						$no_wishlist[] = 'no wishlist found';	
					}
				}
			}
		}
		
		$data['shipping_address'] = (isset($address) && !empty($address)) ? $address : '';
		$data['wishlist_exists'] = (isset($no_wishlist) && !empty($no_wishlist)) ?  $no_wishlist : '';
		$data['checkbox'] = 0;
		$question = $this->input->post('question');
		if($_POST){
			if(!isset($question)){
				$data['checkbox'] = 1;
			}
		}
		
		$this->form_validation->set_rules('firstName', 'First Name', 'required');
        $this->form_validation->set_rules('lastName', 'Last Name', 'required');
        $this->form_validation->set_rules('postcode', 'Postcode', 'required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('mobile', 'Mobile no.', 'required');
		$this->form_validation->set_rules('shipping_method', 'Shipping Method', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		
		
		if(isset($question) && $question == 1){
			$this->form_validation->set_rules('shipfirstName', 'Shipping First Name', 'required');
			$this->form_validation->set_rules('shiplastName', 'Shipping Last Name', 'required');
			$this->form_validation->set_rules('shipAddress', 'Shipping Address1', 'required');
			$this->form_validation->set_rules('shipPostcode', 'Shipping Postcode', 'required');
			$this->form_validation->set_rules('shipKecamatan', 'Shipping Kecamatan', 'required');
			$this->form_validation->set_rules('shipCity', 'Shipping City', 'required');
			$this->form_validation->set_rules('shipMobile', 'Shipping Mobile no.', 'required');
			
		}else{
			if($data['wishlist_exists'] != ''){
				$this->form_validation->set_rules('question', 'Ship to different address', 'required');
			}
		}
		
		
		if ($this->form_validation->run() == TRUE)
		{
			$billing = array();
			$billing['fname'] = $this->input->post('firstName');
			$billing['lname'] = $this->input->post('lastName');
			$billing['address'] = $this->input->post('address');
			$billing['address2'] = $this->input->post('address2');
			$billing['postcode'] = $this->input->post('postcode');
			$billing['kecamatan'] = $this->input->post('kecamatan');
			$billing['city'] = $this->input->post('city');
			$billing['mobile'] = $this->input->post('mobile');
			
			$shipping = array();
			$shipping['shipfirstName'] = $this->input->post('shipfirstName');
			$shipping['shiplastName'] = $this->input->post('shiplastName');
			$shipping['shipAddress'] = $this->input->post('shipAddress');
			$shipping['shipAddress2'] = $this->input->post('shipAddress2');
			$shipping['shipPostcode'] = $this->input->post('shipPostcode');
			$shipping['shipKecamatan'] = $this->input->post('shipKecamatan');
			$shipping['shipCity'] = $this->input->post('shipCity');
			$shipping['shipMobile'] = $this->input->post('shipMobile');
			
			
			$data['billing'] = $billing;
			$data['shipping'] = $shipping;
			$data['question'] = $this->input->post('question');
			$data['shipping_method'] = $this->input->post('shipping_method');
			$data['contents'] = $this->cart->contents();
			$data['billing_same_shipping'] = $this->input->post('billing_same_shipping');
			$billing_name = $billing['fname'].' '.$billing['lname'];
			
			foreach($this->cart->contents() as $contents){
				if($contents['wishlist_id']!=0){
					$contents['address'] = $this->checkout_model->getAddressByWid($contents['wishlist_id']);
				}else{
					$contents['address'] = strtoupper($shipping['shipfirstName'].' '.$shipping['shiplastName'].','.$shipping['shipAddress'].','.$shipping['shipAddress2'].','.$shipping['shipKecamatan'].','.$shipping['shipCity'].','.$shipping['shipPostcode']);
				}
				$cart[] = $contents;	
			}
			
			$data['contents'] = $cart;
			$this->session->set_userdata('order',$data);
			redirect("checkout/payment");	
		}

		if(isset($_SESSION['order'])){
			$details = $this->session->userdata('order');
			$billing = $details['billing'];
			$shipping = $details['shipping'];
			$data['lang_code'] = $this->lang_code;
			$data['page_title'] = $this->lang->line("checkout");
			$data['fname'] = $billing['fname'];
			$data['lname'] = $billing['lname'];
			$data['address'] = $billing['address'];
			$data['address2'] = $billing['address2'];
			$data['postcode'] = $billing['postcode'];
			$data['kecamatan'] = $billing['kecamatan'];
			$data['city'] = $billing['city'];
			$data['mobile'] = $billing['mobile'];
			$data['question'] = $details['question'];
			$data['shipping_method'] = $details['shipping_method'];
			
			$data['shipfirstName'] = isset($shipping['shipfirstName']) ? $shipping['shipfirstName'] :'';
			$data['shiplastName'] = isset($shipping['shiplastName']) ? $shipping['shiplastName'] : '';
			$data['shipAddress'] = isset($shipping['shipAddress']) ? $shipping['shipAddress'] : '';
			$data['shipAddress2'] = isset($shipping['shipAddress2']) ? $shipping['shipAddress2'] : '';
			$data['shipPostcode'] = isset($shipping['shipPostcode']) ? $shipping['shipPostcode'] :'';
			$data['shipKecamatan'] = isset($shipping['shipKecamatan']) ? $shipping['shipKecamatan'] : '';
			$data['shipCity'] = isset($shipping['shipCity']) ? $shipping['shipCity'] : 'Jakarta Raya';
			$data['shipMobile'] = isset($shipping['shipMobile']) ? $shipping['shipMobile'] : '' ;
			$data['billing_same_shipping'] = isset($details['billing_same_shipping']) ? $details['billing_same_shipping'] : '';
		}
		
		$this->load->ftemplate("checkout/checkout",$data);	
	}
	public function payment(){
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("payment");
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'required');

		$photopath = $this->config->item('image_display_path');
		$thumb_size= $this->config->item('thumb_size');
		$no_image = $photopath."product/cash_gift.png";
		
		//$cart_products = $this->checkout_model->get_cart_list($this->sess_id);
		$cart_products = $this->cart->contents();
		//print_r($cart_products);die();
		foreach($cart_products as $product){
			$product_slug = $this->product_model->getProductUrl_name($product['id'],$product['name']);
			$fetch_image = $this->product_model->getProductImageById($product['id']);
			$product['image_path'] = $photopath."product/".$product['id'].$thumb_size.$fetch_image;
			$product['no_image_path'] = $no_image; 
			if($product['wishlist_id']!='' && is_numeric($product['wishlist_id'])){ 
				$wishlist_det = $this->wishlist_model->getwishlistByid($product['wishlist_id']);
				if(isset($wishlist_det)){
					$product['wishlist_name'] = $wishlist_det->title;
					$product['wishlist_url'] = '~'.$wishlist_det->url;	
				}
			}
			$product['product_url'] = $product_slug;
			$all_products[] = $product;
			$finalData[] = $product;
		}
		
		$session = $_SESSION['order'];
		$order_data['user'] = $session['user'];
		$order_data['billing'] = $session['billing'];
		$order_data['shipping'] = $session['shipping'];
		$order_data['question'] = $session['question'];
		$data['shipping_method'] = $order_data['shipping_method'] = $session['shipping_method'];
		
		if($data['shipping_method'] == 'onedayservice_jakarta'){
			
			$shipping = $this->checkout_model->getShippingRate();
			$shipping_charge = trim($shipping->shipping_fees);
			$data['shipping_charge'] = $shipping_charge;
			$intShipCharge = preg_match_all('!\d+!', $data['shipping_charge'], $matches);
			$scharge = (isset($data['shipping_charge']) && $data['shipping_charge']!='') ? $matches[0][0] : 0;
			
			$this->session->set_userdata('shipping',array('shipping_method' => $data['shipping_method'],'shipping_charge' => $data['shipping_charge'],'shipping_amount' => $scharge));
		}
		
		$finalData = (isset($finalData) && !empty($finalData)) ? $finalData : '';
		$all_products = (isset($all_products) && !empty($all_products)) ? $all_products : '';
		
		$this->session->set_userdata('cart_data',$finalData);
		$this->session->set_userdata('order_data',$order_data);
		
		$data['cart_data'] = $all_products;
		$data['total_without_discount'] = $this->cart->total();
		$discount = $this->session->userdata('discount');
		$coupon_code = $this->session->userdata('coupon_code');
		$discChrg = str_replace(',','',$discount);
		//$amt = $this->session->userdata('gross_amount');
		/*if(isset($amt))
		{
			echo "==".$gross = $amt;
		}else{
			echo "--".$gross =($this->cart->total() - $discount);
		}*/
		
		$gross =($this->cart->total() - $discChrg + (isset($scharge) ? $scharge : 0));
		$this->session->set_userdata('gross_amount',$gross);
		$data['total'] = isset($gross) ? $gross : $this->cart->total();
		$data['discount'] = isset($discount) ? $discount : '';
		$data['coupon_code'] = isset($coupon_code) ? $coupon_code : '';
		$payment_method = $this->input->post('payment_method');
		$data['payment_method'] = isset($payment_method) ? $payment_method : '';
		if ($this->form_validation->run() == TRUE)
		{
			$this->session->set_userdata('payment_method',$data['payment_method']);			
			redirect("payment/vtweb_checkout");	
		}else{
			$this->load->ftemplate("checkout/payment",$data);	
		}
	}
	public function success(){
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("success");
		$this->load->ftemplate("checkout/success", $data);	
	}
}

