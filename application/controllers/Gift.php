<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gift extends CI_Controller {
	private $langId;
	private $lang_code;
	 function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
					$this->load->helper(array('form', 'url'));
					$this->load->helper('cookie');
					$this->load->library('session'); 
					 $this->load->library('email');
					$this->load->library('form_validation');
					$this->load->model('gift_model'); 
					 if(empty($this->session->userdata['log_in']['user_id'])){ redirect('/');  }

       }


					
	public function index(){
		 $uid = $this->session->userdata['log_in']['user_id'];
		 $data['lang_code'] = $this->lang_code;
		 $data['page_title'] = $this->lang->line("gift");
		$this->load->ftemplate('gift/index', $data);
	}
	public function deatils(){
		 $uid = $this->session->userdata['log_in']['user_id'];
		 $data['lang_code'] = $this->lang_code;
		 $data['page_title'] = $this->lang->line("gift_details");
		$this->load->ftemplate('gift/deatils',$data);
	}
	public function bank_account(){
		
		 
		  $uid = $this->session->userdata['log_in']['user_id'];
		
		  if(!empty($_POST)){
			 
				$this->form_validation->set_rules('account_holder_name','Account Name','required');
				$this->form_validation->set_rules('account_no','Account Number','required');
				$this->form_validation->set_rules('bank_name','Bank name','required');
				$this->form_validation->set_rules('bank_details','Bank details','required');
				if($this->form_validation->run() == TRUE)
				{
					$insertdata=array(
					'uid'=>$uid,
					'account_holder_name'=>$this->input->post('account_holder_name'),
					'bank_account_no'=>$this->input->post('account_no'),
					'bank_name'=>$this->input->post('bank_name'),
					'bank_details'=>$this->input->post('bank_details'),
					);
				$res=$this->gift_model->add_bank_details($insertdata);
				if($res){
					$this->session->set_flashdata('successmessage','Account added succesfully.');
					redirect('bank-account');
				}
				
				}else{
					$data['lang_code'] = $this->lang_code;
					$data['page_title']=$this->lang->line("add_bank_acc_details");
			        $this->load->ftemplate('gift/bankaccount',$data);
				}
		  }else{
			  
		  $data['result']=$bankdetails=$this->gift_model->get_bank_details($uid);
		  
		  if(!empty($bankdetails)){
			  $data['lang_code'] = $this->lang_code;
			  $data['page_title']=$this->lang->line("bank_acc_details");
			  $this->load->ftemplate('gift/bankaccountdetails',$data);
		  }else{
			  $data['lang_code'] = $this->lang_code;
			  $data['page_title']=$this->lang->line("add_bank_acc_details");
			  $this->load->ftemplate('gift/bankaccount',$data);
		  }
		  }
		 
		 
	}
	public function edit_bank_account(){
		$uid = $this->session->userdata['log_in']['user_id'];
		
		  if(!empty($_POST)){
			 
				$this->form_validation->set_rules('account_holder_name','Account Name','required');
				$this->form_validation->set_rules('account_no','Account Number','required');
				$this->form_validation->set_rules('bank_name','Bank name','required');
				$this->form_validation->set_rules('bank_details','Bank details','required');
				if($this->form_validation->run() == TRUE)
				{
					$updatedata=array(
					'account_holder_name'=>$this->input->post('account_holder_name'),
					'bank_account_no'=>$this->input->post('account_no'),
					'bank_name'=>$this->input->post('bank_name'),
					'bank_details'=>$this->input->post('bank_details'),
					);
				$res=$this->gift_model->edit_bank_details($updatedata,$uid);
				if($res){
					$this->session->set_flashdata('successmessage','Account updated succesfully.');
					redirect('bank-account');
				}
				
				}else{
					 $this->session->set_flashdata('warningmessage','Please edit or disable all the fields by clicking on &nbsp; <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp; to update the bank account details.');
			 		 redirect('bank-account'); 
				}
		  }
		  else{
			 $this->session->set_flashdata('warningmessage','Please click on &nbsp; <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp; to edit a field.');
			 redirect('bank-account'); 
		  }
	}
}