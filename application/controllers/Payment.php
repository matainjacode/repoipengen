<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment extends CI_Controller {

	private $sess_id = 0;
	private $lang_id;
	private $lang_code;			
	public function __construct()
    {
		
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->lang_id = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->lang_id = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
        //$params = array('server_key' => 'VT-server-FIMtOGRqnRtV2T3ozXg3W0OI', 'production' => false);
		$params = array('server_key' => 'VT-server-FM2pqsRBwudKr-rMcWLxG0vK', 'production' => false);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->library('cart');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->model("payment_model");
		$this->load->model("checkout_model");
		$this->load->model("product_model");
		$this->load->model("wishlist_model");
		$this->load->helper(array('url', 'ipengen_email_helper'));
		if($this->session->userdata('log_in')== false){}
		else
		{
			$sess_val = $this->session->userdata('log_in');
			$this->sess_id = $sess_val["user_id"];
		}
		$sess_data = $this->session->userdata("site_lang");
		if(!empty($sess_data)){$this->lang_id = $sess_data['laguage_id'];}
		
    }

	public function index()
	{
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("payment");
		$this->load->ftemplate('checkout_vtweb',$data);
	}
    public function payment_with_card()
	{
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("card_payment");
		$this->load->view('payment/index',$data);
	}
	public function vtweb_checkout()
	{
		$cart_data = $this->session->userdata('cart_data');
		$order_data = $this->session->userdata('order_data');
		
		$discount = $this->session->userdata('discount');
		$discChrg = str_replace(',','',$discount);
		
		$shipping_data = isset($_SESSION['shipping']) ? $this->session->userdata('shipping') : '';
		$shipping_amount = (!empty($shipping_data)) ? $shipping_data['shipping_amount'] : 0;
		$shipping_charge = (!empty($shipping_data)) ? $shipping_data['shipping_charge'] : 0;				
		$gross =($this->cart->total() - $discChrg + (isset($shipping_amount) ? $shipping_amount : 0));	
		$price = isset($gross) ? $gross : $this->cart->total();
		$discount = (isset($discChrg)&&$discChrg!='') ? $discChrg : 0;

		if(!empty($cart_data) && !empty($order_data)){
			$productarray=array();
			$productarr=array();
			
			$billing = $order_data['billing'];
			$shipping = $order_data['shipping'];
			
			// Populate customer's billing address
			$billing_address = array(
				'first_name' 		=>$billing['fname'],
				'last_name' 		=> $billing['lname'],
				'address' 			=> $billing['address'],
				'address2'		=> $billing['address2'],
				'city' 					=> "Jakarta Raya",
				'postal_code' 	=> $billing['postcode'],
				'phone' 				=> $billing['mobile'],
				'country' => 'Jakarta',
				'country_code'	=> 'IDN'
				);
	
			// Populate customer's shipping address
			$shipping_address = array(
				'first_name' 	=> $shipping['shipfirstName'],
				'last_name' 	=> $shipping['shiplastName'],
				'address' 		=> $shipping['shipAddress'].' '.$shipping['shipAddress2'],
				'city' 				=> $shipping['shipCity'],
				'postal_code' => (isset($shipping['shipPostcode'])&& $shipping['shipPostcode']!='') ? $shipping['shipPostcode'] : 12345,
				'phone' 			=> $shipping['shipMobile'],
				'country_code'=> 'IDN',
				'kecamatan'			=> $shipping['shipKecamatan']
				);
			$customer = $order_data['user'];
			$customer_details = array();
			// Populate customer's Info
			if(is_array($customer)){
				foreach($customer as $c){
					$user_id = $c->id;
					$customer_details['first_name'] = $c->fname;
					$customer_details['last_name'] = $c->lname;
					$customer_details['email'] = $c->email;
					$customer_details['phone'] = $c->mobile;
					$customer_details['billing_address'] = $billing_address;
					$customer_details['shipping_address'] = $shipping_address;	
				}
			}else{
				
				$user_id = $customer->id;
				$customer_details['first_name'] = $customer->fname;
				$customer_details['last_name'] = $customer->lname;
				$customer_details['email'] = $customer->email;
				$customer_details['phone'] = $customer->mobile;
				$customer_details['billing_address'] = $billing_address;
				$customer_details['shipping_address'] = $shipping_address;
				
			}
			$products = $cart_data;
			foreach($cart_data as $cart){ 
				$name = $cart['name'];
				$string = (strlen($name) > 50) ? substr($name,0,41) : $name;
			
				$productarr['id']=$cart['id'];
				$productarr['price']= $cart['price'];
				$productarr['quantity']=$cart['qty'];
				$productarr['name']= $string;
				$productarray[]=$productarr;
				$total=$cart['subtotal'];
				//$price=$this->cart->total();
				$price = isset($gross) ? $gross : $this->cart->total();
				$discount = isset($discount) ? $discount : 0;
			}
			
			$product_discount_array = array(
				'id'=>'discount',
				'price'=> (isset($discount) ? ('-'.$discount) : 0),
				'quantity'=>1,
				'name'=>'Discount'
			);
			$product_discount_arrayl=array();

			$product_discount_arrayl[786] = $product_discount_array; 
			$merge_array =  array_merge($productarray,$product_discount_arrayl);
			
			$shipping_charge_array = array(
				'id'=>'shipping_charge',
				'price'=> '+'.(isset($shipping_amount) ? $shipping_amount : 0),
				'quantity'=>1,
				'name'=>'Shipping Charge'
			);
			$shipping_charge_array2=array();

			$shipping_charge_array2[404] = $shipping_charge_array; 
			$shipping_merge_array =  array_merge($merge_array,$shipping_charge_array2);
			
			$lanuagearray=$this->session->userdata('site_lang');
			   if(!empty($lanuagearray)){
				$language_id=$lanuagearray['laguage_id'];
				 $language_name=$lanuagearray['laguage_name'];
				}else{
			   $language_id=1;
			  }
  			$data['user_id'] = isset($user_id) ? $user_id : 0;
			$data['products'] = $products;
			$data['billing_address'] = $billing_address;
			$data['shipping_address'] = $shipping_address;
			$data['customer'] = $customer_details;
			$data['question'] = $order_data['question'];
			$data['shipping_method'] = $order_data['shipping_method'];
			$data['total_price'] = $price;
			$data['total_product_price'] = $this->cart->total();
			$data['discount'] = $discount;
			$data['coupon_code'] = isset($_SESSION['coupon_code']) ? $_SESSION['coupon_code'] : '';
			$data['shipping_charge'] = isset($shipping_amount) ? $shipping_amount : 0;
			$data['language_id'] = $language_id;
			$pm = $this->session->userdata('payment_method');
			$data['payment_method'] = isset($pm)? $pm :"";
			
			$order_id = $this->payment_model->addOrder($data);
			
			$transaction_details = array(
				'order_id' 		=> $order_id,
				'gross_amount' 	=> $price
			);
	
			// Populate items
			$items = $shipping_merge_array;
			
			// Data yang akan dikirim untuk request redirect_url.
			// Uncomment 'credit_card_3d_secure' => true jika transaksi ingin diproses dengan 3DSecure.
			$transaction_data = array(
				'payment_type' 			=> 'vtweb', 
				'vtweb' 				   => array(
					//'enabled_payments' 	=> ['credit_card'],
					'credit_card_3d_secure' => true
				),
				'transaction_details'     => $transaction_details,
				'item_details' 			=> $items,
				'customer_details' 	    => $customer_details
			);
			
			try
			{
				$vtweb_url = $this->veritrans->vtweb_charge($transaction_data);
				header('Location: ' . $vtweb_url);
			} 
			catch (Exception $e) 
			{
				echo $e->getMessage();	
			}
			}else{
				redirect('/');
			}
		
	}

	public function notification()
	{
		
		echo 'test notification handler';
		
		$json_result = file_get_contents('php://input');
		$result = json_decode($json_result);
		if($result){ 
			$notif = $this->veritrans->status($result->order_id);
        //  mail('matainja004@gmail.com','TEST',$result);
		
		error_log(print_r($result,TRUE));
		//notification handler sample
		$cart_data = $this->payment_model->transaction_cart($notif);
		$transaction = $notif->transaction_status;
		$type = $notif->payment_type;
		$order_id = $notif->order_id;
		$fraud = $notif->fraud_status;
        
		if ($transaction == 'capture') {
		  // For credit card transaction, we need to check whether transaction is challenge by FDS or not
		  if ($type == 'credit_card'){
		    if($fraud == 'challenge'){
		      // TODO set payment status in merchant's database to 'Challenge by FDS'
		      // TODO merchant should decide whether this transaction is authorized or not in MAP
		      echo "Transaction order_id: " . $order_id ." is challenged by FDS";
		      } 
		      else {
		      // TODO set payment status in merchant's database to 'Success'
		      echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
		      }
		    }
		  }
		else if ($transaction == 'settlement'){
		  // TODO set payment status in merchant's database to 'Settlement'
			  echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
			  } 
			  else if($transaction == 'pending'){
			  // TODO set payment status in merchant's database to 'Pending'
			  echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
			  } 
			  else if ($transaction == 'deny') {
			  // TODO set payment status in merchant's database to 'Denied'
			  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
			}
		}	

	}
	public function success()
	{
		if($this->session->userdata('log_in')== false){
			redirect('/');
			}
		else
		{
			$order_id = $_GET['order_id'];
		$user_id = $this->sess_id;
		$order_is_yours = $this->payment_model->order_is_yours($order_id,$user_id);
		
		if($order_is_yours){
		$photopath = $this->config->item('image_display_path');
		$thumb_size= $this->config->item('thumb_size');
		$no_image = $photopath."product/cash_gift.png";
		if($order_id!=0){ 
	   		$order_info = $this->payment_model->getOrderById($order_id);
			$contribution = array();
			$wallet_transaction = array();
			$notifications = array();	
			foreach($order_info as $order){ 
				$wishlist = $this->wishlist_model->getwishlistByid($order->wishlist_id);
				$order->wishlist_url = ($order->item_type == 'cash_gift') ? site_url('~'.$wishlist->url) : '';
				if($order->item_type == 'contributed_cash_gift'){
					$checkContributorViaOrder = $this->payment_model->checkContributionOrderId($order_id);
					if($checkContributorViaOrder == 0){					
						$contribution['order_id'] = $order->order_id;
						$contribution['wishlist_id'] = $order->wishlist_id;
						$contribution['product_id'] = $order->product_id;	
						$contribution['user_id'] = $order->id;
						$contribution['user_email'] = $order->email;
						$contribution['product_name'] = $order->name;	
						$contribution['total_amount'] = $order->price;
						$contribution['total_amt_received'] = $order->contribute_amt;
						$contribution['date_added'] = date('Y-m-d H:i:s');
						
						$contributor[] = $contribution;
					}
					
				}
				
				if($order->item_type == 'cash_gift'){
					$checkWalletTransactionViaOrder = $this->payment_model->checkWalletOrderId($order_id);
					if($checkWalletTransactionViaOrder  == 0 ){
						$wallet_transaction['wishlist_id'] = $order->wishlist_id;
						$wallet_transaction['sender_id'] = $order->id;	
						$wallet_transaction['order_id'] = $order->order_id;
						$wallet_transaction['transaction_id'] = '';
						$wallet_transaction['amount'] = $order->price;	
						$wallet_transaction['wallet_status'] = 'received';
						$wallet_transaction['type'] = $order->item_type;
						$wallet_transaction['date_added'] = date('Y-m-d H:i:s');
						$wallet_transaction['is_new'] = 1;
						
						$wallet[] = $wallet_transaction;
					}
					
				}

				$checkNotifications = $this->payment_model->checkNotificationsOrderId($order_id);
				
				
					if($order->item_type == 'contributed_cash_gift'){
						
						$notifications['notification_type'] = 'ContributedCashGift';
						$notifications['notification_amount'] = $order->contribute_amt;
						
					}else if($order->item_type == 'cash_gift'){
						
						$notifications['notification_type'] = 'Cashgift';
						$notifications['notification_amount'] = $order->cash_amt;
						
					}else{
						
						$notifications['notification_type'] = 'Buygift';
						$notifications['notification_amount'] = 0;
						
					}
					$notifications['product_id'] = isset($order->product_id) ? $order->product_id : 0;
					$notifications['order_id'] = $order->order_id;
					$notifications['wishlistid'] = $order->wishlist_id;
					$notifications['notification_date'] = date('Y-m-d H:i:s');
					//$notifications['sender_uuid'] = (isset($wishlist->uid) && $wishlist->uid!=0) ? $wishlist->uid : $user_id;
					//$notifications['uuid'] = $user_id;
					$notifications['sender_uuid'] = $user_id;
					$notifications['uuid'] = (isset($wishlist->uid) && $wishlist->uid!=0) ? $wishlist->uid : $user_id; 
					
					$notifications['notification_description'] = '';

				
				
				$pslug = $this->product_model->getProductUrl_name($order->product_id,$order->name);
				$order->product_url = $pslug;
				$order->image = $fetch_image = $this->product_model->getProductImageById($order->product_id);
				$order->image_url = $photopath."product/".$order->product_id.$thumb_size.$fetch_image;
				$order->no_image = $no_image;
				
				$content[] = $order;
				if($checkNotifications == 0){
					$notifyArr[] = $notifications;
				}
				else{
					$notifyArr = '';
				}
			}
			if(isset($contributor)){
				$this->payment_model->addContributor($contributor,$order_id);	
			}
			if(isset($wallet)){
				$this->payment_model->addWallet($wallet_transaction);	
			}
			if(isset($notifyArr) && !empty($notifyArr)){
				$this->payment_model->addNotifications($notifyArr);	
			}
			
			$shipping_data = isset($_SESSION['shipping']) ? $this->session->userdata('shipping') : '';	
			$data['shipping_method'] = (!empty($shipping_data)) ? $shipping_data['shipping_method'] : '';
			$data['shipping_charge'] = $shipping_charge = (!empty($shipping_data)) ? $shipping_data['shipping_charge'] : 0;
			
			if ($this->payment_model->deleteCartForUser($user_id))
			{
				$this->session->unset_userdata('coupon_code');
				$this->session->unset_userdata('discount');
			    $this->session->unset_userdata('gross_amount');
				$this->session->unset_userdata('order');
				$this->session->unset_userdata('shipping');
			}
			
	  	}
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("success");
		$data['order_id'] = $order_id;
		$data['status'] = 'success';
		$data['order'] = $content;
		/*******Product Quantity Update******/
		$orderQuantity = $order_info[0]->quantity;
		$prodcut_id = $order_info[0]->product_id;
		$productData = $this->payment_model->productData($prodcut_id,$this->lang_id);
		if(!empty($productData)){
			$productQuantity = $productData[0]->stock_quantity;
			if($productQuantity != 0 && $productQuantity > 0)
			{ $updateQuantity = $productQuantity-$orderQuantity; }
			else
			{ $updateQuantity = $productQuantity;}
			
			$update = array("stock_quantity" => $updateQuantity);
		$quantityStatus = $this->payment_model->updateProductQuantityData($prodcut_id, $update);
		}
		/*******Product Quantity Update End******/
		$status = $this->payment_model->updateOrder($data['status'],$order_id);
		/*Email Send*/
		$data["userData"] = $this->payment_model->getUserData($order_id);
		$data["orderData"] = $this->payment_model->orderDetails($order_id);
		$data["wishlistData"] = $this->payment_model->getWishlistData($order_id,$this->lang_id);
		$configEmail = $this->payment_model->getAllConfigEmail();
		$emailFrom = $configEmail[0]->info_email;
		$emailToArr[] = $data["userData"][0]->userEmail;
		$emailToArr[] = $configEmail[0]->admin_email;
		//print_r($data['order']);
		//die();
		$emailBody = $this->load->view("e_template/order_confirmation",$data,true);
		foreach($emailToArr as $emailTo)
		{ 
			$subject = "Order Confirmation - Your order with Ipengen (#".$order_id.") request has been successfully placed!";
			$emailData = array(
						"title" 	=> "Ipengen",
						"from"		=>	$emailFrom,
						"to"		=>	$emailTo,
						"subject"	=>	$subject,
						"message"	=>	$emailBody
								);
			send_email($emailData);
		}
		
		if(!empty($data["wishlistData"]))
		{
			$result = $this->payment_model->getWishlistProductData($data["wishlistData"][0]->wishlistId , $order_id);
			$i = 0;
			foreach($result as $row)
			{
				$prodcutID = $row->product_id;
				if($prodcutID != 0)
				{
					$productData = $this->payment_model->getProductData($data["wishlistData"][0]->wishlistId,$order_id,$prodcutID,$this->lang_id);
					foreach($productData as $rowProduct)
					{
						$mainArray[$i] = (array)$row;
						$mainArray[$i]["sku"] = $rowProduct->sku;
					}
				}
				else
				{
					$mainArray[$i] = (array)$row;
					$mainArray[$i]["sku"] = "";
				}
				
				$i++;
			}
			$data["wishlistProductData"] = $mainArray;
			$W_owner_emailBody = $this->load->view("e_template/Wishlist-Receive-Notification",$data,true);
			$wishlistOwnerEmail = $data["wishlistData"][0]->email;
			$subject = "Good News..!!! Have a surprise.";
			$emailData = array(
						"title" 	=> "Ipengen",
						"from"		=>	$emailFrom,
						"to"		=>	$wishlistOwnerEmail,
						"subject"	=>	$subject,
						"message"	=>	$W_owner_emailBody
								);
			send_email($emailData);	
		}

		$this->load->ftemplate('checkout/success',$data);
		}else{
			redirect('/');
		}
		}/*Email End*/
		
	}
	

}