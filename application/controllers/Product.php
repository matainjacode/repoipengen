<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller {
	private $langId;
	private $lang_code;
	function __construct(){
		parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
		$this->load->library('form_validation');
		$this->load->library('facebook');
		$this->load->model('Product_model');
		$this->load->library('session'); 
		$this->load->library('image_lib');
		$this->load->model('search_model');
		$this->load->helper('cookie');	
		$controller = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		if($controller=='product' && $method!='details'){
			if($this->session->userdata('log_in')== false){
            	redirect();
			}	
		}
		if($this->session->has_userdata("createCatUrl"))
		{ $this->session->unset_userdata("createCatUrl");}
		
	}


	public function index()
	{		

	}
	
	public function details($pid=NULL)
	{
		//print_r($_GET);
		//echo $pid;die();
		$GalleryImg=array();
		$imgcount =0;
		$siteLangArray = $this->session->userdata('site_lang');
		$loginUserArray=$this->session->userdata('log_in');
		$loggedUId=$loginUserArray['user_id'];
   		$Langid=$siteLangArray['laguage_id'];
    	if($Langid==""){ $Langid=1;}
		if($pid!=''){$pid=$pid;}
	    $uid = $loggedUId;
		if($uid!=""){
			$this->Product_model->setresentProduct($pid,$uid);
			$productViewRecently=$this->Product_model->getRecentProduct($loggedUId,$Langid);
		}/*else{
			$i=0;
			if(!empty($this->input->cookie('Recentlogview', false))){
				$recentlyViewed=$this->input->cookie('Recentlogview', false);
				
				if(sizeof($recentlyViewed)>=3){
				   array_shift($recentlyViewed);
				   array_push($recentlyViewed,$pid);
				}else{
				   array_push($recentlyViewed,$pid);
				}
				
				foreach($recentlyViewed as $valcookeis){
			    $cookie = array(
                    'name'   => 'Recentlogview['.$i++.']',
                    'value'  => $valcookeis,
                    'expire' =>  15000000,
					'secure' => FALSE 
                    );
               $this->input->set_cookie($cookie);
				
				}
				
				
			}else{
				$recentlyViewed=array();
				array_push($recentlyViewed,$pid);
			$cookie = array(
                    'name'   => 'Recentlogview['.$i++.']',
                    'value'  => $pid,
                    'expire' =>  15000000,
					'secure' => FALSE 
                    );
            $this->input->set_cookie($cookie);
			}

			
		
			
		 
			$productViewRecently=$this->Product_model->getRecentProduct_withoutlogin($this->input->cookie('Recentlogview', false));
			
		}*/
		
		$productDetails=$this->Product_model->getProductDetails($pid,$Langid);
		
		$WishlistCount=$this->Product_model->getWishlistCount($pid);
		if(!empty($productDetails)){
			$no_image=false;
			$categoryid=$productDetails[0]->category_id;
			$photopath = $this->config->item('image_display_path');
			$photopath_absolute = $this->config->item('image_path');
			$thumb_size= $this->config->item('thumb_size');
			
			$medium_thumb_size= $this->config->item('medium_thumb_size');
			$large_thumb_size= $this->config->item('large_thumb_size');
			$original_size= $this->config->item('original');
			$photopath_is_check =$photopath_absolute."product/".$productDetails[0]->product_id;
			$thumpath = $photopath."product/".$productDetails[0]->product_id.$thumb_size;
			$medpath = $photopath."product/".$productDetails[0]->product_id.$medium_thumb_size;
			$originalpath = $photopath."product/".$productDetails[0]->product_id.$original_size;
			$likethiscategoryProduct=$this->Product_model->category_product($categoryid,$pid,$Langid);
			$additionalIMg=$this->Product_model->getGalleryImages($pid);
			
			if(isset($productDetails[0]->product_image) && is_file($photopath_is_check.$medium_thumb_size.$productDetails[0]->product_image))
			$GalleryImg[]=$productDetails[0]->product_image;
			
			
			if( !empty ($additionalIMg)){
			foreach($additionalIMg as $val){

			if(isset($val) && is_file($photopath_is_check.$medium_thumb_size. $val->image_name))
			$GalleryImg[]= $val->image_name;
			}
			}
			
			if(empty($GalleryImg))
			{
			
			$no_image=true;
			}
			
		
			$myWishlist=$this->Product_model->getUserWishlist($loggedUId,$Langid);
			if($uid!=""){
			//$totalproductadd=$this->Product_model->getUsertotalproduct($loggedUId,$Langid);
			}
			//print_r($myWishlist); 
			$recipientWishlist=$this->Product_model->getrecipientWishlist($loggedUId);
			$data['thumb_size']=$thumb_size;
			$data['no_image']=$no_image;
			$data['no_image_path']=$photopath."product/no_product.jpg";
			$data['thumpath']=$thumpath;
			$data['medpath']=$medpath;
			$data['originalpath']=$originalpath;
			
			$data['wishlistCount']=$WishlistCount;
			$data['productIMG']=$GalleryImg;
			$data['lang_code'] = $this->lang_code;
			$data['page_title']=$productDetails[0]->product_name;
			$data['Details']=$productDetails;
			$likecategoryarray=array();
			$recentviewrray=array();
			$recentviewArr=array();
			$likecategoryArr=array();
			if (!empty ($likethiscategoryProduct)){ 
			foreach ($likethiscategoryProduct as $likecategory){
				 if(is_file($this->config->item('image_path').'/product/'.$likecategory->product_id.$thumb_size.$likecategory->product_image)){
								     $imgpurl=$this->config->item('image_display_path').'product/'.$likecategory->product_id.$thumb_size.$likecategory->product_image ;
									 }  else{
                                	$imgpurl= $this->config->item('image_display_path').'product/no_product.jpg';
							 } 
							$likecategoryarray['product_id']=$likecategory->product_id;
							$likecategoryarray['product_name']=$likecategory->product_name; 
							$likecategoryarray['imgurl']=$imgpurl; 
							$likecategoryarray['product_url']=$this->Product_model->getProductUrl_name($likecategory->product_id,$likecategory->product_name); 
							$likecategoryarray['sale_price']=$likecategory->sale_price;  
							$likecategoryArr[]=$likecategoryarray; 
				
			}}
			if (!empty ($productViewRecently)){
				 foreach ($productViewRecently as $recentview){
				 if(is_file($this->config->item('image_path').'/product/'.$recentview->product_id.$thumb_size.$recentview->product_image)){
								     $imgpurl=$this->config->item('image_display_path').'product/'.$recentview->product_id.$thumb_size.$recentview->product_image ;
									 }  else{
                                	$imgpurl= $this->config->item('image_display_path').'product/no_product.jpg';
							 } 
				            $recentviewrray['product_id']=$recentview->product_id;
							$recentviewrray['product_name']=$recentview->product_name; 
							$recentviewrray['imgurl']=$imgpurl; 
							$recentviewrray['product_url']=$this->Product_model->getProductUrl_name($recentview->product_id,$recentview->product_name); 
							$recentviewrray['sale_price']=$recentview->sale_price;  
							$recentviewArr[]=$recentviewrray; 			 
							 
				
			}}
			$data['likethiscategoryProduct']=$likecategoryArr;
			$data['productViewRecently']=$recentviewArr;
			$data['WishlistCount']=$WishlistCount;
			$data['myWishlist']=$myWishlist;
			$data['loginUserArray']=$loginUserArray;
			$data['recipientWishlist']=$recipientWishlist;
			$data['login_url'] = $this->facebook->getLoginUrl(array(
				'redirect_uri' => site_url('user/login'), 
				'scope' => array("email") // permissions here
			)); 
			//print_r($data);die();
			//////////////////////added by matainja004///////////////
			$data['result']=$this->search_model->all_brand();
				$html="<ul>";
				
				$cresult=$this->search_model->list_status(0);
				
				/*================================================*/
				if(!empty($cresult)){
				 foreach($cresult as $value){
				  $result1=$this->search_model->list_status($value->id);
				  
				$html.='<li><a href="javascript:void(0)" id="'.$value->id.'">'.$value->name.'</a>';
				  
				  if(!empty($result1))
					  {
						 $html.= $this->category_list_child($value->id);
					  }
					  else{
						  $html.='</li>';
						  
						 }
					 }
				 }
				
				
				
				$html.="</ul>";
	
				$data['html_category']=$html;
			
			////////////////////////////////////////////////////////
			
			//print_r($data);die();
			$this->load->ftemplate('product/details',$data);
		}
		else{
			echo "Product not found.";	
		}
		//print_r($productDetails);die();
		//$slug=$this->slugify($productDetails[0]->product_name);
		//redirect(base_url(). 'product/setUrl/' . $productDetails[0]->product_id . '-' .$slug );
	}
	
	
	public function addProductToWishlistAjax()
	{    
    	if($this->input->is_ajax_request()){
			//print_r($_POST);die();
			echo $myWishlist=$this->Product_model->insertProductToWishlist($_POST['wid'],$_POST['uid'],$_POST['pid']);
			die();
			//return $myWishlist;
		}  
	}
	
	public function get_wishlist_address()
	{   
		
	  
	
	}
	public function category_list_child($id){
      
		 $str2="";

		$result=$this->search_model->list_status($id);
   if(!empty($result)){
	  
	  $str2.='<ul>';
	 foreach($result as $value){
			
			
			$str2.='<li><a href="javascript:void(0)" id="'.$value->id.'">'.$value->name.'</a>';

			$str2.=$stval=$this->category_list_child($value->id);
			if($stval=="")
			{
				$str2.='</li>';
			}
			
			
		 }
		 $str2.='</ul>';
   }

		return $str2;

	}
	
	
	public function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		// trim
		$text = trim($text, '-');
		
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		
		// lowercase
		$text = strtolower($text);
		
		if (empty($text)) {
		return 'n-a';
		}
		
		return $text;
	}
	
	public function quantitycheck()
	{
		if($this->input->is_ajax_request())
		{
			$inputval = $this->input->post("val");
			$productId = $this->input->post("p_id");
			$pData = $this->Product_model->getProductDetails($productId,$this->langId);
			if(!empty($pData))
			{
				$productQuantity = $pData[0]->stock_quantity;
				if($productQuantity >= $inputval)
				{ echo 1; }else{ echo 0;}
			}
			else
			{ echo 0;}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
		
	
}
?>