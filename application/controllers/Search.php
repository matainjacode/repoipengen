<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Search extends CI_Controller {
	private $langId;
	private $lang_code;
	 function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
					$this->load->helper(array('form', 'url'));
					$this->load->helper('cookie');
					$this->load->library('session'); 
					$this->load->model('search_model'); 
					$this->load->model('Product_model');
					
	   // if($this->session->userdata('log_in')== false) { redirect(); }
       }
function _remap()
{		
	$segment_1 = $this->uri->segment(2); 

	switch ($segment_1) {
		case null:
		case false:
		case '':$this->index();break;
		case 'product':$this->product();break;
		case 'product_count':$this->product_count();break;	
		default:$this->index();
	}
}	

	public function index(){
	$data['lang_code'] = $this->lang_code;
	$data['page_title']=$this->lang->line("search");
	$data['login_url'] = $this->facebook->getLoginUrl(array(
		'redirect_uri' => site_url('user/login'), 
		'scope' => array("email") // permissions here
	));
	$data['result']=$this->search_model->all_brand();
	$str2="";
	$html="<ul>";
	$cresult=$this->search_model->category_list_status();
	/*================================================*/
	if(!empty($cresult)){ 
	
	 	foreach($cresult as $value){

			$html.='<li><a href="javascript:void(0)" id="'.$value->id.'">'.$value->name.'</a>';
		
			$str2.='<ul>';
	
			$subcategories = (isset($value->subcategory)) ? $value->subcategory : '';
			
			if(!empty($subcategories)){
		
				foreach($subcategories as $sub){
			
					$str2.='<li><a href="javascript:void(0)" id="'.$sub['id'].'">'.$sub['name'].'</a>';
		
				}
				$str2.='</li>';
			}
			
			$str2.='</ul>';
			
			if($str2!=''){
				$html.= $str2;
				
				$str2 = '';
		    }
			else{
				
				$html.='</li>';
			}
		 }
	 }
	
	$html.="</ul>";
	
	$data['html_category']=$html;
	
	$this->load->ftemplate('product/search',$data);
	
	}
	
	public function product(){
	
	$quere_condition="";
	if(!empty($_POST)){
	   if($this->input->post('price')!=""){
		   $pricearray=explode('-',$this->input->post('price'));
		   $quere_condition.=" AND (ig_products.sale_price BETWEEN ".$pricearray[0]." AND ".$pricearray[1].")";
		}
		if($this->input->post('categoryid')!=""){
			//$categoryID=$this->allcategoryId($this->input->post('categoryid'));
			$quere_condition.=" AND (ig_products.category_id =".$this->input->post('categoryid').")";
		   
		}
		if(($this->input->post('#category')!="")&&($this->input->post('categoryid')=="")){
			//$categoryID=$this->allcategoryId($this->input->post('#category'));
			$quere_condition.=" AND (ig_products.category_id =".$this->input->post('#category').")";
		}
		if($this->input->post('#categoryid')!=""){
			
			//$categoryID=$this->allcategoryId($this->input->post('#categoryid'));
		    $quere_condition.=" AND (ig_products.category_id =".$this->input->post('#category').")";
		}
		if(!empty($categoryID))
		{
			$k=1;
			$quere_condition.=" AND (";
			foreach($categoryID as $value)
			{
				if($k==1){
					$quere_condition.=" ig_products.category_id =".$value;
				}else{
					$quere_condition.=" OR ig_products.category_id =".$value;
				}
				$k++;
			}
			$quere_condition.=" )";
		}
		
		if($this->input->post('brand')!=""){
			if($this->input->post('brand')!='NA'){
			$pbrandarray=explode(',',$this->input->post('brand'));
			
			$j=0;
			foreach($pbrandarray  as $val)
			{
				if($j==0)
				 $quere_condition.=" AND ( ig_products.brand_id=".$val;
				if(($j!=0)&&($val!=""))
				 $quere_condition.=" OR ig_products.brand_id=".$val;
				 
				 $j++;
			}
		  $quere_condition.=" )";
			}
		}
		$quere_condition.=" AND ig_products.admin_status=1";
		
	   if($this->input->post('shortby')!=""){
		   
		   $orby=$this->input->post('shortby');
		   if($orby=='nameasc')
		   $quere_condition.=" ORDER BY ig_products_text.product_name ASC";
		   if($orby=='namedesc')
		   $quere_condition.=" ORDER BY ig_products_text.product_name DESC";
		   if ($orby=='pricelow')
		   $quere_condition.=" ORDER BY ig_products.sale_price";
		   if ($orby=='pricehigh')
		   $quere_condition.=" ORDER BY ig_products.sale_price DESC";
		   if ($orby=='salseFirst')
		  $quere_condition.="";
		   
		}
		
		
		if(($this->input->post('limit')!="")&&($this->input->post('limit')!=0)){
		
		$limit=$this->input->post('limit');
		}else{
	   
	   $limit=12;
		}
			 
		
		
		
	
		
		
		if($this->input->post('page')!=""){
			if($this->input->post('page')==1){
				$offset=0;
			}else{
				$offset=($this->input->post('page')-1)*$limit;
			}
			if(($this->input->post('limit')!=0)||($this->input->post('limit')==""))
			$quere_condition.=' LIMIT '.$offset.','.$limit;
			
		}else{
			$offset=0;
			if(($this->input->post('limit')!=0)||($this->input->post('limit')==""))
			$quere_condition.=' LIMIT '.$offset.','.$limit;
			
		}
		
		
	   
	}else{
	   $quere_condition.=" AND ig_products.admin_status=1";
	}
	$result  = $this->search_model->product_list($quere_condition);
	if(!empty($result))
	{
		$jsonarrayval=array();
		foreach($result as $value){
			
			//$slag=$this->slugify($value->product_name);
			$slag=$this->Product_model->getProductUrl_name($value->product_id,$value->product_name);
			$detailsUrl=$slag;
			if($value->product_image!=""){
				if(file_exists('./photo/product/'.$value->product_id.$this->config->item('medium_thumb_size').$value->product_image)){
				$imgUrl=base_url().'photo/product/'.$value->product_id.$this->config->item('medium_thumb_size').$value->product_image;
				}
				else{
					$imgUrl=base_url().'photo/product/no_product.jpg';
				}
			}else{
				$imgUrl=base_url().'photo/product/no_product.jpg';
			}
			if($value->price>$value->sale_price){
				$disview=1;
			}else{
				$disview=0;
			}
			
			$dt=$value->create_date;
			$date1=date_create($dt);
			$date2=date_create(date('Y-m-d'));
			$diff=date_diff($date1,$date2);
			$diffdt=$diff->format("%R%a");
			if(($diffdt<=30)&&($diffdt>=0))
			{
				$isnew=1;
			}else{				
			$isnew=0;
			}
			//$dt=$value->create_date;
			$stDate = date('Y-m-d');
			$stDate=date('Y-m-d', strtotime($stDate));;
			//echo $paymentDate; // echos today! 
			$contractDateBegin = date('Y-m-d', strtotime($value->sale_start_date));
			$contractDateEnd = date('Y-m-d', strtotime($value->sale_end_date));
			$salsornot=0;
			if (($stDate >= $contractDateBegin) && ($stDate <= $contractDateEnd))
			{
			$salsornot=1;
			}
			else
			{
			$salsornot=0;  
			}
			$jsonarrayval['product_id']=$value->product_id;
			$jsonarrayval['price']=number_format($value->price,2);
			$jsonarrayval['sale_price']=number_format($value->sale_price,2);
			$jsonarrayval['product_name']=$value->product_name;
			$jsonarrayval['imgUrl']=$imgUrl;
			$jsonarrayval['disview']=$disview;
			$jsonarrayval['detailsUrl']=$detailsUrl;
			$jsonarrayval['ishot']=$value->feature_status;
			$jsonarrayval['isnew']=$isnew;
			$jsonarrayval['salsornot']=$salsornot;
			$jsonarray[]=$jsonarrayval;
		}
		echo json_encode($jsonarray);
		//echo json_encode($result);
	}else{
		$rest=array('no'=>'no');
		echo json_encode($rest);
	}
	
	}
	public function product_count(){
	
	$quere_condition="";
	if(!empty($_POST)){
	   
	   if($this->input->post('price')!=""){
		   $pricearray=explode('-',$this->input->post('price'));
		   $quere_condition.=" AND (ig_products.sale_price BETWEEN ".$pricearray[0]." AND ".$pricearray[1].")";
		}
		$categoryID=array();
		if($this->input->post('categoryid')!=""){
			//$categoryID=$this->allcategoryId($this->input->post('categoryid'));
			$quere_condition.=" AND (ig_products.category_id =".$this->input->post('categoryid').")";
		   
		}
		if(($this->input->post('#category')!="")&&($this->input->post('categoryid')=="")){
			//$categoryID=$this->allcategoryId($this->input->post('#category'));
			$quere_condition.=" AND (ig_products.category_id =".$this->input->post('#category').")";
		}
		if($this->input->post('#categoryid')!=""){
			
			//$categoryID=$this->allcategoryId($this->input->post('#categoryid'));
			$quere_condition.=" AND (ig_products.category_id =".$this->input->post('#category').")";
		}
		if(!empty($categoryID))
		{
			$k=1;
			$quere_condition.=" AND (";
			foreach($categoryID as $value)
			{
				if($k==1){
					$quere_condition.=" ig_products.category_id =".$value;
				}else{
					$quere_condition.=" OR ig_products.category_id =".$value;
				}
				$k++;
			}
			$quere_condition.=" )";
		}
		$quere_condition.=" AND ig_products.admin_status=1";
		if($this->input->post('brand')!=""){
			if($this->input->post('brand')!='NA'){
			$pbrandarray=explode(',',$this->input->post('brand'));
			
			$j=0;
			foreach($pbrandarray  as $val)
			{
				if($j==0)
				 $quere_condition.=" AND ( ig_products.brand_id=".$val;
				if(($j!=0)&&($val!=""))
				 $quere_condition.=" OR ig_products.brand_id=".$val;
				 
				 $j++;
			}
		  $quere_condition.=" )";
			}
		}
		
		
	   if($this->input->post('shortby')!=""){
		   
		   $orby=$this->input->post('shortby');
		   if($orby=='name')
		   $quere_condition.=" ORDER BY ig_products_text.product_name";
		   if ($orby=='price')
		   $quere_condition.=" ORDER BY ig_products.sale_price";
		   if ($orby=='salseFirst')
		  $quere_condition.="";
		   
		}
	  
	   
	}else{
	   $quere_condition.=" AND ig_products.admin_status=1";
	}
	
	$result  = $this->search_model->product_count($quere_condition);
	
	if(!empty($result))
	{
		echo json_encode($result);
	}else{
		$rest=array('no'=>'no');
		echo json_encode($rest);
	}
	
	}
	
	public function allcategoryId($id){
	$categoryID=array();
	$categoryID[]=$id;
	$result=$this->search_model->list_status($id);
	 if(!empty($result)){
	 foreach($result as $value){
		$categoryID[]=$value->id; 
		$this->allcategoryId($value->id);
	 }
	}
	return $categoryID;
	}
	

}
?>