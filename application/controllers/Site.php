<?php 
 /**
 * 
 */
 include_once APPPATH."libraries/core/Ipengencontroller.php";
 class Site extends Ipengencontroller
 {
 	
 	function __construct()
 	{
 		parent::__construct();
 		$this->lang->load('home');
		$this->load->helper(array('ipengen_email_helper'));
		$this->load->model(array("site_model","payment_model"));
 	}

 	function _remap($uri_segment)
 	{
 		switch ($uri_segment) {
 			case 'affiliate':{$this->affiliate(); break;}
			case 'search':{$this->search(); break;}
 			case 'about-us':{$this->about_us(); break;}
 			case 'careers':{$this->careers(); break;}
 			case 'contact-us':{$this->contact_us(); break;}
 			case 'privacy-security':{$this->privacy_and_security(); break;}
			case 'term-condition':{$this->term_and_condition(); break;}
			case 'help':{$this->question_help(); break;}
			case 'track-order':{$this->order_tracking(); break;}
			case 'news-subscription':{$this->newssubscribe(); break;}
			case 'event-notification':{$this->event_notification(); break;}
			case 'wallet':{$this->whishlist_wallet(); break;}
			case 'cookie':{$this->cookie_check(); break;}
 			default:{$this->index();}
 		}
 	}

 	private function affiliate()
 	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("affiliate");
 		$this->load->ftemplate('site/affiliate',$data);
 	}

 	private function about_us()
 	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("about_us");
 		$this->load->ftemplate('site/about_us',$data);
 	}
 	private function careers()
 	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("careers");
 		$this->load->ftemplate('site/careers',$data);
 	}
 	private function contact_us()
 	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("contact_us");
 		$this->load->ftemplate('site/contact_us',$data);
 	}
 	private function privacy_and_security()
 	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("privacy_security");
 		$this->load->ftemplate('site/privacy_and_security',$data);
 	}
	public function search()
	{
		
		$this->load->model('wishlist_model');
		$this->load->model('search_model');
		$this->load->model('Product_model'); 
       $str="";
		$result=$this->wishlist_model->allWishlistId($this->input->post('search'));
		$product_result=$this->search_model->product_search($this->input->post('search'));
		if(!empty($result)){
			foreach($result as $val){
				$urlSlug=$val->url;
       $str.='<div class="show" align="left">';
	  $str.='<a href="'.base_url().'~'.$urlSlug.'" class="dropCl" style="display: block;">'.$val->title.'</a>';
		
		 $str.='</div>';
			}
			//echo $str;
		}
		if(!empty($product_result)){
			foreach($product_result as $product_val){
				$urlSlug=$this->Product_model->getProductUrl_name($product_val->product_id,$product_val->product_name);
       $str.='<div class="show" align="left">';
	  $str.='<a href="'.$urlSlug.'" class="dropCl" style="display: block;">'.$product_val->product_name.'</a>';
		
		 $str.='</div>';
			}
			
		}
		if($str=="")
		{
			$str='';
		}
		echo $str;
	}
	private function term_and_condition()
	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("terms_conditions");
		$this->load->ftemplate('site/term_and_condition',$data);
	}
	private function question_help()
	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("question_help");
		$this->load->ftemplate('site/help',$data);
	}
	private function order_tracking()
	{
		$data['lang_code'] = $this->lang_code();
		$data['page_title'] = $this->lang->line("order_track");
		$this->load->ftemplate('site/order_track',$data);
	}
	private function newssubscribe()
	{
		//$this->load->library('MailChimp');
		//$this->load->library('mcapi'); 
		if($this->input->is_ajax_request())
		{
			//die('dcfvgbhjkl;');
			$email = $this->input->post("subscription");
			$list_id = '10ca5f2a4d';
			$result = $this->site_model->check_news_subscription($email);
			if($result)
			{
				echo 0;
			}
			else
			{
				$MailChimp = new MailChimp();
				//$result = $MailChimp->get('lists');
				$result = $MailChimp->post("lists/$list_id/members", ['email_address' => $email,'status' => 'subscribed',]);
				//print_r($result);
				if(isset($result['id'])){echo 1;}
				else{echo 0;}
			}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
	public function event_notification()
	{
		$this->load->model('eventreminder_model');
		$result=$this->eventreminder_model->getall_info();
		if(!empty($result)){
			foreach($result as $value)
			{
				/*switch($value->event_repeat)
				{
					case 365:
								$ds=date('Y').'-'.$rdataarray[1].'-'.$rdataarray[2];
								$eventdate1= strtotime('-'+$value->event_remainder." days", strtotime($ds));
								break;
					case 30:
								$ds=date('Y').'-'.$rdataarray[1].'-'.$rdataarray[2];
								$eventdate1= strtotime('-'+$value->event_remainder." days", strtotime($ds));
					            break;
					case 14:
					            $ds=date('Y').'-'.$rdataarray[1].'-'.$rdataarray[2];
								$eventdate1= strtotime('-'+$value->event_remainder." days", strtotime($ds));
								break;
					case 7:
					            $ds=date('Y').'-'.$rdataarray[1].'-'.$rdataarray[2];
								$eventdate1= strtotime('-'+$value->event_remainder." days", strtotime($ds));
								break;
					default:
					$ds=$value->event_repeat;
				}
				*/
				 	$dt=$value->event_date;
					$rdataarray=explode('-',$dt);
					$ds=date('Y').'-'.$rdataarray[1].'-'.$rdataarray[2];
					$eventdate1= strtotime('-'+$value->event_remainder." days", strtotime($ds));
					$eventdate=date('Y-m-d',$eventdate1);
					$date1=date_create($eventdate);
					$date2=date_create(date('Y-m-d'));
					$diff=date_diff($date1,$date2);
					$diffdt=$diff->format("%a");
					//echo $diffdt;die();
				   
				if($diffdt!=0){
					
					$data["name"] = ucfirst($value->fname)." ".ucfirst($value->lname);
					$data["event_name"] = $eventName =  ucwords($value->event_name);
						$dayMonth = date("d-m", strtotime($value->event_date));
						$dayMonthYear = $dayMonth."-".date("Y", strtotime("+1 year",strtotime(date("d-m-Y"))));
					$data["held_date"]= date("l, jS F", strtotime($dayMonthYear));
					$configEmail = $this->payment_model->getAllConfigEmail();
					if(!empty($configEmail))
					{
						$emailFrom = $configEmail[0]->info_email;
						$emailTo = $value->email;
						////////////////////Add mail function////////
						$messageBody = $this->load->view("e_template/wishlist-remainder", $data, true);
						///////////////////////////////////////////////
						$subject = $eventName." ~ Reminder";
						$emailData = array(
									"title" 	=> "Ipengen Event Reminder",
									"from"		=>	$emailFrom,
									"to"		=>	$emailTo,
									"subject"	=>	$subject,
									"message"	=>	$messageBody
											);
						send_email($emailData);
						
					}
					
			
				}
				
			}
		}
		
	}
	/*
	*	Cron Job for Wishlist Contribute amount to Wallet Cash
	*/
	/*public function whishlist_wallet()
	{
		if(!$this->input->is_cli_request())
		{ echo "This script can only be accessed via the command line" . PHP_EOL; return; die(); }
		
		$result = $this->site_model->allWishlistData();
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$today = date("d-m-Y");
				$event_end_date = date("d-m-Y",strtotime($row->e_enddate));
				$event_expire_checking_date = date("d-m-Y", strtotime("+1 day", strtotime($event_end_date)));
				
				$total_amount = $row->total_amount;
				$contribute_amount = $row->contribute_amount;
				$wishlist_owner_id = $row->uid;
				$wishlistId = $row->wishlist_id;
				$product_id = $row->product_id;
				if($event_expire_checking_date >= $today)
				{
					if($total_amount != $contribute_amount)
					{
						$walletData = array("receipent_uid" => $wishlist_owner_id, "wallet_ammount" => $contribute_amount);
						$walletDataResult = $this->site_model->addWallet($walletData,$wishlist_owner_id);
						$walletDataTransaction = array(
												"wishlist_id" => $row->wishlist_id,
												"amount" => $contribute_amount,
												"date_added" => date("Y-m-d H:i:s"),
												"wallet_status" => "received",
												"type" => "cash_gift",
												"is_new" => "1",
														);
						$result = $walletDataTransactionResult = $this->site_model->addWalletTransaction($walletDataTransaction);
						if($result)
						{$this->site_model->updateContribute($product_id, $wishlistId);}
					}
				}
				
			}
		}
		
	}*/
	
	

 	
 }
?>