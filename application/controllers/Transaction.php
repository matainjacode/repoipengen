<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction extends CI_Controller {
	private $langId;
	private $lang_code;
	 function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
					$this->load->helper(array('form', 'url'));
					$this->load->helper('cookie');
					$this->load->library('session'); 
					 $this->load->library('email');
					$this->load->library('form_validation');
					$this->load->model('Transaction_model');
					$this->load->model('product_model');
					$this->load->model('checkout_model');
					$this->load->library('pagination');	
					//$params = array('server_key' => 'VT-server-FIMtOGRqnRtV2T3ozXg3W0OI', 'production' => false);
					$params = array('server_key' => 'VT-server-FM2pqsRBwudKr-rMcWLxG0vK', 'production' => false);
					$this->load->library('veritrans');
					$this->veritrans->config($params);
					 if(empty($this->session->userdata['log_in']['user_id'])){
					   redirect('/');
					 }

       }


					
	public function index(){
		$uid = isset ($this->session->userdata['log_in']['user_id']) ? $this->session->userdata['log_in']['user_id'] : '';
		$data['lang_code'] = $this->lang_code;
		$data['page_title']= $this->lang->line("transaction").'/'.$this->lang->line("order");
		$row_count = $this->Transaction_model->row_count($uid);
		$config =array(
						'base_url' 				=> base_url().'transaction/page',
						'total_rows'			=>	$row_count,
						'per_page'				=>	10,
						'use_page_numbers' 		=>	TRUE,						//For Proper Page Number to Show in Url
						'uri_segment'			=>	3,
						'full_tag_open'			=>	"<ul class='pagination'>",	//Main Tag Open
						'full_tag_close'		=>	'</ul>',					//Main Tag Close
						'first_tag_open'		=>	'<li>',						//Sub-Main First Tag Open Ex. - '< First'
						'first_tag_close'		=>	'</li>',					//Sub-Main First Tag Close
						'last_tag_open'			=>	'<li>',						//Sub-Main Last Tag Open Ex. - 'Last >'
						'last_tag_close'		=>	'</li>',					//Sub-Main Last Tag Close
						'next_tag_open'			=>	'<li>',	
						'next_tag_close'		=>	'</li>',
						'next_link' 			=> 	'Next',
						'prev_tag_open'			=>	'<li>',
						'prev_tag_close'		=>	'</li>',
						'prev_link'				=>	'Previous',
						'num_tag_open'			=>	'<li>',
						'num_tag_close'			=>	'</li>',
						'cur_tag_open'			=>	'<li class="active"><a>',	//Sub-Main Current Tag Open
						'cur_tag_close'			=>	'</a></li>',
					);
			$this->pagination->initialize($config); 
			if($this->uri->segment(3)!=""){$offset=$config['per_page']*$this->uri->segment(3)-$config['per_page'];}else{$offset=0;}
			$data['paymentdetails']= $this->Transaction_model->getTransaction($uid,$config['per_page'],$offset);
			$this->load->ftemplate('transaction/index',$data);
	}
	public function deatils($orderID = NULL){
		$data['lang_code'] = $this->lang_code;
		$data['page_title']= $this->lang->line("transaction_details");
		$uid = isset ($this->session->userdata['log_in']['user_id']) ? $this->session->userdata['log_in']['user_id'] : '';
		$orderdetails = $this->Transaction_model->getOrderDetails($orderID,$uid);
		$data['order'] = $this->Transaction_model->getOrder($orderID,$uid);
		$photopath = $this->config->item('image_display_path');
		$photopath_absolute = $this->config->item('image_path');
		$thumb_size= $this->config->item('thumb_size');
		$medium_thumb_size= $this->config->item('medium_thumb_size');
		
        foreach($orderdetails as $odetails)
		{
			
			$productId = $odetails->product_id;
			$productName = $odetails->name;
			$odetails->pslug = $this->product_model->getProductUrl_name($productId,$productName);
			$fetch_image = $this->product_model->getProductImageById($productId);
			
				$photopath_is_check =$photopath_absolute."product/".$productId;
				$thumpath = $photopath."product/".$productId.$medium_thumb_size;
				
				if(is_file($this->config->item('image_path').'/product/'.$productId.$medium_thumb_size.$fetch_image)){
				$odetails->imgpurl=$this->config->item('image_display_path').'product/'.$productId.$medium_thumb_size.$fetch_image ;
				}  else{
				$odetails->imgpurl= $this->config->item('image_display_path').'product/no_product.jpg';
				}
				
			$order_details[] = $odetails;
			
		}
		$data['orderdetails']=$order_details;
		if(!empty($data['orderdetails'])){
		if ($data['orderdetails'][0]->id == $uid )
		{
		$this->load->ftemplate('transaction/deatils',$data);
		}else{ redirect ('transaction'); } 
		}else{ redirect ('transaction'); } 
	}
/*	    public function index()
    {
    	$this->load->view('transaction');
    }*/

    public function process()
    {
    	$order_id = $this->input->post('order_id');
    	$action = $this->input->post('action');
    	switch ($action) {
		    case 'status':
		        $this->status($order_id);
		        break;
		    case 'approve':
		        $this->approve($order_id);
		        break;
		    case 'expire':
		        $this->expire($order_id);
		        break;
		   	case 'cancel':
		        $this->cancel($order_id);
		        break;
		}

    }

	public function status($order_id)
	{
		echo 'test get status </br>';
		print_r ($this->veritrans->status($order_id) );
	}

	public function cancel($order_id)
	{
		echo 'test cancel trx </br>';
		echo $this->veritrans->cancel($order_id);
	}

	public function approve($order_id)
	{
		echo 'test get approve </br>';
		print_r ($this->veritrans->approve($order_id) );
	}

	public function expire($order_id)
	{
		echo 'test get expire </br>';
		print_r ($this->veritrans->expire($order_id) );
	}
}