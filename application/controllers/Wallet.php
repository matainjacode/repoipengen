<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wallet extends CI_Controller {
	private $langId;
	private $lang_code;
	function __construct(){
		parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
		$this->load->library('form_validation');
		$this->load->model('Wallet_model');
		$this->load->library('session'); 
		if($this->session->userdata('log_in')== false){
		  redirect();
		}
	

		// $uid = $this->session->set_userdata('user_id');
	}
	
	public function index()
	{
		
	}
	public function my_cash_gift_list()
	{
		$uid = $this->session->userdata['log_in']['user_id'];
		$data['lang_code'] = $this->lang_code;
		$data['page_title']=$this->lang->line("cash_gift");
		$data['cashgiftlist'] = $this->Wallet_model->getMyCashGift($uid);
		$this->load->ftemplate('wallet/my_cash_gift_list',$data);		

	}
	public function details($id=NULL)
		{
			$uid = $this->session->userdata['log_in']['user_id'];
			$this->Wallet_model->getViewStatus($id,$uid);
			$data['lang_code'] = $this->lang_code;
			$data['page_title'] = $tis->lang->line("details");
			$data['cashgiftlist'] = $this->Wallet_model->getSingleCashGift($id,$uid);
			$this->load->ftemplate('wallet/details',$data);		
	
		}

}
?>