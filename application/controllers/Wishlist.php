<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wishlist extends CI_Controller {
	private $user_id = "";
	private $langId;
	private $lang_code;
	function __construct(){
		parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
		$this->load->library('form_validation');
		$this->load->model(array('Wishlist_model','search_model','Product_model','user_model','payment_model'));
		$this->load->library('session'); 
		$this->load->helper('cookie');
		$this->load->library('image_lib');
		$this->load->library('email');
		$this->load->library('pagination');	
		$this->load->helper('common_helper');
		$this->load->helper('ipengen_email_helper');
		$this->load->helper('url');
		$controller = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		if($method != "searchlist")
		{
			if($controller=='wishlist' && $method!='wishlist'){	
				if($this->session->userdata('log_in')== false){
				  redirect('/');
			   }
			}
		}
		if($log_sess = $this->session->userdata('log_in'))
		{$this->user_id = $log_sess['user_id'];}
		if($sess_data = $this->session->userdata("site_lang"))
		{$this->langId = $sess_data["laguage_id"];}
		

		// $uid = $this->session->set_userdata('user_id');
	}
	
	public function index()
	{		

	}
	
	public function create()
	{
		$data['lang_code'] = $this->lang_code;  
	    $data['page_title']= $this->lang->line("create_wishlist");   
		$uid = $this->session->userdata['log_in']['user_id'];
		$data['status'] = $this->Wishlist_model->getid($uid);
		$data['groups'] = $this->Wishlist_model->getAllcategory();
		$data['all_wishlist'] = $this->Wishlist_model->get_dropdown_list($uid);
		$data['maincategory'] = $this->Wishlist_model->getAllmaincategory();
		$data['totalwishlist'] = $this->Wishlist_model->totalWishlistCount($uid);
		$this->load->ftemplate('Wishlist/create',$data);
	}
	
	public function checkId()
	{    
        $uid = $this->session->userdata['log_in']['user_id'];
		$data['lang_code'] = $this->lang_code;
		$data['page_title']=$this->lang->line("wishlist_check"); 
		$data['status'] = $this->Wishlist_model->getid($uid);
		$this->load->ftemplate('Wishlist/create',$data);
	}
	
	public function get_wishlist_address()
	{   
		if(isset($_POST['option'])){
			echo json_encode($this->Wishlist_model->getaddress($_POST['option']));
       	}
	}
	
	public function get_url()
	{
		
		if($this->input->is_ajax_request()){
			
			$uid = $this->session->userdata['log_in']['user_id'];
			$wishname = $this->input->post('wishname');
			$wishnametext = str_replace(" ", "-", $wishname);
			$uniqnamecount = $this->Wishlist_model->checkUrl($wishnametext,$uid);
			if($uniqnamecount == 0){
				  $result['msg'] = "success";
				}else { $result['msg'] = "error"; }
				
				echo json_encode($result); 
			
		}
		
		
	}
	
	public function getWishlistUrlById()
	{
		if($this->input->is_ajax_request()){
			$wid = $this->input->post('wid');
			$wishlist = $this->Wishlist_model->getwishlistByid($wid);
			$result = base_url().'~'.$wishlist->url;
			echo json_encode($result); 
		}
	}
		
	public function create_wishlist(){
		 
     if($this->input->is_ajax_request()){
			//set validation rules
			$uid = $this->session->userdata['log_in']['user_id'];
			
			//$this->form_validation->set_rules('wishlistname', 'Wishlist Name', 'trim|required|is_unique[ig_wishlist.title]');
			$this->form_validation->set_rules('eventdate', 'Event Start Date', 'trim|required');
			$this->form_validation->set_rules('eventenddate', 'Event End Date', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('addr', 'Address', 'trim|required');
			$this->form_validation->set_rules('pcode', 'Postcode', 'trim|required');

			if ($this->form_validation->run() == FALSE){
				// fails.
				$data['lang_code'] = $this->lang_code;
				$data['page_title']=$this->lang->line("create_wishlist");
				$result['msg'] = "unique";
				$uid = $this->session->userdata('user_id');
				$data['status'] = $this->Wishlist_model->getid($uid);
				$data['groups'] = $this->Wishlist_model->getAllcategory();
				$data['maincategory'] = $this->Wishlist_model->getAllmaincategory();
				$data['all_wishlist'] = $this->Wishlist_model->get_dropdown_list($uid);
				$data['totalwishlist'] = $this->Wishlist_model->totalWishlistCount($uid);
				$this->load->ftemplate('Wishlist/create',$data);
			}
			else{
				$wishlistaddress = array(
					'uid' => $uid,
					'name' => ($this->input->post('firstname').' '.$this->input->post('lastname')),
					'city' => $this->input->post('city'),
					'district' => $this->input->post('dist'),
					'street_address' => ($this->input->post('addr').','.$this->input->post('addr2')),
					'zip' => $this->input->post('pcode'),
					'ph_no' => $this->input->post('mobileno'),
					
				);
				$addressid = $this->Wishlist_model->wishlistaddress($wishlistaddress);
				
				//echo $this->input->post('eventdate').'<br>';
				$startDatePost=$this->input->post('eventdate');
				$starttimestamp = strtotime($startDatePost);
				$sDate=date("Y-m-d H:i:s", $starttimestamp);
				
			    $endDatePost=$this->input->post('eventenddate');
				$endtimestamp = strtotime($endDatePost);
				$eDate=date("Y-m-d H:i:s", $endtimestamp);
				
				$wishlistdata = array(
					'uid' => $uid,
					'title' => $this->input->post('wishlistname'),
					'category' => $this->input->post('category'),
					'e_startdate' => $sDate,
					'e_enddate' => $eDate,
					'is_case_received' => ($this->input->post('cashgift')=='yes') ? 'yes' : 'no',
					'is_public' => $this->input->post('privacy'),
					'wishlist_password' => ($this->input->post('visitpassword')) ? md5($this->input->post('visitpassword')) : '',
					'created' => date('Y-m-d H:i:s'),
					'url' => $this->input->post('url'),
					'is_show_shipping_address' => ($this->input->post('shiprivacy')=='yes') ? 'yes' : 'no',
					
				);

				$getwishid = $this->Wishlist_model->insertwishlist($wishlistdata);
			
			
				$shippingddress = array(
					'uid' => $uid,
					'wid' => $getwishid,
					'name' => ($this->input->post('firstname').' '.$this->input->post('lastname')),
					'city' => $this->input->post('city'),
					'district' => $this->input->post('dist'),
					'street_address' => ($this->input->post('addr').','.$this->input->post('addr2')),
					'zip' => $this->input->post('pcode'),
					'ph_no' => $this->input->post('mobileno'),
					
				);
				$getresult = $this->Wishlist_model->shippingddress($shippingddress);
			    $user_details = $this->user_model->userdeatails($uid);

				if(!empty($getwishid)){
				  //redirect('wishlist/mywishlist');
				  $wdata['wishlist_name'] = $this->input->post('wishlistname');
				  $wdata['wishlist_url'] = $this->input->post('url');
				  $wdata['user_name'] = ucfirst($user_details[0]->fname) .' '. ucfirst($user_details[0]->lname) ;
				  $configEmail = $this->payment_model->getAllConfigEmail();
		          $emailFrom = $configEmail[0]->info_email;
		          $emailBody = $this->load->view("e_template/new_wishlist_notification",$wdata,true);
			      $subject = "Congratulation on your new wishlist : ".$this->input->post('wishlistname');
				  $emailTo = $user_details[0]->email;
			      $emailData = array(
						"title" 	 =>    "Ipengen",
						"from"	  =>	$emailFrom,
						"to"		=>	$emailTo,
						"subject"   =>	$subject,
						"message"   =>	$emailBody
				  );
			      send_email($emailData);
			
				  
				  $result['msg'] = "success";
				  $result['getwishid'] = $getwishid;
				  //$this->session->set_flashdata('sussess_message', 'Registered successfully! Please check your mail for verification link');
				}

			}
			echo json_encode($result); 
		}
	}
	
	public function edit_wishlist(){
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("edit_wishlist");
        $result1 = array();
		if($this->input->is_ajax_request()){
			$uid = $this->session->userdata['log_in']['user_id'];
			//set validation rules
			$uid = $this->session->userdata['log_in']['user_id']; 
			$this->form_validation->set_rules('eventdate', 'Event Start Date', 'trim|required');
			$this->form_validation->set_rules('eventenddate', 'Event End Date', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('addr', 'Address', 'trim|required');
			$this->form_validation->set_rules('pcode', 'Postcode', 'trim|required');
			//validate form input
			if ($this->form_validation->run() == FALSE){
				// fails
				$result1['msg'] = "unique";
				$data['lang_code'] = $this->lang_code;
				$data['page_title']=$this->lang->line("edit_wishlist");;
				$uid = $this->session->userdata('user_id');
				$data['status'] = $this->Wishlist_model->getid($uid);
				$data['groups'] = $this->Wishlist_model->getAllcategory();
				$data['maincategory'] = $this->Wishlist_model->getAllmaincategory();
				$data['all_wishlist'] = $this->Wishlist_model->get_dropdown_list($uid);
				$data['totalwishlist'] = $this->Wishlist_model->totalWishlistCount($uid);
				$data['getpassword'] = $this->Wishlist_model->getpassword($this->input->post('wishid'));
				$this->load->ftemplate('Wishlist/create',$data);
			}
			else{
				
				//$edwid = $this->input->post('wishid');
		        $edwid = ($this->input->post('lastinsertid'))? $this->input->post('lastinsertid') : $this->input->post('wishid');
				$startDatePost=$this->input->post('eventdate');
				$starttimestamp = strtotime($startDatePost);
				$sDate=date("Y-m-d H:i:s", $starttimestamp);
				
				$endDatePost=$this->input->post('eventenddate');
				$endtimestamp = strtotime($endDatePost);
				$eDate=date("Y-m-d H:i:s", $endtimestamp);
				$getpassword = $this->Wishlist_model->getpassword($this->input->post('wishid'));
				foreach ($getpassword as $getpasswords)
				{
					 $pass = $getpasswords->wishlist_password;
				}
				if($this->input->post('visitpassword') == '')
				{
					$postpassword = $pass;
				}else {$postpassword = md5($this->input->post('visitpassword')); }
				
				$wishlistdata = array(
					'uid' => $uid,
					'title' => $this->input->post('wishlistname'),
					'category' => $this->input->post('category'),
					'e_startdate' => $sDate,
					'e_enddate' => $eDate,
					'is_case_received' => ($this->input->post('cashgift')=='yes') ? 'yes' : 'no',
					'is_public' => ($this->input->post('privacy')=='public')?'public':'private',
					'wishlist_password' => ($this->input->post('privacy')=='private') ? $postpassword : '',
					'created' => date('Y-m-d H:i:s'),
					'url' =>  $this->input->post('url'),
					
				);
				$getwishid = $this->Wishlist_model->editwishlist($wishlistdata,$edwid);
			
			   $shippingddress = array(
					'uid' => $uid,
					'name' => ($this->input->post('firstname').' '.$this->input->post('lastname')),
					'city' => $this->input->post('city'),
					'district' => $this->input->post('dist'),
					'street_address' => ($this->input->post('addr').','.$this->input->post('addr2')),
					'zip' => $this->input->post('pcode'),
					'ph_no' => $this->input->post('mobileno'),
					
				);
				$getresult = $this->Wishlist_model->editshippingddress($shippingddress,$edwid);
		
			

				if($getwishid){
				  //redirect('wishlist/mywishlist');
				  $result1['msg'] = "success";
				  $result1['getwishid'] = $getwishid;
				}

			}
			echo json_encode($result1); 
		}
	}

	public function create_personalize(){

		if($this->input->is_ajax_request()){
			$uid = $this->session->userdata['log_in']['user_id'];
			$id = $this->input->post('lastwid');
			if(!is_dir($this->config->item('image_path').'wishlist/'.$id.'/')) {
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/');
			}
			if(!is_dir($this->config->item('image_path').'wishlist/'.$id.'/100-100/')) {
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/100-100/', 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'wishlist/'.$id.'/700-700/')) { 
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/700-700/', 0777, true); 
			}
			if (!is_dir($this->config->item('image_path').'wishlist/'.$id.'/50-50/')) { 
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/50-50/', 0777, true); 
			} 
			$imagepath = $this->config->item('image_path').'wishlist/'.$id.'/';
			$img = $this->input->post('wishlist_image');
			
			$pos = strpos($img, 'data:image');
			if ($pos === false) {
				$segments = explode('/', $img);
               $lastimage = end($segments);
			
			} else {
				
			   $lastimage ='';
			}
			if($lastimage == '')
			{
			
			$img = str_replace('data:image/jpeg;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$filename = uniqid() . '.jpeg';		
			$file = $imagepath . $filename;
			$success = file_put_contents($file, $data);		
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/100-100/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 250, 
				'height'            => 250,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize(); 
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/700-700/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 600, 
				'height'            => 510,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize();	
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/50-50/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 50, 
				'height'            => 50,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize();
			}
			else
			{
				$filename ='';
			}
			$eventDatePost=$this->input->post('eventdate');
			//$eventDate=explode("/",$eventDatePost);
			//$eventDateFiltered=$eventDate[1].'/'.$eventDate[0].'/'.$eventDate[2];
			$eventtimestamp = strtotime($eventDatePost);
			$eDate=date("Y-m-d H:i:s", $eventtimestamp);
			
						
		 	$wishlistiamge = array(
				'rsvp_info' => $this->input->post('rsvp_info'),
				'wishlist_address' => $this->input->post('wishlist_address'),
				'event_date'=>$eDate,
				'wishlist_image' => $filename,
				'lat' =>($this->input->post('latitude')!= '') ? $this->input->post('latitude') : '',
				'long' => ($this->input->post('longitute') !='') ? $this->input->post('longitute') : '',
			);

			$addressid = $this->Wishlist_model->wishlistpersonalize($wishlistiamge,$id);
			if($addressid){
			  $result['msg'] = "success";
			}			
			echo json_encode($result); 
		 }
	 }
	 
	public function upload_image_personalize(){

		if($this->input->is_ajax_request()){
			$uid = $this->session->userdata['log_in']['user_id'];
			$id = $this->input->post('lastwid');
		 	if(!is_dir($this->config->item('image_path').'wishlist/'.$id.'/')) {
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/');
			}
			if(!is_dir($this->config->item('image_path').'wishlist/'.$id.'/100-100/')) {
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/100-100/', 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'wishlist/'.$id.'/700-700/')) { 
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/700-700/', 0777, true); 
			}
			if (!is_dir($this->config->item('image_path').'wishlist/'.$id.'/50-50/')) { 
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/50-50/', 0777, true); 
			} 
			$imagepath = $this->config->item('image_path').'wishlist/'.$id.'/';
			$img = $this->input->post('wishlist_image');
			
			$pos = strpos($img, 'data:image');
			if ($pos === false) {
				$segments = explode('/', $img);
               $lastimage = end($segments);
			
			} else {
				
			   $lastimage ='';
			}
			if($lastimage == '')
			{
			
			$img = str_replace('data:image/jpeg;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$filename = uniqid() . '.jpeg';		
			$file = $imagepath . $filename;
			$success = file_put_contents($file, $data);		
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/100-100/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 250, 
				'height'            => 250,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize(); 
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/700-700/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 600, 
				'height'            => 510,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize();	
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/50-50/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 50, 
				'height'            => 50,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize();
			}
			else
			{
				$filename ='';
			}

						
		 	$wishlistiamge = array(
				'wishlist_image' => $filename,
			);

			$addressid = $this->Wishlist_model->wishlistpersonalize($wishlistiamge,$id);
			if($addressid){
			  $result['msg'] = "success";
			}			
			echo json_encode($result); 
		 }
	 }
	 
	 public function edit_personalize(){
		// print_r($_POST);
		// die();
		if($this->input->is_ajax_request()){
			$id = $this->input->post('lastwid');
		 	if(!is_dir($this->config->item('image_path').'wishlist/'.$id.'/')) {
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/');
			}
			if(!is_dir($this->config->item('image_path').'wishlist/'.$id.'/100-100/')) {
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/100-100/', 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'wishlist/'.$id.'/700-700/')) { 
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/700-700/', 0777, true); 
			}
			if (!is_dir($this->config->item('image_path').'wishlist/'.$id.'/50-50/')) { 
				mkdir($this->config->item('image_path').'wishlist/'.$id.'/50-50/', 0777, true); 
			} 
			$imagepath = $this->config->item('image_path').'wishlist/'.$id.'/';
			$img = $this->input->post('wishlist_image');
			$pos = strpos($img, 'data:image');
			if ($pos === false) {
				$segments = explode('/', $img);
               $lastimage = end($segments);
			
			} else {
				
			   $lastimage ='';
			}
			
			$getimgid = $this->Wishlist_model->getimageid($id);
			//unlink($this->config->item('image_path').'wishlist/'.$id.'/'.$this->session->userdata['log_in']['profile_pic']);
			if($lastimage == '')
			{
			$img = str_replace('data:image/jpeg;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			 $filename = uniqid() . '.jpeg';	
			
				
			$file = $imagepath . $filename;
			$success = file_put_contents($file, $data);		
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/100-100/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 250, 
				'height'            => 250,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize(); 
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/700-700/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 600, 
				'height'            => 510,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize();	
			$config['image_library'] = 'gd2'; 
			$config = array( 
				'source_image'      => $this->config->item('image_path').'wishlist/'.$id.'/'.$filename, 
				'new_image'         => $this->config->item('image_path').'wishlist/'.$id.'/50-50/'.$filename, 
				'maintain_ratio'    => TRUE, 
				'width'             => 50, 
				'height'            => 50,  
			); 
			$this->image_lib->clear();
    		$this->image_lib->initialize($config);
    		$this->image_lib->resize();
			}
			
			else
			{
				$filename = $lastimage;
			}

			
			$eventDatePost=$this->input->post('eventdate');
			//$eventDate=explode("/",$eventDatePost);
			//$eventDateFiltered=$eventDate[1].'/'.$eventDate[0].'/'.$eventDate[2];
			$eventtimestamp = strtotime($eventDatePost);
			$eDate=date("Y-m-d H:i:s", $eventtimestamp);	
			
						
		 	$wishlistiamge = array(
				'rsvp_info' => $this->input->post('rsvp_info'),
				'wishlist_address' => htmlentities($this->input->post('wishlist_address')),
				'event_date'=>($eDate) ? $eDate : '',
				'wishlist_image' => $filename,
				'lat' =>($this->input->post('latitude')!= '') ? $this->input->post('latitude') : '',
				'long' => ($this->input->post('longitute') !='') ? $this->input->post('longitute') : '',
			);
			

			$addressid = $this->Wishlist_model->editwishlistpersonalize($wishlistiamge,$id);
			if($addressid){
			  $result['msg'] = "success";
			}			
			echo json_encode($result); 
		 }
	 }
	 
	 
     public function my_wishlist()
	 {
		 $data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("my_wishlist");
		$uid = $this->session->userdata['log_in']['user_id'];
		$rowNO = $this->Wishlist_model->row_count($uid);
			//+-------------------Pagination Code------------------------------------------------------------------
				$config =array(
						'base_url' 				=> 	base_url('my-wishlist/page'),
						'total_rows'			=>	$rowNO,
						'per_page'				=>	5,
						'use_page_numbers' 		=>	TRUE,						//For Proper Page Number to Show in Url
						'full_tag_open'			=>	"<ul class='pagination'>",	//Main Tag Open
						'full_tag_close'		=>	'</ul>',					//Main Tag Close
						'first_tag_open'		=>	'<li>',						//Sub-Main First Tag Open Ex. - '< First'
						'first_tag_close'		=>	'</li>',					//Sub-Main First Tag Close
						'last_tag_open'			=>	'<li>',						//Sub-Main Last Tag Open Ex. - 'Last >'
						'last_tag_close'		=>	'</li>',					//Sub-Main Last Tag Close
						'next_tag_open'			=>	'<li>',	
						'next_tag_close'		=>	'</li>',
						'next_link' 			=> 	'Next',
						'prev_tag_open'			=>	'<li>',
						'prev_tag_close'		=>	'</li>',
						'prev_link'				=>	'Previous',
						'num_tag_open'			=>	'<li>',
						'num_tag_close'			=>	'</li>',
						'cur_tag_open'			=>	'<li class="active"><a>',	//Sub-Main Current Tag Open
						'cur_tag_close'			=>	'</a></li>',				//Sub-Main Current Tag Close
						'uri_segment'           => '3',
					);
			$this->pagination->initialize($config); 
			if($this->uri->segment(3)!=""){$offset=$config['per_page']*$this->uri->segment(3)-$config['per_page'];}else{$offset=0;}
		//+-------------------Pagination Code End---------------------------------------------------------			
		 $data['my_wishlist'] = $this->Wishlist_model->get_my_wishlist($uid,$config['per_page'],$offset);
		 $this->load->ftemplate('Wishlist/mywishlist',$data);
	 }
	 
	 public function wishlisttransfer()
	 {
		 $wishlistid= $this->input->post('wishlistid');
		 $is_mail= $this->input->post('is_mail');
		 $getwishurl = $this->Wishlist_model->sendmailurl($wishlistid);
		 if(!empty($getwishurl)){
		 foreach($getwishurl as $url)
		 {
			  $sendurl = $url->url;
			 
		 }}
		 
		$this->email->from('info@amazonaws.com', 'Ipengen');
		$this->email->to($is_mail);
		
		$this->email->subject('Received Wishlist');
		$this->email->message('My wishlist url is :'.base_url().'~'.$sendurl);
		
		if($this->email->send())
		$result['msg'] = "success";
		else
		$result['msg'] = "error";
		echo json_encode($result);
		 
		 
	 }
	 
	 public function changeprivacy()
	 {
		$wishlistid= $this->input->post('wishlistid');
		$is_public= $this->input->post('is_public');
		$password= $this->input->post('password');
		
		$updateprivacy = array(
				'is_public' => $this->input->post('is_public'),
				'wishlist_password' => ($this->input->post('is_public')=='private') ? md5($this->input->post('password')) : '' ,
			);
			
		
			$pupdate = $this->Wishlist_model->updateprivacy($updateprivacy,$wishlistid);
		
		if($pupdate){
			  $result['msg'] = "success";
			  $result['privacy'] = $this->input->post('is_public');
			}
			else
			{
				$result['msg'] = "error";
			}
			echo json_encode($result); 
		
		/*$result = $this->Wishlist_model->getwishlistByid($wishlistid);
		if(!empty($result)){
			if($result->is_public=="public"){
				$pub='private';
			}else{
				$pub='public';
			}
			$resultA= $this->Wishlist_model->changeprivacy();
			if($resultA){
				echo $pub;
			}else{
				echo 'error';
			}
		}	*/
	 }
	 
	 // Wishlist details 
	public function wishlist($id){
		//echo $this->input->cookie('checkWishlist');
		$loginUserArray= array();
		$my_wishlist=$this->Wishlist_model->getwishlistByUrl($id);
		$wishlist_owner = $this->user_model->userdeatails($my_wishlist[0]->uid);
		$uids = isset ($this->session->userdata['log_in']['user_id']) ? $this->session->userdata['log_in']['user_id'] : '';
		if(!empty($my_wishlist)){
			$siteLangArray = $this->session->userdata('site_lang');
			$Langid=$siteLangArray['laguage_id'];
			if($Langid==""){ $Langid=1;}
			if($this->session->has_userdata('log_in')){ $loginUserArray=$this->session->userdata('log_in');	}
			$data['wishlistID']=$id;
			$data['loginUserArray']=$loginUserArray;
			$data['my_wishlist'] = $wishlist = $my_wishlist[0];
			$data['wishlist_owner'] = $wishlist_owner[0];
			$data['wishlist_owner_url'] = $this->user_model->getuserUrl($my_wishlist[0]->uid);
			$data['my_wishlist_product'] = $this->Wishlist_model->getProduct($data['my_wishlist']->id,$Langid);
			$data['my_wishlist_cashgift'] = $this->Wishlist_model->getAllCashGift($data['my_wishlist']->id,$Langid);
			if($this->session->userdata('log_in')== true){
				$uids = $this->session->userdata['log_in']['user_id'];
              $data['check_contribution'] = $this->Wishlist_model->checkContribution($data['my_wishlist']->id,$uids);
           }
			
						
			$data['my_wishlist_shipping'] = $this->Wishlist_model->getWishlistShippingById($data['my_wishlist']->id);
			if(!empty($loginUserArray['user_id'])){
				$data['checkHasRecipient'] = $this->Wishlist_model->checkHasRecipent($loginUserArray['user_id'],$data['my_wishlist']->id,$data['my_wishlist']->uid);	
			}
			
			
			$data['lang_code'] = $this->lang_code;
			$data['page_title']=$data['my_wishlist']->title;
			
			/*Meta Wishlist image Start*/
			if($wishlist->wishlist_image!='' && file_exists('./photo/wishlist/'.$wishlist->id.'/700-700/'.$wishlist->wishlist_image))
			{ $imagepath = base_url()."photo/wishlist/".$wishlist->id."/".$wishlist->wishlist_image; }
			else
			{ $imagepath = base_url()."photo/wishlist/no-event.png"; }
			/*Meta wishlist image end*/
			$data['wishlistimg']= $imagepath;
			$data['wishlisturl']= base_url()."~".$wishlist->title;
			$data['wishlisttitle']= ucwords($wishlist->title);
			$data['wishlistdes']= $wishlist->description;
			$data['wishlistAppId']= "153337848436868";
			$data['login_url'] = $this->facebook->getLoginUrl(array(
				'redirect_uri' => site_url('user/login'), 
				'scope' => array("email") // permissions here
			));
			$formSubmit=$this->input->post('wishPassSubmit');
			$reqPass=$this->input->post('reqPass');
			//print_r($data);
			//die();
			if(!empty($reqPass) && isset($reqPass)){
				$data['fname']=$fname=$this->input->post('fname');
				$data['lname']=$this->input->post('lname');
				$data['reqemail']=$reqEmail=$this->input->post('reqemail');
				$data['wishlistName']=$data['my_wishlist']->title;
				$data['wishlistUrl']= base_url()."~".$data['my_wishlist']->title;
				if($fname=='' || empty($fname)) {
					$data['errorFname']=true;
					$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
				}
				elseif(!filter_var($reqEmail, FILTER_VALIDATE_EMAIL)) {
					$data['errorReqEmail']=true;
					$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
				}
				else{
					$wishlistOwnerEmail = $wishlist_owner[0]->email;
					$emailFrom = $reqEmail;
					$subject = 'Request Password for "'.strtoupper($my_wishlist[0]->title).'" wishlist.';
					$W_owner_emailBody = $this->load->view("e_template/Wishlist-password-request",$data,true);
					$emailData = array(
						"title" 	=> "Ipengen",
						"from"		=>	$emailFrom,
						"to"		=>	$wishlistOwnerEmail,
						"subject"	=>	$subject,
						"message"	=>	$W_owner_emailBody
								);
			//print_r($emailData);
			//die();
			
					$emailSendStatus = send_email($emailData);	
					//$emailSendStatus=send_email($mailData);
					if(isset($emailSendStatus)){
						$data['notificationEmail']='Email Sent Successfully.';
						$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);	
					}
					else{
						$data['notificationEmail']="Email sending Failed.";
						$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
					}
				}
			}
			if(!empty($formSubmit) && isset($formSubmit)){
				$wishPassRaw=$this->input->post('wishPass');
				if(!empty($wishPassRaw) && $wishPassRaw!=''){
					$wishPass=md5($wishPassRaw);
					$checkPass=$this->Wishlist_model->checkWishlistPass($wishPass,$data['my_wishlist']->id);
					if($checkPass){
						if($this->input->cookie('checkWishlist')!=''){
							$cookieVal=$this->input->cookie('checkWishlist');
							$cookieDecodeArray=json_decode($cookieVal,true);
							if(!array_key_exists($data['my_wishlist']->id,$cookieDecodeArray)){
								$cookieDecodeArray[$data['my_wishlist']->id]=$wishPass;
								$cookieArray=json_encode($cookieDecodeArray);
								set_cookie('checkWishlist', $cookieArray, $expire = '864000');
							}
						}
						else{
							$cookieArray=json_encode(array($data['my_wishlist']->id=>$wishPass));
							set_cookie('checkWishlist', $cookieArray, $expire = '864000');	
						}
						if(!empty($data['loginUserArray'])){
							$this->load->ftemplate('Wishlist/viewWishlistPublic',$data);
						}else{
							if($my_wishlist[0]->admin_status==0){redirect('/');}else{
							$this->load->ftemplate('Wishlist/viewWishlistGuest',$data);
							}
						}
					}
					else{
						$data['error']=true;
						$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
					}	
				}
				else{
					$data['error']=true;
					$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
				}
			}
			else{ 
				if(!empty($data['loginUserArray'])){
					if($data['loginUserArray']['user_id']!=''){
						if($data['loginUserArray']['user_id']!=$data['my_wishlist']->uid){
							if($data['my_wishlist']->is_public=='private'){
								if(!$this->input->cookie('checkWishlist', TRUE)){	
									$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
								}
								else{
									$cookieVal=$this->input->cookie('checkWishlist', TRUE);
									$cookieDecodeArray=json_decode($cookieVal,true);
									if(array_key_exists($data['my_wishlist']->id,$cookieDecodeArray)){
										$checkPass=$this->Wishlist_model->checkWishlistPass($cookieDecodeArray[$data['my_wishlist']->id],$data['my_wishlist']->id);
										if($checkPass){
											if($data['my_wishlist']->admin_status==0){redirect('/');}else{ 
											$this->load->ftemplate('Wishlist/viewWishlistPublic',$data);
											}
										}
										else{
											if($data['my_wishlist']->admin_status==0){redirect('/');}else{
											$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
											}
										}
									}
									else{
										if($data['my_wishlist']->admin_status==0){redirect('/');}else{
										$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
										}
									}
								}
							}
							if($data['my_wishlist']->is_public=='public'){
								if($data['my_wishlist']->admin_status==0){redirect('/');}else{
								$this->load->ftemplate('Wishlist/viewWishlistPublic',$data);
								}
							}
						}
						else{
							$data['is_block']=$data['my_wishlist']->admin_status;
							$this->load->ftemplate('Wishlist/viewWishlistPublic',$data);
						}
					}
				}
				else{ 
					if($data['my_wishlist']->is_public=='public'){
						if($data['my_wishlist']->admin_status==0){redirect('/');}else{
							$this->load->ftemplate('Wishlist/viewWishlistGuest',$data);
							}
						
						//$this->load->ftemplate('Wishlist/viewWishlistGuest',$data);
						
					}
					if($data['my_wishlist']->is_public=='private'){
						if($this->input->cookie('checkWishlist')!=''){
							$cookieVal=$this->input->cookie('checkWishlist');
							$cookieDecodeArray=json_decode($cookieVal,true);	
							if(array_key_exists($data['my_wishlist']->id,$cookieDecodeArray)){
								$checkPass=$this->Wishlist_model->checkWishlistPass($cookieDecodeArray[$data['my_wishlist']->id],$data['my_wishlist']->id);
								if($checkPass){
											if($data['my_wishlist']->admin_status==0){redirect('/');}else{
											    $this->load->ftemplate('Wishlist/viewWishlistGuest',$data);
											}
								}
								else{
									if($data['my_wishlist']->admin_status==0){redirect('/');}else{
									$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
									}
								}
							}
							else{
								if($data['my_wishlist']->admin_status==0){redirect('/');}else{
								$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
								}
							}
						}
						else{
							if($data['my_wishlist']->admin_status==0){redirect('/');}else{
							$this->load->ftemplate('Wishlist/viewWishlistPrivate',$data);
							}
						}
					}
				}
			}
		}else{
			redirect(base_url());
		}	
		
	}

	 
	public function edit($id){
		$data['lang_code'] = $this->lang_code;
		$data['page_title']=$this->lang->line("edit_wishlist");
		$uid = $this->session->userdata['log_in']['user_id'];
		$data['wishlist_details'] = $this->Wishlist_model->getdetailwishlistByid($id);
		$data['all_wishlist'] = $this->Wishlist_model->get_dropdown_list($uid);
		$data['groups'] = $this->Wishlist_model->getAllcategory();
		$data['maincategory'] = $this->Wishlist_model->getAllmaincategory();
		$data['totalwishlist'] = $this->Wishlist_model->totalWishlistCount($uid);
		$data['getpassword'] = $this->Wishlist_model->getpassword($id);
		$data['status'] = $this->Wishlist_model->getid($uid);
		$this->load->ftemplate('Wishlist/editWishlist',$data);
	}
	
	public function addRecipient(){
		//print_r($_POST);die();
		if($this->input->is_ajax_request()){
			echo $addRecipientResponse=$this->Wishlist_model->addRecipentModel($_POST['uid'],$_POST['wid'],$_POST['wishUid']);
			die();
		}
	}
	
	public function removeRecipient(){
		//print_r($_POST);die();
		if($this->input->is_ajax_request()){
			echo $removeRecipientResponse=$this->Wishlist_model->removeRecipentModel($_POST['uid'],$_POST['wid'],$_POST['wishUid']);
			die();
		}
	}
	
	public function recipient()
	{
		$data['lang_code'] = $this->lang_code;
		$data['page_title']= $this->lang->line("my_recipients");
		$uid = $this->session->userdata['log_in']['user_id'];
		$data['recipient_result']=$this->Wishlist_model->recipient_list($uid);
		
		$this->load->ftemplate('Wishlist/recipientlist',$data);
	}
	public function delete_recipient($wid)
	{

		$result=$this->Wishlist_model->delete_recipient($wid);
		if($result)
		{
			redirect('recipient');
		}

	}
	public function sendmail()
	{
		$userID = $this->user_id;
		$emails_to = $this->input->post("emailall");
		$wishlistID = $this->input->post("wisid");
		$productID = $this->input->post("proid");
		$data["wishlistData"] = $this->Wishlist_model->getDetailWishlistAndProductByWishlistid($wishlistID, $productID, $this->langId);
		$data["userData"] = $this->Wishlist_model->getUserdataUsingId($userID);
		if(!empty($data["wishlistData"]) && !empty($data["userData"]))
		{
			$userName = ucwords($data["userData"][0]->fname." ".$data["userData"][0]->lname);
			$fromEmail = $data["userData"][0]->email;
			$subject = "Friend Contribution | Notification";
			foreach($emails_to as $email)
			{
				$data["receiverData"] = $this->Wishlist_model->receiverDataByEmailId($email);
			    $emailBody = $this->load->view("e_template/wishlist-email-contribute",$data, true);
				$emailData = array(
						"title" 	=>  $userName,
						"from"		=>	$fromEmail,
						"to"		=>	$email,
						"subject"	=>	$subject,
						"message"	=>	$emailBody
								);
				$result = send_email($emailData);	
				
			}
			if($result)
			{echo 1;}else{echo 0;}
		}
		else
		{
			echo 0;
		}
		
	
	}
	public function searchlist()
	{
		if($this->input->get("searchbutton"))
		{
			$allList = array();
			$inputdata = $this->input->get("searchdata");
			$result=$this->Wishlist_model->allWishlistId($inputdata);
			$product_result=$this->search_model->product_search($inputdata);
			if(!empty($result))
			{
				foreach($result as $row)
				{ 
					if(is_array($row))
					{
						foreach($row as $newRow)
						{
							if(!empty($newRow))
							{
								$allList[] = (array)$newRow; 
							}
							
						}
					}
					else
					{
						$allList[] = (array)$row;
					}
					
				}
			}
			if(!empty($product_result))
			{
				foreach($product_result as $row_new)
				{ $allList[] = $row_new; }
			}
			
			$new_arr = array_unique($allList, SORT_REGULAR);
			$data['lang_code'] = $this->lang_code;
			$data["page_title"] = $this->lang->line("search");
			$data["result"] = $new_arr;
			$data["searchkey"] = $inputdata;
			$this->load->ftemplate('Wishlist/searchlist',$data);
			//print_r($allList);
		}
		else
		{
			redirect("shop");
		}
	}
	
	public function getwishlistproduct()
	{
		if($this->input->is_ajax_request())
		{
			$siteLangArray = $this->session->userdata('site_lang');
			$Langid=$siteLangArray['laguage_id'];
			if($Langid==""){ $Langid=1;}
			$wishlistId = $this->input->post("wishlistid");
			$shortVal = $this->input->post("shortVal");
			$my_wishlist=$this->Wishlist_model->getwishlistByUrl($wishlistId);
			$id = $my_wishlist[0]->id;
			$my_wishlist_product = $this->Wishlist_model->getwishlistProduct($id,$Langid,$shortVal);
			echo $result = $this->listready($my_wishlist_product, $id);

		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
	
	protected function listready($my_wishlist_product, $id)
	{
		$list = "";
		 $usersarr=$this->session->userdata('log_in');
		  $usersid=$usersarr['user_id'];
		  if(!empty($my_wishlist_product)){
		   //Foreach Start
		   foreach($my_wishlist_product as $val){
			 /*Image Path Config*/
			$photopath = $this->config->item('image_display_path');
			$photopath_absolute = $this->config->item('image_path');
			$thumb_size= $this->config->item('thumb_size');
			$medium_thumb_size= $this->config->item('medium_thumb_size');
			
			$photopath_is_check =$photopath_absolute."product/".$val->product_id;
			$thumpath = $photopath."product/".$val->product_id.$thumb_size;
			/*Product Image Checking*/
			if(is_file($this->config->item('image_path').'/product/'.$val->product_id.$thumb_size.$val->product_image))
			{ $imgpurl=$this->config->item('image_display_path').'product/'.$val->product_id.$thumb_size.$val->product_image ; }
			else
			{ $imgpurl= $this->config->item('image_display_path').'product/no_product.jpg'; }
			/*Contribution Check*/
			 if( !empty ($val->contribution))
			 {
				foreach ($val->contribution as $contribution)
				{
					$getId =  $contribution->contributor_id;
					$totalamount =  isset($contribution->total_amount) ? $contribution->total_amount : '';
					$totalreceived = (isset($contribution->total)) ? $contribution->total : '' ;
					if($totalreceived !='')
					{ $persentageVal=(($totalreceived/$totalamount)*100); }
				}
			}
			else
			{ $getId =  ''; $totalamount =  ''; $totalreceived = '';  $persentageVal= ''; }
			
		 $list .= 
'<div class="row">
  <div class="allrows"> 
    
    <!--list detail item 2-->
    <div class="product-layout product-list">
      <div class="product-thumb">
        <div class="image"><a href="'.$val->slug.'"><img src="'.$imgpurl.'" width="228" height="228" class="img-responsive"></div>
        <div>
          <div class="caption">
            <div class="row">
            <!--List Left Pane-->
              <div class="col-md-6">
                <ul class="list-inline">
                  <li>'.$this->lang->line("desired").' <span class="badge badgeblue">2</span></li>
                  <li>'.$this->lang->line("received").' <span class="badge badgeblue">0</span></li>';
				  if ( isset($getId) &&  $getId !=''){
         $list .='<li><span class="label label-danger">'.$this->lang->line("contribution_text").'</span></li>';
				  }
         $list .='</ul>
                <h4><a href="'.$val->slug.'">'.$val->product_name.'</a></h4>
                <p> '.$val->short_description.'</p>
                <p class="price"> 
					<span class="price-new">'.$this->config->item('currency').' '.$val->sale_price.'</span>
					<span class="price-old">'.$this->config->item('currency').' 1,500,000</span>
				</p>
                <!--progress bar-->.';
		if ( isset($getId) &&  $getId !=''){
        $list .='<div class="row">
                  <div class="progressbox"> '.$this->lang->line("amount_contribution").' :
                    <div class="progress">
                      <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="'.$totalreceived.'" aria-valuemin="0" aria-valuemax="'.$totalamount.'" style="min-width: 2em; width: '.$persentageVal.'%"> '.$this->config->item('currency').' '.$totalreceived.' </div>
                    </div>
                    <!--Contibutors--> 
                    <a class="btn btn-danger" role="button" data-toggle="collapse" href="#collapseExample'.$val->product_id.'" aria-expanded="false" aria-controls="collapseExample"> '.$this->lang->line("view_contributors").' <i class="fa fa-eye" aria-hidden="true"></i> </a>
                    <p></p>
                    <div class="collapse" id="collapseExample'.$val->product_id.'">
                      <table class="table table-condensed contributorlist">
					  <thead>
                        <tr>
                          <th>'.$this->lang->line("name").'</th>
                          <th>'.$this->lang->line("amount").'</th>
                          <th>'.$this->lang->line("table_date").'</th>
                        </tr>
					  </thead>
					  <tbody>';
		if( !empty ($val->user)){
	  foreach($val->user as $users){
		  $name = $users->fname.' '.$users->lname;
		  $amount = $users->total_amt_received;
		  $cDate = new DateTime($users->date_added);
		  $contributionDate=$cDate->format('dS F o');		  
              $list .=' <tr>
                          <td>'.$name.'</td>
                          <td>'.$this->config->item('currency').' '.$amount.'</td>
                          <td>'.$contributionDate.'</td>
                        </tr>';
				  }}
						
			  $list .='</tbody>
                      </table>
                    </div>
                    <!--eof contributors--> 
                  </div>
                </div>';
		}
        $list .= '<!--eof progress bar--> 
              </div>
	
        <!--eof-col-md-6-->
			<!--List Right Pane-->
			
              <div class="col-md-5 pull-right">';
		if($usersid!=$val->uid){
			  if($val->cnbtn_count==0){
      $list .='<div class="listform">
                          <button type="button" class="btn btn-primary button_buy_gift" data-wishlist-id="'.$id.'" data-product-id="'.$val->product_id.'" data-product-name="'.$val->product_name.'" data-product-stock="'.$val->stock_quantity.'"><i class="fa fa-gift" aria-hidden="true"></i> '.$this->lang->line("buy_this_as_gift").'</button>
                        </div>';
			  }
		if ( $totalreceived !='' && ($totalreceived >= $totalamount) ){
           $list .= '<button type="button" class="btn btn-primary"><i class="fa fa-gift" aria-hidden="true"></i> Contribution Fulfilled</button>';
		 }else{
      $list .='<div class="buttoncontribute">';
	  			if($val->gift_count==0){
					$ch = $totalreceived != "" ? $totalreceived : 0;
         $list .='<div class="listform text-success">'.$this->lang->line("contribute").' <em><strong>'.$this->lang->line("any_amount").'</strong></em> '.$this->lang->line("gift_text").'</div>
                  <div class="form-inline">
                    <div class="form-group">
                      <label class="sr-only" for="contributeInputAmount_'.$val->product_id.'">'.$this->lang->line("amount_info").'</label>
                      <div class="input-group">
                        <div class="input-group-addon">'.$this->config->item('currency').'</div>
                        <input type="hidden" name="contrbutetotal"  value="'.$ch .'" />
						<input type="hidden" name="pro_price"  value="'.$val->sale_price.'" />
						<input type="text" class="form-control amnt" id="contributeInputAmount_'.$val->product_id.'" placeholder="'.$this->lang->line("amount").'">
                      </div>
                      <button class="btn btn-info btn-block contributeBtn" data-wishlist-id="'.$id.'" data-product-id="'.$val->product_id.'" data-product-name="'.$val->product_name.'"><i class="fa fa-users" aria-hidden="true"></i> '.$this->lang->line("contribute_with_friends").'</button>
                      <h5 class="msgBox"></h5>
                    </div>
                  </div>';
				}
            $list .=' <a class="btn btn-warning taginputInit" role="button" data-toggle="collapse" href=".inviteEmail_'.$val->product_id.'" aria-expanded="false" aria-controls="inviteEmail"> <i class="fa fa-envelope" aria-hidden="true"></i> '.$this->lang->line("email_friends_contribute").' </a>
                  <div class="collapse inviteEmail_'.$val->product_id.'">
                    <div class="alert alert-warning" role="alert">
						<select multiple data-role="tagsinput" id="emailall" name="emailall"></select>
                      <br>
                      <input type="hidden" name="pro_id"  value="'.$val->product_id.'" />
                      <button type="submit" class="btn btn-info Invitecls" id="inviteCls'.$val->product_id.'">'.$this->lang->line("invite").'</button>
                    </div>
                  </div>
                  <!--eof collapse--></div>';
		 		}
      $list .='</div>';
				}
      $list .='</div>
          </div>
        </div>
      </div>
    </div>
    <!--list detail item2--> 
  </div>
</div>';
		 } 
		  //Foreach End
		 
		  }
		return $list;
	}	

}
?>
