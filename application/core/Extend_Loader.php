<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";?>
<?php class Extend_Loader extends MX_Loader {
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('templates/header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('templates/footer', $vars, $return);
        return $content;
    else:
        $this->view('templates/header', $vars);
        $this->view($template_name, $vars);
        $this->view('templates/footer', $vars);
    endif;
    }
 public function ftemplate($template_name, $vars = array(), $return = FALSE)
    {
		$facebook = $this->facebook->getLoginUrl(array('redirect_uri' => site_url('user/login'),'scope' => array("email") ));// permissions here
		$vars["login_url"] = $facebook;
        if($return):
			$content  = $this->view('common/header', $vars, $return);
			$content .= $this->view($template_name, $vars, $return);
			$content .= $this->view('common/footer', $vars, $return);
			return $content;
		else:
			$this->view('common/header', $vars);
			$this->view($template_name, $vars);
			$this->view('common/footer', $vars);
		endif;
    }
}