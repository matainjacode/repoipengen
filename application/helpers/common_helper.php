<?php
if (!defined('BASEPATH')) exit ('No direct script access allowed');

if (!function_exists('getWishlistUrl')) {
	/*Function Declare*/
		function getWishlistUrl($url)
		{ 
			$CI = get_instance();
			$CI->load->helper('url');
			return base_url().'~'.$url;
		}
}

if(!function_exists("slugify"))
{
	function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		// trim
		$text = trim($text, '-');
		
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		
		// lowercase
		$text = strtolower($text);
		
		if (empty($text)) {
		return 'n-a';
		}
		
		return $text;
	}
}

if (!function_exists('getuserprofileurl')) {
	/*Function Declare*/
		function getuserprofileurl($userName)
		{ 
			$CI = get_instance();
			$CI->load->helper('url');
			return base_url().'u/'.$userName;
		}
}

if (!function_exists('getproducturl')) {
	/*Function Declare*/
		function getproducturl($productId,$productname)
		{ 
			$CI = get_instance();
			$CI->load->helper(array('url', 'common_helper'));
			$url = base_url()."p/".$productId."-".slugify($productname);
			return '<a href="'.$url.'">'.$productname.'</a>';
		}
}