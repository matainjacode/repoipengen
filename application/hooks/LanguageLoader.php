<?php
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
        $siteLang1 = $ci->session->userdata('site_lang');
		$siteLang=$siteLang1['laguage_name'];
		
        if ($siteLang) {
            $ci->lang->load('home',$siteLang);
        } else {
            $ci->lang->load('home','english');
        }
    }
}
?>