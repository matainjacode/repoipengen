<?php
/*
*	All Page Title
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang['cart'] 						= 'Gerobak';
	$lang['checkout'] 					= 'Periksa';
	$lang['payment'] 					= 'Pembayaran';
	$lang['event_remind'] 				= 'Event Reminder';
	$lang['gift'] 						= 'Hadiah';
	$lang['gift_details'] 				= 'Detail hadiah';
	$lang['add_bank_acc_details'] 		= 'Tambahkan Rincian akun Bank';
	$lang['bank_acc_details'] 			= 'Detail rekening bank';
	$lang['card_payment'] 				= 'Gerobak Pembayaran';
	$lang['search'] 					= 'Pencarian';
	$lang['my_surprise_box'] 			= 'Saya kejutan kotak';
	$lang['my_surprise_details'] 		= 'Saya Detail kejutan';
	$lang['question_help'] 				= 'pertanyaan Bantuan';
	$lang['order_track'] 				= 'agar Lacak';
	$lang['transaction_details'] 		= 'Detil transaksi';
	$lang['password_change'] 			= 'Perubahan Sandi';
	$lang['password_reset'] 			= 'Atur Ulang Sandi';
	$lang['details'] 					= 'Detail';
	$lang['create_wishlist'] 			= 'Membuat a Wishlist';
	$lang['wishlist_check'] 			= 'Wishlist Memeriksa';
	$lang['edit_wishlist'] 				= 'mengedit Wishlist';
	$lang['home_tag'] 					= 'Bagi wishlist Anda ke teman - Ipengen';
	$lang['shop'] 						= 'Toko - Ipengen';
	
/*Language file for Fronted*/

	$lang['welcome_message'] 			= 'Welcome to Ipengen';
	$lang['home'] 						= 'Home';
	$lang['men'] 						= 'Men';
	$lang['ladies'] 					= 'Ladies';
	$lang['login-register'] 			= 'Login | Daftar';
	$lang['contact'] 					= 'Contact';
	$lang['send']						= "Kirim";
	$lang['logout']						= "LOGOUT";
	$lang['profile']					= "PROFILE";
/*
*	Dashboard Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/
	/*Header Panel*/
	$lang["browse_menu"] 				= "Cari Kado";
	$lang["search_placeholder"] 		= "Cari Wishlist & Kado";
	$lang["already_reagister"] 			= "SUDAH DAFTAR ?";
	$lang['login'] 						= 'Login';
	$lang['register'] 					= 'Daftar';
	$lang['f_password']					= "Lupa password";
	$lang['auth_error']					= "Username / Password kamu salah";
	$lang['f_password_msg']				= "Masukan alamat email terdaftar dan mohon check emailmu.";
	$lang['register_with_email'] 		= 'DAFTAR DENGAN EMAIL';
	$lang['email'] 						= "Email"; 
	$lang['password']					= "Password"; 
	$lang['re_password']				= "Reset Password";
	$lang['retrive_password']			= "Retrieve Password";
	$lang['f_name']						= "First Name";
	$lang['l_name']						= "Last Name";
	$lang['enter_your_mail']			= "Masukan Email kamu";
	$lang['agrement']					= "SAYA INGIN";
	$lang['agrement_part1']				= "Dengan mendaftar, Saya setuju ";
	$lang['agrement_part2']				= "website iPengen.";
	$lang['create_wishlist'] 			= 'Buat Wishlist';
	$lang['buy_gift'] 					= 'Beli Kado';
	$lang['empty_cart_msg'] 			= 'Shopping Cart Kosong!';
	$lang["eyeview"] 					= "Teaser";
	$lang["signup_text"] 				= "
											<h4 class='text-white'>Daftar Sekarang. </h4>
											<h4 class='text-white'>Step inside the world of giving and receiving. </h4>
											";
	/*Body Panel*/
	$lang["category_info"] 				= "
											<h2>Gratis dan Mudah ! </h2>
											<h4>Tinggal pilih event istimewa kamu untuk membuat wishlist</h4>
											<h2>Atau,</h2>
											<h4>Kamu juga bisa membeli untuk dirimu sendiri...</h4>
											";
	$lang["gift_section"] 				= "Gift Ideas";
	$lang["wishlist_testimonial"] 		= "Kasih Kado Apa Yah ?...";
	/*Footer Panel*/
	$lang["wishlist"] 					= "Wishlist";
	$lang["askQuestion"] 				= "Pertanyaan ?";
	$lang["help"] 						= "Bantuan";
	$lang["track_orders"] 				= "Track Order";
	$lang["terms_conditions"] 			= "Terms and Conditions";
	$lang["company"] 					= "Our Company";
	$lang["about_us"] 					= "About Us";
	$lang["affiliate"] 					= "Affiliate ";
	$lang["careers"] 					= "Careers";
	$lang["privacy_security"] 			= "Privacy & Security";
	$lang["contact_us"] 				= "Contact Us";
	$lang["goto_contact"] 				= "Contact Us";
	$lang["get_the_news"] 				= "Update saya";
	$lang["subscribe"] 					= "Subscribe!";
	$lang["stay_in_touch"] 				= "Stay in touch";
	$lang["policy_privacy"] 			= "Privacy Policy";
	$lang["terms_of_use"] 				= "Terms of Use";
	$lang["feedback"] 					= "Feedback";
/*
*	Dashboard LeftSide Bar Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/
	$lang["greeting_text"] 					= "Hello";
	$lang["wallet"] 						= "Dompet";
	$lang['dashboard']						="Dashboard";
	$lang['my_wishlist']					="Wishlist saya";
	$lang['event_reminder']					="Event Reminder";
	$lang['surprise_box']					="Surprise Box";
	$lang['new']							="New";
	$lang['cash_gift']						="Angpao";
	$lang['cash_gift_receive']				="Angpao Terkumpul";
	$lang['bank_account_details']			="Detil Bank Account";
	$lang['recipient_list']					="Daftar Penerima";
	$lang['transaction']					="Transaksi";
	$lang['order']							="Order";
	//$lang['contribution']					="Patungan Saya"; delete this from Dashboard
	$lang['profile_text']					="My Profile";
	$lang['dob']							="Tanggal Lahir";
	$lang['view']							="Lihat";
	$lang['edit']							="Edit";
	$lang['pwd_change']						="Ganti Password";
	$lang['sign_out']						="Logout";
/*
*	Dashboard Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["greeting_info"] 					= "Apa yang ingin kamu lakukan ?";
	$lang["wishlist_find"] 					= "CARI WISHLIST";
	$lang["browse_catalog"]					= "CARI KADO";
	$lang["notifications"]					= "Notifikasi";
	$lang["empty_notifications"]			= "TIDAK ADA NOTIFIKASI";
	$lang["your_wishlist"]					= "Wishlist-Ku";
	$lang["wishlist_title"]					= "Nama Wishlist";
	$lang["wishlist_action"]				= "Action";
	$lang["created"]						= "Dibuat";
	$lang["end"]							= "Berakhir";
	$lang["privacy"]						= "Privacy";
	$lang["change_privacy"]					= "Change privacy";
	$lang["public"]							= "Publik";
	$lang["private"]						= "Private";
	$lang["warning_notify"]					= "max. 15 chars";
	$lang["save_privacy_btn"]				= "Save Privacy";
	$lang["friend_transfer"]				= "Transfer ke teman";
	$lang["transfer_email"]					= "Masukan email termanmu";
	$lang["transfer"]						= "Transfer";
	$lang["empty_wishlist"]					= "<strong>Maaf ! </strong>Tidak ada wishlist";
/*
*	Create Wishlist Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["your_event"] 					= "Event kamu";
	$lang["personalize"] 					= "Personalize";
	$lang["ready"] 							= "Ready !";
	$lang["w_name_placeholder"] 			= "Nama Wishlist";
	$lang["w_event_cat"] 					= "Kategori Event";
	$lang["w_date"] 						= "TANGGAL WISHLIST MULAI";
	$lang["w_end_date"] 					= "TANGGAL WISHLIST SELESAI";
	$lang["w_warning_info"] 				= "Maksimum dalam 60 hari dari tanggal wishlist dimulai";
	$lang["w_p_url"] 						= "Wishlist Publik URL";
	$lang["w_receive_cash_gift"] 			= "Apakah kamu ingin menerima Angpao?";
	$lang["w_receive_info_1"] 				= "Angpao akan ditransfer ke bank kamu";
	$lang["w_receive_info_2"] 				= "Kado patungan yang tidak mencapai total harga kado akan dikonversi menjadi Angpao.";
	$lang["w_gift_sent"] 					= "Ke mana kado kamu akan dikirim ?";
	$lang["w__prev_event_add"] 				= "Setting alamat dari event sebelumnya";
	$lang["w_event_choose"] 				= "Pilih event";
	$lang["w_event_add"] 					= "Address";
	$lang["w_event_add_2"] 					= "Address Line 2";
	$lang["w_event_postcode"] 				= "Postcode";
	$lang["w_event_district"] 				= "Kecamatan";
	//$lang["w_event_city"] 					= "--- City ---";
	$lang["w_event_contact"] 				= "Mobile No";
	$lang["w_event_privacy"] 				= "Izinkan wishlist saya";
	$lang["w_event_add_show"] 				= "Perlihatkan alamat saya (atau alamat kamu akan disembunyikan)";
	$lang["w_event_visitor_pwd"] 			= "Gunakan password untuk tamu ";
	$lang["w_event_visitor_pwd_warning"]	= "Password max 15 chars";
	$lang["w_event_save_btn"] 				= "Save & continue";
	$lang["w_event_update_btn"] 			= "Update & continue";
	$lang["w_personalize"] 					= "PERSONALIZE";
	$lang["w_img_upload"] 					= "Upload Photo";
	$lang["w_note"] 						= "PESAN UNTUK TAMU (optional)";
	$lang["w_rsvp_info"] 					= "RSVP info, Dress Code & etc.";
	$lang["w_e_date"] 						= "TANGGAL EVENT";
	$lang["w_e_add"] 						= "ALAMAT EVENT (optional)";
	$lang["previous"] 						= "Previous";
	$lang["skip"] 							= "Skip, Do this later";
	$lang["w_e_save_finish"] 				= "Save & Finish";
	$lang["w_almost_ready"] 				= "ALMOST READY";
	$lang["w_add_item"] 					= "Mari memilih kado untuk wishlist kamu";
	$lang["w_my_wishlist"] 					= "Lihat My Wishlist";
	$lang["w_browse_gift"] 					= "CARI KADO";
/*
*	Event Reminder Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["event_reminder"] 					= "EVENT REMINDER";
	$lang["event_add"] 							= "Tambah Event";
	$lang["event_add_name"] 					= "Nama Event";
	$lang["event_add_date"] 					= "Tanggal Event";
	$lang["event_repeat_1"] 					= "Repeat";
	$lang["event_repeat_2"] 					= "None";
	$lang["event_repeat_3"] 					= "Setiap Minggu";
	$lang["event_repeat_4"] 					= "Setiap 2 Minggu";
	$lang["event_repeat_5"] 					= "Setiap Bulan";
	$lang["event_repeat_6"] 					= "Setiap Tahun";
	$lang["event_remind_1"] 					= "Ingatkan Saya";
	$lang["event_remind_2"] 					= "3 hari sebelumnya";
	$lang["event_remind_3"] 					= "1 minggu sebelumnya";
	$lang["event_remind_4"] 					= "2 minggu sebelumnya";
	$lang["event_remind_5"] 					= "Sebulan sebelumnya";
	$lang["event_save_btn"] 					= "Save Event";
	$lang["event_edit"] 						= "Edit Event";
	$lang["event_update_btn"] 					= "Update Event";
	$lang["empty_eventremainder"] 				= "Tidak ada event";
/*
*	Serprise Box Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["serprise_title"] 					= "SURPRISE BOX-Ku";
	$lang["serprise_info"] 						= "Nikmati kado yang kamu terima. Jika kamu memiliki pertanyaan, silahkan hubungi ";
	$lang["table_order"] 						= "Order";
	$lang["table_date"] 						= "Date";
	$lang["table_total"] 						= "Total";
	$lang["table_status"] 						= "Status";
	$lang["table_action"] 						= "Action";
	$lang["empty_supprisebox"] 					= "Maaf, tidak ada surprise !";
	
/*
*	Cash Gift List Box Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["cash_gift_title"] 					= "Angpao-Ku";
	$lang["empty_wallet"] 						= "Tidak ada Angpao";
	$lang["cash_gift_info"] 					= "Angpao akan ditransfer ke bank account kamu setiap akhir bulan. Jika kamu memiliki pertanyaan, silahkan hubungi ";
	//$lang["cash_gift_from_my"] 					= "Data From my";
	$lang["cash_gift_from"] 					= "dari";
	$lang["cash_gift_bank_title"] 				= "Angpao-Ku - DETIL BANK ACCOUNT";
	$lang["cash_gift_bank_info_1"] 				= "Mohon masukan detil bank account kamu. Angpao akan ditransfer ke bank account kamu setiap akhir bulan. Jika kamu memiliki pertanyaan, silahkan hubungi";
	$lang["cash_gift_bank_AC"] 					= "Nama Pemilik Account";
	$lang["cash_gift_bank_AC_no"] 				= "No Account";
	$lang["cash_gift_bank_AC_name"] 			= "Bank";
	$lang["cash_gift_bank_AC_detail"] 			= "Detil Bank";
	$lang["cash_gift_bank_info_2"] 				= "Alamat Bank, KCP atau info( jika ada )";
	$lang["cash_gift_save_btn"] 				= "Save and Finish";
/*
*	My Recipient Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["search_wishlist_placeholder"] 		= "Cari Wishlist";
	$lang["go_btn"] 							= "Go!";
	$lang["my_recipients"] 						= "PENERIMA KADO";
	$lang["delete"] 							= "Delete";
	$lang["no_recipients"] 						= "MAAF ! Tidak ada penerima kado.";
/*
*	Transaction list Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["transaction_info"] 					= "Kado dibeli untuk teman. Jika kamu memiliki pertanyaan, silahkan hubungi";
	//$lang["payment_info"] 						= "Data From BUY THIS FOR in product detail";
/*
*	My Profile Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["select_image"] 						= "Select image";
	$lang["user_name"] 							= "User Name";
	$lang["social_media"] 						= "Social Media";
	$lang["facebook"] 							= "Facebook";
	$lang["instagram"] 							= "Instagram";
	$lang["twitter"] 							= "Twitter";
	$lang["shipping_address"] 					= "Alamat Pengiriman";
	$lang["kecamatan"] 							= "Kecamatan";
	$lang["save_profile"] 						= "Save Profile";
	$lang["picture_crop"] 						= "Crop Profile Picture";
	$lang["crop"] 								= "Crop";
	$lang["user_name_change_text"] 				= "Ganti User Name";
	$lang["update"] 							= "Update";
	$lang["close"] 								= "Close";
/*
*	Change Password Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["current_pwd"] 						= "Password Sekarang";	
	$lang["new_pwd"] 							= "Password Baru";	
	$lang["confirm_new_pwd"] 					= "Confirm Password Baru";	
	$lang["save_pwd"] 							= "Save Password";	
	$lang["empty_order"] 					   = "Anda tidak memiliki perintah)";
/*
*	Wishlist View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["add_recipient_list"] 			= "+ Daftar Penerima Kado";
	$lang["remove_recipient_list"] 			= "Hapus dari Penerima Kado";
	$lang["wishlist_Block"] 				= "Daftar keinginan yang Diblokir";
	$lang["event_info"] 					= "INFO EVENT";
	$lang["time"] 							= "Jam";
	$lang["special_note"] 					= "SPECIAL NOTE";
	$lang["event_address"] 					= "ALAMAT EVENT";
	$lang["shipping_add"] 					= "Alamat Pengiriman";
	$lang["not_specified"] 					= "tidak Ditentukan";
	$lang["send_cash_gift"] 				= "Kirim Hadiah Kas";
	$lang["send_cash_gift_instead"] 		= "Apakah Anda ingin mengirimkan hadiah uang tunai bukan  ?";
	$lang["amount_info"] 					= "Jumlah (dalam Rupiah)";
	$lang["amount"] 						= "Jumlah";
	$lang["desired"] 						= "Diinginkan";
	$lang["received"] 						= "Menerima";
	$lang["contribution_text"] 				= "KONTRIBUSI";
	$lang["amount_contribution"] 			= "jumlah kontribusi";
	$lang["view_contributors"] 				= "Lihat Kontributor";
	$lang["name"] 							= "Nama";
	$lang["buy_this_as_gift"] 				= "Membeli ini sebagai hadiah";
	$lang["contribution_total_amount"] 		= "Jumlah kontribusi mencapai jumlah total";
	$lang["contribute"] 					= "Menyumbang";
	$lang["any_amount"] 					= "berapa saja";
	$lang["gift_text"] 						= "untuk membeli hadiah ini dengan teman-teman";
	$lang["contribute_with_friends"] 		= "Berkontribusi dengan teman-teman";
	$lang["email_friends_contribute"] 		= "teman Email untuk berkontribusi";
	$lang["invite"] 						= "Undang";
	$lang["success"] 						= "Keberhasilan";
	$lang["view_cart"] 						= "Lihat ke troli";
	$lang["checkout"] 						= "Periksa";
/*
*	Product Search View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["categories"] 			= "KATEGORI";	
	$lang["brands"] 				= "MEREK";	
	$lang["clear"] 					= "Bersih";
	$lang["apply"] 					= "Menerapkan";
	$lang["price_range"] 			= "Kisaran harga";
	$lang["show"] 					= "Menunjukkan";
	$lang["product_show_1"] 		= "Menampilkan";
	$lang["product_show_2"] 		= "dari";
	$lang["product_show_3"] 		= "produk";
	$lang["all"] 					= "Semua";
	$lang["sort_by"] 				= "Sortir dengan";
	$lang["Price_Low_to_High"] 		= "Harga Rendah ke Tinggi";
	$lang["Price_High_to_Low"] 		= "Harga Tinggi ke Rendah";
	$lang["Name_A_Z"] 				= "Nama A - Z";
	$lang["Name_Z_A"] 				= "Nama Z - A";
	$lang["sales_first"] 			= "penjualan pertama";	
/*
*	Product View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["sale"] 					= "PENJUALAN";		
	$lang["share_it"] 				= "Bagikan ini";	
	$lang["goDescription"] 			= "Gulir ke detail produk, bahan &amp; peduli dan ukuran";	
	$lang["add_wishlist"] 			= "Tambahkan ke Daftar Keinginan";	
	$lang["add_to_your_wishlist"]	= "Tambahkan ke Daftar Keinginan ANDA";	
	$lang["select_Your_Wishlist"]	= "Pilih Daftar Keinginan Anda";
	$lang["or_buy"]					= "ATAU, MEMBELI INI UNTUK";
	$lang["quantity"] 				= "Kuantitas";
	$lang["qty"] 					= "Kuantitas";
	$lang["comming_Soon"] 			= "Segera akan datang";
	$lang["product_like"] 			= "Anda mungkin juga ingin produk ini";
	$lang["no_product"] 			= "Tidak ada produk";
	$lang["recent_product"] 		= "Produk melihat baru-baru ini";
	$lang["new_wishlist_item"] 		= "Sebuah item baru telah ditambahkan ke daftar keinginan Anda.";
	$lang["view_Wishlist"] 			= "Lihat Daftar Keinginan";
	$lang["continue_shop"] 			= "Lanjutkan Belanja";
/*
*	Private Wishlist View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["private_wishlist"] 		= "daftar keinginan ini pribadi";	
	$lang["wishlist_password_info"]	= "Untuk melihat daftar harapan, masukkan password yang disediakan oleh pemilik daftar keinginan";
	$lang["proceed"] 				= "Memproses";
	$lang["password_resquest"] 		= "Silakan meminta password dari pemilik daftar keinginan";
	$lang["resquest"] 				= "Permintaan";
/*
*	Public Profile View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - Bahasa
*/	
	$lang["member_since"] 		= "Anggota Sejak";		
	$lang["total_wishlist"] 	= "Total Daftar Keinginan dibuat";
	$lang["total_gift"] 		= "Total Hadiah Diterima";
	$lang["share_btn"] 			= "Bagikan ini";
	$lang["send_msg"] 			= "Mengirim pesan";
	$lang["send_msg_info"] 		= "Semua pesan yang direkam dan spam tidak akan ditolerir. Anda harus terdaftar untuk mengirim pesan";
	$lang["message"] 			= "Pesan";
	$lang["recent_gift"] 		= "Hadiah terbaru Diterima";
	$lang["more_gift_wishlist"] = "Tambahkan lebih banyak hadiah ke daftar keinginan";
	$lang["no_data_found"] 		= "Tidak ada data Ditemukan";	
	
	
/*
*	Remaining View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["shopping_cart"] 			= "Kereta Belanja";	
	$lang["product"] 				= "Produk";
	$lang["price"] 					= "Harga";
	$lang["buygift"] 				= "Beli Hadiah";
	$lang["cashgift"] 				= "Hadiah uang tunai";
	$lang["discount"] 				= "Diskon";
	$lang["coupon_code"] 			= "Kode Kupon";
	$lang["update_basket"] 			= "Perbarui Keranjang";
	$lang["proceed_to_checkout"]	= "Lanjutkan ke pembayaran";
	$lang["billing_address"]		= "alamat tagihan";
	$lang["ask_ship_address"]		= "Kapal ke alamat yang berbeda ?";
	$lang["ask_ship_address_1"]		= "Hanya berlaku jika Anda membeli untuk diri sendiri atau teman yang tidak memiliki daftar harapan";
	$lang["ask_ship_same"]			= "Alamat Pengiriman sama seperti Alamat Penagihan.";
	$lang["delivery"]				= "Pengiriman";
	$lang["free_shipping"]			= "Bebas biaya kirim";
	$lang["days"]					= "hari-hari";
	$lang["one_day_service"]		= "Satu Layanan Sehari";
	$lang["same_day_service"]		= "Hari yang sama jika pesanan ditempatkan sebelum 3 atau Dapatkan besok";
	$lang["view_basket"]			= "Lihat keranjang";
	$lang["continue_to_payment_method"]	= "Terus Cara Pembayaran";
	$lang["your_order"]				= "Pesanan Anda";
	$lang["for_wishlist"]			= "untuk Daftar Keinginan";
	$lang["form_wishlist"]			= "dari daftar keinginan";
	$lang["if_transaction_fee"]		= "jika biaya transaksi adalah";
	$lang["shipping_charge"]		= "Biaya pengiriman";
	$lang["payment_method"]			= "Metode pembayaran";
	$lang["master"]					= "Menguasai";
	$lang["visa"]					= "Visa";
	$lang["agree"]					= "Saya setuju dengan";
	$lang["confirm_my_order"]		= "Konfirmasi Pesanan saya";
	$lang["shipping_method"]		= "Metode pengiriman";
	$lang["thank_you"]				= "Terima kasih";
	$lang["order_number_info"]		= "Nomor pesanan Anda";
	$lang["email_confirm_text"]		= "Anda akan menerima email konfirmasi segera";
	$lang["share_glory_gifting"]	= "Bagi The Glory of Gifting";
	$lang["share"]					= "Bagikan";
	$lang["customer"]				= "Pelanggan";
	$lang["u_p_invalid"]			= "Nama pengguna Atau Sandi tidak valid";
	$lang["login"]					= "masuk";
	$lang["not_register"]			= "Belum terdaftar?";
	$lang["register_now"]			= "Daftar sekarang";
	$lang["register_now_info"]		= "Sangat mudah dan dilakukan dalam 1 & nbsp; menit dan memberikan Anda akses ke diskon khusus dan banyak lagi!";
	$lang["email_sent"]				= "Email terkirim! Jika alamat email yang Anda masukkan terdaftar di Freelancer, Anda akan menerima email berisi petunjuk tentang cara mengatur password baru.";
	$lang["unique_email"]			= "Silahkan Masukan Email id Unik";
	$lang["subtotal"]				= "SUBTOTAL";
	$lang["pwd_reset_info"]			= "Anda meminta reset password untuk memulihkan akses ke akun Anda.";
	$lang["dear"]					= "sayang";
	$lang["pwd_reset_info_1"]		= "Untuk me-reset password, klik ini";
	$lang["link"]					= "link";
	$lang["or"]						= "Atau";
	$lang["copy"]					= "Salinan";
	$lang["info"]					= "Info";
	$lang["pwd_lenth_info"]			= "8 password terakhir yang digunakan tidak diperbolehkan untuk mendapatkan password baru.";
	$lang["note"]					= "Catatan";
	$lang["pwd_reset_info_2"]		= "Jika Anda tidak meminta reset password untuk account Anda, orang lain mungkin telah memasukkan alamat email Anda. Jika demikian, mengabaikan pesan ini.";
	$lang["account"]				= "Rekening";
	$lang["bank_ac_no"]				= "Nomor rekening bank";
	$lang["bank"]					= "Bank";
	$lang["you_received"]			= "Anda telah menerima";
	$lang["on_for"]					= "selama";
	$lang["and_is_currently"]		= "dan saat ini";
	$lang["being_prepared"]			= "Sedang disiapkan";
	$lang["sender"]					= "Pengirim";
	$lang["hidden_address"]			= "(alamat tersembunyi)";
	$lang["success_email_sent"]		= '"Terima kasih" mail telah berhasil dikirim.';
	$lang["gift_bought_by"]			= "Hadiah dibeli oleh / dari";
	$lang["say_thank_you"]			= "Katakan terima kasih";
	$lang["sub_total"]				= "Sub Total";
	$lang["code"]					= "Kode";
	$lang["shipping"]				= "pengiriman";
	$lang["information_updated."]	= "Informasi yang diperbarui.";
	$lang["choose_new_password."]	= "Pilih Password Baru Anda.";
	$lang["url"]					= "Url";
	$lang["sent_password"]			= "sandi sudah ditetapkan";
	$lang["edit_continue"]			= "Mengedit dan terus";
	$lang["search_list"]			= "Daftar Pencarian";
	$lang["not_found_pls_try"]		= "tidak ditemukan..... coba lagi.";
	$lang["filter_by"]				= "Filter By";
	$lang["latest"]					= "Terbaru";	
	
	
	
	
	
?>