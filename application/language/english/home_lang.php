<?php
/*
*	All Page Title
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang['cart'] 						= 'Cart';
	$lang['checkout'] 					= 'Checkout';
	$lang['payment'] 					= 'Payment';
	$lang['success'] 					= 'Success';
	$lang['event_remind'] 				= 'Event Reminder';
	$lang['gift'] 						= 'Gift';
	$lang['gift_details'] 				= 'Gift Details';
	$lang['add_bank_acc_details'] 		= 'Add Bank account Details';
	$lang['bank_acc_details'] 			= 'Bank account Details';
	$lang['card_payment'] 				= 'Card Payment';
	$lang['success'] 					= 'Success';
	$lang['search'] 					= 'Search';
	$lang['my_surprise_box'] 			= 'My Surprise Box';
	$lang['my_surprise_details'] 		= 'My Surprise Details';
	$lang['question_help'] 				= 'Question Help';
	$lang['order_track'] 				= 'Order Track';
	$lang['transaction_details'] 		= 'Transaction Details';
	$lang['password_change'] 			= 'Password Change';
	$lang['password_reset'] 			= 'Password Reset';
	$lang['details'] 					= 'Details';
	$lang['create_wishlist'] 			= 'Create a Wishlist';
	$lang['wishlist_check'] 			= 'Wishlist Check';
	$lang['edit_wishlist'] 				= 'Edit Wishlist';
	$lang['home_tag'] 					= 'Share Your Wishlist to Friends - Ipengen';
	$lang['shop'] 						= 'Shop - Ipengen';
/*Language file for Fronted*/

	$lang['welcome_message'] 			= 'Welcome to Ipengen';
	$lang['home'] 						= 'Home';
	$lang['men'] 						= 'Men';
	$lang['ladies'] 					= 'Ladies';
	$lang['login-register'] 			= 'Login | Register';
	$lang['contact'] 					= 'Contact';
	$lang['send']						= "Send";
	$lang['logout']						= "LOGOUT";
	$lang['profile']					= "PROFILE";
/*
*	Index Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	/*Header Panel*/
	$lang["browse_menu"] 				= "Browse Gift";
	$lang["search_placeholder"] 		= "Find Wishlist & Product";
	$lang["already_reagister"] 			= "ALREADY REGISTERED ?";
	$lang['login'] 						= 'Login';
	$lang['register'] 					= 'Register';
	$lang['f_password']					= "Forgot password";
	$lang['auth_error']					= "Username Or Password is invaild";
	$lang['f_password_msg']				= "Enter your email address and emailed to you.";
	$lang['register_with_email'] 		= 'REGISTER WITH EMAIL';
	$lang['email'] 						= "Email"; 
	$lang['password']					= "Password"; 
	$lang['re_password']				= "Reset Password";
	$lang['retrive_password']			= "Retrieve Password";
	$lang['f_name']						= "First Name";
	$lang['l_name']						= "Last Name";
	$lang['enter_your_mail']			= "Enter Your Email";
	$lang['agrement']					= "I WANT TO";
	$lang['agrement_part1']				= "By joining, you agree to the ";
	$lang['agrement_part2']				= "of our website.";
	$lang['create_wishlist'] 			= 'Create Wishlist';
	$lang['buy_gift'] 					= 'Buy Gifts';
	$lang['empty_cart_msg'] 			= 'Shopping Cart is empty!';
	$lang["eyeview"] 					= "Teaser";
	$lang["signup_text"] 				= "
											<h4 class='text-white'>Sign Up Now. </h4>
											<h4 class='text-white'>Step inside the world of giving and receiving. </h4>
											";
	/*Body Panel*/
	$lang["category_info"] 				= "
											<h2>It's Free and Easy ! </h2>
											<h4> Just choose your occassions to create your wishlist</h4>
											<h2>Or,</h2>
											<h4> You can also purchase any products for yourself...</h4>
											";
	$lang["gift_section"] 				= "GIFT IDEAS";
	$lang["wishlist_testimonial"] 		= "WHAT TO GIVE...";
	/*Footer Panel*/
	$lang["wishlist"] 					= "Wishlist";
	$lang["askQuestion"] 				= "Questions ?";
	$lang["help"] 						= "Help";
	$lang["track_orders"] 				= "Track Orders";
	$lang["terms_conditions"] 			= "Terms and Conditions";
	$lang["company"] 					= "Our Company";
	$lang["about_us"] 					= "About Us";
	$lang["affiliate"] 					= "Affiliate ";
	$lang["careers"] 					= "Careers";
	$lang["privacy_security"] 			= "Privacy & Security";
	$lang["contact_us"] 				= "Contact Us";
	$lang["goto_contact"] 				= "Go to contact page";
	$lang["get_the_news"] 				= "Get the news";
	$lang["subscribe"] 					= "Subscribe!";
	$lang["stay_in_touch"] 				= "Stay in touch";
	$lang["policy_privacy"] 			= "Privacy Policy";
	$lang["terms_of_use"] 				= "Terms of Use";
	$lang["feedback"] 					= "Feedback";
/*
*	Dashboard LeftSide Bar Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/
	$lang["greeting_text"] 					= "Hello";
	$lang["wallet"] 						= "Wallet";
	$lang['dashboard']						="Dashboard";
	$lang['my_wishlist']					="My Wishlist";
	$lang['event_reminder']					="Event Reminder";
	$lang['surprise_box']					="My Surprise Box";
	$lang['new']							="New";
	$lang['cash_gift']						="My Cash Gift";
	$lang['cash_gift_receive']				="Gift Cash Received";
	$lang['bank_account_details']			="Bank Account Details";
	$lang['recipient_list']					="My Recipient List";
	$lang['transaction']					="Transaction";
	$lang['order']							="Orders";
	$lang['contribution']					="My Contributions";
	$lang['profile_text']					="My Profile";
	$lang['dob']							="Date Of Birth";
	$lang['view']							="View";
	$lang['edit']							="Edit";
	$lang['pwd_change']						="Change Password";
	$lang['sign_out']						="Sign Out";
/*
*	Dashboard Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["greeting_info"] 					= "What Would You Like To Do ?";
	$lang["wishlist_find"] 					= "FIND WISHLIST";
	$lang["browse_catalog"]					= "BROWSE CATALOG";
	$lang["notifications"]					= "Notifications";
	$lang["empty_notifications"]			= "No notification";
	$lang["your_wishlist"]					= "Your Wishlist";
	$lang["wishlist_title"]					= "Wishlist Name";
	$lang["wishlist_action"]				= "Action";
	$lang["created"]						= "Created";
	$lang["end"]							= "End";
	$lang["privacy"]						= "Privacy";
	$lang["change_privacy"]					= "Change privacy";
	$lang["public"]							= "Public";
	$lang["private"]						= "Private";
	$lang["warning_notify"]					= "max. 15 chars";
	$lang["save_privacy_btn"]				= "Save Privacy";
	$lang["friend_transfer"]				= "Transfer to a friend";
	$lang["transfer_email"]					= "Enter valid Email Address";
	$lang["transfer"]						= "Transfer";
	$lang["empty_wishlist"]					= "<strong>Sorry! </strong> Your Wishlist is empty";
/*
*	Create Wishlist Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["your_event"] 					= "Your Event";
	$lang["personalize"] 					= "Personalize";
	$lang["ready"] 							= "Ready !";
	$lang["w_name_placeholder"] 			= "Your Wishlist Name";
	$lang["w_event_cat"] 					= "Event Category";
	$lang["w_date"] 						= "WISHLIST DATE";
	$lang["w_end_date"] 					= "WISHLIST END DATE";
	$lang["w_warning_info"] 				= "Maximum Range 60 days from wishlist date";
	$lang["w_p_url"] 						= "Wishlist Public URL";
	$lang["w_receive_cash_gift"] 			= "Would you like to receive Cash Gift ?";
	$lang["w_receive_info_1"] 				= "Cash Gift Fund can be used for anything";
	$lang["w_receive_info_2"] 				= "Contributed Gift that doesn't reach the total amount will be converted to cash gift.";
	$lang["w_gift_sent"] 					= "Where Should Your Gift Be Sent ?";
	$lang["w__prev_event_add"] 				= "Set address from previous Event";
	$lang["w_event_choose"] 				= "Select event";
	$lang["w_event_add"] 					= "Address";
	$lang["w_event_add_2"] 					= "Address Line 2";
	$lang["w_event_postcode"] 				= "Postcode";
	$lang["w_event_district"] 				= "District";
	$lang["w_event_city"] 					= "City";
	$lang["w_event_contact"] 				= "Mobile Number";
	$lang["w_event_privacy"] 				= "Allow my wishlist to be";
	$lang["w_event_add_show"] 				= "Show my Shipping address (by default, it's hidden)";
	$lang["w_event_visitor_pwd"] 			= "Enable visitor's password ";
	$lang["w_event_visitor_pwd_warning"]	= "Password must be less than 15 chars";
	$lang["w_event_save_btn"] 				= "Save and continue";
	$lang["w_event_update_btn"] 			= "Update and continue";
	$lang["w_personalize"] 					= "PERSONALIZE";
	$lang["w_img_upload"] 					= "Upload Image";
	$lang["w_note"] 						= "NOTE TO YOUR GUESTS (optional)";
	$lang["w_rsvp_info"] 					= "RSVP info, Dress Code and etc.";
	$lang["w_e_date"] 						= "EVENT DATE.";
	$lang["w_e_add"] 						= "EVENT ADDRESS (optional)";
	$lang["previous"] 						= "Previous";
	$lang["skip"] 							= "Skip, Do this later";
	$lang["w_e_save_finish"] 				= "Save and Finish";
	$lang["w_almost_ready"] 				= "ALMOST READY";
	$lang["w_add_item"] 					= "Let's add wonderful items to your wishlist";
	$lang["w_my_wishlist"] 					= "View My Wishlist";
	$lang["w_browse_gift"] 					= "BROWSE GIFT CATEGORIES";
/*
*	Event Reminder Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["event_reminder"] 					= "EVENT REMINDER";
	$lang["event_add"] 							= "Add Event";
	$lang["event_add_name"] 					= "Event Name";
	$lang["event_add_date"] 					= "Event Date";
	$lang["event_repeat_1"] 					= "Repeat";
	$lang["event_repeat_2"] 					= "None";
	$lang["event_repeat_3"] 					= "Every Week";
	$lang["event_repeat_4"] 					= "Every 2 Weeks";
	$lang["event_repeat_5"] 					= "Every Month";
	$lang["event_repeat_6"] 					= "Every Year";
	$lang["event_remind_1"] 					= "Remind Me";
	$lang["event_remind_2"] 					= "3 Days Before";
	$lang["event_remind_3"] 					= "1 Week Before";
	$lang["event_remind_4"] 					= "2 Weeks Before";
	$lang["event_remind_5"] 					= "A Month Before";
	$lang["event_save_btn"] 					= "Save Event";
	$lang["event_edit"] 						= "Edit Event";
	$lang["event_update_btn"] 					= "Update Event";
	$lang["empty_eventremainder"] 				= "You have no remainder";
/*
*	Serprise Box Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["serprise_title"] 					= "MY SURPRISE BOX";
	$lang["serprise_info"] 					 = "Enjoy the gifts received. If you have any questions, please feel free to";
	$lang["table_order"] 					   = "Order";
	$lang["table_date"] 						= "Date";
	$lang["table_total"] 					   = "Total";
	$lang["table_status"] 					  = "Status";
	$lang["table_action"] 					  = "Action";
	$lang["empty_supprisebox"] 				 = "You have no gift received yet.";
/*
*	Cash Gift List Box Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["cash_gift_title"] 					= "MY CASH GIFT";
	$lang["empty_wallet"] 						= "Your Wallet is Blank";
	$lang["cash_gift_info"] 					= "Enjoy the cash gifts received. Cash Gift fund will be transfered to your bank account every end of the month. If you have any questions, please feel free to ";
	$lang["cash_gift_from_my"] 					= "Data From my";
	$lang["cash_gift_from"] 					= "from";
	$lang["cash_gift_bank_title"] 				= "MY CASH GIFT - BANK ACCOUNT DETAIL";
	$lang["cash_gift_bank_info_1"] 				= "Please set your bank account detail here. Cash Gift fund will be transfered to your bank account every end of the month. If you have any questions, please feel free to";
	$lang["cash_gift_bank_AC"] 					= "Account Name";
	$lang["cash_gift_bank_AC_no"] 				= "Account Number";
	$lang["cash_gift_bank_AC_name"] 			= "Bank Name";
	$lang["cash_gift_bank_AC_detail"] 			= "Bank Details";
	$lang["cash_gift_bank_info_2"] 				= "Bank Address, KCP or info( if applicable )";
	$lang["cash_gift_save_btn"] 				= "Save and Finish";
/*
*	My Recipient Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["search_wishlist_placeholder"] 		= "Search Wishlist";
	$lang["go_btn"] 							= "Go!";
	$lang["my_recipients"] 						= "MY RECIPIENTS";
	$lang["delete"] 							= "Delete";
	$lang["no_recipients"] 						= "Sorry! No recipient list here.";
/*
*	Transaction list Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["transaction_info"] 					= "Gifts purchased for friends. If you have any questions, please feel free to";
	$lang["payment_info"] 						= "Data From BUY THIS FOR in product detail";
/*
*	My Profile Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["select_image"] 						= "Select image";
	$lang["user_name"] 							= "User Name";
	$lang["social_media"] 						= "Social Media";
	$lang["facebook"] 							= "Facebook";
	$lang["instagram"] 							= "Instagram";
	$lang["twitter"] 							= "Twitter";
	$lang["shipping_address"] 					= "Shipping Address";
	$lang["kecamatan"] 							= "Kecamatan";
	$lang["save_profile"] 						= "Save Profile";
	$lang["picture_crop"] 						= "Crop Your Profile Picture";
	$lang["crop"] 								= "Crop";
	$lang["user_name_change_text"] 				= "Change Your User Name";
	$lang["update"] 							= "Update";
	$lang["close"] 								= "Close";
/*
*	Change Password Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["current_pwd"] 						= "Current password";	
	$lang["new_pwd"] 							= "New Password";	
	$lang["confirm_new_pwd"] 					= "Confirm New Password";	
	$lang["save_pwd"] 							= "Save Password";	
	
	
	$lang["empty_order"] 							= "You have no order(s)";
/*
*	Wishlist View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["add_recipient_list"] 			= "Add To My Recipient List";
	$lang["remove_recipient_list"] 			= "Remove From My Recipient List";
	$lang["wishlist_Block"] 				= "Wishlist is Block";
	$lang["event_info"] 					= "EVENT INFO";
	$lang["time"] 							= "Time";
	$lang["special_note"] 					= "SPECIAL NOTE";
	$lang["event_address"] 					= "EVENT ADDRESS";
	$lang["shipping_add"] 					= "Shipping Address";
	$lang["not_specified"] 					= "Not Specified";
	$lang["send_cash_gift"] 				= "Send Gift Cash";
	$lang["send_cash_gift_instead"] 		= "Would you like to send gift cash instead  ?";
	$lang["amount_info"] 					= "Amount (in Rupiah)";
	$lang["amount"] 						= "Amount";
	$lang["desired"] 						= "Desired";
	$lang["received"] 						= "Received";
	$lang["contribution_text"] 				= "CONTRIBUTION";
	$lang["amount_contribution"] 			= "Amount contributed";
	$lang["view_contributors"] 				= "View Contributors";
	$lang["name"] 							= "Name";
	$lang["buy_this_as_gift"] 				= "Buy this as gift";
	$lang["contribution_total_amount"] 		= "contribution amount reach the total amount";
	$lang["contribute"] 					= "Contribute";
	$lang["any_amount"] 					= "any amount";
	$lang["gift_text"] 						= "to buy this gift with friends";
	$lang["contribute_with_friends"] 		= "Contribute with friends";
	$lang["email_friends_contribute"] 		= "Email friends to contribute";
	$lang["invite"] 						= "Invite";
	$lang["success"] 						= "Success";
	$lang["view_cart"] 						= "View to cart";
	$lang["checkout"] 						= "Checkout";
/*
*	Product Search View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["categories"] 			= "CATEGORIES";	
	$lang["brands"] 				= "BRANDS";	
	$lang["clear"] 					= "Clear";
	$lang["apply"] 					= "Apply";
	$lang["price_range"] 			= "Price Range";
	$lang["show"] 					= "Show";
	$lang["product_show_1"] 		= "Showing";
	$lang["product_show_2"] 		= "of";
	$lang["product_show_3"] 		= "products";
	$lang["all"] 					= "All";
	$lang["sort_by"] 				= "Sort by";
	$lang["Price_Low_to_High"] 		= "Price Low to High";
	$lang["Price_High_to_Low"] 		= "Price High to Low";
	$lang["Name_A_Z"] 				= "Name A - Z";
	$lang["Name_Z_A"] 				= "Name Z - A";
	$lang["sales_first"] 			= "Sales first";
/*
*	Product View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["sale"] 					= "SALE";		
	$lang["share_it"] 				= "Share it";	
	$lang["goDescription"] 			= "Scroll to product details, material &amp; care and sizing";	
	$lang["add_wishlist"] 			= "Add to Wish List";	
	$lang["add_to_your_wishlist"]	= "ADD TO YOUR WISHLIST";	
	$lang["select_Your_Wishlist"]	= "Select Your Wishlist";
	$lang["or_buy"]					= "OR, BUY THIS FOR";
	$lang["quantity"] 				= "Quantity";
	$lang["qty"] 					= "Qty";
	$lang["comming_Soon"] 			= "Comming Soon";
	$lang["product_like"] 			= "You may also like these products";
	$lang["no_product"] 			= "No product";
	$lang["recent_product"] 		= "Products viewed recently";
	$lang["new_wishlist_item"] 		= "A new item has been added to your wishlist.";
	$lang["view_Wishlist"] 			= "View Wishlist";
	$lang["continue_shop"] 			= "Continue shopping";

/*
*	Private Wishlist View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["private_wishlist"] 		= "This wishlist is private";	
	$lang["wishlist_password_info"]	= "To view the wishlist, please enter the password provided by the wishlist owner";
	$lang["proceed"] 				= "Proceed";
	$lang["password_resquest"] 		= "Please request the password from the wishlist owner";
	$lang["resquest"] 				= "Request";
/*
*	Public Profile View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["member_since"] 			= "Member since";		
	$lang["total_wishlist"] 		= "Total Wishlist created";
	$lang["total_gift"] 			= "Total Gift Received";
	$lang["share_btn"] 				= "Share this";
	$lang["send_msg"] 				= "Send Message";
	$lang["send_msg_info"] 			= "All messages are recorded and spam is not tolerated.You must be registered to send message";
	$lang["message"] 				= "Message";
	$lang["recent_gift"] 			= "Recent Gift Received";
	$lang["more_gift_wishlist"] 	= "Add more gifts to wishlist";
	$lang["no_data_found"] 			= "No Data Found";
	
/*
*	Remaining View Section
*	Author	 - Matainja Technologies Pvt. Ltd
*	Language - English
*/	
	$lang["shopping_cart"] 			= "Shopping cart";	
	$lang["product"] 				= "Product";
	$lang["price"] 					= "Price";
	$lang["buygift"] 				= "Buy Gift";
	$lang["cashgift"] 				= "Cash Gift";
	$lang["discount"] 				= "Discount";
	$lang["coupon_code"] 			= "Coupon Code";
	$lang["update_basket"] 			= "Update Basket";
	$lang["proceed_to_checkout"]	= "Proceed to Checkout";
	$lang["billing_address"]		= "Billing Address";
	$lang["ask_ship_address"]		= "Ship to different address ?";
	$lang["ask_ship_address_1"]		= "Only applicable if you are buying for yourself or a friend who doesn't have a wishlist";
	$lang["ask_ship_same"]			= "Shipping Address is same as Billing Address.";
	$lang["delivery"]				= "Delivery";
	$lang["free_shipping"]			= "Free Shipping";
	$lang["days"]					= "days";
	$lang["one_day_service"]		= "One Day Service";
	$lang["same_day_service"]		= "Same Day if order is placed before 3 pm or Get it by tomorrow";
	$lang["view_basket"]			= "View basket";
	$lang["continue_to_payment_method"]	= "Continue to Payment Method";
	$lang["your_order"]				= "Your Order";
	$lang["for_wishlist"]			= "for Wishlist";
	$lang["form_wishlist"]			= "FROM WISHLIST";
	$lang["if_transaction_fee"]		= "if transaction fee is";
	$lang["shipping_charge"]		= "Shipping Charge";
	$lang["payment_method"]			= "Payment Method";
	$lang["master"]					= "Master";
	$lang["visa"]					= "Visa";
	$lang["agree"]					= "I agree to the";
	$lang["confirm_my_order"]		= "Confirm My Order";
	$lang["shipping_method"]		= "Shipping Method";
	$lang["thank_you"]				= "Thank You";
	$lang["order_number_info"]		= "Your Order number is";
	$lang["email_confirm_text"]		= "You will receive an email confirmation shortly";
	$lang["share_glory_gifting"]	= "Share The Glory of Gifting";
	$lang["share"]					= "Share";
	$lang["customer"]				= "Customer";
	$lang["u_p_invalid"]			= "Username Or Password is invaild";
	$lang["login"]					= "Log in";
	$lang["not_register"]			= "Not registered yet?";
	$lang["register_now"]			= "Register now";
	$lang["register_now_info"]		= "It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!";
	$lang["email_sent"]				= "Email sent! If the email address you entered is registered at Freelancer, you'll receive an email with instructions on how to set a new password.";
	$lang["unique_email"]			= "Please Put a Unique Email id";
	$lang["subtotal"]				= "SUBTOTAL";
	$lang["pwd_reset_info"]			= "You requested a password reset to restore access to your account.";
	$lang["dear"]					= "Dear";
	$lang["pwd_reset_info_1"]		= "To reset the password, click this";
	$lang["link"]					= "link";
	$lang["or"]						= "Or";
	$lang["copy"]					= "Copy";
	$lang["info"]					= "Info";
	$lang["pwd_lenth_info"]			= "The last 8 passwords used are not allowed for a new password.";

	$lang["note"]					= "Note";
	$lang["pwd_reset_info_2"]		= "If you did not request a password reset for your account, someone else might have entered your email address by mistake. If so, simply ignore this message.";
	$lang["account"]				= "Account";
	$lang["bank_ac_no"]				= "Bank Account Number";
	$lang["bank"]					= "Bank";
	$lang["you_received"]			= "You received";
	$lang["on_for"]					= "on for";
	$lang["and_is_currently"]		= "and is currently";
	$lang["being_prepared"]			= "Being prepared";
	$lang["sender"]					= "Sender";
	$lang["hidden_address"]			= "(hidden address)";
	$lang["success_email_sent"]		= '"Thank You" mail has been sent successfully.';
	$lang["gift_bought_by"]			= "Gift bought by/ from";
	$lang["say_thank_you"]			= "Say Thank You";
	$lang["sub_total"]				= "Sub Total";
	$lang["code"]					= "Code";
	$lang["shipping"]				= "Shipping";
	$lang["information_updated."]	= "Your information updated.";
	$lang["choose_new_password."]	= "Choose Your New Password.";
	$lang["url"]					= "Url";
	$lang["sent_password"]			= "your password is already set";
	$lang["edit_continue"]			= "Edit and continue";
	$lang["search_list"]			= "Search List";
	$lang["not_found_pls_try"]		= "not found. please try again....";
	$lang["filter_by"]				= "Filter By";
	$lang["latest"]					= "Latest";
	
?>
