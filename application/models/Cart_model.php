<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model {
   public function __construct(){
		parent::__construct();
		$this->load->database();
   }
    
   public function add($data){
	    $this->db->select("count(*) as total"); 
		$this->db->from('ig_cart');
		$this->db->where(array('ig_cart.uid' =>$data['uid'],'ig_cart.product_id' =>$data['product_id'],'ig_cart.session_id' =>$data['session_id']));		
		$query = $this->db->get(); 
		$q = $query->result();
		foreach($q as $row){
			$total = $row->total;	
		}
		if($total!=0){ 
			$array = array('quantity' => 'quantity' + $data['quantity']);
			$this->db->where(array('ig_cart.uid' =>$data['uid'],'ig_cart.product_id' =>$data['wishlist_id'],'ig_cart.session_id' =>$data['session_id']));
			$this->db->update('ig_cart', $array);
	    }else{
			$this->db->insert('ig_cart', $data); 
			//$this->db->insert_id();
		}		
   }
   
   public function getDiscountByCoupon($coupon,$total_product_price){
	   
	 $query = $this->db->get_where('ig_coupon',array('coupon_code' => $coupon));
	 	if($query->num_rows() > 0){
		 	foreach($query->result() as $row){
				
				if(($row->c_start_date <= date('Y-m-d')) && (date('Y-m-d') <= $row->c_end_date)){
					$discount_percent = $row->coupon_percentage;
					$discount_amt = $row->coupon_fixed;
					if($discount_percent!=0){
						$discount  = 	($total_product_price * $discount_percent)/100;
					}else{
						
						$discount  = 	$discount_amt;
					}
				}else{
					$discount = 'Coupon Expired';	
				}
				
			}
			if(is_numeric($discount)){
				return number_format($discount);
			}else{
				return $discount;
			}
		 
		}  
	   
	   
	 }
   
   
       
}