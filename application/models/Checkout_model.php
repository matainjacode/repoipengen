<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Checkout_model extends CI_Model
{
    function __construct()
	{   
		parent::__construct();
		$this->load->database();
		$this->load->model('product_model');
		$this->load->model('wishlist_model');
	}
    public function get_cart_list($uid){
		$query=$this->db->get_where('ig_cart',array('uid'=>$uid));
		if($query->num_rows() > 0){ 
			foreach($query->result() as $row){
				$product_id = $row->product_id;
				$image = $this->product_model->getProductImageById($product_id);
				$row->product_url = $this->product_model->getProductUrl_name($product_id,$row->product_name);
				$row->image = $image;
				if($row->wishlist_id!='' && is_numeric($row->wishlist_id)){ 
						$wishlist_det = $this->wishlist_model->getwishlistByid($row->wishlist_id);
						if(isset($wishlist_det)){
							$row->wishlist_name = $wishlist_det->title;
							$row->wishlist_url =   '~'.$wishlist_det->url;	
						}
					}
				$data[] = $row;
			}
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function getAddressByWid($wid){
		$query = $this->db->get_where('ig_wishlist_shipping',array('wid' => $wid));
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$address = 	strtoupper($row->name.','. $row->street_address.','.$row->country.','.$row->state.','.$row->city.','.
				$row->land_mark.','.$row->zip);	
				
			}
			
			return $address;
		}
		
	}
	
	
	public function getShippingRate(){
		$query = $this->db->get_where('ig_shipping',array('admin_status' => 1))->row();
		return $query;
	}

}

?>