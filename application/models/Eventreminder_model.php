<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Eventreminder_model extends CI_Model {

    function __construct(){
							parent::__construct();
							$this->load->database();
    }
	/*Method Name: eventreminder
	  Tableaname : ig_user
	  Parameter: fname, lname, email, password
	*/

  public function addevent($data){
	  $query= $this->db->insert('ig_event',$data);
	 if($query){return $this->db->insert_id();}
	 else {return false;}
	  
  }
  	public function get_event_list($uid)
	{ 
	    $result = array();
	    $this->db->select('*'); 
		$this->db->from('ig_event');
		$this->db->where('uid',$uid);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
 	public function event_delete($id)
	{ 
	   $this->db->where('event_id',$id);
		$query = $this->db->delete('ig_event');
	 if($query){return TRUE;}
	 else {return false;}
	}
	public function get_dropdown_list($uid)
	{ 
	    $result = array();
	    $this->db->select('*'); 
		$this->db->from('ig_wishlist');
		$this->db->where('uid',$uid);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function event_edit($id,$data)
	{ 
	$this->db->where('event_id',$id);
		$query = $this->db->update('ig_event',$data);
	 if($query){return TRUE;}
	 else {return false;}

	}
	public function get_eventdate($data)
	{ 
	    $result = $this->db->get_where('ig_wishlist',$data);
		if($result->num_rows() > 0){
			return $result->result();
		}
		else{
			return $result->result();
			}
	}
	public function getall_info()
	{ 
	    $result = array();
	    $this->db->select('*'); 
		$this->db->from('ig_event');
		$this->db->join('ig_user','ig_event.uid=ig_user.id');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}