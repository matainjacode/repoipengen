<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends CI_Model
{
	function __construct()
	{ parent::__construct();
	$this->load->database();
	}
		 
	public function getProductDetails($id,$langId)
	{
		$this->db->select('ig_products.*,ig_products_text.*'); 
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products_text.product_id = ig_products.product_id');
		$this->db->where('ig_products.product_id',$id);
		$this->db->where('ig_products_text.language_id',$langId);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$data=$query->result();
			//print_r($data[0]);
			return $data;
	    }
	    
	}
	public function getRecentProduct($id,$langId)
	{
		
	$this->db->select('ig_products.product_id,ig_products_text.product_name,ig_products.sale_price,ig_products.product_image');
	$this->db->from('ig_products');
    $this->db->join('ig_products_text', 'ig_products_text.product_id = ig_products.product_id');
    $this->db->join('ig_product_recent_view','ig_product_recent_view.pid=ig_products.product_id');
	$this->db->where('ig_products_text.language_id',1);
	$this->db->where('ig_product_recent_view.uid',$id);
	$this->db->order_by("ig_product_recent_view.p_count","desc");
	$this->db->limit(3,0);
    $query = $this->db->get();
	if($query->num_rows() > 0){
			return  $query->result();
	    }	
		
		
	    
	}
	public function getWishlistCount($pid)
	{
		    $this->db->select('count(id) as totid');
		    $this->db->from('ig_wishlist_product');
			$this->db->where('product_id',$pid);
			$this->db->group_by('product_id');
			$query = $this->db->get();
			
			return  $query->num_rows();
			
	    
	}
	public function getGalleryImages($id)
	{
		$data=array();
		$this->db->select('ig_product_img_gallery.*'); 
		$this->db->from('ig_product_img_gallery');
		$this->db->where('ig_product_img_gallery.product_id',$id);
		$query = $this->db->get();
		//print_r($query->result());die();
		if($query->num_rows() > 0){
			$row=$query->result();
			return $row;
			//print_r($row);die();
		}
		
		//print_r($data);die();
		
	}
	
	public function getUserWishlist($uid)
	{
		$data=array();
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->where('uid',$uid);
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		//$result = $query->row();
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$data[]=$row;
			}
			return $data;
	    }
		//return $result;
	    
	}
	
	public function insertProductToWishlist($wid,$uid,$pid)
	{
		$this->db->select('*');
		$this->db->from('ig_wishlist_product');
		$this->db->where('uid',$uid);
		$this->db->where('wishlist_id',$wid);
		$this->db->where('product_id',$pid);
		$selectQuery = $this->db->get();
		//$result = $query->row();
		
		$this->db->select('url');
		$this->db->from('ig_wishlist');
		$this->db->where('id',$wid);
		$query=$this->db->get();
		$res=$query->row();
		if($selectQuery->num_rows() < 1){
			$data = array(
			'uid' => $uid,
			'wishlist_id' => $wid,
			'product_id' => $pid
			);
			$this->db->insert('ig_wishlist_product', $data);
			//return ($this->db->affected_rows() != 1) ? "Error adding Prioduct to Wishlist. Please Try Again." : "Successfully Added Product in Wishlist.";
			//return $result;
			return base_url().'~'.$res->url;
	    }
		else{
			return base_url().'~'.$res->url;
		}
		
	    
	}
	
	
	public function getrecipientWishlist($id){
		//echo $id;
		$data=array();
		$pic=array();
		$this->db->select('*');
		$this->db->from('ig_wishlist_recipient');
		$this->db->where('uuid',$id);
		$selectQuery = $this->db->get();
		if($selectQuery->num_rows() > 0){
			foreach($selectQuery->result() as $row){
				$this->db->select('ig_wishlist.*,ig_user.*,ig_wishlist.id as wid'); 
				$this->db->from('ig_wishlist');
				$this->db->join('ig_user', 'ig_wishlist.uid = ig_user.id');
				$this->db->where('ig_wishlist.uid',$row->wishlist_uid);
				$this->db->where('ig_wishlist.id',$row->wishlist_id);
				$query = $this->db->get();
				//echo $this->db->last_query();die();
				if($query->num_rows() > 0){
					//$data[]=$query->result();
					$data[]=$query->result();
				}
			}
			//print_r($data);die();
			return $data;
		}
		//die();
	}
	
	public function getproducturl($id)
	{
		
		$this->db->select('ig_products.product_id,ig_products.product_name'); 
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products_text.product_id = ig_products.product_id');
		$this->db->where('ig_products.product_id',$id);
		$this->db->where('ig_products_text.language_id',1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			
			$result=$query->row();
			$slag=$this->slugify($result->product_name);
			return  base_url().'p/'.$result->product_id.'-'.$slag;
	    }
	    
	}
	public function getProductUrl_name($id,$name){
		
		$slag=$this->slugify($name);
		return  base_url().'p/'.$id.'-'.$slag;
	}
	public function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		// trim
		$text = trim($text, '-');
		
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		
		// lowercase
		$text = strtolower($text);
		
		if (empty($text)) {
		return 'n-a';
		}
		
		return $text;
	}
	public function getProductImageById($product_id){
		
		$this->db->select('ig_products.product_image'); 
		$this->db->from('ig_products');
		$this->db->where('ig_products.product_id',$product_id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$image = $row->product_image;
			}
			return $image;
	    }		
		
		return false;
	}
	
		public function setresentProduct($pid,$uid){
		
	    $query = $this->db->get_where('ig_product_recent_view',array('pid'=>$pid,'uid'=>$uid));
		$date=date_create(date('Y-m-d H:i:s', time()));
        $date_time= date_format($date,"Y/m/d H:i:s");
		 if($query->num_rows() > 0){
			 $result=$query->row();
			 $data=array('p_count'=>$result->p_count+1,'r_date'=>$date_time);
			 $this->db->where(array('pid'=>$pid,'uid'=>$uid));
			 $this->db->update('ig_product_recent_view', $data);
		 }else{
			 
			 $data=array('pid'=>$pid,'uid'=>$uid,'r_date'=>$date_time,'p_count'=>1);
			 $this->db->insert('ig_product_recent_view', $data);
		 }
		
		return true;
	}
	public function category_product($categoryId,$pid,$langId)
	{
		$this->db->select('ig_products.*,ig_products_text.*'); 
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products_text.product_id = ig_products.product_id');
		$this->db->where('ig_products.product_id!=',$pid);
		$this->db->where('ig_products.category_id',$categoryId);
		$this->db->where('ig_products_text.language_id',$langId);
		$this->db->limit(3,0);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return  $query->result();
	    }
	    
	}
	
	/*public function getProductPriceById($product_id){
		
		$this->db->select('ig_products.price,ig_products.sale_price'); 
		$this->db->from('ig_products');
		$this->db->where('ig_products.product_id',$product_id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$price = (isset($row->sale_price) &&  !empty($row->sale_price)) ? $row->sale_price : $row->price;
			}
			return $price;
	    }		
		
		return false;
	}*/
	
	
	public function getProductPriceById($product_id){
		
		$query = $this->db->get_where('ig_products',array('product_id' => $product_id))->row(); 
		$start_date = $query->sale_start_date;
		$end_date = $query->sale_end_date;
		$current_date = date('Y-m-d');
		if(($start_date <= $current_date) && ( $current_date <= $end_date )){
			return $price = $query->sale_price;	
		}else{
			return $price = $query->price;	
		}
		return false;
	}
	
	public function getRecentProduct_withoutlogin($productidArray){
		
	$data=array_flip(array_flip($productidArray));
	$this->db->from('ig_products');
    $this->db->join('ig_products_text', 'ig_products_text.product_id = ig_products.product_id');
    $this->db->join('ig_product_recent_view','ig_product_recent_view.pid=ig_products.product_id');
	$this->db->where('ig_products_text.language_id',1);
	$this->db->where_in('ig_products.product_id',$data);
	
	$this->db->limit(3,0);
    $query = $this->db->get();
	if($query->num_rows() > 0){
			return  $query->result();
	    }	
	}
}