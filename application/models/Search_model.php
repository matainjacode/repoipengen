<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search_model extends CI_Model {

    function __construct(){
							parent::__construct();
							$this->load->database();
    }
	/*Method Name: userregister
	  Tableaname : ig_user
	  Parameter: fname, lname, email, password
	*/
	public function all_brand(){
		$query = $this->db->get_where('ig_brand',array('brand_status'=>1));
		return $query->result();
	}
	public function list_status($id){
		 $siteLangArray = $this->session->userdata('site_lang');
		 $Langid=$siteLangArray['laguage_id'];
		  if($Langid==""){ $Langid=1;}
			$this->db->select('ipengen_category.id,ipengen_category.parent_id,ig_category_details.name');
			$this->db->from('ipengen_category');
			$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
			$this->db->where(array('ipengen_category.parent_id'=>$id,'ig_category_details.lang'=>$Langid));
			$query=$this->db->get(); 
		if($query){
			return $query->result();
		}else{
			return FALSE;
		}
	
	}
	
	public function category_list_status(){
		 $siteLangArray = $this->session->userdata('site_lang');
		 $Langid=$siteLangArray['laguage_id'];		 
		  if($Langid==""){ $Langid=1;}
			$this->db->select('ipengen_category.id,ipengen_category.parent_id,ig_category_details.name');
			$this->db->from('ipengen_category');
			$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
			$this->db->where(array('ig_category_details.lang'=>$Langid));
			$query=$this->db->get();
			
			foreach($query->result() as $q){
				if($q->parent_id == 0){
					$category[] = $q;
				}
				else{
					$subCategory[] = $q;	
				}
			}
			
			foreach($category as $c){ 
				
				foreach($subCategory as $sub){ 
					if($sub->parent_id == $c->id){
						$subPid = $sub->parent_id;
						$data['id'] = $sub->id;
						$data['name'] = $sub->name;						
						$subcat[] = $data;
						$c->subcategory = $subcat;
					}
				}
				$subcat = array();		
			  }
				
			if(!empty($category))
				return $category;
			else
				return array();
	}
	
	
    public function product_list($quere_condition){
		
		 $siteLangArray = $this->session->userdata('site_lang');
		 $Langid=$siteLangArray['laguage_id'];
		  if($Langid==""){ $Langid=1;}
		 
		 $query = $this->db->query("SELECT ig_products.*,ig_products_text.* FROM ig_products JOIN ig_products_text ON ig_products.product_id=ig_products_text.product_id  where ig_products_text.language_id=".$Langid.' '.$quere_condition);
		//echo $this->db->last_query();
		return $query->result();
	}
	
	public function product_count($quere_condition){
		
		 $siteLangArray = $this->session->userdata('site_lang');
		 $Langid=$siteLangArray['laguage_id'];
		  if($Langid==""){ $Langid=1;}
		 
		 $query = $this->db->query("SELECT ig_products.*,ig_products_text.* FROM ig_products JOIN ig_products_text ON ig_products.product_id=ig_products_text.product_id  where ig_products_text.language_id=".$Langid.' '.$quere_condition);
		 
		 return $query->result();
	}
 public function product_search($key){
		
		 $siteLangArray = $this->session->userdata('site_lang');
		 $Langid=$siteLangArray['laguage_id'];
		  if($Langid==""){ $Langid=1;}
		 
		 $this->db->select('
		 					ig_products.product_id as id,
							ig_products_text.product_name as title,
							ig_products.product_image as image,
							ig_products.price,
							ig_products_text.short_description,
						  ');
	$this->db->from('ig_products');
	$this->db->join('ig_products_text','ig_products_text.product_id=ig_products.product_id');
	$this->db->where(array('ig_products_text.language_id'=>$Langid));
	$this->db->like('ig_products_text.product_name',$key);
	$query=$this->db->get();
         if($query->num_rows() > 0){
			 $proArr = array();
			if(!empty($query->result()))
			{
				$i=0;
				foreach($query->result() as $row)
				{
					$proArr[$i] = (array)$row;
					$proArr[$i]["url"] = "";
					$proArr[$i]["fname"] = "";
					$proArr[$i]["lname"] = "";
					$i++;
				}
			}
			return $proArr;
		}else{
			return array();
		}
	 }

	}
