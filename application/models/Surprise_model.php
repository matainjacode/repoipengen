<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Surprise_model extends CI_Model {

    function __construct(){
							parent::__construct();
							$this->load->database();
    }
	
	public function getSurprise($id)
	{
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_transaction', 'ig_transaction.order_id=ig_order_items.order_id');
		$this->db->join('ig_wishlist','ig_wishlist.id=ig_order_items.wishlist_id');
		$this->db->join('ig_user','ig_user.id=ig_order_items.user_id');
		$this->db->where('ig_wishlist.uid',$id);
		$this->db->where('ig_order_items.item_type','buy_gift');
		$this->db->order_by("ig_transaction.tid","desc");
		$query = $this->db->get();
		if($query->num_rows() > 0){
		return  $query->result();
		}	
	}
	public function getSurpriseDetails($oid,$uid)
	{
		$lanuagearray=$this->session->userdata('site_lang');
				if(!empty($lanuagearray)){
					$language_id=$lanuagearray['laguage_id'];
						$language_name=$lanuagearray['laguage_name'];
					}else{
						$language_id=1;
			}
	
			$this->db->select('ig_order.*,ig_order_items.*,ig_user.*,ig_products.*,ig_products_text.*,ig_wishlist.title,ig_wishlist.uid as u_id,ig_wishlist.url,ig_transaction.*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id', 'left');
			$this->db->join('ig_user', 'ig_user.id = ig_order_items.user_id', 'left');
			$this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id', 'left');
			$this->db->join('ig_products_text', 'ig_products_text.product_id = ig_order_items.product_id', 'left');
			$this->db->join('ig_wishlist','ig_wishlist.id=ig_order_items.wishlist_id', 'left');
			$this->db->join('ig_transaction', 'ig_transaction.order_id=ig_order_items.order_id', 'left');
			$this->db->where('ig_order.order_id',$oid);
			$this->db->where('ig_wishlist.uid',$uid);
		    $this->db->where('ig_order_items.item_type','buy_gift');
			$this->db->where('ig_products_text.language_id',$language_id);
			$query = $this->db->get();

			$result = $query->result();
			return $result;
	}
	
	
}