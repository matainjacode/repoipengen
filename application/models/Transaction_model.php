<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

    function __construct(){
		parent::__construct();
		$this->load->database();
    }
	public function getTransaction($id,$limit,$offset){
       
		$this->db->select('*');
		$this->db->from('ig_transaction');
		$this->db->join('ig_order', 'ig_order.order_id = ig_transaction.order_id');
		$this->db->where('ig_order.user_id',$id);
		$this->db->limit($limit,$offset);
		$this->db->order_by("ig_transaction.tid","desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
		
		
	}
	public function row_count($uid)
	{
		$this->db->select('*');
		$this->db->from('ig_transaction');
		$this->db->join('ig_order', 'ig_order.order_id = ig_transaction.order_id');
		$this->db->where('ig_order.user_id',$uid);
		$query = $this->db->get();
		
		return $query->num_rows();
	}
	public function getOrderDetails($oid,$uid){
		
			$lanuagearray=$this->session->userdata('site_lang');
				if(!empty($lanuagearray)){
					$language_id=$lanuagearray['laguage_id'];
						$language_name=$lanuagearray['laguage_name'];
					}else{
						$language_id=1;
			}
	
			$this->db->select('ig_order.*,ig_order_items.*,ig_user.*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id');
			$this->db->join('ig_user', 'ig_user.id = ig_order.user_id');
			$this->db->where('ig_order.order_id',$oid);
			$this->db->where('ig_order_items.user_id',$uid);
			$query = $this->db->get();
			$result = $query->result();
			return $result;
			
			
		}
	
	public function getOrder($oid,$uid){
		
			$query = $this->db->get_where('ig_order',array('order_id'=>$oid,'user_id'=>$uid))->row();
			return $query;
			
		}

}