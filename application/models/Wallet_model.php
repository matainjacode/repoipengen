<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wallet_model extends CI_Model
{
	function __construct()
	{ parent::__construct();
	$this->load->database();
	}
		 
    public function getMyCashGift($id)
	{
		$this->db->select('*');
		$this->db->from('ig_wallet_transaction');
		$this->db->join('ig_wishlist', 'ig_wallet_transaction.wishlist_id = ig_wishlist.id');
		$this->db->join('ig_user','ig_user.id=ig_wallet_transaction.sender_id');
		$this->db->where('ig_wishlist.uid',$id);
		$this->db->order_by("ig_wallet_transaction.wallet_id","desc");
		$query = $this->db->get();
		if($query->num_rows() > 0){
		return  $query->result();
		}	
	}

	public function getSingleCashGift($id,$uid)
	{
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->join('ig_wallet_transaction', 'ig_wallet_transaction.wishlist_id = ig_wishlist.id');
		$this->db->join('ig_user','ig_user.id=ig_wallet_transaction.sender_id');
		$this->db->where('ig_wishlist.id',$id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
		return  $query->result();
		}	
	}
	public function getViewStatus($id,$uid)
	{
			$data=array('is_new'=>'no');
			$this->db->where('wishlist_id',$id);
			$this->db->update(' ig_wallet_transaction ',$data);
	}
}