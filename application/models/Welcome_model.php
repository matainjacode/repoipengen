<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome_model extends CI_Model
{
	function __construct()
		 { parent::__construct();
		   $this->load->database();
		 }
		 
	public function getAlleventcategory($langId)
	{ 		
		$this->db->select('ig_event_category_text.event_name,ig_event_category.image_name,ig_event_category.id');
		$this->db->from('ig_event_category');
		$this->db->join('ig_event_category_text', 'ig_event_category.id = ig_event_category_text.event_category_id');
		$this->db->where('ig_event_category_text.lang_id',$langId);
		$this->db->where('ig_event_category.status',1);
		$this->db->where('ig_event_category.feature_status',1);
		$this->db->order_by("ig_event_category.id","desc");
		$this->db->limit(8);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}	 
	public function getAllfeaturedproduct($langId)
	{ 
		$this->db->select('*');
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products.product_id = ig_products_text.product_id');
		$this->db->where('ig_products.feature_status',1);
		$this->db->where('ig_products.admin_status',1);
		$this->db->where('ig_products_text.language_id',$langId);
		$this->db->order_by("ig_products.product_id","desc");
		$this->db->limit(12);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function getAllblog($langId){
		$this->db->select('*');
		$this->db->from('ig_blog');
		$this->db->join('ig_blog_text', 'ig_blog.blog_id = ig_blog_text.blog_id');
		$this->db->where('ig_blog.blog_status',1);
		$this->db->where('ig_blog_text.blog_language',$langId);
		$this->db->limit(2);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}
	public function getBlogById($blogId, $langId)
	{
		$this->db->select('*');
		$this->db->from('ig_blog');
		$this->db->join('ig_blog_text', 'ig_blog.blog_id = ig_blog_text.blog_id');
		$this->db->where('ig_blog.blog_status',1);
		$this->db->where('ig_blog_text.blog_language',$langId);
		$this->db->where('ig_blog.blog_id',$blogId);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}