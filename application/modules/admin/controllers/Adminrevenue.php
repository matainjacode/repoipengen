<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminrevenue extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array("adminrevenue_model", "dashboard_model"));
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		
		
	}
	
    public function revenue_list()
	{
		$data["page_title"] = "Revenue | iPengen";
		$data["revenueList"] = $this->adminrevenue_model->get_all_revenue();
		$data["revenue_count"] = $this->dashboard_model->revenue_count();
		$this->load->template('adminrevenue/list',$data);
	}
	
}
?>