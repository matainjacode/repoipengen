<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

  function __construct()
	{
		parent::__construct();
		if ( ! $this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->helper(array('url','form','cookie'));
		$this->load->library(array('session','pagination','email','upload','image_lib','form_validation')); 
		$this->load->model('banner_model'); 
	}
	
	public function do_upload() 
	{ 
	     $config['upload_path']   = getcwd().'/assets/uploads/banner/'; 
         $config['allowed_types'] = 'gif|jpg|png'; 
         $config['max_size']      = 100; 
         $config['max_width']     = 1024; 
         $config['max_height']    = 768;  
         $this->load->library('upload', $config);
		 $this->upload->initialize($config);
			
         if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors()); 
            $this->load->view('groups/create', $error); 
         }
			
         else { 
            $data = array('upload_data' => $this->upload->data()); 
            $this->load->view('groups/create', $data); 
         } 
      
    }  
	
	public function customresize($imagePath,$opts=null,$dpath)
	{
	# start configuration
	#Imrtype used for resize according to height/fixed/width
	#iscanvas used for suppose u have to rezise 380X260 , but yr imge resize after 300X135 . some part of 380x260 will be canvas color.
	
	if(!isset($opts['quality']))
	$quality = 90; # image quality to use for ImageMagick (0 - 100)
	else
	$quality = $opts['quality'];

	$path_to_convert = '/usr/bin/convert';'convert'; # this could be something like /usr/bin/convert or /opt/local/share/bin/convert
	
	## you shouldn't need to configure anything else beyond this point

	if(isset($opts['w'])): $w = $opts['w']; endif;
	if(isset($opts['h'])): $h = $opts['h']; endif;

	$filename = md5_file($imagePath);

	$create = true;

	if($create == true):
		if(!empty($w) and !empty($h)):

			list($width,$height) = getimagesize($imagePath);
			$resize = $w;
		
		if($opts['Imrtype']=='fixed')
		{
			$resize = $w."x".$h."\!";
		}
		elseif($opts['Imrtype']=='height')
		{
		$resize = "x".$h;
		}
		elseif($opts['Imrtype']=='width')
		{
		$resize = $w;
		}
		elseif($opts['Imrtype']=='ratio')
		{
			if($width > $height):
				$resize = $w;
				if(isset($opts['crop']) && $opts['crop'] == true):
					$resize = "x".$h;				
				endif;
			else:
				$resize = "x".$h;
				if(isset($opts['crop']) && $opts['crop'] == true):
					$resize = $w;
				endif;
			endif;
		}
		

			if(isset($opts['iscanvas']) && $opts['iscanvas'] == true):
			$cmd = $path_to_convert." "."'".$imagePath."'"." -resize ".$resize." -size ".$w."x".$h." xc:".(isset($opts['canvas-color'])?$opts['canvas-color']:"transparent")." +swap -gravity center -composite -quality ".$quality." "."'".$dpath."'";
			
				
			else:
			$cmd = $path_to_convert." "."'".$imagePath."'"." -resize ".$resize." -quality ".$quality." "."'".$dpath."'";
				
			endif;
		
		endif;
		
		$c = exec($cmd);
		
	endif;

	# return cache file path
	return $dpath;
	}
	
	
	public function add()
	{  
		$mainImagePath =$this->config->item('image_path').'banner/original/';
		$imagepath_1910x800 = $this->config->item('image_path').'banner/1910x800/';               
		
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		//Validating Name Field
		$this->form_validation->set_rules('banner_name', 'Banner Name', 'required|min_length[2]|max_length[50]');
		//Validating skill desc Field
		$this->form_validation->set_rules('banner_desc', 'Banner Description', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data["page_title"] = "Add Banner | iPengen";
			$data['error'] = "";
			$this->load->template('banner/banner_add',$data);
		}
		else
		{
			if(!is_dir($mainImagePath)): mkdir($mainImagePath, 0777, true); endif;
			$image_name = time()."_".$_FILES["user_file"]['name'];	
			$config['file_name'] = $image_name;
			$config['upload_path']   = $mainImagePath; 
			$config['allowed_types'] = 'jpeg|gif|jpg|png'; 
			$config['max_size']      = 300000000; 
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('user_file'))
			{
				$data["page_title"] = "Add Banner | iPengen";
				$data['error'] = "Please Upload an Image";
				//Transfering data to Model
				$this->load->template('banner/banner_add',$data);
				//redirect('admin/banner/add');
			}
			else
			{
				$data = array(
							'banner_name' => $this->input->post('banner_name'),
							'banner_desc' => $this->input->post('banner_desc'),
							'banner_image' => $image_name 
							);
				//Transfering data to Model
				$last_id=  $this->banner_model->banner_add($data);
				$returnVal = $this->_image_resize($image_name,$mainImagePath,$imagepath_1910x800,"1910","800");
				
				$this->session->set_flashdata('success','New banner has been successfully added.');
				redirect('admin/banner/bannerlist');
				  
			}
			
		}
		
		
		
	}
	
	public function bannerlist()
	{
		$data["page_title"] = "Banner List | iPengen";
		$data['bannerlist']= $this->banner_model->banner_list();
		$this->load->template('banner/banner_list', $data);
	}
		
	public function bannerupdate()
	{  
		$mainImagePath =$this->config->item('image_path').'banner/original/';
		$imagepath_1910x800 = $this->config->item('image_path').'banner/1910x800/';               
		
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('banner_name', 'Banner Name', 'required|min_length[2]|max_length[50]');
		$this->form_validation->set_rules('banner_desc', 'Banner Description', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata("failed","Opps..!!! Something wrong went..try again..");
			redirect('admin/banner/bannerlist');
		}
		else
		{
			if(!is_dir($mainImagePath)): mkdir($mainImagePath, 0777, true); endif;
			$image_name = time()."_".$_FILES["user_file"]['name'];	
			$config['file_name'] = $image_name;
			$config['upload_path']   = $mainImagePath; 
			$config['allowed_types'] = 'jpeg|gif|jpg|png'; 
			$config['max_size']      = 300000000; 
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$banner_id = $this->input->post('banner_id_hidden');
			if (!$this->upload->do_upload('user_file'))
			{
				$bannerName = $this->input->post('banner_name');
				$data = array(
							'banner_name' => $this->input->post('banner_name'),
							'banner_desc' => $this->input->post('banner_desc'),
							);
				$result = $this->banner_model->bannerupdate($data,$banner_id);
				if($result)
				{
					$this->session->set_flashdata("success", $bannerName." has been successfully updated.");
					redirect('admin/banner/bannerlist');
				}
				else
				{
					$this->session->set_flashdata("success", $bannerName." has been successfully updated with same data.");
					redirect('admin/banner/bannerlist');
				}
			}
			else
			{
				$prevImageName = $this->banner_model->getBannerImageNameById($banner_id);
				if($prevImageName != "")
				{
					$this->_image_unlink($mainImagePath,$prevImageName);
					$this->_image_unlink($imagepath_1910x800,$prevImageName);
				}
				$data = array(
							'banner_name' => $this->input->post('banner_name'),
							'banner_desc' => $this->input->post('banner_desc'),
							'banner_image' => $image_name 
							);
				$bannerName = $this->input->post('banner_name');
				//Transfering data to Model
				$result = $this->banner_model->bannerupdate($data,$banner_id);
				$returnVal = $this->_image_resize($image_name,$mainImagePath,$imagepath_1910x800,"1910","800");
				if($result)
				{
					$this->session->set_flashdata("success", $bannerName." has been successfully updated.");
					redirect('admin/banner/bannerlist');
				}
				else
				{
					$this->session->set_flashdata("success", $bannerName." has been successfully updated with same data.");
					redirect('admin/banner/bannerlist');
				}
				  
			}
			
		}
		
		
		
	}
	
	public function bannerdelete($bannerId)
	{
		$mainImagePath =$this->config->item('image_path').'banner/original/';
		$imagepath_1910x800 = $this->config->item('image_path').'banner/1910x800/';
		$prevImageName = $this->banner_model->getBannerImageNameById($banner_id);
		if($prevImageName != "")
		{
			$this->_image_unlink($mainImagePath,$prevImageName);
			$this->_image_unlink($imagepath_1910x800,$prevImageName);
		}
		$result = $this->banner_model->bannerdelete($bannerId);
		if($result):
			$this->session->set_flashdata("success","Banner data deleted successfully.");
			redirect("admin/banner/bannerlist");
		else:
			$this->session->set_flashdata("failed","Opps..!!! Somwthing Wrong went...");
			redirect("admin/banner/bannerlist");
		endif;
	}
		
	private function _image_resize($imagename = false, $imageSourcePath = false, $imageUploadPath = false, $width = false, $height = false )
	{
		if($imagename != false && $imageSourcePath != false && $imageUploadPath != false && $width != false && $height != false):
			
			if(is_file($imageSourcePath.$imagename)) :
				if(!is_dir($imageUploadPath)):
					mkdir($imageUploadPath, 0777, true);
				endif;
			endif;
			$config = array(
						'image_library' 	=> 	'gd2',
						'source_image'		=>	$imageSourcePath.$imagename,
						'new_image'			=> 	$imageUploadPath.$imagename,
						'maintain_ratio'	=>	false,
						'width'				=>	$width,
						'height'			=>	$height,
							);
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
		else:
			return false;
		endif;
	}
	private function _image_unlink($filePath = false, $imageName = false)
	{
		if($filePath != false && $imageName != false):
			if(is_file($filePath.$imageName)) :	unlink($filePath.$imageName); endif;
		endif;
	}
}