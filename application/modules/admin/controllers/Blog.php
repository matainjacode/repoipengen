<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MX_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("blog_model"));
		$this->load->library('form_validation');
		$this->load->library('imageresize');
		$this->load->library('image_lib');  
		
	}

	function create()
	{
		$data["page_title"] = "Add Blog | iPengen";
		$data['language']=$this->blog_model->laguage_list();
		if(!empty($_POST))
		{
			
			$this->form_validation->set_rules('blog_title[]','Title','required');
			$this->form_validation->set_rules('blog_description[]','Category Description','required');
			//$this->form_validation->set_rules('blog_image','Category Name','required');
			$this->form_validation->set_rules('blog_link','Category Description','required');
		
			if($this->form_validation->run() == TRUE)
			{
				$result = $this->blog_model->add_blog();
				if($result)
				{
					
					/////////
					if (!is_dir($this->config->item('image_path').'blog/')) { 
								mkdir($this->config->item('image_path').'blog/'.$result, 0777, true); 
							} 
					if (!is_dir($this->config->item('image_path').'blog/'.$result.'/thumb/')) { 
								mkdir($this->config->item('image_path').'blog/'.$result.'/thumb/', 0777, true); 
							} 
					if(isset($_FILES['blog_image']['name']))
					{
						//print_r($_FILES);die();
						//$result = false;
						$blog_image=time()."_".$_FILES['blog_image']['name'];
							 $config = array(
								'upload_path' 	=> 	$this->config->item('image_path').'blog/'.$result,
								'file_name'		=>	$blog_image,
								'allowed_types'	=>	'jpg|jpeg|png',
								'max_size'		=>	'1048576',
							 );
							 
						
						
						$this->load->library('upload',$config);
						  $this->upload->initialize($config); 
										
						if($this->upload->do_upload('blog_image'))
						{
							$config = array( 
							'source_image'      => $this->config->item('image_path').'blog/'.$result.'/'.$blog_image, 
							'new_image'         => $this->config->item('image_path').'blog/'.$result.'/thumb/'.$blog_image, 
							'width'             => 200, 
							//'height'            => 200, 
							'create_thumb'	 =>	false,	//if TRUE then 'thumb_' word will be create before FIle name
							'maintain_ratio'   =>	true, 
												); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
							$this->blog_model->update_blog_img($blog_image,$result);	
						}
						else
						{
							echo $this->upload->display_errors();
						}
					}
					
					
	
					$this->session->set_flashdata("successMsg","Blog added successfully.");
					redirect("admin/blog/list");
				}
				else
				{
					
					$this->load->template("blog/create",$data);
				}
			}
			else
			{
				
				$this->load->template("blog/create",$data);
			}
		}
		else
		{
			
			$this->load->template("blog/create",$data);
		}
	}

	function edit($id = NULL)
	{
		$data["page_title"] = "Edit Blog | iPengen";
		if($id != NULL)
		{
			if(!empty($_POST))
			{
				$this->form_validation->set_rules('blog_title[]','Title','required');
				$this->form_validation->set_rules('blog_description[]','Category Description','required');
				//$this->form_validation->set_rules('blog_image','Category Name','required');
				$this->form_validation->set_rules('blog_link','Category Description','required');
				
				if($this->form_validation->run() == TRUE)
				{
				$result = $this->blog_model->update_blog($id);
				if($result)
				{
					
					if (!is_dir($this->config->item('image_path').'blog/'.$id)) { 
							mkdir($this->config->item('image_path').'blog/'.$id, 0777, true); 
						} 
					if (!is_dir($this->config->item('image_path').'blog/'.$id.'/thumb/')) { 
								mkdir($this->config->item('image_path').'blog/'.$id.'/thumb/', 0777, true); 
							} 
					if(isset($_FILES['blog_image']['name']))
					{
						//print_r($_FILES);die();
						//$result = false;
						$blog_image=time()."_".$_FILES['blog_image']['name'];
							 $config = array(
								'upload_path' 	=> 	$this->config->item('image_path').'blog/'.$id,
								'file_name'		=>	$blog_image,
								'allowed_types'	=>	'jpg|jpeg|png',
								'max_size'		=>	'1048576',
							 );
							 
						
						
						$this->load->library('upload',$config);
						  $this->upload->initialize($config); 
										
						if($this->upload->do_upload('blog_image'))
						{
							$blogData = $this->blog_model->getBlogById($id);
							if(!empty($blogData))
							{
								$imageName = $blogData[0]->blog_image;
								$mainPath = $this->config->item('image_path').'blog/'.$id.'/';
								$resizePath = $this->config->item('image_path').'blog/'.$id.'/thumb/';
								$this->_image_unlink($mainPath, $imageName);
								$this->_image_unlink($resizePath,$imageName);
							}
							$config = array( 
							'source_image'      => $this->config->item('image_path').'blog/'.$id.'/'.$blog_image, 
							'new_image'         => $this->config->item('image_path').'blog/'.$id.'/thumb/'.$blog_image, 
							'width'             => 200, 
							//'height'            => 200, 
							'create_thumb'	 =>	false,	//if TRUE then 'thumb_' word will be create before FIle name
							'maintain_ratio'   =>	TRUE, 
												); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
							$this->blog_model->update_blog_img($blog_image,$id);		
						}
					}
					$this->session->set_flashdata("successMsg","Blog updated successfully.");
					redirect("admin/blog/list");
				}
				else
				{
					$this->session->set_flashdata("successMsg","Blog updated successfully with same data.");
					redirect("admin/blog/list");
				}
				}else
				{
					$this->session->set_flashdata("successMsg","Blog updated successfully with same data.");
					redirect("admin/blog/list");
				}
			}
			else
			{
				$data["id"] = $id;
				$data['result'] = $this->blog_model->blog_data_for_edit($id);
				$data['language']=$this->blog_model->laguage_list();	
				$this->load->template("blog/edit",$data);
	
			}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}

	function bloglist()
	{
		$data["page_title"] = "Blog List | iPengen";
		$data["blogList"] = $this->blog_model->get_all_blog();

		$this->load->template("blog/list", $data);
	}

	function delete($Code = NULL)
	{
		if($Code != NULL)
		{
			$blogData = $this->blog_model->getBlogById($Code);
			if(!empty($blogData))
			{
				$imageName = $blogData[0]->blog_image;
				$mainPath = $this->config->item('image_path').'blog/'.$Code.'/';
				$resizePath = $this->config->item('image_path').'blog/'.$Code.'/thumb/';
				$this->_image_unlink($mainPath, $imageName);
				$this->_image_unlink($resizePath,$imageName);
			}
			$result = $this->blog_model->deleteblogById($Code);
			if($result)
			{
				$this->session->set_flashdata("successMsg","Deleted successfully.");
				redirect("admin/blog/list");
			}
			else
			{
				$this->session->set_flashdata("errorMsg","Opps..Something went wrong.");
				redirect("admin/blog/list");
			}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}

	function status($id = NULL, $value = NULL)
	{
		if($id != NULL && $value != NULL)
		{
			echo $result = $this->blog_model->StatusChange($id, $value);	
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
	function display_status($id = NULL, $value = NULL)
	{
		if($id != NULL && $value != NULL)
		{
			echo $result = $this->blog_model->display_StatusChange($id, $value);	
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
	private function _image_unlink($image_path = false, $prev_img_name = false)
	{
		if(is_file($image_path.$prev_img_name))
		{
			unlink($image_path.$prev_img_name);
		}
		else
		{
			return false;
		}
	}
}
?>