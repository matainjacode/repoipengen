<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends MX_Controller{
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')){redirect('admin/login');} 
		$this->load->library('form_validation');
		$this->load->library('session');
	    $this->load->model(array("login_model","category_model"));
		$this->load->helper('url');
		
	}
   public function  add()
    {
	   if(isset($_POST['submit']))
	   {
			$this->form_validation->set_rules('name[]','Category Name','required');
			$this->form_validation->set_rules('description[]','Category Description','required');
			
			if($this->form_validation->run() == TRUE)
			{
			  
			/*	$data=array('name'=>$this->input->post('name'),
							'description'=>$this->input->post('description'),
							'parent_id'=>$this->input->post('parent_id'),
							);*/
				$result_id = $this->category_model->insert_category();
				if($result_id!=""){
					$category = $this->category_model->category_name($result_id);
					$cat_name = $category->name;
					$parent_id = $this->category_model->getcatParentId($result_id);
					if($parent_id != 0){
						$catgry = $this->category_model->category_name($parent_id);
						$category_name = $catgry->name;
						$parent_details = $this->category_model->getcategoryById($parent_id);
						foreach($parent_details as $parent){
							$category_id = $parent->parent_id;	
							if($category_id == 0){
								$string = 'Root->'.ucfirst($category_name).'->'.ucfirst($cat_name);
							}
						}
					}else{
						$string = 'Root->'.ucfirst($cat_name);
						
					}
						
					if($_FILES['banner']['name'])
					{
						
						$config = array(
							'upload_path' 	=> 	$this->config->item('image_path').'category/'.$result_id.'/',
							'allowed_types'	=>	'jpg|jpeg|png|gif',
							'max_size'		=>	'1048576',
									);
									
						if (!is_dir($this->config->item('image_path').'category/'.$result_id.'/')) {mkdir($this->config->item('image_path').'category/'.$result_id.'/', 0777, true);}
					
						if (!is_dir($this->config->item('image_path').'category/'.$result_id.'/thumb/')) {mkdir($this->config->item('image_path').'category/'.$result_id.'/thumb/', 0777, true);}
						//$user_id=$_SESSION['contractor_signup']['uid'];
						$this->load->library('upload',$config);
						if($this->upload->do_upload('banner'))
						{
							
							
							$file_data = $this->upload->data();
							$img_name = $file_data['raw_name'].$file_data['file_ext'];
							
								$thum_config = array(
									'image_library'	=>	'gd2',
									'source_image'	=>	'./photo/category/'.$result_id.'/'.$img_name,
									'new_image'		=>	'./photo/category/'.$result_id.'/thumb/',
									'create_thumb'	=>	FALSE,	//if TRUE then 'thumb_' word will be create before FIle name
									'maintain_ratio'	=>	TRUE,
									'width'			=> 	'200',
									'height'		=>	'200'
								);
									
									$this->load->library('image_lib', $thum_config); 
									$this->image_lib->resize();	
								//$data = array($img_name,$user_id);
								$query = $this->db->update('ipengen_category',array('banner'=>$img_name,'path'=>$string),array('id'=>$result_id));
								if($query)
								{
									
									$this->session->set_flashdata('successMsg','New category added successfully.');
									redirect('admin/category/list');
								}
						}
						else
						{
							$query = $this->db->update('ipengen_category',array('path'=>$string),array('id'=>$result_id));
							if($query)
							{
								$this->session->set_flashdata('successMsg','New category added successfully.');
								redirect('admin/category/list');
							}
						}
					}
					else
					{
						
						//echo $string; die('ddddddddddd');
						$query = $this->db->update('ipengen_category',array('path'=>$string),array('id'=>$result_id));
						if($query)
						{
							echo "kkkkkkkk";
							$this->session->set_flashdata('successMsg','New category added successfully.');
							redirect('admin/category/list');
						}
						
					}
			
				}
			}
			else
			{
				$result=$this->category_model->category_list_status(0);
				$data['page_title']="Add Category | iPengen"; 
				$data['language']=$this->category_model->laguage_list();
				$data['result']=$result;
				$this->load->template('admin/category/add',$data);
			}
		   }
		   else
		  {
			$result=$this->category_model->category_list_status(0);
			$data['page_title']="Add Category | iPengen"; 
			$data['language']=$this->category_model->laguage_list();
			$data['result']=$result;
			$this->load->template('admin/category/add',$data);
	   } 
    }
	
	public function category_list_child($id,$sid =NULL){
        $str="";
		 $str2="";

		$result=$this->category_model->category_list_status($id);
   if(!empty($result)){
	  
	  $str2.='<ul class="dropdown-menu">';
	 foreach($result as $value){
			$selected=($value->parent_id==$sid?'selected="selected"':"");
			$str.='<option '.$selected.' value="'.$value->id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$value->name.'</option>';
			$str2.='<li class="dropdown-submenu"><a class="catnameid"  id='.$value->id.' href="javascript:void(0)">'.$value->name.'</a>';

			$str2.=$stval=$this->category_list_child($value->id);
			if($stval=="")
			{
				$str2.='</li></ul>';
			}
			else{
				$str2.='</li>';
			}
			
		 }
   }

		return $str2;

	}


	
	public function categorylist()
	{ 
		$total_result['page_title']="Category List | iPengen";
		$total_result['result']=$this->category_model->category_list();
		$this->load->template('admin/category/list',$total_result);
	}
	
	public function edit($id=NULL)
	 {
	   if(isset($_POST['submit']))
	    { 

	    $this->form_validation->set_rules('name[]','Category Name','required');
		$this->form_validation->set_rules('description[]','Category Description','required');
		//$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[admin_login.email]');
		if($this->form_validation->run() == TRUE)
		{

			$result = $this->category_model->category_edit($id);
			if($result){
				
				if($_FILES['banner']['name'])
					{
						
						$config = array(
							'upload_path' 	=> $this->config->item('image_path').'category/'.$id.'/',
							'allowed_types'	=>	'jpg|jpeg|png|gif',
							'max_size'		=>	'1048576',
									);
									$result1 = $this->category_model->getcategoryById($id);
									$mainFileExistsCheck = 'photo/category/'.$id.'/'.$result1[0]->banner;
									$thumbFIleExistsCheck = 'photo/category/'.$id.'/thumb/'.$result1[0]->banner;
									if(file_exists($mainFileExistsCheck) && file_exists($thumbFIleExistsCheck) )
									{
										@unlink('photo/category/'.$id.'/'.$result1[0]->banner);
										@unlink('photo/category/'.$id.'/thumb/'.$result1[0]->banner);
									}
								
					if (!is_dir($this->config->item('image_path').'category/'.$id.'/')) {mkdir('./'.$this->config->item('image_path').'category/'.$id.'/', 0777, true);}else{
						
						
					}
					
					if (!is_dir($this->config->item('image_path').'category/'.$id.'/thumb/')) {mkdir('./'.$this->config->item('image_path').'category/'.$id.'/thumb/', 0777, true);}
						//$user_id=$_SESSION['contractor_signup']['uid'];
						$this->load->library('upload',$config);
						if($this->upload->do_upload('banner'))
						{
							$file_data = $this->upload->data();
							$img_name = $file_data['raw_name'].$file_data['file_ext'];
								$thum_config = array(
									'image_library'	=>	'gd2',
									'source_image'	=>	'./photo/category/'.$id.'/'.$img_name,
									'new_image'		=>	'./photo/category/'.$id.'/thumb/',
									'create_thumb'	=>	FALSE,	//if TRUE then 'thumb_' word will be create before FIle name
									'maintain_ratio'	=>	TRUE,
									'width'			=> 	'200',
									'height'		=>	'200'
								);
									
									$this->load->library('image_lib', $thum_config); 
									$this->image_lib->resize();	
								//$data = array($img_name,$user_id);
								$query = $this->db->update('ipengen_category',array('banner'=>$img_name),array('id'=>$id));
								if($query)
								{
									
									$this->session->set_flashdata('successMsg','Category data updated successfully.');
									redirect('admin/category/list');
								}
						}
						else
						{
							
							$this->session->set_flashdata('successMsg','Category data updated successfully.');
							redirect('admin/category/list');
						}
					}
				
				$this->session->set_flashdata('successMsg','Category data updated successfully.');
			          redirect('admin/category/list');
					  
					  
					  
			       }
			}
		else
		{     
			$result = $this->category_model->getcategoryById($id);
			$result1=$this->category_model->category_list_status(0);
			foreach ($result as $value) {
				$data['parentId'] = $value->parent_id;
			}
			$data['page_title']="Edit Category | iPengen";
			$data['result1']=$result1;
			$data['result']=$result;
			$this->load->template('admin/category/edit',$data);
		}	
	}
	   else
	   	{
			$result = $this->category_model->getcategoryById($id);
			$result1=$this->category_model->category_list_status(0);
			foreach ($result as $value) {
				$data['parentId'] = $value->parent_id;
			}
			$data['page_title']="Edit Category | iPengen";
			$data['result1']=$result1;
			$data['result']=$result;
			$this->load->template('admin/category/edit',$data);
		  } 
    }	
  public function delete($id=NULL)
  {
	$result = $this->category_model->delete_category($id); 
	if($result){$this->session->set_flashdata('adminDeleteMsg','Category data deleted successfully.');
			          redirect('admin/category/list');	
						
						}	 
	  }	 
	public function enablestatus($id=NULL)
	{
		$enableAdmin = $this->category_model->enable_category($id);
		if($enableAdmin)
		{echo 1;}else{echo 0;}
	}	
	public function disablestatus($id=NULL)
	{
	  
		$disableAdmin = $this->category_model->disable_category($id);
		if($disableAdmin )
		{echo 1;}else{echo 0;}
	}
	public function enablesmegatatus($id=NULL)
	{
		echo $enableMegaAdmin = $this->category_model->enable_mega_category($id);
	}
	public function disablemegastatus($id=NULL)
	{
		echo $disableMegaAdmin = $this->category_model->disable_mega_category($id);
	}

}
?>
