<?php
/**
* 
*/
class Contribute extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("contribute_model"));
		
	}
	
	function contributelist()
	{
		$data["page_title"] = "Contribute List | iPengen";
		$data["lists"] = $this->contribute_model->getContributelist();
		$this->load->template("contribute/list",$data);
	}
}
?>