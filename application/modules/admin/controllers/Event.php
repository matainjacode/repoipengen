<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Event extends MX_Controller{
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('image_lib');
	    $this->load->model('event_model');
		$this->load->helper('url');
	}
	public function  add()
	{ 
	   if(isset($_POST['submit']))
	   { 
			$this->form_validation->set_rules('name[]','Event Category Name','required');
			if($this->form_validation->run()  && isset($_FILES['eventImage']['name']))
			{
				$eventImageName = 'event_'.time().'_'.$_FILES['eventImage']['name'];
				$event_image_main_path = $this->config->item('image_path').'eventcategory/';
				
				if($this->_orignial_image_upload($eventImageName,$event_image_main_path,'eventImage'))
				{
					$data=array(
							'image_name'=>$eventImageName,
								);
						/*Get Insert Event ID*/
						$event_id = $this->event_model->insert_event($data);
						/*Get Array Count Value*/
						$event_lang_count = count($this->input->post("name"));
						for($i=0; $i < $event_lang_count; $i++)
						{
							$data = array(
									'event_category_id' => 	$event_id,
									'event_name'		=>	$this->input->post("name[".$i."]"),
									'lang_id'			=>	$this->input->post("langId[".$i."]")
										  );
							$this->event_model->insert_event_text($data);
						}
					/* Image Resize 100 x 100 resulation */
					$resize_path_100 = $this->config->item('image_path').'eventcategory/'.$event_id.'/100-100/';
					$this->_image_resize($event_image_main_path,$resize_path_100,$eventImageName,'100');
					/* Image Resize 400 x 400 resulation */
					$resize_path_400 = $this->config->item('image_path').'eventcategory/'.$event_id.'/400-400/';
					$this->_image_resize($event_image_main_path,$resize_path_400,$eventImageName,'400');
					if($event_id)
					{
						$this->session->set_flashdata('success_message','Event category added succesfully.');
						redirect('admin/event/list');
					}
					else
					{
						$this->session->set_flashdata('error_message','Opps..!!! Something went wrong.Try again.');
						redirect('admin/event/list');
					}
					
					
				}
				else
				{
					$this->session->set_flashdata('errEventImg','<strong>Please choose event image.</strong>');
					$data["page_title"] = "Add Event | iPengen";
					$data["langs"] = $this->event_model->get_lang_data();
					$this->load->template('admin/eventcategory/add',$data);
				}
	
			}
			else
			{
				if(!isset($_FILES['eventImage']))
				{
					$this->session->set_flashdata('errEventImg','<strong>Please choose event image.</strong>');
				}
				$data["page_title"] = "Add Event | iPengen";
				$data["langs"] = $this->event_model->get_lang_data();
				$this->load->template('admin/eventcategory/add',$data);
			}
		}
		else
		{
			$data["page_title"] = "Add Event | iPengen";
			$data["langs"] = $this->event_model->get_lang_data();
			$this->load->template('admin/eventcategory/add',$data);
		} 
	}
	
	public function eventlist()
	{
		$total_result["page_title"] = "Event List | iPengen"; 
		$total_result['result']=$this->event_model->event_list();
		$this->load->template('admin/eventcategory/list',$total_result);
	}
	
	public function edit($id=NULL)
	{
		if(isset($_POST['submit']))
		{ 
			$this->form_validation->set_rules('name[]','Event category name','required');
			if($this->form_validation->run())
			{
				if($_FILES['eventImage']['name'])
				{
					$eventImageName = 'event_'.time().'_'.$_FILES['eventImage']['name'];
					$event_image_main_path = $this->config->item('image_path').'eventcategory/';
					/* Image Resize 100 x 100 resulation */
					$resize_path_100 = $this->config->item('image_path').'eventcategory/'.$id.'/100-100/';
					/* Image Resize 400 x 400 resulation */
					$resize_path_400 = $this->config->item('image_path').'eventcategory/'.$id.'/400-400/';
					
					if($this->_orignial_image_upload($eventImageName,$event_image_main_path,'eventImage'))
					{
						$previous_image_name = $this->event_model->getPreviousImagebyId($id);
						$this->_image_unlink($event_image_main_path,$previous_image_name);
						$this->_image_unlink($resize_path_100,$previous_image_name);
						$this->_image_unlink($resize_path_400,$previous_image_name);
						$data=array(
								'image_name'=>$eventImageName,
									);
						
						$event_id = $this->event_model->edit_event($id,$data);
						/*Get Array Count Value*/
						$event_lang_count = count($this->input->post("name"));
						for($i=0; $i < $event_lang_count; $i++)
						{
							$eventTextId = $this->input->post("hideEventTextId[".$i."]");
							$data = array(
									'event_name'		=>	$this->input->post("name[".$i."]"),
									'lang_id'			=>	$this->input->post("langId[".$i."]"),
										  );
							$result = $this->event_model->update_event_text($data, $eventTextId);
						}
						$this->_image_resize($event_image_main_path,$resize_path_100,$eventImageName,'100');
						$this->_image_resize($event_image_main_path,$resize_path_400,$eventImageName,'400');
						if($event_id || $result)
						{
							$this->session->set_flashdata('success_message','Event category updated succesfully.');
							redirect('admin/event/list');
						}
						else
						{
							$this->session->set_flashdata('error_message','Opps..!!! Something went wrong.Try again.');
							redirect('admin/event/list');
						}
						
						
					}
					else
					{
						$this->session->set_flashdata('errEventImg','<strong>Please choose event image.</strong>');
						$this->load->template('admin/eventcategory/add');
					}
				}
				else
				{
					/*Get Array Count Value*/
					$event_lang_count = count($this->input->post("name"));
					for($i=0; $i < $event_lang_count; $i++)
					{
						$eventTextId = $this->input->post("hideEventTextId[".$i."]");
						$data = array(
									'event_name'		=>	$this->input->post("name[".$i."]"),
									'lang_id'			=>	$this->input->post("langId[".$i."]"),
										  );
						$result = $this->event_model->update_event_text($data, $eventTextId);
					}
					if($result)
					{
						$this->session->set_flashdata('success_message','Event category updated succesfully.');
						redirect('admin/event/list');
					}
					else
					{
						$this->session->set_flashdata('success_message','Event category updated succesfully with same data.');
						redirect('admin/event/list');
					}
				}
			}
			else
			{     
				$data["page_title"] = "Edit Event | iPengen";
				$data['event_id'] = $id;
				$data['result'] = $this->event_model->getEventById($id);
				foreach($data['result'] as $list)
				{
					$data['eventUrl'] = 'photo/eventcategory/'.$id.'/400-400/'.$list->event_image;
				}
				$data["langs"] = $this->event_model->get_lang_data();
				
				$this->load->template('admin/eventcategory/edit',$data);
			}	
		}
		else
		{
			$data["page_title"] = "Edit Event | iPengen";
			$data['event_id'] = $id;
			$data['result'] = $this->event_model->getEventById($id);
			foreach($data['result'] as $list)
			{
				$data['eventUrl'] = 'photo/eventcategory/'.$id.'/400-400/'.$list->event_image;
			}
			$data["langs"] = $this->event_model->get_lang_data();
			
			$this->load->template('admin/eventcategory/edit',$data);
		} 
    }	
	public function delete($id=NULL)
	{
	$result = $this->event_model->delete_event($id); 
	if($result){$this->session->set_flashdata('success_message','Event category deleted successfully.');
					  redirect('admin/event/list');	
						
						}	 
	  }	
	public function enablestatus($id=NULL)
	{
	  
	 $enableevent = $this->event_model->enable_event($id);
	  if( $enableevent > 0)
	  {$this->session->set_flashdata('success_message','Event category activated.');
		  redirect('admin/event/list');}
	  }	
	public function disablestatus($id=NULL)
	{
	  
	 $disableEvent = $this->event_model->disable_event($id);
	  if( $disableEvent > 0)
	  { $this->session->set_flashdata('success_message','Event category deactivated.');
		  redirect('admin/event/list');}
	  }
	public function enablefeaturestatus($id = NULL)
	{
		echo $result = $this->event_model->enable_feature_event($id);
	}
	public function disablefeaturestatus($id = NULL)
	{
		echo $result = $this->event_model->disable_feature_event($id);
	}		  
	private function _orignial_image_upload($new_image_name = false, $image_path = false, $main_image_name = false)
	{
		if(!is_dir($image_path))
		{
			mkdir($image_path, 0777, true);
		}
		$event_image_config = array(
					'upload_path' 		=>	$image_path,
					'file_name'			=>	$new_image_name,
					'allowed_types' 	=>	'jpg|jpeg|png|gif',
					'overwrite'			=>	false,
									);
		$this->upload->initialize($event_image_config);
		if($this->upload->do_upload($main_image_name)){return true;}else{return false;}
		
	}
	private function _image_resize($main_image_path = false, $new_path = false, $image_name = false, $image_size = false )
	{
		if(!is_dir($new_path)){mkdir($new_path, 0777, true);}
			$config = array(
				'image_library'		=>	'gd2',
				'source_image'      => 	$main_image_path.$image_name,
				'new_image'         => 	$new_path.$image_name,
				'maintain_ratio'    => 	false,
				'quality'			=>  100,
				'width'             => 	$image_size, 
				'height'            => 	$image_size, 
			);  
			$this->image_lib->initialize($config);
			if($this->image_lib->resize()) {return true;}else{return false;}
	}
	private function _image_unlink($image_path = false, $prev_img_name = false)
	{
		if(is_file($image_path.$prev_img_name))
		{
			unlink($image_path.$prev_img_name);
		}
		else
		{
			return false;
		}
	}
}
?>