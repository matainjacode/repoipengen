<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Multiadmin extends MX_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		//$this->lang->load('admin_lang', 'english');
	    $this->load->model('admin_model');$this->load->model('login_model');
		$this->load->helper('url');
		if(!$this->session->userdata('logged_in')){redirect('admin/login');} 
	}
   public function  add()
    { 
	   if(isset($_POST['submit']))
	   { 
	   $this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('re_password','Re-enter Password','required|matches[password]');
		$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[admin_login.email]');
		if($this->form_validation->run() == TRUE)
		{
			$data=array('first_name'=>$this->input->post('first_name'),
			            'last_name'=>$this->input->post('last_name'),
						'position'=>$this->input->post('position'),
						'email'=>$this->input->post('email'),
						'password'=>md5($this->config->item('email_verification_salt').$this->input->post('password')),
						'status'=>'1');
				$result = $this->admin_model->insert_admin($data);
				if($result)
				{
					$this->session->set_flashdata('adminInsertMsg','New admin added successfully.');
					redirect("admin/list");
				}
			}
		else
		{
			$data["page_title"] = "Add Multi-Admin | iPengen";
			$this->load->template('admin/multiadmin/add', $data);}
	   }
	   else
	  {$data["page_title"] = "Add Multi-Admin | iPengen";
			$this->load->template('admin/multiadmin/add', $data);} 
    }
	
	public function adminlist()
	{ 
		$total_result["page_title"] = "Multi-Admin List | iPengen";
		$total_result['result']=$this->login_model->admin_exist();
		$this->load->template('admin/multiadmin/list',$total_result);
	}
	
	public function edit($id=NULL)
	 {
	   if(isset($_POST['editsubmit']))
	    { 
	       if($this->input->post('password'))
			{
	   $this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('re_password','Re-enter Password','required|matches[password]');
		//$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[admin_login.email]');
		if($this->form_validation->run() == TRUE)
		{
			$data=array('first_name'=>$this->input->post('first_name'),
			            'last_name'=>$this->input->post('last_name'),
						'position'=>$this->input->post('position'),
						'password'=>md5($this->config->item('email_verification_salt').$this->input->post('password')),
						'status'=>'0');
			$fullName = ucwords($this->input->post('first_name')." ".$this->input->post('last_name'));
			$result = $this->admin_model->edit_admin($id,$data);
			if($result){$this->session->set_flashdata('adminEditMsg',$fullName."'s Profile updated successfully.");
			          redirect('admin/list');
			       }
			}
		else
		{  
			 
			$result = $this->admin_model->getAdminById($id);
			$this->load->template('admin/multiadmin/edit',array('data'=>$result,'page_title'=>'Edit Multi-Admin | iPengen'));
			}	
			}
		 else
			{$data=array('first_name'=>$this->input->post('first_name'),
			            'last_name'=>$this->input->post('last_name'),
						'position'=>$this->input->post('position'),
						'status'=>'0');
					$fullName = ucwords($this->input->post('first_name')." ".$this->input->post('last_name'));
					$result = $this->admin_model->edit_admin($id,$data);
			if($result){$this->session->set_flashdata('adminEditMsg',$fullName."'s Profile updated successfully (Password has not been changed).");
			          redirect('admin/list');	
						
						}	
	           }
	   }
	   else{$result = $this->admin_model->getAdminById($id);
		  $this->load->template('admin/multiadmin/edit',array('data'=>$result,'page_title'=>'Edit Multi-Admin | iPengen'));
		  } 
    }	
  public function delete($id=NULL)
  {
	$result = $this->admin_model->delete_admin($id); 
	if($result){$this->session->set_flashdata('adminDeleteMsg','Deleted Successfully.');
			          redirect('admin/list');	
						
						}	 
	  }	
    public function enablestatus($id=NULL)
	  {
		  
		 $enableAdmin = $this->admin_model->enable_admin($id);
		  if( $enableAdmin > 0)
		  {$this->session->set_flashdata('adminEditMsg','Enable Successfully.');
			  redirect('admin/list');}
		  }	
    public function disablestatus($id=NULL)
	  {
		  
		 $disableAdmin = $this->admin_model->disable_admin($id);
		  if( $disableAdmin > 0)
		  { $this->session->set_flashdata('adminDeleteMsg','Disable Successfully.');
			  redirect('admin/list');}
		  }		  

}
?>