<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("order_model");
		$this->load->helper(array('ipengen_email_helper'));
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		
		
	}
	

	
	public function order_list()
	{
		$data["page_title"] = "Order List | iPengen";
		$data["orderList"] = $this->order_model->get_all_orders();
		$this->load->template('order/list',$data);
	}
	
	
	
	
	
	public function details($orderId=NULL)
	{
		//$orderId=117;
		$data['page_title']="Order Details |iPengen";
		$data['orderdetails']= $this->order_model->getOrderDetails($orderId);
//		print_r($data['orderdetails']);
//		die();
		$data['orderdetailshistory']= $this->order_model->getOrderDetailshistory($orderId);
		$this->load->template('order/details',$data);
	}
	public function change_status()
	{
		$emailFrom = "";
		
		$orderid=$this->input->post('orderid');
		$userid=$this->input->post('userid');
		$status=$this->input->post('status');
		$remark=$this->input->post('remark');
		
		$result=$this->order_model->change_status($orderid,$status,$remark);
		if($status == "shipping")
		{
			$productName = "";
			$data["order_id"] = $orderid;
			$data["userData"] = $this->order_model->getUserData($orderid);
			$data['orderdetails'] = $this->order_model->getOrderDetails($orderid);
			$emailBody = $this->load->view("email_template/order-status-delivery",$data,true);
			$supportData = $this->order_model->support_data();
			$emailTo = $data["userData"][0]->userEmail;
			if(!empty($supportData))
			{
				if(count($data['orderdetails']) == 1)
				{ $productName = 'of "'.$data['orderdetails'][0]->name.'"'; }
				
				$emailFrom = $supportData[0]->info_email;
				$subject = 'Your Ipengen order (#'.$orderid.') '.$productName.' has been dispatched!';
				$emailData = array(
							"title" 	=> "Ipengen",
							"from"		=>	$emailFrom,
							"to"		=>	$emailTo,
							"subject"	=>	$subject,
							"message"	=>	$emailBody
									);
				send_email($emailData);
			}
		}
		
		echo 'success';
	}
	
}
?>