<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pages extends MX_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
	    $this->load->model('pages_model'); 
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('ckeditor');
	}
	function index()
	{ 
		redirect('admin/pages/add');
	}
   
   public function add()
   {
	   $data['page_title']="Add New Page | iPengen"; 
	   $data['language']=$this->pages_model->language_list();
       $this->load->template('admin/pages/add',$data);
   }
   

  public function submit()
  {
	  $this->form_validation->set_rules('page_name[]', 'Page name', 'required');   
      $this->form_validation->set_rules('page_description[]', 'Page description', 'required');
	  if($this->form_validation->run() == TRUE)
	  {
		  $query = $this->pages_model->insert();
		  if($query !='')
		   {   
				$this->session->set_flashdata('pagesInsertMsg','Page added successfully');
				redirect('admin/pages/pagelist');
		   }
	  }
	  else
	  {
		  redirect('admin/pages/add');
	  }
  }
  
  public function pagelist()
  {
	  $data["page_title"] = "Page List | iPengen";		
	  $data['result'] = $this->pages_model->listing();
	  $this->load->template('admin/pages/list',$data); 
  }
  
  public function edit($id)
  {
	  if(isset($_POST['editsubmit']))
	  {
		  
		  $this->pages_model->update_details($id);
		  $this->session->set_flashdata('pagesEditMsg','Updated successfully');
		  redirect('admin/pages/pagelist');
	  }
	  else
	  {
		  
		  $data['result'] = $pagedata = $this->pages_model->get_details($id);
		  if(!empty($pagedata))
		  {
			  $data["page_title"] = ucfirst($pagedata[0]->page_name);
			  $this->load->template('admin/pages/edit',$data);
		  }
	  }
	  
  }
  
  public function delete($id)
  {
	  $this->pages_model->delete_pages($id);
	  $this->session->set_flashdata('pagesDeleteMsg','Deleted successfully');
	  redirect('admin/pages/pagelist');
  }
}
?>