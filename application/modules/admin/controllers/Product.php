<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MX_Controller { 

	public function __construct() 

				{parent::__construct('pagination'); 

				   $this->load->model('product_model'); 
				    $this->load->library('session'); 
					$this->load->library('imageresize');    
				    if (!$this->session->userdata('logged_in')){redirect('admin/login');}
                 
				} 

	
	

			public function index() 
			{   
				$data["page_title"] = "Add Product | iPengen";
				$data['productimages'] = glob('mediafiles/thumbnail/*', GLOB_NOSORT);
				$data['language'] = $this->product_model->get_language();
				$data['cat_list'] = $this->product_model->get_category_list();
				$data['brands'] = $this->product_model->get_brand_list();
				$this->load->template('products/addproduct',$data); 
				
			} 

			public function addproduct()	 
			{ 
			 $this->load->helper('directory'); 
			 if($this->input->post('price') >= $this->input->post('saleprice'))
			 {

					$data=array( 
								 'sku'=>$this->input->post('SKU'), 
								 'stock_quantity'=>$this->input->post('stock_quantity'), 
								 'stock' => $this->input->post('stock'),
								 'price'=>$this->input->post('price'),
								 'category_id'=>$this->input->post('category'),
								 'brand_id'=>$this->input->post('brand'),
								 'sale_price'=>($this->input->post('saleprice') != '')?$this->input->post('saleprice'):$this->input->post('price'),
								 'sale_start_date'=>$this->input->post('startdate'),
								 'sale_end_date'=>$this->input->post('enddate'),
								 'create_date'=>date('Y-m-d H:i:s'), 
								 'shipping_weight'=>$this->input->post('shipping_weight'),
								 'shipping_dimension'=>$this->input->post('shipping_dimension'),
								  ); 
							$getProductId=$this->product_model->insert_products($data); 
							
							$row_ids = $this->input->post('row');
							$product_name = $this->input->post('product_name');
							$language_id = $this->input->post('language_id');
							$short_description = $this->input->post('s_description'); 
							$message = $this->input->post('editor'); 
							$datatext = array();
							//print_r($language_id); die();
							$post = $this->input->post(); 
							for ($i = 0; $i < ($this->input->post('row')); $i++)
							{
								$datatext[] = array(
								'product_id' => $getProductId,
								'language_id' => $language_id[$i],
								'product_name' => $product_name[$i],
								'short_description' => $short_description[$i],
								'message' => $message[$i],
								);
								
						
							}

							$this->product_model->insert_producttext($datatext);	

						 
						if(!empty($_POST['product_image'] )) 
						{ 
						$product_image = $_POST['product_image'];
						/*if (!is_dir('media/'.$getProductId)) { 
							mkdir('./media/'.$getProductId, 0777, true); 
						}*/ 
						if (!is_dir($this->config->item('image_path').'product/')) { 
							mkdir($this->config->item('image_path').'product/', 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'), 0777, true); 
						}
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'), 0777, true); 
						}
						
								$productImageExtension= explode('.',$product_image); 
								$img_file_name = $getProductId."_".time().'.'.$productImageExtension[1]; 

								$config['image_library'] = 'gd2'; 

								$this->load->library('image_lib', $config); 

						list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$product_image);
						if ($width>$height) {
						$orientation = "width";
						} else {
						$orientation = "height";
						}
						/*------------------------original Image Upload----------------------------*/
						$config = array( 
										'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
										'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$img_file_name, 
										'maintain_ratio'    => FALSE, 
										'width'             => $width, 
										'height'            => $height, 
										'master_dim' =>'width' 
										); 
								$this->image_lib->initialize($config); 
								$this->image_lib->resize();

							/*--------------------Image Resize for 250*250------------------------------------------*/
							$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
							$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
							
							/*------------------------Image Resize for 250*250------------------------------------------*/
							/*-------------------------Image Resize for 400*310------------------------------------------*/
							
							$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
							$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.
$this->config->item('medium_thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
							/*----------------------------Image Resize for 400*310------------------------------------------*/
							/*--------------------------Image Resize for 650*500------------------------------------------*/
							
							$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
							$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
							
							/*-----------------------Image Resize for 650*500------------------------------------------*/
							$file_name = $img_file_name;  
							$this->product_model->updateProductByImages($getProductId,$file_name); 
							
						} 
						/*additional images computer*/
						if(isset($_POST['serverhiddenid']))
						{
	
							$productImageIds = $_POST['serverhiddenid'];
	
							$index=0;
	
							foreach($productImageIds as $productImageId)
	
							{
	
								$productImageExtension= explode('.',$productImageId);
	
								$additional_file_name = $getProductId.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  
	
								$config['image_library'] = 'gd2'; 
	
								$this->load->library('image_lib', $config); 
	
						list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$productImageId);
						if($width > $height){
						$orientation = "width";
						} else {
						$orientation = "height";
						}
						
						/*------------------------original Image Upload----------------------------*/
						$config = array( 
										'source_image'      => $this->config->item('upload_image_path').'uploads/'.$productImageId, 
										'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$additional_file_name, 
										'maintain_ratio'    => FALSE, 
										'width'             => $width, 
										'height'            => $height, 
										'master_dim' =>'width' 
										); 
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
						
						/*--------------------Image Resize for 250*250------------------------------------------*/
						/*-----------------------Image Resize for 250*250------------------------------------------*/
						$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
						$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
						
						/*---------------Image Resize for 250*250------------------------------------------*/
						/*-----------------Image Resize for 450*450------------------------------------------*/
						
						$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
						$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
						/*-----------------Image Resize for 450*450------------------------------------------*/
						/*-------------------Image Resize for 800*800------------------------------------------*/
						
						$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
						$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
						
						/*-------------------Image Resize for 800*800------------------------------------------*/
						
						$this->product_model->additionProductByImages($getProductId,$additional_file_name);
						$index = $index+1;
					  }
	
					}	
				redirect('admin/product/productlist');	
			 }
			 else
			 {
				 $this->session->set_flashdata("error_msg","Sale Price must be less than Main Price.");
				 $data["page_title"] = "Add Product | iPengen";
				$data['productimages'] = glob('mediafiles/thumbnail/*', GLOB_NOSORT);
				$data['language'] = $this->product_model->get_language();
				$data['cat_list'] = $this->product_model->get_category_list();
				$data['brands'] = $this->product_model->get_brand_list();
				$this->load->template('products/addproduct',$data); 
			}

										

					} 

			public function view($id = null)
			{ 
				$data["page_title"] = "View Product | iPengen";
				$data['product'] = $this->product_model->getProductById($id); 
				$data['customers'] = $this->product_model->getCustomers(); 
				$data['assign_customers'] = $this->product_model->getCustomerByProductId($id);	 
				$this->load->template('products/view',$data);	 

			} 

	public function assignCustomer(){ 

		 

		$cusID = $_GET['customerID']; 

		$productID = $_GET['product_id']; 

            

         $rowno =$this->customer_model->isAssignproduct($productID,$cusID); 

		  

		 if($rowno) 

		{ 

		$products['success'] = $this->product_model->delete_customer($cusID,$productID); 

		$products['type'] = "delete"; 

		$products['status'] = $rowno; 

		} 

		else 

		{ 

		$products['success']  = $this->product_model->assignCustomersToProduct($cusID,$productID); 

		$products['type'] = "assign"; 

		$products['status'] = $rowno; 

		} 

		 

		echo json_encode($products); 

	}	 

	public function deleteCustomer(){ 

		$customer_id = $_GET['customer_id']; 

		$product_id = $_GET['product_id']; 

		$response = $this->product_model->delete_customer($customer_id,$product_id);	 

		echo json_encode($response); 

	} 

	public function checkassignCustomer(){ 

		$pro_id = $_GET['product_id']; 

		echo json_encode($this->product_model->getCustomerByProductId($pro_id)); 	 

		 

	} 

	public function productlist() {
		$data["page_title"] = "Product List | iPengen";
		$data['p_list']=$this->product_model->list_products();
		$this->load->template('products/productlist',$data); 
	
		} 

	public function edit($id = NULL) { 
		$data["page_title"] = "Edit Product | iPengen";
		$data['row'] = $this->product_model->edit_product($id); 
		$data['language'] = $this->product_model->get_editlanguage($id); 
		$data['galleries']=$this->product_model->exit_id($id);
		$data['cat_list'] = $this->product_model->get_category_list();
		$data['brands'] = $this->product_model->get_brand_list();
		$this->load->template('products/edit',$data); 
	
	}	 

	public function delete($pro_id = NULL) { 
	
	  $this->product_model->deleteProductById($pro_id); 
	
	  redirect('admin/product/list'); 
	
	  }	    

	public function editproduct($p_id = NULL)
	{ 
		if($this->input->post('price') >= $this->input->post('saleprice'))	
		{				
				$data=array( 
						 'sku'=>$this->input->post('SKU'), 
						 'stock_quantity'=>$this->input->post('stock_quantity'), 
						 'stock' => $this->input->post('stock'),
						 'price'=>$this->input->post('price'),
						 'sale_price'=>($this->input->post('saleprice') != '')?$this->input->post('saleprice'):$this->input->post('price'),
						 'category_id'=>$this->input->post('category'),
						 'brand_id'=>$this->input->post('brand'),
						 'sale_start_date'=>$this->input->post('startdate'),
						 'sale_end_date'=>$this->input->post('enddate'),
						 'create_date'=>date('Y-m-d H:i:s'), 
						 'shipping_weight'=>$this->input->post('shipping_weight'),
						 'shipping_dimension'=>$this->input->post('shipping_dimension'),
						  ); 

					$getProductId=$this->product_model->update_product($data,$p_id);
					
					$row_ids = $this->input->post('row');
					$product_name = $this->input->post('product_name');
					$language_id = $this->input->post('language_id');
					$short_description = $this->input->post('s_description'); 
					$message = $this->input->post('editor'); 
					$datatext = array();
					//print_r($language_id); die();
					$post = $this->input->post(); 
					for ($i = 0; $i < ($this->input->post('row')); $i++)
						{
							$datatext[] = array(
							'product_id' => $getProductId,
							'language_id' => $language_id[$i],
							'product_name' => $product_name[$i],
							'short_description' => $short_description[$i],
							'message' => $message[$i],
							);
						 }	
					$this->product_model->updet_producttext($datatext,$p_id);	
	
								
					if (!empty($_POST['product_image'] )) 
					{ 

	$product_image = $_POST['product_image'];
	$product_previous_image= $this->product_model->getProductImageByID($p_id);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$product_previous_image->product_image);
	
	
	
	if (!is_dir($this->config->item('image_path').'product/')) { 
		 mkdir($this->config->item('image_path').'product/', 0777, true); 
		} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'), 0777, true); 
	} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'), 0777, true); 
	} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'), 0777, true); 
	}
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'), 0777, true); 
	}
	
	$productImageExtension= explode('.',$product_image); 
	$img_file_name = $p_id."_".time().'.'.$productImageExtension[1]; 
	$config['image_library'] = 'gd2'; 
	$this->load->library('image_lib', $config); 
					

					/*$config = array( 

					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 

					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/100-100/'.$img_file_name, 

					'maintain_ratio'    => FALSE, 

					'width'             => 250, 

					'height'            => 250, 

					'master_dim' 		=> 'width' 

					);  

				   $this->image_lib->initialize($config); 

					$this->image_lib->resize();*/ 

					

					/*$config1 = array( 

					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 

					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/400-400/'.$img_file_name, 
					'maintain_ratio'    => TRUE, 
							
					'width'             => 250, 
				
					'height'             => 250, 

					'master_dim' 		=> 'width' 

					); 

					$this->image_lib->initialize($config1); 
					$this->image_lib->resize();	*/

		
					list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$product_image);
					if ($width > $height) {
						 $orientation = "width";
					} else {
						 $orientation = "height";
					}
					
		/*------------------------original Image Upload----------------------------*/
					$config = array( 
									'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
									'new_image'         => $this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$img_file_name, 
									'maintain_ratio'    => FALSE, 
									'width'             => $width, 
									'height'            => $height, 
									'master_dim' =>'width' 
									); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();

		/*----------------------------Image Resize for 250*250------------------------------------------*/
			$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
			$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$product_image;
			$despath_path250_250 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$img_file_name;
			$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
		
		/*--------------------------Image Resize for 250*250------------------------------------------*/
		/*-----------------------------Image Resize for 400*310------------------------------------------*/

			$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>100);
			$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
			$despath_path400_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$img_file_name;
			$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
		/*-------------------Image Resize for 400*310------------------------------------------*/
		/*---------------Image Resize for 650*500------------------------------------------*/
		
		$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>100);
		$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
		$despath_path650_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$img_file_name;
		$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
	
		/*------------------------------Image Resize for 650*500------------------------------------------*/
					

					 

					/*$config = array( 

					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 

					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/800-800/'.$img_file_name, 

					'maintain_ratio'    => FALSE, 

					'width'             => 650, 

					'height'            => 500, 

					'master_dim' 		=> 'width' 

					); 

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();	*/ 

					 

					 $file_name = $img_file_name;  

					 $this->product_model->updateProductByImages($p_id,$file_name); 

		} 
	
					if(isset($_POST['deleteproductsimages']))
					{
				 $deleteproductsimages = $_POST['deleteproductsimages'];
		 foreach($deleteproductsimages as $deleteproductsimage){
			  if($deleteproductsimage!=''){  
				 $getProductId=$this->product_model->deleteExistProductByID($deleteproductsimage,$p_id);
				}
			}
		}
					/*additional images computer*/
					if(isset($_POST['serverhiddenid']))
					{
						//echo "Starting Error";
						$productImageIds = $_POST['serverhiddenid'];
						$index=0;
						
						if (!is_dir($this->config->item('image_path').'product/')) { 
							mkdir($this->config->item('image_path').'product/', 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'), 0777, true); 
						}
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'), 0777, true); 
						}
						/*---------------------------------------- End Create directory----------------------------------*/

						foreach($productImageIds as $productImageId)
						{

							$productImageExtension= explode('.',$productImageId);

							$additional_file_name = $p_id.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  

							$config['image_library'] = 'gd2'; 

							$this->load->library('image_lib', $config); 

							
					list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$productImageId);
						if($width > $height)
						{
						$orientation = "width";
						}
						else
						{
							$orientation = "height";
						}
					
					/*------------------------original Image Upload----------------------------*/
					$config = array( 
									'source_image'      => $this->config->item('upload_image_path').'uploads/'.$productImageId, 
									'new_image'         => $this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$additional_file_name, 
									'maintain_ratio'    => FALSE, 
									'width'             => $width, 
									'height'            => $height, 
									'master_dim' =>'width' 
									); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
					
					
				$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path250_250 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
				
				/*------------------Image Resize for 250*250------------------------------------------*/
				/*---------------Image Resize for 400*310------------------------------------------*/
				
				$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path400_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
				/*----- ----------Image Resize for 400*310------------------------------------------*/
				/*----------------Image Resize for 650*500------------------------------------------*/
				
				$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path650_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
				
				/*-----------------------Image Resize for 650*500------------------------------------------*/
							$this->product_model->updationProductByImages($p_id,$additional_file_name);
							$index = $index+1;
						}
					}	

			redirect('admin/product/productlist');	
		}
		else
		{
			$this->session->set_flashdata("error_msg","Sale Price must be less than Main Price.");
			$data["page_title"] = "Edit Product | iPengen";
			$data['row'] = $this->product_model->edit_product($p_id); 
			$data['language'] = $this->product_model->get_editlanguage($p_id); 
			$data['galleries']=$this->product_model->exit_id($p_id);
			$data['cat_list'] = $this->product_model->get_category_list();
			$data['brands'] = $this->product_model->get_brand_list();
			$this->load->template('products/edit',$data);
		}
	
	}	    

	public function gallery($p_id = NULL ) { 
	
	$files = $_FILES;   
	$product_img_arr = $_FILES['pimages']['name']; 
	
	$p_name=$this->input->post('get_p_name'); 
	
	$p_img_id=$this->input->post('pimageId[]'); 
	
	for($i=0;$i<count($product_img_arr);$i++) 
	
	{ 
	
		$_FILES['pimages']['name']= $files['pimages']['name'][$i]; 
	
		$_FILES['pimages']['type']= $files['pimages']['type'][$i]; 
	
		$_FILES['pimages']['tmp_name']= $files['pimages']['tmp_name'][$i]; 
	
		$_FILES['pimages']['error']= $files['pimages']['error'][$i]; 
	
		$_FILES['pimages']['size']= $files['pimages']['size'][$i];     
	
					$config = array(); 
	
					$config['upload_path'] = $this->config->item('image_path').'product/'.$p_id.'/originalImages/'; 
	
					$config['allowed_types'] = 'jpg|jpeg|gif|png'; 
	
					$config['file_name'] = $p_id.'_gallery_'.date("Ymdhis"); 
	
					$config['overwrite']     = FALSE; 
	
					$this->upload->initialize($config); 
	
					 
	
					 if ($this->upload->do_upload('pimages')) 
	
						 { 
	
							$img = $this->upload->data(); 
	
							 
	
							 
	
							$this->load->library('image_lib', $config);  
	
							 $config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/400*400/', 
	
							'maintain_ratio'    => TRUE, 
	
							'width'             => 400, 
	
							'height'            => 300, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							 
	
							 
	
							 
	
							$config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/800*800/', 
	
							'maintain_ratio'    => TRUE, 
	
							'width'             => 650, 
	
							'height'            => 500, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							$config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/100*100/', 
	
							'maintain_ratio'    => FALSE, 
	
							'width'             => 250, 
	
							'height'            => 250, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							$file_name = $img['file_name']; 
	
							$get_product_gallery_id=$this->product_model->exit_id($p_id); 
	
							if(is_array($p_img_id) && isset($p_img_id[$i]) && $p_img_id[$i]!="" ) 
	
							{ 
	
								 
	
							$this->product_model->edit_gallery($file_name,$p_img_id[$i]); 
	
							} 
	
							else 
	
							{ 
	
							$this->product_model->insert_gallery($file_name,$p_id); 
	
							} 
	
						 } 
	
				} 
	
			$this->session->set_flashdata('message', 'Galleries updated successfully. '); 
	
				$data['row'] = $this->product_model->edit_product($p_id); 
	
				$data['gallery']=$this->product_model->exit_id($p_id); 
	
			   // $this->load->template('products/edit/'.$id.'#tab_1_2',$data); 
	
				redirect('admin/product/edit/'.$p_id.'#productIMG',$data);	 
	
					exit(); 
	
	}
	/*end function gallery*/ 
	public function search() { 
	
	$search=$this->input->get('searchData'); 
	
	 
	
	$elements = $this->product_model->searchProductByElement($search); 
	
	  
	
	 foreach($elements as $element) 
	
	 { 
	
		 $editUrl = site_url('admin/product/edit/'.$element->product_id); 
	
		$deleteUrl = site_url('admin/product/delete/'.$element->product_id); 
	
		 $imgPath= base_url().'uploads/productImages/'.$element->product_id.'/100*100/'.$element->product_image; 
	
		 $url = site_url('admin/product/view/'.$element->product_id); 
	
		 echo '<div class="col-md-3 col-sm-3 gapbottom_10" > 
	
											<div style="border: 1px solid #dcdbd9;"> 
	
												<div > 
	
												 
	
												 <a href="'.$url.'"><img  src="'.$imgPath.'"  width="100%"  id="product-img"/></a></div> 
	
												  
	
												  
	
												 <div style="height: 90px; border-top: 1px solid #dcdbd9; background-color: rgb(255, 255, 255);"> 
	
												<div class="text-center prodTxt" style="color:#000;font-size: 14px;font-weight: bold; margin-top:20px;">'.$element->product_name.'</div> 
	
												 
	
												 
	
												 <div class="text-center priceTxt" style="font-size:14px;font-weight:bold;color:#fb0b23;">'.$this->config->item('currency').$element->price.'</div> 
	
												  
	
												  
	
												  <div class="btn-group"> 
	
																	<span class="btn green"  data-toggle="dropdown"> 
	
																						  Actions 
	
																		<i class="fa fa-angle-down"></i> 
	
																	</span> 
	
																	<ul class="dropdown-menu"> 
	
																		<li> 
	
																			<a href="'.$editUrl.'"> 
	
																				<i class="fa fa-pencil"></i> Edit </a> 
	
																		</li> 
	
																		<li> 
	
																		   <a href="'.$deleteUrl.'"> 
	
																				<i class="fa fa-trash-o"></i> Delete </a> 
	
																		</li> 
	
																	</ul> 
	
																</div>';     
	
									   
	
												   if($element->stock_quantity=='0' || $element->stock=='0'){ 
	
		echo '<div class="outOfstock" style="color: red; width: 50%; position: absolute; right: 21px; z-index: 100; bottom: 13px; ; background: rgb(222, 231, 222) none repeat scroll 0% 0%; text-align:center; ">out of stock</div>'; 
	
	}                               
	
												  
	
												echo'<input type="hidden" name="chkbox_'.$element->product_id.'" id="chkbox_'.$element->product_id.'" value=""  /> 
	
												  
	
												 </div> 
	
												 
	
											</div> 
	
										</div>'; 
	
	 } 
	
	} 

	public function uploadserverimages()
	{ //print_r($_REQUEST['allimageid']);

	$allimageids = $_REQUEST['allimageid'];

	$i=0;

	foreach($allimageids as $allimageid )

	{

		//$result = $this->product_model->getimagebyproductid($allimageid);

		$imagedivision = explode('/',$allimageid);

		$diffentimage= base_url().$allimageid;

		echo "<div style='position: relative; width: 100px; float: left; margin-right: 6px;' id='".$i."'><img style=height:100px;width:100px; src='".$diffentimage."'  ><input type=hidden value='".$allimageid."' name=serverhiddenid[]/>

		<div style='position: absolute; z-index: 1000; top: 0px; right: 7px; color: red;'><a style='color: red' onclick= productdelete('".$i."','".$imagedivision[2]."');><i class='fa fa-times' aria-hidden='true'></i></a></div></div>";

		$i++;

		}

	}

	public function status($id=false,$value=NULL)
	{
		if($id != false && $value != NULL)
		{
			echo $result = $this->product_model->product_admin_status_change($id,$value);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}
	
	public function feature_status($id=false,$value=NULL)
	{
		if($id != false && $value != NULL)
		{
			echo $result = $this->product_model->product_admin_feature_status_change($id,$value);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}

	public function productview($id=false)
	{
		if ($id!=false) {
			$data["page_title"] = "View Product | iPengen";
			$data['data'] = $this->product_model->getProductDataById($id);
			$data['galary'] = $this->product_model->getProductImgGalaryById($id);
			$data['product_id'] = $id;
			$this->load->template('products/productview',$data);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
		
	}

	/*Method Name:additionalImagedelete
	  model: Dont Use
	  parameter: additionalimageid
	*/
	public function additionalImagedelete(){
					$additionimageid = $this->input->post('additionalimageid');
					$additionimageurl = $this->input->post('imageurl');
					$productid = $this->input->post('productid');
					$data = array('img_id' => $additionimageid);
					$delete = $this->product_model->additionalimagedelete($data);
					if($delete){
						$result['msg'] = 'success';
								unlink($this->config->item('image_path').'product/'.$productid.$this->config->item('thumb_size').$additionimageurl);
								unlink($this->config->item('image_path').'product/'.$productid.
$this->config->item('medium_thumb_size').$additionimageurl);
								unlink($this->config->item('image_path').'product/'.$productid.$this->config->item('large_thumb_size').$additionimageurl);
								unlink($this->config->item('image_path').'product/'.$productid.$this->config->item('original').$additionimageurl);
								
						}
					else{
						$result['msg'] = 'error';

						}
					
					echo json_encode($result);

					}
	

     }/*end class Product */ 

	 