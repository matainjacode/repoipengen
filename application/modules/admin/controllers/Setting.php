<?php 
class Setting extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("setting_model"));
		
	}
	
	function transaction_fee()
	{
		if($this->input->post("transactionSubmit"))
		{
			$fees_val = $this->input->post("transaction_fees");
			$fees_value = array("transaction_fees"=>$fees_val);
			$result = $this->setting_model->add_fees($fees_value);
			if($result)
			{
				$this->session->set_flashdata("succeess_message","Transaction Fee added successfully.");
				redirect("admin/setting/transaction-fee");
			}
		}
		elseif($this->input->post("transactionUpdate"))
		{
			$fees_id = $this->input->post("feesID");
			$fees_val = $this->input->post("transaction_fees");
			$fees_value = array("transaction_fees"=>$fees_val);
			$result = $this->setting_model->update_fees($fees_value, $fees_id);
			if($result)
			{
				$this->session->set_flashdata("succeess_message","Transaction Fee updated successfully.");
				redirect("admin/setting/transaction-fee");
			}
			else
			{
				$this->session->set_flashdata("succeess_message","Transaction Fee updated successfully with same data.");
				redirect("admin/setting/transaction-fee");
			}
		}
		else
		{
			$data["page_title"] = "Transaction | Admin";
			$data["transactiondata"] = $this->setting_model->getAllTransaction();
			if(!empty($data["transactiondata"]))
			{
				foreach($data["transactiondata"] as $list)
				{
					$data["fees_id"] = $list->transaction_fees_id;
					$data["transaction_fees"] = $list->transaction_fees;
				}
			}
			$this->load->template("setting/transaction",$data);
		}
	}
	
	function transaction_delete($fees_id = NULL)
	{
		if($fees_id != NULL)
		{
			$result = $this->setting_model->delete_fees($fees_id);
			if($result)
			{
				$this->session->set_flashdata("succeess_message","Transaction Fee deleted successfully.");
				redirect("admin/setting/transaction-fee");
			}
			else
			{
				$this->session->set_flashdata("error_message","Opps..!!! Something went wrong...Try again.");
				redirect("admin/setting/transaction-fee");
			}
			
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
	
	function admin_configuration()
	{
		if($this->input->post("configSubmit"))
		{
			$adminEmail = $this->input->post("admin_email");
			$supportEmail = $this->input->post("support_email");
			$data = array("admin_email" => $adminEmail, "info_email" => $supportEmail);
			$result = $this->setting_model->add_config($data);
			if($result)
			{
				$this->session->set_flashdata("successMsg","E-Mail Configuration added successfully.");
				redirect("admin/setting/config-email-list");
			}
			else
			{
				$this->session->set_flashdata("adminDeleteMsg","Opps..!!! Something wrong went...Try again.");
				redirect("admin/setting/config-email-list");
			}
		}
		else
		{
			$data["page_title"] = "Configuration | iPengen";
			$this->load->template("setting/configaration",$data);
		}
	}
	
	function config_list()
	{
		$data["page_title"] = "Email Config List | iPengen";
		$data["email_config"] = $this->setting_model->getConfigData();
		$this->load->template("setting/configList",$data);
	}
	function config_edit($configID = NULL)
	{
		if($this->input->post("configUpdateSubmit"))
		{
			$adminEmail = $this->input->post("admin_email");
			$supportEmail = $this->input->post("support_email");
			$data = array("admin_email" => $adminEmail, "info_email" => $supportEmail);
			$result = $this->setting_model->update_config($data, $configID);
			if($result)
			{
				$this->session->set_flashdata("successMsg","E-Mail Configuration updated successfully.");
				redirect("admin/setting/config-email-list");
			}
			else
			{
				$this->session->set_flashdata("successMsg","E-Mail Configuration updated successfully with same data.");
				redirect("admin/setting/config-email-list");
			}
		}
		else
		{
			$data["config_id"] = $configID;
			$data["page_title"] = "Edit Config Email | iPengen";
			$data["configArray"] = $this->setting_model->getEmailDataById($configID);
			if(!empty($data["configArray"]))
			{
				$this->load->template("setting/config-email-edit",$data);
			}
			else
			{
				$this->load->view("errors/ipengen_error/505");
			}
		}
	}
	function config_delete($configID = NULL)
	{
		$result = $this->setting_model->delete_config($configID);
		if($result)
		{
			$this->session->set_flashdata("successMsg","E-Mail Configuration deleted successfully.");
			redirect("admin/setting/config-email-list");
		}
		else
		{
			$this->load->view("errors/ipengen_error/505");
		}
	}
	function shipping_charge()
	{
		if($this->input->post("ShippingSubmit"))
		{
			$shippingVal = $this->input->post("shipping_val");
			$db_data = array( "shipping_fees" => $shippingVal);
			$result = $this->setting_model->add_shippingData($db_data);
			if($result)
			{
				$this->session->set_flashdata("successMsg","Shipping Charge added Successfully.");
				redirect("admin/setting/shipping-charge");
			}
			else
			{
				$this->session->set_flashdata("adminDeleteMsg","Opps..!!! Something wrong went...Try again");
				redirect("admin/setting/shipping-charge");
			}
		}
		elseif($this->input->post("ShippingUpdate"))
		{
			$shippingID = $this->input->post("shipping_id");
			$shippingVal = $this->input->post("shipping_val");
			$db_data = array( "shipping_fees" => $shippingVal);
			$result = $this->setting_model->update_shippingData($db_data, $shippingID);
			if($result)
			{
				$this->session->set_flashdata("succeess_message","Shipping Charge updated successfully.");
				redirect("admin/setting/shipping-charge");
			}
			else
			{
				$this->session->set_flashdata("succeess_message","Shipping Charge updated successfully with same data.");
				redirect("admin/setting/shipping-charge");
			}
		}
		else
		{
			$data["page_title"] = "Shipping Charge | iPengen";
			$data["shippingData"] = $this->setting_model->get_shippingData();
			if(!empty($data["shippingData"]))
			{
				foreach($data["shippingData"] as $list)
				{
					$data["shipping_id"] = $list->shipping_id;
					$data["shipping_charge"] = $list->shipping_fees;
				}
			}
			$this->load->template("setting/shipping-charge",$data);
		}
	}
	function sheeping_delete($shippingID = NULL)
	{
		if($shippingID != NULL)
		{
			$result = $this->setting_model->delete_shipping_charge($shippingID);
			if($result)
			{
				$this->session->set_flashdata("successMsg","Shipping Charge deleted successfully.");
				redirect("admin/setting/shipping-charge");
			}
			else
			{
				$this->session->set_flashdata("adminDeleteMsg","Opps..!!! Something went wrong...Try again");
				redirect("admin/setting/shipping-charge");
			}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
}
?>