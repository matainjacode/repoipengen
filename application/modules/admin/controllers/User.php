<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends MX_Controller{
	
	function __construct()
	{
		parent::__construct('pagination');
		$this->load->library('form_validation');

	    $this->load->model('user_model'); 
		if(!$this->session->userdata('logged_in')){redirect('admin/login');}
	}
	function index()
	{
		//$this->load->view("admin/dashboard/index");
		//$this->load->template('admin/dashboard/index');
		redirect('admin/index');
	}
	public function userlist()
	{
		$result['page_title']   = "Users List | iPengen";
		$result['result']   = $this->user_model->userlist();
		$this->load->template('admin/user/userlist',$result);
	}
   public function edit($id)
	{
		if(!empty($_POST))
		{
			$this->form_validation->set_rules('fname', 'First Name', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lname', 'Last Name', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('email', 'Email', 'required');
			if ($this->form_validation->run()==TRUE) {
				$result   = $this->user_model->updateuser($id);
				if($result){
					$this->session->set_flashdata('sussess_message', 'Updated successfully');
					redirect('admin/user/userlist');
				}else
				{
					$this->session->set_flashdata('error_message', 'Updated error');
					redirect('admin/user/userlist');
				}
				}
				
			else
				{
					$this->session->set_flashdata('error_message', 'You have got an error');
					$result['page_title']   = "Edit User | iPengen";
					$result['result']   = $this->user_model->userinfo($id);
					$this->load->template('admin/user/useredit',$result);
				}
		}
		else
		{
			$result['page_title']   = "Edit User | iPengen";
			$result['result']   = $this->user_model->userinfo($id);
			$this->load->template('admin/user/useredit',$result);
		}
	}
 public function view($id)
	{
		$result['page_title']   = "View User | iPengen";
		$result['result']   = $this->user_model->userinfo($id);
		$this->load->template('admin/user/views',$result);
	}
	 public function delete($id)
	{
		
			$result   = $this->user_model->delete($id);
			if($result){
			$this->session->set_flashdata('sussess_message', 'Deleted successfully');
			redirect('admin/user/userlist');
	        }
	}
	 public function isblock($id)
	{
		
			$result   = $this->user_model->isblock($id);
			if($result){
			$this->session->set_flashdata('sussess_message', 'Updated successfully');
			redirect('admin/user/userlist');
	        }
	}
	 public function isnotblock($id)
	{
		
			$result   = $this->user_model->isnotblock($id);
			if($result){
			$this->session->set_flashdata('sussess_message', 'Updated successfully');
			redirect('admin/user/userlist');
	        }
	}
	 public function resetpassword()
	{
		
		if(!empty($_POST)){
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|max_length[50]');
			$this->form_validation->set_rules('cpassword', 'Confirm password', 'required|min_length[4]|max_length[50]|matches[password]');
			if ($this->form_validation->run()==TRUE) {
		
			$result   = $this->user_model->resetpassword($id);
			if($result){
			$this->session->set_flashdata('sussess_message', 'Password changed successfully');
			redirect('admin/user/userlist');
	        }
				}else{
					$this->session->set_flashdata('error_message', 'Password does not change');
			        redirect('admin/user/userlist');
				}
	}
     }
	
	 public function wishlist($id){
			$data['page_title'] = "User Wishlist | iPengen";
			$data['userdetails'] = $this->user_model->userinfo($id);
			$data['wishlist'] = $this->user_model->wishlist($id);
			$this->load->template('admin/user/userwishlist',$data);
			
								  }

}


?>