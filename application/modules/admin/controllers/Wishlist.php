<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends MX_Controller{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')){redirect('admin/login');}
	    $this->load->model(array('wishlist_model','event_model')); 
	}

	public function index()
	{
		$data['page_title'] = "Wishlist List | iPengen";
		$data['wishlist'] = $this->wishlist_model->wishlist();
		$this->load->template('admin/wishlist/wishlist',$data);
	}
	 
	function view($wishlistID = NULL)
	{
		if($wishlistID != NULL)
		{
			$data["wishlistData"] = $this->wishlist_model->wishlistGetId($wishlistID);
			$data["wishlistProduct"] = $this->wishlist_model->wishlistGetWithProductId($wishlistID);
			//$data["eventlist"] = $this->event_model->event_list();
			if(!empty($data["wishlistData"]))
			{
				$data["page_title"] = "Wishlist View | iPengen";
				$data["wishlistId"] = $wishlistID;
				$this->load->template('admin/wishlist/wishlistview',$data);
			}
			else
			{
				$this->load->view("errors/ipengen_error/505");
			}
			
		}
	}
	
	function status($wishlist = NULL, $value = NULL)
	{
		if($wishlist != NULL && $value != NULL)
		{
			echo $this->wishlist_model->update_status($wishlist, $value);
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
}