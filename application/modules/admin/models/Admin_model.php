<?php
class Admin_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	public function insert_admin($data)
	{$query= $this->db->insert('admin_login',$data);
	 if($query){return $this->db->insert_id();}
	 else {return false;}
	}
	public function getAdminById($id)
	{
		 $query= $this->db->get_where('admin_login',array('log_id'=>$id));
	 if($query->num_rows()>0){return $query->row();}
	 else {return false;}
	}
	public function edit_admin($id , $data)
	{         $this->db->where('log_id',$id);
		$query = $this->db->update('admin_login',$data);
	 if($query){return TRUE;}
	 else {return false;}
	}
	public function delete_admin($id)
	{         $this->db->where('log_id',$id);
		$query = $this->db->delete('admin_login');
	 if($query){return TRUE;}
	 else {return false;}
	}
	 public function enable_admin($adminId)
	 {
		$this->db->where('log_id',$adminId);
		$queryid = $this->db->update('admin_login',array('status'=>'1'));
		return $queryid;
		 }
	public function disable_admin($adminId)
	 {$this->db->where('log_id',$adminId);
		$queryid = $this->db->update('admin_login',array('status'=>'0'));
		return $queryid;
		 }	
}
?>