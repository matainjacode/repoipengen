<?php
class Adminrevenue_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	


function get_all_revenue()
	{
		$query = $this->db->select("oi.order_id,w.title,SUM(oi.transaction_amt) AS amount")
						  ->from("ig_order_items oi")
						  ->join('ig_wishlist w', 'oi.wishlist_id = w.id')
						  ->where(array("oi.item_type"=>"cash_gift"))
						  ->group_by("oi.order_id")
						  ->get();

			$result = $query->result();
			return $result;

	}

		
}
?>