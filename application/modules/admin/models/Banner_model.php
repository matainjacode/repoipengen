<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends CI_Model {
	
	 function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	public function banner_add($data){
		
		$this->db->insert('ig_banner', $data);
	    $last_id = $this->db->insert_id();
		
		return $last_id;

		
		
		}
		
    public function banner_list(){
		
		$query= $this->db->get('ig_banner');
		$bannerlist   = $query->result();        
		return $bannerlist;
		
		}
	public function bannerdelete($banner_id)
	{
			$this->db->delete('ig_banner',array("banner_id"=>$banner_id)); 
			return $this->db->affected_rows();
	}
		
    public function bannerupdate($data,$banner_id)
	{
		$this->db->update('ig_banner', $data, array("banner_id"=>$banner_id)); 
		return $this->db->affected_rows();
	}
		
	public function bannerImagelist($data){
		
		$this->db->where('banner_id', $data['banner_id']);
		$query= $this->db->get('ig_banner');
		$bannerlist   = $query->result();     
		   
		return $bannerlist;
		
		
	}
	function getBannerImageNameById($banner_id)
	{
		$query = $this->db->get_where("ig_banner",array("banner_id"=>$banner_id));
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $data)
			{
				$imageName = $data->banner_image;
			}
		}
		else
		{
			$imageName = "";
		}
		return $imageName;
	}
	
	
}