<?php

class Block_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	function language_list()
	{
		$query=$this->db->get_where('ig_language', array('status'=>1)); 
		if($query){
			return $query->result();
		}else{
			return FALSE;
		}
    }
		
    function insert($result)
	{  
	  if($result!='')
	  {    
		  $data=array('block_status'=>$this->input->post('state'));
		  $sql=$this->db->insert('ig_static_block', $data); 
		  $last_id=$this->db->insert_id();
		  $i=0;
		  $nameArr=$this->input->post('block_name');
		  $block_descriptionArr=$this->input->post('block_description');
		  $langArr=$this->input->post('langId');
		   
		  foreach($nameArr as $val)
		  {
			 $dataArr=array('block_id'=>$last_id,
							'lang_id'=>$langArr[$i],
							'block_name'=>$val,
							'block_key'=>strtolower(str_replace(' ','-',$val)),
							'block_description'=>$block_descriptionArr[$i],
			 );
			 $query=$this->db->insert('ig_static_block_text', $dataArr); 
			 $i++;
		  }
		  if($query){
			return $last_id;
		  }else{
			return FALSE;
		  }
	  }
	}
	  
	function delete_block($id)
	{
	 $this->db->where('block_id', $id);
     $query= $this->db->delete('ig_static_block_text'); 
	 if($query){
			$this->db->where('block_id', $id);
            $q=$this->db->delete('ig_static_block');
            if($q){
				return true;
			}			
		    else{
			  return FALSE;
		    }
	  }
	}
  
    function getblockById($id)
	{
		$this->db->select('ig_static_block.*,ig_static_block_text.*,ig_language.language_id,ig_language.name as lang_name');
		$this->db->from('ig_static_block');
		$this->db->join('ig_static_block_text','ig_static_block.block_id=ig_static_block_text.block_id','right');
		$this->db->join('ig_language','ig_static_block_text.lang_id=ig_language.language_id');
		$this->db->where(array('ig_static_block_text.block_id'=>$id));
		$query=$this->db->get(); 
		if($query){
			return $query->result();
		}else{
			return FALSE;
		}
	}
		
	function update_details($id)
	{       
	  if($id!='')
	  {
		   $block_status=$this->input->post('state');
		   $data=array('block_status' => $block_status);
		   $this->db->where('block_id', $id);
		   $query=$this->db->update('ig_static_block', $data); 
	
			$i=0;
			$descripArr=$this->input->post('block_description');
			$langArr=$this->input->post('language');
			$nameArr=$this->input->post('block_name');
			 foreach($nameArr as $val)
			 {
				 $dataArr=array(
								'block_name'=>$val,
								'block_description'=>$descripArr[$i]
														
				 );
				 $this->db->where(array('lang_id'=>$langArr[$i],'block_id'=>$id));
				 $sql=$this->db->update('ig_static_block_text', $dataArr); 
				 $i++;
			 }
			if($sql){
				return true;
			}else{
				return FALSE;
			}
	  }
	 
	}
 
	  
	function index2()
	{   
		$this->db->select('*');
		$this->db->from('ig_static_block');
		$this->db->join('ig_static_block_text','ig_static_block.block_id=ig_static_block_text.block_id');
		$this->db->where(array('ig_static_block_text.lang_id'=>1));
		$query=$this->db->get(); 
		if($query){
			return $query->num_rows() > 0 ? $query->result_array() : array();;
		}else{
			return FALSE;
		}
	}
		
	function update_state($block_key,$state)
	{
		  if( $block_key!='' || $state!='' )
		  {       
				$data = array('state' => $state);
				$this->db->update('ig_static_block', $data,array('block_key'=>$block_key)) ;
				return  $this->db->affected_rows(); 
		  }
	 }
	 
	function enable_block($id)
	{
		$this->db->where('block_id', $id);
		$this->db->update('ig_static_block',array('block_status'=>1));
		
		if($this->db->affected_rows()){
				return true;
			}else{
				return FALSE;
			}
    }
	
    function disable_block($id)
	{
		$this->db->where('block_id', $id);
		$this->db->update('ig_static_block',array('block_status'=>0)); 
		if($this->db->affected_rows()){return true;}else{return FALSE;}
    }
}	

?>