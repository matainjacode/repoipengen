<?php
/**
* 
*/
class Blog_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function laguage_list(){
	
	$query=$this->db->get('ig_language'); 
	if($query){
			return $query->result();
		}else{
			return FALSE;
		}
	
	}
	function add_blog()
	{
		$language=$this->input->post("language");
		$blog_title=$this->input->post("blog_title");
		$blog_description=$this->input->post("blog_description");
		
		$data1=array('blog_link_video'=>$this->input->post("blog_link"));
		$this->db->insert("ig_blog",$data1);
		$id=$this->db->insert_id();
	
		$i=0;
		foreach($language as $val)
		{
			if(strpos($blog_title[$i], " ") !== false){
				$blog_auto_title[$i] = str_replace(' ','-',strtolower($blog_title[$i]));
			}else{
				$blog_auto_title[$i] = strtolower($blog_title[$i]);
			}
			
			$data=array(
						'blog_id'=>$id,
						'blog_title'=>$blog_title[$i],
						'blog_auto_title'=>$blog_auto_title[$i],
						'blog_description'=>$blog_description[$i],
						'blog_language'=>$val						
			);
			$i++;
			$this->db->insert("ig_blog_text",$data);
		}
		
		return $id;
	}
	
  public function update_blog($id)
	{
		$language=$this->input->post("language");
		$blog_title=$this->input->post("blog_title");
		$blog_description=$this->input->post("blog_description");
		$data1=array('blog_link_video'=>$this->input->post("blog_link"));
		$this->db->where(array("blog_id"=>$id));
		$this->db->update("ig_blog",$data1);
		
	
		$i=0;
		foreach($language as $val)
		{
			
			$data=array(
						
						'blog_title'=>$blog_title[$i],
						'blog_description'=>$blog_description[$i],
											
			);
			$i++;
			$this->db->where(array("blog_id"=>$id,'blog_language'=>$val));
			$this->db->update("ig_blog_text",$data);
		}
		
		return true;
	}
	
	
	function get_all_blog()
	{
		
		$this->db->select('*');
		$this->db->from("ig_blog");
		$this->db->join("ig_blog_text",'ig_blog.blog_id=ig_blog_text.blog_id');
		$this->db->where(array('ig_blog_text.blog_language'=>1));
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	/*function blog_data_for_edit($id)
	{
		$this->db->select('*');
		$this->db->from("ig_blog");
		$this->db->join("ig_blog_text",'ig_blog.blog_id=ig_blog_text.blog_id');
		$this->db->where(array('ig_blog.blog_id'=>$id));
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}*/
	
	function blog_data_for_edit($id)
	{
		$this->db->select('*');
		$this->db->from('ig_blog');
		$this->db->join('ig_blog_text','ig_blog.blog_id=ig_blog_text.blog_id','right');
		$this->db->join('ig_language','ig_blog_text.blog_language=ig_language.language_id');
		$this->db->where(array('ig_blog.blog_id'=>$id));
		$query=$this->db->get(); 
		if($query){
				return $query->result();
			}else{
				return array();
			}
	}
	function update_city($data,$cityCode)
	{
		$this->db->update("ig_city",$data,array("city_id"=>$cityCode));
		return $this->db->affected_rows();
	}
	function deleteblogById($id)
	{
		$this->db->delete("ig_blog",array("blog_id"=>$id));
		$this->db->delete("ig_blog_text",array("blog_id"=>$id));
		return $this->db->affected_rows();
	}
	function StatusChange($id, $value)
	{
		$this->db->update("ig_blog",array("blog_status"=>$value),array("blog_id"=>$id));
		return $this->db->affected_rows();
	}
	function display_StatusChange($id, $value)
	{
		$this->db->update("ig_blog",array("display_status"=>$value),array("blog_id"=>$id));
		return $this->db->affected_rows();
	}
		function update_blog_img($img_name,$id)
	{
		$this->db->update("ig_blog",array("blog_image"=>$img_name),array("blog_id"=>$id));
		return $this->db->affected_rows();
	}
	public function getBlogById($id)
	{
		$query = $this->db->get_where("ig_blog", array("blog_id" => $id));
		return $query->num_rows() > 0 ? $query->result() : array();
	}
}
?>