<?php
class Brand_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

    function add_brand($data)
    {
    	return $this->db->insert('ig_brand',$data);
    }

    function get_all_brand()
    {
    	$db_query = $this->db->get('ig_brand');
    	return $db_query->num_rows() > 0 ? $db_query->result() : array();
    }

    function getBrandDataById($brandId)
    {
    	$db_query = $this->db->get_where('ig_brand',array('brand_id'=>$brandId));
    	return $db_query->num_rows() > 0 ? $db_query->result() : array();
    }

    function update_event($brandId,$data)
    {
    	$this->db->update('ig_brand',$data,array('brand_id'=>$brandId));
    	return $this->db->affected_rows();
    }

    function delete_brand($brandId)
    {
    	$this->db->delete('ig_brand',array('brand_id'=>$brandId));
    	return $this->db->affected_rows();
    }

    function update_status($brandId,$value)
    {
    	$this->db->update('ig_brand',array('brand_status'=>$value),array('brand_id'=>$brandId));
    	return $this->db->affected_rows();
    }


}
?>