<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model 
{
	private $category_parent_path_array = [];
	private $category_parent_path = "Root";
	private $category_name = '';
	private $category_id = '';
	private function _get_category_path($categoryList)
	{
		$this->_get_parent_path($categoryList);
		return $this->category_parent_path_array;
	}
	
	private function _get_parent_path($listArray)
	{
		
		foreach($listArray as $list)
		{
			$parentId = $list["parent_id"];
			$categoryName = $list["name"];
			if($this->category_id == "")
			{$this->category_id = $list["id"];}
			if(empty($this->category_parent_path_array[$this->category_id]))
			{
				$this->category_parent_path_array[$this->category_id] = $list;
			}
			if($this->category_name != "")
			{$total_cat_name = " -> ".$this->category_name;}
			else
			{$total_cat_name = "";}
			
			if($parentId == 0)
			{
				$this->category_name = " -> ".ucfirst($categoryName).$total_cat_name;
				$this->category_parent_path .= $this->category_name;
				$this->category_parent_path_array[$this->category_id]["path"] = $this->category_parent_path ;
				
				$this->category_parent_path = "Root";
				$this->category_name = "";
				$this->category_id = ""; 
			}
			else
			{
				$this->category_name = ucfirst($categoryName).$total_cat_name;
				//$query = $this->_get_parent_data($parentId);
				//$this->_get_parent_path($query);
			}
			
		}
	}
	
	private function _get_parent_data($c_id)
	{
			$this->db->select('
						ipengen_category.id,
						ipengen_category.banner,
						ipengen_category.status,
						ipengen_category.parent_id,
						ipengen_category.megamenu_status,
						ig_category_details.name,
						');
			$this->db->from('ipengen_category');
			$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
			$this->db->where(array('ipengen_category.id'=>$c_id,'ig_category_details.lang'=>1)); 
			$query=$this->db->get();
			return $query->num_rows() > 0 ? $query->result_array() : array();
	}
public function insert_category(){
	
	$parent_id = $this->input->post('parent_id');
	$string = '';
	
	$data=array('parent_id'=>$parent_id,
					'created_date'=>date('Y-m-d H:i:s'),
					'modification_date'=>date('Y-m-d H:i:s'),
					'path'=>$string
					);
	$query=$this->db->insert('ipengen_category', $data); 
	
	$last_id=$this->db->insert_id();
	$i=0;
	$descripArr=$this->input->post('description');
	$langArr=$this->input->post('language');
	$nameArr=$this->input->post('name');
	 foreach($nameArr as $val)
	 {
		 $dataArr=array('cat_id'=>$last_id,
		                'name'=>$val,
						'description'=>$descripArr[$i],
						'lang'=>$langArr[$i]		 				
		 );
		 $query=$this->db->insert('ig_category_details', $dataArr); 
		 $i++;
	 }
	
	if($query){
			return $last_id;
		}else{
			return FALSE;
		}
	
}

public function category_name($id){
	
	$this->db->select('name');
	$this->db->from('ig_category_details');
	$this->db->where(array('cat_id'=>$id,'lang'=>1)); 
	$query=$this->db->get();
	return $query->num_rows() > 0 ? $query->row() : '';
	
}

public function get_category_path($id){
	
	$this->db->select('path');
	$this->db->from('ipengen_category');
	$this->db->where(array('id'=>$id)); 
	$query=$this->db->get();
	return $query->num_rows() > 0 ? $query->row() : '';
	
}

public function category_edit($id){
	
	

	$data=array('parent_id'=>$this->input->post('parent_id'),
				'modification_date'=>date('Y-m-d H:i:s')
						);
	$this->db->where('id', $id);
	$query=$this->db->update('ipengen_category', $data); 
	
	$i=0;
	$descripArr=$this->input->post('description');
	$langArr=$this->input->post('language');
	$nameArr=$this->input->post('name');
	 foreach($nameArr as $val)
	 {
		 $dataArr=array(
		                'name'=>$val,
						'description'=>$descripArr[$i]
								 				
		 );
		 $this->db->where(array('lang'=>$langArr[$i],'cat_id'=>$id));
		 $query=$this->db->update('ig_category_details', $dataArr); 
		 $i++;
	 }
	if($query){
			return true;
		}else{
			return FALSE;
		}
	
}
public function category_list(){ 
	
	$this->db->select('
					ipengen_category.id,
					ipengen_category.banner,
					ipengen_category.status,
					ipengen_category.parent_id,
					ipengen_category.megamenu_status,
					ipengen_category.path,
					ig_category_details.name,
					');
	$this->db->from('ipengen_category');
	$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
	$this->db->where(array('ig_category_details.lang'=>1));
	$query=$this->db->get();
	
	if($query){
			if($query->num_rows() > 0){
				$result = $query->result_array();
				foreach($result as $res){
					$category_path = $res['path'];
					if($category_path == ''){
						$parent_id = $res['parent_id'];	
						if($parent_id != 0){
							$catgry = $this->category_name($parent_id);
							$category_name = $catgry->name;
							$parent_details = $this->getcategoryById($parent_id);
							foreach($parent_details as $parent){
								
								$category_id = $parent->parent_id;	
								
								if($category_id == 0){
									$string = 'Root->'.ucfirst($category_name).'->'.ucfirst($res['name']);
								}
							}
						}else{
							$string = 'Root->'.ucfirst($res['name']);
							
						}
						
						$res['path'] = $string;
					}
					
					
					$content[] = $res;	
				}
				return $content;				
			}
		}else{
			return FALSE;
		}
	
}
public function category_list_status($id){
	$this->db->select('ipengen_category.id,ipengen_category.parent_id,ig_category_details.name');
	$this->db->from('ipengen_category');
	$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
	$this->db->where(array('ipengen_category.parent_id'=>$id,'ig_category_details.lang'=>1));
	$this->db->order_by("ig_category_details.name","asc");
	$query=$this->db->get(); 
	if($query){
			return $query->result();
		}else{
			return FALSE;
		}
	
}
public function getcategoryById($id){
	
	//$query=$this->db->get_where('ipengen_category', array('id'=>$id)); 
	$this->db->select('ipengen_category.id,ipengen_category.banner,ipengen_category.status,ipengen_category.parent_id,ig_category_details.name,ig_category_details.description,ig_language.language_id,ig_language.name as lang_name');
	$this->db->from('ipengen_category');
	$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id','right');
	$this->db->join('ig_language','ig_category_details.lang=ig_language.language_id');
	$this->db->where(array('ig_category_details.cat_id'=>$id));
	$query=$this->db->get(); 
	if($query){
			return $query->result();
		}else{
			return FALSE;
		}
	
}
public function laguage_list(){
	
	$query=$this->db->get_where('ig_language', array('status'=>1)); 
	if($query){
			return $query->result();
		}else{
			return FALSE;
		}
	
}
public function delete_category($id){
	
	$this->db->where('id', $id);
     $query= $this->db->delete('ipengen_category'); 
	 $this->db->where('cat_id', $id);
     $query= $this->db->delete('ig_category_details'); 
	if($query){
			return true;
		}else{
			return FALSE;
		}
	
}
public function enable_category($id){
	
	$this->db->where('id', $id);
    $this->db->update('ipengen_category',array('status'=>1));
	
	if($this->db->affected_rows()){
			return true;
		}else{
			return FALSE;
		}
	
}
public function disable_category($id){
	
	$this->db->where('id', $id);
	$this->db->update('ipengen_category',array('status'=>0)); 
	if($this->db->affected_rows()){return true;}else{return FALSE;}
	
}
	public function enable_mega_category($id)
	{
		$this->db->update('ipengen_category',array('megamenu_status'=>'1'),array('id'=>$id));
		if($this->db->affected_rows())
		{return 1;}else{return 0;}
	}
	public function disable_mega_category($id)
	{
		$this->db->update('ipengen_category',array('megamenu_status'=>'0'),array('id'=>$id));
		if($this->db->affected_rows())
		{return 1;}else{return 0;}
	}
	
	public function getcatParentId($id){
		$this->db->select('parent_id');
		$this->db->from('ipengen_category');
		$this->db->where(array('id'=>$id)); 
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			$category = $query->row();
			$parent_id = $category->parent_id;
		}
		return $parent_id;
		
	}
	
}
?>