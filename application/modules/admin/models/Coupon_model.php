<?php
/**
* 
*/
class Coupon_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function coupon_add($data)
	{
		$this->db->insert("ig_coupon",$data);
		return $this->db->affected_rows();
	}
	function getAllList()
	{
		$query = $this->db->get("ig_coupon");
		return $query->num_rows() > 0 ? $query->result() : array();
		
	}
	function getCouponQueryById($couponId)
	{
		$query = $this->db->get_where("ig_coupon",array("coupon_id"=>$couponId));
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function delete_coupon($couponId)
	{
		$this->db->delete("ig_coupon",array("coupon_id"=>$couponId));
		return $this->db->affected_rows();
	}
	function coupon_update($data,$couponId)
	{
		$this->db->update("ig_coupon",$data,array("coupon_id"=>$couponId));
		return $this->db->affected_rows();
	}
	
}
?>