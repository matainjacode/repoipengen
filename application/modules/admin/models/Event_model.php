<?php
class Event_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	public function get_lang_data()
	{
		$query = $this->db->get("ig_language");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function insert_event($data)
	{$query= $this->db->insert('ig_event_category',$data);
	 if($query){return $this->db->insert_id();}
	 else {return false;}
	}
	public function insert_event_text($data)
	{
		$query = $this->db->insert("ig_event_category_text",$data);
		if($query)
		{return true;}else{return false;}
	}
	public function getEventById($id)
	{
		 $query= $this->db->select("
		 					ig_event_category.id as event_id,
							ig_event_category.image_name as event_image,
							ig_event_category_text.id as eventtextid,
							ig_event_category_text.event_name as event_name,
		 							")
		 					->from("ig_event_category")
							->where(array("ig_event_category.id"=>$id))
							->join("ig_event_category_text","ig_event_category_text.event_category_id = ig_event_category.id")
							->get();
	 if($query->num_rows()>0){return $query->result();}
	 else {return array();}
	}
	public function event_list()
	{
		$query= $this->db->select("
		 					ig_event_category.id as event_id,
							ig_event_category.image_name as image_name,
							ig_event_category.feature_status as feature_status,
							ig_event_category.status as status,
							ig_event_category_text.id as event_text_id,
							ig_event_category_text.event_name as name,
							ig_event_category_text.lang_id as lang_id,
		 							")
		 					->from("ig_event_category")
							->where(array("ig_event_category_text.lang_id"=>1))
							->join("ig_event_category_text","ig_event_category_text.event_category_id = ig_event_category.id")
							->order_by("ig_event_category_text.event_name","asc")
							->get();
	 if($query->num_rows()>0){return $query->result();}
	 else {return array();}
	}
	public function edit_event($id , $data)
	{        
		$query = $this->db->update('ig_event_category',$data,array('id'=>$id));
	 	if($this->db->affected_rows() == 1)
	 	{return TRUE;}
		 else 
		{return false;}
	}
	public function update_event_text($data, $id)
	{
		$query = $this->db->update('ig_event_category_text',$data,array('id'=>$id));
	 	return $this->db->affected_rows();
	}
	public function getPreviousImagebyId($id)
	{
		$db_query = $this->db->get_where('ig_event_category',array('id'=>$id));
		if($db_query->num_rows() > 0)
		{
			foreach($db_query->result() as $eventData)
			{
				$image_name = $eventData->image_name;
			}
			return $image_name;
		}
		else
		{
			return '';
		}
	}
	public function delete_event($id)
	{         $this->db->where('id',$id);
		$query = $this->db->delete('ig_event_category');
	 if($query){return TRUE;}
	 else {return false;}
	}
	public function enable_event($Id)
	{
		$this->db->where('id',$Id);
		$queryid = $this->db->update('ig_event_category',array('status'=>'1'));
		return $queryid;
	}
	public function disable_event($Id)
	{
		$this->db->where('id',$Id);
		$queryid = $this->db->update('ig_event_category',array('status'=>'0'));
		return $queryid;
	}
	public function enable_feature_event($id)
	{
		$this->db->update('ig_event_category',array("feature_status"=>"1"),array("id"=>$id));
		if($this->db->affected_rows())
		{return 1;}else{return 0;}
	}
	public function disable_feature_event($id)
	{
		$this->db->update('ig_event_category',array("feature_status"=>"0"),array("id"=>$id));
		if($this->db->affected_rows())
		{return 1;}else{return 0;}
	}	
}
?>