<?php
class Order_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	

		public function getOrderDetails($oid)
		{
			$this->db->select('ig_order.*,ig_order.status as stuts,ig_order_items.*,ig_products.*,ig_user.*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id', 'left');
			$this->db->join('ig_user', 'ig_user.id = ig_order.user_id', 'left');
			$this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id', 'left');
			//$this->db->join('ig_products_text', 'ig_products_text.product_id = ig_order_items.product_id', 'left');
			$this->db->where('ig_order.order_id',$oid);
			
			
			$query = $this->db->get();
			$result = $query->result();
			return $result;			
		}
		public function change_status($orderid,$status,$remark){
			
			$this->db->select('status');
			$this->db->from('ig_order');
			$this->db->where(array('order_id'=>$orderid));
			$query = $this->db->get();
			$result = $query->row();
		
			if($status!=$result->status){			
			$this->db->where(array('order_id'=>$orderid));
			$this->db->update('ig_order',array('status'=>$status));
			
			$date=date_create(date('Y-m-d H:i:s', time()));
        $date_time= date_format($date,"Y/m/d H:i:s");
		$data=array(
		'order_id'=>$orderid,
		'status'=>$result->status,
		'remark'=>$remark,
		'orderchange_date'=>$date_time
		
		);
		$this->db->insert('ig_order_status_history',$data);
	
			}
		return true;	
		}
		public function getOrderDetailshistory($orderid){
			$this->db->where(array('order_id'=>$orderid));
		$query=	$this->db->get('ig_order_status_history');
			return $query->result();
		}

	function get_all_orders()
	{
		$query = $this->db->select("
							ig_order.order_id as orderID,
							ig_user.fname as client_fname,
							ig_user.lname as client_lname,
							ig_order.total as total_amount,
							ig_order.date_added as order_date,
							ig_transaction.transaction_status as transaction_status,
							ig_order.status as order_status,
									")
						  ->from("ig_order")
						  ->where(array("ig_order.language_id"=>"1"))
						  ->join("ig_transaction","ig_transaction.order_id = ig_order.order_id")
						  ->join("ig_user","ig_user.id = ig_order.user_id")
						  ->get();
		//echo $this->db->last_query();
		//die();
		return $query->num_rows() > 0 ? $query->result() : array();
		//print_r($query->result());
		//die();
	}
	public function getUserData($orderId = NULL)
	{
		$query =	$this->db->select("
								ig_order.order_id as order_id,
								ig_order.payment_firstname as fname,
								ig_order.payment_lastname as lname,
								ig_user.mobile as mobile,
								ig_user.email as userEmail,
								ig_order.payment_address_1 as address,
								ig_order.payment_address_2 as address_1,
								ig_order.payment_city as city,
								ig_order.payment_postcode as postCode,
										")
							 ->from("ig_order")
							 ->where(array("ig_order.order_id"=>$orderId))
							 ->join("ig_user","ig_user.id = ig_order.user_id")
							 ->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function support_data()
	{
		$query = $this->db->get("ig_setting");
		return $query->num_rows() > 0 ? $query->result() : array();
	}

}
?>