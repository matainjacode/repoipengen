<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends CI_Model
{
	function __construct()
	{ 
		parent::__construct();
		$this->load->database();
	}
	public function insert_products($data)
	{  
		$this->db->insert('ig_products',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}	
		public function insert_producttext($datatext)
	    {  

             $this->db->insert_batch('ig_products_text', $datatext); 
			
			}		
	public function updateProductByImages($p_id,$images)
	    {  $this->db->where('product_id',$p_id);
		   $data=array('product_image' => $images);
		   $this->db->update('ig_products',$data);
			} 		
	public function insert_uploaded_gallery($gallery, $id)
	    {
				$insert_data=array('product_id' => $id,
				               'image_name' =>$gallery );
			$this->db->insert('ig_product_img_gallery',$insert_data);
			}
	public function getProductById($id){
        $q = $this->db->get_where('ig_products', array('product_id' => $id), 1);
        if ($q->num_rows() > 0) {
			foreach($q->result() as $row){
				$data = $row;
			}
			return $data;
        }
        return FALSE;
	   }
	public function getCustomers(){
		$this->db->select('customer_id,first_name,last_name,email,contact_no,address,city,post_code,company,status');
		$this->db->from('customer_details');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
			foreach($q->result() as $row){
				$data[] = $row;
			  }
			 return $data;
           }
          return FALSE;
	    }
	public function assignUsersToProduct($customer_id,$product_id){
		$data = array('customer_id' => $customer_id, 'product_id' => $product_id, 'product_name'=>'','product_image'=>'','assign_status' => '1');
		$this->db->insert('assign_products',$data);
		$assign_id = $this->db->insert_id();
		if($assign_id > 0){
			$q = $this->db->get_where('customer_details', array('customer_id' => $customer_id), 1);
			if ($q->num_rows() > 0) {
				foreach($q->result() as $row){
					$user_data = $row;
				}
				return $user_data;
			}
		  }
	    }	
	public function list_products()
	   {
			$this->db->select('*');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->where('ig_products_text.language_id', 1);
			$query = $this->db->get();
			 return $query->num_rows() > 0 ? $query->result() : array(); 
			
	   }
      public function get_language()
	   {
		$this->db->select('*'); 
		$query  = $this->db->get('ig_language');
        return $query->result();
	   } 
	   
	   
	public function delete_customer($id,$product_id){
		$this->db->where('customer_id', $id);
		$this->db->where('product_id', $product_id);
		if($this->db->delete('assign_products')){ return true;}
	    }
	public function getCustomerById($id){
		
		$this->db->select('customer_id,first_name,last_name,company,status');
		$this->db->from('customer_details');
		$this->db->where('customer_id',$id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
			foreach($q->result() as $row){
				$data = $row;
			}
			return $data;
        }
        return FALSE;
	   }	
	public function getassignCustomers($product_id){
		$this->db->select('*');
		$this->db->from('assign_products');
		$this->db->where('product_id',$product_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
			foreach($q->result() as $row){
				$data[] = $this->getCustomerById($row->customer_id);
			}
			return $data;
        }
        return FALSE;
	   }	
	public function edit_product($p_id)
	  { 
	  
	    
			$this->db->select('*');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->where('ig_products.product_id',$p_id);
			$query = $this->db->get();
			return $query->row();
		
		}
		
	 public function get_editlanguage($p_id)
	   {
		/*$this->db->select('*'); 
		$query  = $this->db->get('ig_language');
        return $query->result();*/

	
		$this->db->select('*');
		$this->db->from('ig_products_text');
		$this->db->join('ig_products',' ig_products.product_id=ig_products_text.product_id');
		$this->db->join('ig_language',' ig_products_text.language_id=ig_language.language_id');
		$this->db->where(array('ig_products_text.product_id'=>$p_id));
		$query=$this->db->get(); 
		if($query){
		return $query->result();
		}else{
		return FALSE;
		}
		
	   }  	
		
	public function deleteProductById($ProductId)
	 {
		//$this->db->where('product_id',$ProductId);
		//$this->db->delete('ig_products');
		$this->db->delete('ig_products', array('product_id' => $ProductId));
		$this->db->delete('ig_products_text', array('product_id' => $ProductId));
		
        rmdir('uploads/productImages/'.$ProductId);
		rmdir($this->config->item('image_path').'product/'.$ProductId);
		 }	
	
	public function update_product($all_data,$p_id)
	  { 
		  $this->db->update('ig_products',$all_data,array('product_id'=>$p_id));
		return $this->db->affected_rows();}	
		
	public function updet_producttext($all_data,$p_id)
	  { 
	
	 foreach($all_data as $val)
	 {
		 $dataArr=array(
		                'product_name'=>$val['product_name'],
						'short_description'=>$val['short_description'],
						'message'=>$val['message']		 				
		 );
		 $this->db->where(array('language_id'=>$val['language_id'],'product_id'=>$p_id));
		 $query=$this->db->update('ig_products_text', $dataArr); 
		 
	 }
		  
	}		
		
	public function insert_gallery($file_name,$p_id)
	  {   
	  $data=array('product_id' => $p_id,'image_name' => $file_name);
		  $this->db->insert('ig_product_img_gallery',$data);
		  }	
	public function exit_id($id)
	  { 
		 $this->db->from('ig_product_img_gallery'); 
		 $this->db->where('product_id',$id);
		$query  = $this->db->get();
		
		if($query->num_rows()>0)
		{
        return $query->result();
		  }	 
		
	  }
	public function edit_gallery($file_name,$pimg_id)
	   {
		     $data=array('image_name' => $file_name);
			 $this->db->where('img_id',$pimg_id);
		  $this->db->update('ig_product_img_gallery',$data);
		   }	   
	
    public function assignCustomersToProduct($customer_id,$product_id){
		$assign_id=0;
		$query = $this->db->get_where('customer_details', array('customer_id' => $customer_id));
		if ($query->num_rows() > 0) {
			$row = $query->result();
		$data = array('product_id' => $product_id, 'customer_id' => $customer_id);
		$this->db->insert('assign_products',$data);
		$assign_id = $this->db->insert_id();
		/*if($assign_id > 0){
			$q = $this->db->get_where('customer_details', array('customer_id' => $customer_id), 1);
			if ($q->num_rows() > 0) {
				foreach($q->result() as $row){
					$row->assign_id = $assign_id;
					$cust_data = $row;
				}
				return $cust_data;
			}
	
		}*/
		
		return $assign_id;
		}
	} 
	public function getCustomerByProductId($product_id)
	 {
		$this->db->select('customer_id');
		$this->db->from('assign_products');
		$this->db->where('product_id',$product_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
			foreach($q->result() as $row){
				$data[] = $row;
			}
			return $data;
        }
        return FALSE;
		 }
		 
		 public function searchProductByElement($data)
		 {
			  $this->db->select('*');
			 $this->db->from('ig_products');
			 $this->db->like('product_id',$data);
			 $this->db->or_like('product_name', $data); 
			  $this->db->or_like('price', $data); 
			   $q = $this->db->get();
			   return $q->result();
		 }
		 public function additionProductByImages($p_id,$product_name)
		{
			$data = array('product_id' => $p_id, 'image_name' => $product_name);
		$this->db->insert('ig_product_img_gallery',$data);
			}
   
       public function deleteExistProductByID($existProductID, $p_id)
	   {
		   $this->db->where('img_id',$existProductID);
		   $query =  $this->db->get('ig_product_img_gallery')->row();
		   $this->db->where('img_id',$existProductID);
		   $delQuery = $this->db->delete('ig_product_img_gallery'); 
			/*if($delQuery){
				if(is_file('./uploads/productImages/'.$p_id.'/100*100/'.$query->image_name))
					unlink('./uploads/productImages/'.$p_id.'/100*100/'.$query->image_name);
				if(is_file('./uploads/productImages/'.$p_id.'/400*400/'.$query->image_name))
					unlink('./uploads/productImages/'.$p_id.'/400*400/'.$query->image_name);
				if(is_file('./uploads/productImages/'.$p_id.'/800*800/'.$query->image_name))
					unlink('./uploads/productImages/'.$p_id.'/800*800/'.$query->image_name);
			}*/
		}
		public function updationProductByImages($g_id,$file_name)
		{
			//echo $g_id.'----'.$file_name;die();
			$data = array('product_id' => $g_id, 'image_name' => $file_name);
          $this->db->where('img_id',$g_id);
		  $this->db->insert('ig_product_img_gallery',$data);
			
		}   
		public function getProductImageByID($p_id)
		{
			$this->db->where('product_id',$p_id);
			$query =  $this->db->get('ig_products')->row();
		   if($query)
		   {return $query;}
		}
		public function get_category_list()
		{
			$q = $this->db->select('
							ig_category_details.cat_id as categoryID,
							ig_category_details.name as categoryName,
									')
							->from('ig_category_details')
							->where(array('ig_category_details.lang'=>'1'))
							->order_by("ig_category_details.name","asc")
							->get();
			return ($q->num_rows() > 0) ? $q->result() : array();
		}
		public function product_admin_status_change($id,$value)
		{
			$this->db->update('ig_products',array('admin_status'=>$value),array('product_id'=>$id));
			return $this->db->affected_rows();
		}
		public function product_admin_feature_status_change($id,$value)
		{
			$this->db->update('ig_products',array('feature_status'=>$value),array('product_id'=>$id));
			return $this->db->affected_rows();
		}
		public function getProductDataById($id)
		{
			
			$db_query = $this->db->select('*')
								 ->from('ig_products')
								 ->where(array('ig_products.product_id'=>$id))
								 ->join('ig_products_text','ig_products_text.product_id = ig_products.product_id')
								 //->join('ig_category_details','ig_category_details.cat_id = ig_products.category_id')
								 ->get();
			
			
			return $db_query->num_rows() > 0 ? $db_query->result() : array();

		}
		public function getProductImgGalaryById($id)
		{
			$db_query = $this->db->select('
									ig_products.product_image,
									ig_product_img_gallery.image_name
										')
								 ->from('ig_products')
								 ->where(array('ig_products.product_id'=>$id))
								 ->join('ig_product_img_gallery','ig_product_img_gallery.product_id = ig_products.product_id')
								 ->get();
			$all_image = $db_query->result_array();
			foreach($all_image as $list)
			{
				$mainArr[] = $list['product_image'];
				$mainArr[] = $list['image_name'];
				
			}
			return $db_query->num_rows() > 0 ? array_unique($mainArr) : $this->_get_product_image($id);
		}
		
		public function get_brand_list()
		{
			$this->db->order_by("ig_brand.brand_name","asc");
			$db_query = $this->db->get('ig_brand');
			return $db_query->num_rows() > 0 ? $db_query->result() : array();
		} 
		
		private function _get_product_image($id) 
		{
			$db_query = $this->db->get_where("ig_products",array("product_id"=>$id));
			$productData = $db_query->result();
			foreach($productData as $data)
			{
				$productArr[] = $data->product_image;
			}
			return $db_query->num_rows() > 0 ? $productArr : array();
			
		}

		/*Method Name: additionalimagedelete
		  Table Name: ig_product_img_gallery
		  Parameter: id
		 */
		public function additionalimagedelete($data){
					$this->db->where($data);
					if($this->db->delete('ig_product_img_gallery')){ 
						return true;
						}
	    }
				


}