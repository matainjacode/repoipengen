<?php
class Setting_model extends CI_Model
{
	function add_fees($fees_value)
	{
		$this->db->insert("ig_transaction_fees",$fees_value);
		return $this->db->affected_rows();
	}
	
	function getAllTransaction()
	{
		$query = $this->db->get("ig_transaction_fees");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	function update_fees($fees_value, $fees_id)
	{
		$this->db->update("ig_transaction_fees",$fees_value, array("transaction_fees_id"=>$fees_id));
		return $this->db->affected_rows();
	}
	
	function delete_fees($fees_id)
	{
		$this->db->delete("ig_transaction_fees", array("transaction_fees_id"=>$fees_id));
		return $this->db->affected_rows();
	}
	
	function add_config($data)
	{
		$this->db->insert("ig_setting",$data);
		return $this->db->affected_rows();
	}
	
	function getConfigData()
	{
		$query = $this->db->get("ig_setting");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function getEmailDataById($configID)
	{
		$query = $this->db->get_where("ig_setting",array("setting_id" => $configID));
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function update_config($data, $configID)
	{
		$this->db->update("ig_setting",$data,array("setting_id" => $configID));
		return $this->db->affected_rows();
	}
	function delete_config($configID)
	{
		$this->db->delete("ig_setting",array("setting_id" => $configID));
		return $this->db->affected_rows();
	}
	function add_shippingData($db_data)
	{
		$this->db->insert("ig_shipping",$db_data);
		return $this->db->affected_rows();
	}
	function get_shippingData()
	{
		$query = $this->db->get("ig_shipping");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function update_shippingData($db_data, $shippingID)
	{
		$this->db->update("ig_shipping",$db_data,array("shipping_id" => $shippingID));
		return $this->db->affected_rows();
	}
	function delete_shipping_charge($shippingID)
	{
		$this->db->delete("ig_shipping",array("shipping_id" => $shippingID));
		return $this->db->affected_rows();
	}
}
?>