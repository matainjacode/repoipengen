<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	public function userlist(){
		

			$this->db->select('*');
			$this->db->from('ig_user');
			
			$this->db->order_by('id', 'desc');
			$query =$this->db->get();
			if($query->num_rows()>0){
			return $query->result();
			}
			
	}
		public function userinfo($uid){

			$query = $this->db->get_where('ig_user', array('id'=>$uid));
			return $query->result();
	}
	
	public function delete($uid){
		$db_query=$this->db->delete('ig_user', array('id' => $uid)); 
		if($db_query)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		
	}
		public function isblock($uid){
			
	    $this->db->where('id', $uid);
		$db_query=$this->db->update('ig_user', array('is_block' => 'yes')); 
		if($db_query)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		
	}
public function isnotblock($uid){
			
	    $this->db->where('id', $uid);
		$db_query=$this->db->update('ig_user', array('is_block' => 'no')); 
		if($db_query)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		
	}
public function updateuser($uid)
	{
		$data = array(
               'fname' => $this->input->post('fname'),
               'lname' => $this->input->post('lname'),
               'email' => $this->input->post('email'),
			   'is_block' => $this->input->post('optionsRadios'),
            );

			$this->db->where('id', $uid);
			$db_query=$this->db->update('ig_user', $data); 
			if($db_query)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
	}
 public function resetpassword()
 {
	 $data = array(
                'password' => md5($this->input->post('password'))
			   );
			   
			   $this->db->where('id', $this->input->post('uid'));
			$db_query=$this->db->update('ig_user', $data); 
			if($db_query)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
	 
 }
	
	public function wishlist($uid){
								
								$query = $this->db->get_where('ig_wishlist', array('uid'=>$uid));
			                    return $query->result();


								}

	


}