<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	public function wishlist(){
			$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
			$this->db->from('ig_wishlist');
			$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
			$query =$this->db->get();
			if($query->num_rows()>0){
					return $query->result();
			}
								
	}
	function wishlistGetId($wishlistID)
	{
		$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
		$this->db->from('ig_wishlist');
		$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
		$this->db->where(array("ig_wishlist.id"=>$wishlistID));
		$query =$this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	function update_status($wishlistId, $value)
	{
		$this->db->update("ig_wishlist",array("admin_status"=>$value),array("id"=>$wishlistId));
		return $this->db->affected_rows();
	}
	function wishlistGetWithProductId($wishlistID)
	{
		$query =	$this->db->select("
								ig_products.product_id as productID,
								ig_products.stock as product_stock,
								ig_products.price as product_price,
								ig_products_text.product_name as product_name,
								ig_products.sale_price as product_sale_price,
								ig_products.sale_start_date as product_sale_start_date,
								ig_products.sale_end_date as product_sale_end_date,
								ig_products.product_image as product_image,
									 ")
							 ->from("ig_wishlist_product")
							 ->where(array("ig_wishlist_product.wishlist_id"=>$wishlistID, "ig_products_text.language_id"=>"1"))
							 ->join("ig_products","ig_products.product_id = ig_wishlist_product.product_id")
							 ->join("ig_products_text","ig_products_text.product_id = ig_wishlist_product.product_id")
							 ->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
}