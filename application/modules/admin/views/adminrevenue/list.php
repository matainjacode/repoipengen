<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Revenue</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin")?>">Home</a> </li>
      <li> <a>Revenue</a> </li>
      <li class="active"> <strong>Revenue </strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php //print_r($revenueList);?>

<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class=" float-e-margins admin-flash-msg"> </div>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight" style="z-index: 0;">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title text-right" style="color: rgb(25, 170, 140);"> 
        	<h2><strong>Total Revenue</strong> - <?php echo strtoupper($this->config->item('currency')).' '.number_format($revenue_count); ?></h2>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover " id="admintable">
              <thead>
                <tr>
                  <th>Order Id</th>
                  <th>Wishlist</th>
                  <th>Total Amount</th>
                </tr>
              </thead>
              <tbody>
              <?php if(!empty($revenueList)) { foreach($revenueList as $row){ if($row->amount !=0){ ?>
                <tr>
                  <td><?php echo $row->order_id; ?></td>
                  <td><?php echo strtoupper($row->title); ?></td>
                  <td><?php echo number_format($row->amount); ?></td>
                  
                </tr>
                <?php } } }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
    var oTable = $('#admintable').DataTable({
			order:[[0,"desc"]],
			pageLength: 50,
		});
});
</script> 