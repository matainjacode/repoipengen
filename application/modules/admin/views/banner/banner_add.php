<style>
.error {
	color: red;
	font-size: 13px;
	margin-bottom: -15px
}
.img-text{
    color: #18a689;
    font-size: 16px;
    font-weight: bold;
    margin-left: 12px;
}
input[type="file"] {
    display: none;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Add New Banner</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Banner</a> </li>
      <li class="active"> <strong>Add New Banner</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<br/>
<div class="col-lg-12">
  <?php if($this->session->flashdata('success')){ ?>
  <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->session->flashdata('success'); ?></strong> </div>
  <?php } ?>
</div>
<div id="" class="gray-bg">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>Banner Add</h5>
          </div>
          <div class="ibox-content"> <?php echo form_open_multipart('admin/banner/add',array('class' => 'form-horizontal', 'id' => 'myform')); ?>
            <div class="form-group">
              <label class="col-lg-2 control-label">Banner Name<span style="color:#F00">*</span></label>
              <div class="col-lg-10">
                <?php
					$banner_name = array(
								'name'       	=> 'banner_name',
								'id'         	=> 'banner_name_id',
								'class'      	=> 'banner_name_class form-control',
								'maxlength'  	=> '100',
								'size'        	=> '50',
								'placeholder'	=> 'Banner name',
								'required' 		=> true,
								'value'       	=>  set_value('banner_name')
								);
					echo form_input($banner_name);
					echo form_error('banner_name'); 
				?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Banner Description<span style="color:#F00">*</span></label>
              <div class="col-lg-10">
                <?php
					$banner_desc = array(
								'name'			=> 'banner_desc',
								'id'			=> 'banner_desc_id',
								'class'			=> 'banner_desc_class form-control',
								'maxlength'		=> '100',
								'rows'			=> '5',
								'placeholder'	=> 'Banner Description',
								'required' 		=> true,
								'value'			=>  set_value('banner_desc')
								);
					echo form_textarea($banner_desc);
					echo form_error('banner_desc'); 
				?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Banner Image</label>
              <div class="col-lg-4">
              	<span><label for="banner_id" class="banner_submit_class btn btn-primary">Add Image</label></span><span id="bannerImgName" class="img-text"></span>
                <?php
					$banner = array(
								'name'        => 'user_file',
								'id'          => 'banner_id',
								'class'       => 'banner_class',
								'value'       =>  set_value('user_file')
								);
					echo form_upload($banner);
					echo form_error('user_file');
				?>
                
              </div>
              <div class="col-lg-6" >
              		<img class="preview img-responsive" id="preview" alt="">
                    <span id="bannerImgError" class="error"><?php echo $error; ?></span>
             </div>
              <div class="col-lg-12 text-center"> <span style="color:#F00">(Please upload 1,910px × 800px diamension Image)</span> </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <?php 
					  $banner_submit = array(
											'name'        => 'banner_submit',
											'id'          => 'banner_submit_id',
											'class'       => 'banner_submit_class btn btn-primary',
											'value'       => 'Submit'
											);
					  echo form_submit($banner_submit); 
				?>
              </div>
            </div>
            <?php echo form_close(); ?> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
    $(document).on("change","#banner_id",function(){
            var fileSupport = ['gif','png','jpg','jpeg'];
            if (this.files && this.files[0]) {
                var imageData = this.files[0];
                var fileName = imageData.name;
                var fileExtention = imageData.type.split("/").pop().toLowerCase();
                if ($.inArray(fileExtention,fileSupport) != -1) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#bannerImgError").html("");
                        $('#preview').attr('src', e.target.result);
						$("#bannerImgName").html(fileName);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
                else
                {
                    var input = $("#banner_id");
                    input.replaceWith(input.val('').clone(true));
                    $("#bannerImgError").html("File must be  - jpeg, jpg, gif, png.");
					$('#preview').attr('src', "");
					$("#bannerImgName").html("");
                };
                
            }
        });
});
</script>