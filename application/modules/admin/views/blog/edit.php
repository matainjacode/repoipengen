<style>
.fileinput-button {
	display: inline-block;
	overflow: hidden;
	position: relative;
}
input[type="file"] {
	display: none;
}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Edit Blog</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Blog</a> </li>
      <li class="active"> <strong>Edit Blog</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('successMsg')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
    </div>
  </div>
  <?php }?>
  <?php $attributes = array('class' => 'form-horizontal', 'id' => 'blogform','method' => 'post' ,'role' =>'form');
				echo form_open_multipart('admin/blog/edit/'.$result[0]->blog_id, $attributes);?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#genaral">Genaral</a></li>
            <li><a data-toggle="tab" href="#data">Data</a></li>
          </ul>
        </div>
      </div>
      <div class="tab-content">
        <div id="genaral" class="tab-pane fade in active">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <ul class="nav nav-tabs">
                <?php $classAc="";$j=0;
                     if(!empty($result)){
						
					 foreach($result as $val){
						 
						 if($j==0){$classAc="active";}else{$classAc="";}?>
                <li class="<?php echo $classAc; ?>"><a data-toggle="tab" href="#<?php echo $val->name;?>"><?php echo ucfirst($val->name);?></a></li>
                <?php $j++;}} ?>
                <!--<li><a data-toggle="tab" href="#barsha">Barsha</a></li>-->
              </ul>
            </div>
          </div>
          <div class="tab-content">
            <?php $class="";$i=0;
					if(!empty($result)){ 
						
					 foreach($result as $val){?>
            <?php 
					if($i==0){$class=" in active";}else{$class="";}
					
					?>
            <div id="<?php echo $val->name;?>" class="tab-pane fade <?php echo $class;?>">
              <input type="hidden" value="<?php echo $val->language_id;?>" name="language[]" />
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Edit Blog in <?php echo ucfirst($val->name);?></h5>
                </div>
                <div class="ibox-content"> <strong></strong>
                  <div class="form-group">
                    <div class="col-md-3">
            	<?php echo  form_label('Blog Title','blog_title',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
              <?php echo form_input(array('name' => 'blog_title[]','class' => 'form-control','placeholder' => 'Blog Title','required'=>'required','value' =>$result[$i]->blog_title)); ?>
            </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                  <div class="col-sm-3">
                    <?php  $blog_label = array('class'=>' control-label');
								echo  form_label('Blog Description','blog_dse',$blog_label);?>
                                </div>
                    <div class="col-sm-9">
                      <?php $blog_des = array('name' => 'blog_description[]','class' => 'form-control','placeholder' => 'Category description','required'=>'required');
                    
                    echo form_textarea('blog_description[]', $result[$i]->blog_description,  $blog_des); ?>
                    </div>
                  </div>
                  
                  <div class="hr-line-dashed"></div>
                  
                </div>
              </div>
            </div>
            <?php  $i++; }} ?>
          </div>
        </div>
        <div id="data" class="tab-pane fade">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Edit Blog Data</h5>
            </div>
            <div class="ibox-content">
              <div class="form-group">
            <div class="col-md-3">
            	<?php echo  form_label('Blog Image','blog_img',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
            <span class="btn btn-success fileinput-button">
             <label for="blog_image" style="cursor:pointer"> <i class="glyphicon glyphicon-plus"></i>Add Image... </label>
              <?php
					$blog_image = array(
								'name'        => 'blog_image',
								'id'          => 'blog_image',
								'class'       => 'blog_image',
								'value'       =>  set_value('blog_image')
								);
					echo form_upload($blog_image); ?>
               </span>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
           <div class="form-group">
            <div class="col-md-3">
            	<?php echo  form_label('Video link','blog_link',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
              <?php echo form_input(array('name' => 'blog_link','class' => 'form-control','placeholder' => 'Add Blog video link From Youtube.com','required'=>'required','value' => $result[0]->blog_link_video)); ?>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
      <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
      <?php echo form_submit('blogsubmit','Save','class="btn btn-primary"');?> </div>
  </div>
  <?php echo form_close();?> </div>
