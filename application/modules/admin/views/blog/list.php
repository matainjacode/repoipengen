<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Blog List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Blog</a> </li>
      <li class="active"> <strong>Blog List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Blog List</h5>
      </div>
      <?php if($this->session->flashdata('successMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <?php if($this->session->flashdata('errorMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('errorMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <div class="row admin-list-msg" style="display:none">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-flash-msg"></div>
        </div>
      </div>
      <div class="ibox-content">
        <div class=""> <a href="<?= base_url('admin/blog/create'); ?>" class="btn btn-success ">Add New</a> </div>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
				<th>ID</th>
				<th>Blog Title</th>
                <th>Blog description</th>
				<th>Display image</th>
				<th>Status</th>
                <th>Action</th>
            </tr>
          </thead>
          <tbody>
          	<?php if(!empty($blogList)){ $i = 0; foreach($blogList as $row){ ?>
            <tr>
                <td><?php echo ++$i;; ?></td>
                <td><?php echo $row->blog_title; ?></td>
                <td><?php echo $row->blog_description; ?></td>
                <?php /*?><td>
                	<?php if($row->display_status == 1){$checkVal = "checked";}else{$checkVal = "";} ?>
                    <input type="checkbox" value="<?= $row->display_status; ?>" class="js-switch" <?php echo $checkVal ?>  />
                </td>
                <td>
                	<?php if($row->blog_status == 1){$checkVal = "checked";}else{$checkVal = "";} ?>
                    <input type="checkbox" value="<?= $row->blog_status; ?>" class="js_switch_feature" <?php echo $checkVal ?>  />
                </td><?php */?>
                <td><?php if ($row->display_status == 0) {$check = '';}else{$check = 'checked';} ?>
                <input type="checkbox" value="<?= $row->blog_id ?>" class="js_switch_feature" <?= $check; ?> /></td>
              <td><?php if ($row->blog_status == 0) { $check = ''; }else{ $check = 'checked'; } ?>
                <input type="checkbox" value="<?= $row->blog_id ?>" class="js-switch" <?= $check; ?> /></td>
                <td>
                	<a href="<?php echo base_url("admin/blog/edit/".$row->blog_id); ?>" title="City Edit"><i class="fa fa-pencil-square-o fa-fw action-btn"></i></a>
                    <a href="javascript:void(0)" id="<?= $row->blog_id; ?>" class="cityDelete" title="City Delete"><i class="fa fa-trash-o fa-fw action-btn text-danger" ></i></a>
                </td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
   	$('#admintable').DataTable({
		order:[[0,"desc"]],
		pageLength: 25,
	});
	$(".imageZoom").imageTooltip();
	$(document).on('click','.cityDelete',function(){
		var catid=$(this).attr('id');
		 if(catid!=""){
			bootbox.confirm("Are you sure you want delete ?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/blog/delete/"+catid; 
				}
			}); 
		 }
	});
	/*******Switcher On-Off*************/
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature')); //console.log();
		elems.forEach(function(data) {
		var switchery = new Switchery(data,{ color: '#1AB394' });
	});
	/***after Click on Pagination Link*******/
	$(document).on('click', '.paginate_button a', function(){
		$( "#admintable tbody tr" ).each(function() {
				if(!$(this).find('td:eq(3) span').hasClass('switchery') && !$(this).find('td:eq(4) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				
				var elem_new = this.querySelector('.js_switch_feature');
		 	    Switchery(elem_new, { color: '#1AB394' });
				$(".imageZoom").imageTooltip();
			 }
		});
	});
	$(document).on("keyup",".dataTables_filter input", function(){
		$( "#admintable tbody tr" ).each(function() {
				if(!$(this).find('td:eq(3) span').hasClass('switchery') && !$(this).find('td:eq(4) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				
				var elem_new = this.querySelector('.js_switch_feature');
		 	    Switchery(elem_new, { color: '#1AB394' });
				$(".imageZoom").imageTooltip();
			 }
		});
	});
	$(document).on("change","#admintable_length select", function(){
		$( "#admintable tbody tr" ).each(function() {
				if(!$(this).find('td:eq(3) span').hasClass('switchery') && !$(this).find('td:eq(4) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				
				var elem_new = this.querySelector('.js_switch_feature');
		 	    Switchery(elem_new, { color: '#1AB394' });
				$(".imageZoom").imageTooltip();
			 }
		});
	});

	
$(document).on("change",".js-switch",function(e){
		  var Id = $(this).attr("value");
		  var blogname = $(this).closest("tr").find("td:eq(1)").html();
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo base_url('admin/blog/status/'); ?>" + Id + "/1" ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(blogname +" will display on Homepage.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/blog/status/'); ?>"  + Id + "/0" ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(blogname +" will hide from Homepage.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
							
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});
$(document).on("change",".js_switch_feature",function(e){
  var Id = $(this).attr("value");
  var blogname = $(this).closest("tr").find("td:eq(1)").html();
  if (this.checked) {
	  $.ajax({
		  url:"<?php echo base_url('admin/blog/display_status/'); ?>" + Id + "/1",
		  success: function(result){
				if($.trim(result) != 0)
				{
					$(this).attr("checked", "checked");
					$(".admin-list-msg").css("display","");
					$(".admin-flash-msg").html("Image should be shown in "+blogname+".");
					$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
				}
				else
				{$(this).removeAttr("checked");}
			  }
		  });
  }else{
	  $.ajax({
		  url:"<?php echo base_url('admin/blog/display_status'); ?>/" + Id + "/0",
		  success: function(result){
				if($.trim(result) != 0)
				{
					$(this).removeAttr("checked");
					$(".admin-list-msg").css("display","");
					$(".admin-flash-msg").html("Video should be shown in "+blogname+".");
					$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
				}
				else
				{$(this).attr("checked", "checked");}
			  }
		  });
  };
});
	
});
</script>