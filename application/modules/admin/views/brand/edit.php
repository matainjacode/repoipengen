<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Edit Brand</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Brand</a> </li>
      <li class="active"> <strong>Edit Brand</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edit Brand</h5>
        </div>
        <div class="ibox-content">
          <?php 
		  		$attributes = array('class' => 'form-horizontal','id' => 'brandform');
				echo form_open('admin/brand/edit/'.$brandID, $attributes);?>
          <div class="form-group">
            <div class="col-md-3">
              <?php  
      					$name_label = array('class'=>'control-label');
      					echo  form_label('Brand Name','name',$name_label);
				      ?>
            </div>
            <div class="col-md-9">
              <?php if(!set_value('name')){$value = ucfirst($brandName);}else{$value = set_value('name');} ?>
              <?php $cat_name = array('name' => 'name','class' => 'form-control','placeholder' => 'Brand Name','required'=>'required','value' => $value); ?>
              <?= form_input($cat_name); ?>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
              <?php echo form_submit('submit','Update','class="btn btn-primary"');?> </div>
          </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
