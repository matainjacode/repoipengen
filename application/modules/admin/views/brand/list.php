<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Brand List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Brand</a> </li>
      <li class="active"> <strong>Brand List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('success_message')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('success_message');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->flashdata('error_message')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
  </div>
</div>
<?php }?>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg">  </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Brand List</h5>
      </div>
      <div class="ibox-content"> <a href="<?= base_url('admin/brand/add');?>" class="btn btn-success ">Add New</a>
        <?php if(!empty($brandlists)){ ?>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Status</th>
              <th class="center">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=0;
			 foreach($brandlists as $row) { $i++;?>
            <tr class="gradeA">
              <td><?php echo $i; ?></td>
              <td><?php echo ucfirst($row->brand_name); ?></td>
              <td>
			  <?php
                  if ($row->brand_status == 0) {
                    $check = '';
                  }else{
                    $check = 'checked';
                  }
                ?>
                <input type="checkbox" value="<?= $row->brand_id ?>" class="js-switch" <?= $check; ?> />
                </td>
              <td class="center"><a href="<?php echo base_url()?>admin/brand/edit/<?php echo $row->brand_id; ?>" title="Edit"><i class="fa fa-pencil-square-o action-btn"></i></a>&nbsp; <a class="brand-delete" id="<?php echo $row->brand_id; ?>" href="javascript:void(0)" title="Delete"><i class="fa fa-trash-o action-btn text-danger" ></i></a></td>
            </tr>
            <?php }?>
          </tbody>
        </table>
        <?php }else
			{
				echo "No data Avalable";
			}?>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
    /* Init DataTables */
	$('#admintable').DataTable({
		order:[[0,"desc"]],
		pageLength: 25,
	});

	$(document).on('click','.brand-delete',function(){
		var ecatid=$(this).attr('id');
         if(ecatid!=""){
		bootbox.confirm("Are you sure want to delete?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/brand/delete/"+ecatid; 
				}
			}); 
		 }
	});
	
	/****Switcher****/
	// For Single Checkbox
	// var elem = document.querySelector('.js-switch');
	// Switchery(elem, { color: '#1AB394' });
	// For multiple Checkbox
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch')); //console.log();
		elems.forEach(function(data) {
		var switchery = new Switchery(data,{ color: '#1AB394' });
	});
	/***after Click on Pagination Link*******/
	$(document).on('click', '.paginate_button a', function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(2) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });	
			 }
		});
	});
	$(document).on("keyup",".dataTables_filter input", function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(2) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });	
			 }
		});
	});
	$(document).on("change","#admintable_length select", function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(2) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });	
			 }
		});
	});
	
	
 /*****Product Status Change*****/
 $(document).on("change",".js-switch",function(e){
      var brandId = $(this).attr("value");
	  var brandName = $(this).closest("tr").find("td:eq(1)").html();
      if (this.checked) {
		  $.ajax({
			  	url:"<?= base_url('admin/brand/changestatus/'); ?>" + brandId + "/1",
				beforeSend: function(){$("body").append(lhtml);},
				success: function(e){
					$('#loaderBg2').remove();
					if($.trim(e) != 0)
					{
						$(this).attr("checked", "checked");
						$(".admin-list-msg").css("display","");
						$(".admin-flash-msg").html(brandName +" activated.");
						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
					}
					else
					{$(this).removeAttr("checked");}
				}
			});
      }else{
		  $.ajax({
			  	url:"<?= base_url('admin/brand/changestatus/'); ?>" + brandId + "/0",
				beforeSend: function(){$("body").append(lhtml);},
				success: function(e){
					$('#loaderBg2').remove();
					if($.trim(e) != 0)
					{
						$(this).attr("checked", "checked");
						$(".admin-list-msg").css("display","");
						$(".admin-flash-msg").html(brandName +" deactivated.");
						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
					}
					else
					{$(this).removeAttr("checked");}
				}
			});
      };
 });
	
 });

</script>