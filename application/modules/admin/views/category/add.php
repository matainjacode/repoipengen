<style>
.fileinput-button {
	display: inline-block;
	overflow: hidden;
	position: relative;
}
input[type="file"] {
	display: none;
}
</style>
<?php
	$option[""] = "Choose Category";
	foreach ($result as $value) {
	 	$option[$value->id] = ucfirst($value->name);
	}
?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Add category</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Category</a> </li>
      <li class="active"> <strong>Add Category</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('successMsg')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
    </div>
  </div>
  <?php }?>
  <?php $attributes = array('class' => 'form-horizontal', 'id' => 'categoryform','method' => 'post' ,'role' =>'form');
				echo form_open_multipart('admin/category/add', $attributes);?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#genaral">Genaral</a></li>
            <li><a data-toggle="tab" href="#data">Data</a></li>
          </ul>
        </div>
      </div>
      <div class="tab-content">
        <div id="genaral" class="tab-pane fade in active">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <ul class="nav nav-tabs">
                <?php $classAc="";$j=0;
                     if(!empty($language)){
						
					 foreach($language as $val){
						 
						 if($j==0){$classAc="active";}else{$classAc="";}?>
                <li class="<?php echo $classAc; ?>"><a data-toggle="tab" href="#<?php echo $val->name;?>"><?php echo ucfirst($val->name);?></a></li>
                <?php $j++;}} ?>
                <!--<li><a data-toggle="tab" href="#barsha">Barsha</a></li>-->
              </ul>
            </div>
          </div>
          <div class="tab-content">
            <?php $class="";$i=0;
					if(!empty($language)){
						
					 foreach($language as $val){?>
            <?php 
					if($i==0){$class=" in active";}else{$class="";}
					
					?>
            <div id="<?php echo $val->name;?>" class="tab-pane fade <?php echo $class;?>">
              <input type="hidden" value="<?php echo $val->language_id;?>" name="language[]" />
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Add Category </h5>
                </div>
                <div class="ibox-content"> <strong></strong>
                  <div class="form-group">
                    <?php  $category_name_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Category Name','category_name',$category_name_label);?>
                    <div class="col-sm-10">
                      <?php $category_name = array('name' => 'name[]','class' => 'form-control','placeholder' => 'Category name','required'=>'required','value' => set_value('name['.$i.']')); ?>
                      <?= form_input($category_name); ?>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <?php  $category_des_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Category Description','last_name',$category_des_label);?>
                    <div class="col-sm-10">
                      <?php $category_des = array('name' => 'description[]','class' => 'form-control','placeholder' => 'Category description','required'=>'required');
                    
                    echo form_textarea('description[]', set_value('description['.$i.']'),  $category_des); ?>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  
                </div>
              </div>
            </div>
            <?php $i++; }} ?>
          </div>
        </div>
        <div id="data" class="tab-pane fade">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Add Category data</h5>
            </div>
            <div class="ibox-content">
              <div class="form-group">
                <?php  $category_parent_label = array('class'=>'col-sm-2 control-label',"autocomplete"=>"off");
								echo  form_label('Parent Category','last_name',$category_parent_label);?>
                <div class="col-sm-10">
                	<?php echo form_dropdown('parent_id',$option,'',['class'=>'form-control categorySelect']); ?>
                </div>
              </div>
              <div class="hr-line-dashed"></div>
              <div class="form-group">
                <?php  $category_banner_label = array('class'=>'col-sm-2 control-label');
						echo  form_label('Category Image',"",$category_banner_label);?>
                <div class="col-sm-10"> <span class="btn btn-success fileinput-button"> <i class="glyphicon glyphicon-plus"></i>
                  <label for="category-banner" style="cursor: pointer;">Browse...</label>
                  <input type="file" name="banner" id="category-banner">
                  </span> 
                  <!--<input type="file" name="banner" id="category-banner" class="" />--> 
                </div>
              </div>
              <div class="hr-line-dashed"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
      <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
      <?php echo form_submit('submit','Save changes','class="btn btn-primary"');?> </div>
  </div>
  <?php echo form_close();?> </div>
<script>
/*$(document).ready(function() {
	$(".categorySelect").select2({
			placeholder:"Type Name..",
		});
	$('.catnameid').on('click',function(){
	$('#dLabel').html($(this).text()+'<span class="caret"></span>');
	$('#parent_id').val($(this).attr('id'));
	});
$('li.category').addClass('plusimageapply');
$('li.category').children().addClass('selectedimage');
$('li.category').children().hide();


$('li.category').each(

function(column) {
	
	
$(this).click(function(event){
	
	



  if (this == event.target) {
  if($(this).is('.plusimageapply')) {
  	$('#parent_id').val($(this).attr('id'));$('#parent_cat').val($(this).attr('id'));
  $(this).children().show();
  $(this).removeClass('plusimageapply');
  $(this).addClass('minusimageapply');
  }
  else
  {
  $('#parent_id').val($(this).attr('id'));
  $('#parent_cat').val($(this).attr('id'));
  $(this).children().hide();
  $(this).removeClass('minusimageapply');
  $(this).addClass('plusimageapply');
  }
  }
  });
  }
  );
  
$('#category_data').focusout(function(){
    var val = $('#category_data').val();
    var cat = $("#categoryList option").filter(function() {return this.value == val;}).data('xyz');
    $('#parent_id').val(cat);
});

});*/
</script>