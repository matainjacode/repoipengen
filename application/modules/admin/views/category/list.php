<div class="row wrapper border-bottom white-bg page-heading" >
  <div class="col-lg-10">
    <h2>Category List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Category</a> </li>
      <li class="active"> <strong>Category List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('successMsg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->flashdata('adminDeleteMsg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('adminDeleteMsg');?> </div>
  </div>
</div>
<?php }?>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg">  </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Category List</h5>
      </div>
      <div class="ibox-content">
        <div class=""> <a href="<?= base_url('admin/category/add'); ?>" class="btn btn-success ">Add New</a> </div>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Parent Category</th>
              <th>Image</th>
              <th>Mega Menu</th>
              <th>Status</th>
              <th class="center">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($result as $row) {?>
            <tr class="gradeA">
              <td><?php echo $row["id"] ?></td>
              <td><?php echo $row["name"] ?></td>
              <td><?php echo $row["path"] ?></td>
              <?php if(is_file($this->config->item('image_path').'/category/'.$row["id"].'/thumb/'.$row["banner"])){
					$impath=base_url().'photo/category/'.$row["id"].'/thumb/'.$row["banner"];
					$mainImgPath = base_url().'photo/category/'.$row["id"].'/'.$row["banner"];
				}else{
					$impath=base_url().'photo/category/images.png';
					$mainImgPath = "";
				}?>
              <td><img src="<?php echo $impath; ?>" width="50px" height="50px" class="img-circle imageZoom" id="<?php echo $mainImgPath; ?>"></td>
              <td>
              	<?php
                  if ($row["megamenu_status"] == 0) {
                    $mega_check = '';
                  }else{
                    $mega_check = 'checked';
                  }
                ?>
              	<input type="checkbox" value="<?= $row["id"] ?>" class="js-switch-mega" <?php echo $mega_check ?>  />
              </td>
              <td>
			  <?php
                  if ($row["status"] == 0) {
                    $check = '';
                  }else{
                    $check = 'checked';
                  }
                ?>
                <input type="checkbox" value="<?= $row["id"] ?>" class="js-switch" <?= $check; ?> />
                </td>
              <td class="center"><a href="<?php echo base_url()?>admin/category/edit/<?php echo $row["id"]; ?>" title="Edit"><i class="fa fa-pencil-square-o action-btn"></i></a>&nbsp; 
                <!-- <a href="<?php //echo base_url()?>admin/view/<?php //echo $row->log_id; ?>" class="viewNews" data-id="">
                      				<i class="fa fa-eye action-btn text-success" data-toggle="modal" data-target="#myModal"></i></a>-->&nbsp; <a class="deletecategory" id="<?php echo $row["id"]; ?>" href="javascript:void(0)" title="Delete"><i class="fa fa-trash-o action-btn text-danger" ></i></a></td>
            </tr>
            <?php }?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
	
		 /* Init DataTables */
	$('#admintable').DataTable({
		order:[[0,"desc"]],
		pageLength: 10,
	});
	$(".imageZoom").imageTooltip();
	$(document).on('click','.deletecategory',function(){
		var catid=$(this).attr('id');
		 if(catid!=""){
			bootbox.confirm("Are you sure you want delete this Category?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/category/delete/"+catid; 
				}
			}); 
		 }
	});
	
	/*******Switcher On-Off*************/
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js-switch-mega')); //console.log();
		elems.forEach(function(data) {
		var switchery = new Switchery(data,{ color: '#1AB394' });
	});
	/***after Click on Pagination Link*******/
	$(document).on('click', '.paginate_button a', function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(4) span').hasClass('switchery') && !$(this).find('td:eq(5) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				var elem_new = this.querySelector('.js-switch-mega');
				Switchery(elem_new, { color: '#1AB394' });
				/*For Image Tooltip Re-Initialize*/
				$(".imageZoom").imageTooltip();	
			 }
		});
	});
	$(document).on("keyup",".dataTables_filter input", function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(4) span').hasClass('switchery') && !$(this).find('td:eq(5) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				var elem_new = this.querySelector('.js-switch-mega');
				Switchery(elem_new, { color: '#1AB394' });
				/*For Image Tooltip Re-Initialize*/
				$(".imageZoom").imageTooltip();	
			 }
		});
	});
	$(document).on("change","#admintable_length select", function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(4) span').hasClass('switchery') && !$(this).find('td:eq(5) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				var elem_new = this.querySelector('.js-switch-mega');
				Switchery(elem_new, { color: '#1AB394' });
				/*For Image Tooltip Re-Initialize*/
				$(".imageZoom").imageTooltip();	
			 }
		});
	});

	$(document).on("change",".js-switch",function(e){
		  var categoryId = $(this).attr("value");
		  var catName = $(this).closest("tr").find("td:eq(1)").html();
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo base_url('admin/category/enablestatus/'); ?>" + categoryId ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(catName +" now has been activated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/category/disablestatus/'); ?>" + categoryId ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(catName +" now has been deactivated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});
	$(document).on("change",".js-switch-mega", function(){
		var categoryID = $(this).attr("value");
		var catName = $(this).closest("tr").find("td:eq(1)").html();
		var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
		if (this.checked) {
			  $.ajax({
				  url:"<?php echo base_url('admin/category/enablesmegatatus/'); ?>" + categoryID ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();	
					  	if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(catName +" should be shown on Mega-Menu.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/category/disablemegastatus/'); ?>" + categoryID ,
				  beforeSend: function(){
					  		$("body").append(lhtml);
					  },
				  success: function(result){
					    $('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(catName +" should not be shown on Mega-Menu.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});
});

		</script>