<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Add City</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Admin</a> </li>
      <li class="active"> <strong>Add New City</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('succeess_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('succeess_message');?> </div>
    </div>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Add New City </h5>
        </div>
        <div class="ibox-content">
          <?php echo form_open('admin/city/add', array('class' => 'form-horizontal','id' => 'Cityform'));?>
          <div class="form-group">
            <div class="col-md-3">
            	<?php echo  form_label('City Name','name',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
              <?php echo form_input(array('name' => 'name','class' => 'form-control','placeholder' => 'City Name','required'=>'required','value' => set_value('name'))); ?>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
         
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
              <?php echo form_submit('citysubmit','Save','class="btn btn-primary"');?> </div>
          </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
