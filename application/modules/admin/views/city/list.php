<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>City List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Admin</a> </li>
      <li class="active"> <strong>City List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>City List</h5>
      </div>
      <?php if($this->session->flashdata('successMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <?php if($this->session->flashdata('errorMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('errorMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <div class="row admin-list-msg" style="display:none">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-flash-msg"></div>
        </div>
      </div>
      <div class="ibox-content">
        <div class=""> <a href="<?= base_url('admin/city/add'); ?>" class="btn btn-success ">Add New</a> </div>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
				<th>ID</th>
				<th>City Name</th>
				<th>Status</th>
				<th>Action</th>
            </tr>
          </thead>
          <tbody>
          	<?php if(!empty($cityList)){ $i = 0; foreach($cityList as $row){ ?>
            <tr>
                <td><?php echo ++$i;; ?></td>
                <td><?php echo $row->city_name; ?></td>
                <td>
                	<?php if($row->status == 1){$checkVal = "checked";}else{$checkVal = "";} ?>
                    <input type="checkbox" value="<?= $row->city_id; ?>" class="js-switch" <?php echo $checkVal ?>  />
                </td>
                <td>
                	<a href="<?php echo base_url("admin/city/edit/".$row->city_id); ?>" title="City Edit"><i class="fa fa-pencil-square-o fa-fw action-btn"></i></a>
                    <a href="javascript:void(0)" id="<?= $row->city_id; ?>" class="cityDelete" title="City Delete"><i class="fa fa-trash-o fa-fw action-btn text-danger" ></i></a>
                </td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
   	$('#admintable').DataTable({
		order:[[0,"desc"]],
		pageLength: 25,
	});
	$(document).on('click','.cityDelete',function(){
		var catid=$(this).attr('id');
		 if(catid!=""){
			bootbox.confirm("Are you sure you want delete ?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/city/delete/"+catid; 
				}
			}); 
		 }
	});
	/*******Switcher On-Off*************/
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch')); //console.log();
		elems.forEach(function(data) {
		var switchery = new Switchery(data,{ color: '#1AB394' });
	});
	/***after Click on Pagination Link*******/
	$(document).on('click', '.paginate_button a', function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(2) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
			 }
		});
	});
	
	$(document).on("change",".js-switch",function(e){
		  var cityId = $(this).attr("value");
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo base_url('admin/city/status/'); ?>" + cityId + "/1" ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html("Active Successfully.");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/city/status/'); ?>"  + cityId + "/0" ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html("Block Successfully.");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});

});
</script>