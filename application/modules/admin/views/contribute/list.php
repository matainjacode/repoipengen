<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Contribution List Panel</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Contribution</a> </li>
      <li class="active"> <strong>Contribution List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Contribution List</h5>
      </div>
      <?php if($this->session->flashdata('successMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <?php if($this->session->flashdata('errorMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('errorMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <div class="ibox-content">
        <div class=""> <!--<a href="<? //= base_url('admin/coupon/add'); ?>" class="btn btn-success ">Add New</a>--> </div>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
				<th>SL. NO.</th>
                <th>Order No</th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Wishlist Name</th>
                <th>Wishlist Owner</th>
                <th>Total Amount</th>
				<th>Cont. Amount</th>
				
				<!--<th>Cont. Name</th>
				<th>Cont. Date</th>-->
            </tr>
          </thead>
          <tbody>
          	<?php if(!empty($lists)){ $i=0; foreach($lists as $row){ if($row['language_id'] == 1){ 
					if($row["product_price"] <= $row["contribute_amount"])
					{ $css = "style='background-color: rgb(247, 143, 30); color: black;'"; }else{ $css = "";}
			?>
            <tr <?= $css ?>>
           		<td><?php echo ++$i; ?></td>
                <td><?php echo $row["order_id"]; ?></td>
                <td>
                	<?php 
			if(is_file($this->config->item('image_path').'/product/'.$row["product_id"].$this->config->item('thumb_size').$row["product_image"]))
			{	$imageURL = base_url('photo/product/'.$row["product_id"].$this->config->item('thumb_size').$row["product_image"]); }
			else
			{	$imageURL = base_url('photo/product/no_product.jpg'); }
                    ?>
                	<img src="<?= $imageURL ?>" alt="<?php echo $row["product_image"]; ?>" class="img-circle img-responsive imageZoom" width="40">
               	</td>
                <td><?php echo ucfirst($row["product_name"]); ?></td>
                <td><?php echo ucfirst($row["wishlist_name"]); ?></td>
                <td><?php echo ucfirst($row["w_owner_fname"])." ".ucfirst($row["w_owner_lname"]); ?></td>
                <td><?php echo $this->config->item("currency")." ".$row["product_price"]; ?></td>
                <td><?php echo $this->config->item("currency")." ".$row["contribute_amount"]; ?></td>
                
           		<!--<td><?php //echo ucfirst($row["contributer_fname"])." ".ucfirst($row["contributer_lname"]); ?></td>
           		<td><?php //echo date("d-M-Y",strtotime($row["contribute_date"])); ?></td>-->
           		
           		
               </tr> 
             <?php } } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
   	$('#admintable').DataTable({
		order:[[0,"desc"]],
		pageLength: 25,
	});
	/*$(document).on('click','.deleteCoupon',function(){
		var catid=$(this).attr('id');
		 if(catid!=""){
			bootbox.confirm("Are you sure you want delete this Category?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/coupon/delete/"+catid; 
				}
			}); 
		 }
	});*/
	$(".imageZoom").imageTooltip();

});
</script>