<style>
#ui-datepicker-div{
	z-index:100 !important;
}
</style>
<?php
	$option = array(
			"" 	=> 	"----Choose Type----",
			"1"	=>	"Fixed",
			"2"	=>	"Percentage",
					);
?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Edit Coupon</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Coupon</a> </li>
      <li class="active"> <strong>Edit Coupon</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('errorMsg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('errorMsg');?> </div>
  </div>
</div>
<?php }?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edit Coupon </h5>
        </div>
        <div class="ibox-content">
          <?php $attributes = array('class' => 'form-horizontal', 'id' => 'couponform','method' => 'post' ,'role' =>'form');

				echo form_open_multipart('admin/coupon/edit/'.$couponId, $attributes);?>
          <strong></strong>
          <div class="form-group">
                <div class="col-md-3">
                    <?php echo  form_label('Coupon Code','couponName',array('class'=>'control-label')); ?>
                    <span class="text-danger">*</span>
                </div>
                <div class="col-md-9">
                  <div class="col-md-7">
                  <?php
				  	if(set_value('couponcode'))
					{$value = set_value('couponcode');}else{$value = $couponCode;}
                  ?>
                  	<?= form_input(array('name' => 'couponcode','id' => 'couponcode','class' => 'form-control','placeholder' => 'Coupon Code','required'=>'required','value' => $value)); ?>
                  </div>
                  <div class="col-md-5">
                  	<label class="btn btn-primary" id="randomCoupon"> Generate Coupon </label>
                  </div>
                </div>
          </div>
          <div class="form-group">
                <div class="col-md-3">
                    <?php echo  form_label('Coupon Type','couponType',array('class'=>'control-label')); ?>
                    <span class="text-danger">*</span>
                </div>
                <div class="col-md-9">
                    <div class="col-md-7">
                    	<?php
							if(set_value('couponType'))
							{$value = set_value('couponType');}else{$value = $couponDropVal;}
						  ?>
                        <?= form_dropdown("couponType",$option,$value,array('class' => 'form-control','id'=>'couponType','required'=>'required')); ?>
                    </div>
                    <?php if(set_value('coupontypevalue') || $couponVal != "")
							{
								if($couponDropVal  == 1)
								{$floatVal = "right"; $signVal = "Rp";}else{$floatVal = "left"; $signVal = "%";}
								$style = "display:block";
								if(set_value('coupontypevalue'))
								{$value = set_value('coupontypevalue');}else{$value = $couponVal;}
							}
							else
							{
								$style = "display:none";
							}
					?>
                    <div class="col-md-5" style=" <?php echo $style; ?> " id="coupontypeVal">
                    	<span style="padding: 0px 10px; line-height: 29px; font-weight: bold;" id="ValSign"><?php echo $signVal; ?></span>
                        <span style="float: <?php echo $floatVal; ?>; width: 83%;" id="couponText">
                       <?= form_input(array("name"=>"coupontypevalue","class"=>"form-control","id"=>"coupontypevalue",'required'=>'required','value' => $value)); ?>
                        </span>
                        <span id="couponValErr" class="text-danger"></span>
                    </div>
                </div>
          </div>
          <div class="form-group">
                <div class="col-md-3">
                    <?php echo  form_label('Coupon Start Date','couponStartDate',array('class'=>'control-label')); ?>
                    <span class="text-danger">*</span>
                </div>
                <div class="col-md-9">
                    <div class="col-md-7">
                    	<?php
							if(set_value('couponStartDate'))
							{$value = set_value('couponStartDate');}else{$value = $couponStartDate;}
						  ?>
                        <?= form_input(array("name"=>"couponStartDate","id"=>"couponStartDate","class"=>"form-control","placeholder"=>"Start Date e.g - YYYY-MM-DD",'required'=>'required','value' => $value)); ?>
                    </div>
                    <div class="col-md-5">
                        &nbsp;
                    </div>
                </div>
          </div>
          <div class="form-group">
                <div class="col-md-3">
                    <?php echo  form_label('Coupon End Date','couponEndDate',array('class'=>'control-label')); ?>
                    <span class="text-danger">*</span>
                </div>
                <div class="col-md-9">
                    <div class="col-md-7">
                    	<?php
							if(set_value('couponEndDate'))
							{$value = set_value('couponEndDate');}else{$value = $couponEndDate;}
						  ?>
                        <?= form_input(array("name"=>"couponEndDate","id"=>"couponEndDate","class"=>"form-control","placeholder"=>"End Date e.g - YYYY-MM-DD",'required'=>'required','value' => $value)); ?>
                    </div>
                    <div class="col-md-5">
                        &nbsp;
                    </div>
                </div>
          </div>
          
          <div class="hr-line-dashed"></div>
          
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
              <?php echo form_submit('couponSubmit','Save','class="btn btn-primary", id ="couponAdd"');?> </div>
          </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
    $('#couponStartDate,#couponEndDate').datepicker({
		minDate: '+0d',
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true,
		dateFormat: "yy-mm-dd",
	});
	$(document).on("click","#randomCoupon",function(){
		var randomCode = Math.random().toString(36).substr(2);
		$("#couponcode").val(randomCode);
	});
	$(document).on("change","#couponType",function(){
		var dropVal = $(this).val();
		if(dropVal == "1")
		{
			$("#ValSign").html("Rp");
			$("#couponText").css("float","right");
			$("#coupontypeVal").css("display","");
			$("#coupontypevalue").attr("placeholder","Coupon Fixed Value");
		}
		else if(dropVal == "2")
		{
			$("#ValSign").html("%");
			$("#couponText").css("float","left");
			$("#coupontypeVal").css("display","");
			$("#coupontypevalue").attr("placeholder","Coupon Percentage Value");
		}
		else
		{
			$("#ValSign").html("");
			$("#couponText").css("float","left");
			$("#coupontypeVal").css("display","none");
			$("#coupontypevalue").removeAttr("placeholder").val("").removeAttr("value");
		}
	});
	$(document).on("click", "#couponAdd", function(){
		var couponVal = $("#coupontypevalue").val();
		var couponType = $("#couponType").val();
		if(couponType == 2)
		{
			if(couponVal != "")
			{
				if($.isNumeric(couponVal))
				{
					if(couponVal > 100)
					{
						$("#couponValErr").html("Percentage value not more than 100.");
						return false;
					}
					else if(couponVal == 0 || couponVal < 0)
					{
						$("#couponValErr").html("Percentage value not less than 0 or equal.");
						return false;
					}
					else
					{
						$("#couponValErr").html("");
					}
				}
				else
				{
					$("#couponValErr").html("Percentage value must be numeric.");
					return false;
				}
			}
		}
	});
});
</script>
