<style>
input[type="file"] {
	display: none;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Add Event Category</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Event Category</a> </li>
      <li class="active"> <strong>Add Event Category</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('succeess_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('succeess_message');?> </div>
    </div>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Add Event Category </h5>
        </div>
        <div class="ibox-content">
          <?php $attributes = array('class' => 'form-horizontal', 'id' => 'eventform','method' => 'post' ,'role' =>'form');

				echo form_open_multipart('admin/event/add', $attributes);?>
          <strong></strong>
          <div class="form-group">
          	<!--Tab Option Panel-->
              <ul class="nav nav-tabs">
              <?php if(!empty($langs)){ $a=0; foreach($langs as $lang) { if($a==0){$class = "active";}else{$class = "";} ?>
                <li class="<?php echo $class; ?>"><a data-toggle="tab" href="#<?php echo $lang->name; ?>"><?php echo ucfirst($lang->name); ?></a></li>
              <?php $a++;}} ?>
              </ul>
            <!--Tab Body Panel-->
              <div class="tab-content">
              <?php if(!empty($langs)){ $a=0; foreach($langs as $lang) { if($a==0){$class = "active";}else{$class = "";} ?>
                <div id="<?php echo $lang->name; ?>" class="tab-pane fade in <?php echo $class; ?>">
                    <div style="margin-top: 14px;">
                    	<?= form_input(array('name' => 'langId[]','type' => 'hidden','value' => $lang->language_id )); ?>
                        <div class="col-md-3">
                            <?php echo  form_label('Event Category Name in '.ucfirst($lang->name),'name',array('class'=>'control-label')); ?>
                            <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-9">
                          <?= form_input(array('name' => 'name[]','class' => 'form-control','placeholder' => 'Event category Name','required'=>'required','value' => set_value('name['.$a.']'))); ?>
                        </div>
                   </div>
                </div>
              <?php $a++;}} ?>
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">
              <?php  $name_label = array('class'=>'control-label');
               echo  form_label('Event Category Image','image',$name_label);?>
               <span class="text-danger">*</span>
            </div>
            <div class="col-md-9"> <span class="btn btn-success fileinput-button">
              <label for="cat_image" style="cursor:pointer"> <i class="glyphicon glyphicon-plus"></i>Add Image... </label>
              <?php echo form_upload(array('name'=>'eventImage', 'id'=>'cat_image')); ?> </span> <br>
              <span class="text-danger"><strong>* Supported file are jpeg , jpg , png , gif.</strong></span> <br>
              <span class="text-danger"><strong>* Please upload 1366*478 diamention image.</strong></span>
              <div class="text-danger" id="errCatgoryMsg"><?= $this->session->flashdata('errEventImg'); ?></div>
            </div>
          </div>
          <div class="form-group" id="eventCatDiv" style="display:none">
            <div class="col-md-3">
              <?php  $name_label = array('class'=>'control-label');
               echo  form_label('Preview','previewimage',$name_label);?>
            </div>
            <div class="col-md-9"> 
                <img src="" width="200" height="150" id="ImagePreview">
                <i class="fa fa-times-circle fa-3x text-danger closeBtn" style="cursor: pointer;"></i>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
              <?php echo form_submit('submit','Save','class="btn btn-primary"');?> </div>
          </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(e){
        $(document).on("change","#cat_image",function(){
            var fileSupport = ['gif','png','jpg','jpeg'];
            if (this.files && this.files[0]) {

                var imageData = this.files[0];
                var fileName = imageData.name;
                var fileExtention = imageData.type.split("/").pop().toLowerCase();
                console.log(imageData);
                if ($.inArray(fileExtention,fileSupport) != -1) {

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#errCatgoryMsg").html("");
                        $('#ImagePreview').attr('src', e.target.result);
                        $("#eventCatDiv").css('display','');
                    }
                    reader.readAsDataURL(this.files[0]);
                }
                else
                {
                    var input = $("#cat_image");
                    input.replaceWith(input.val('').clone(true));
                    $("#errCatgoryMsg").html("<strong>file must be  - jpeg,jpg,gif,png.</strong>");
                };
                
            }
        });

        $(document).on("click",".closeBtn",function(){
            // Clean Input File Value
            var input = $("#cat_image");
            input.replaceWith(input.val('').clone(true));
            // Remove Attribute of Image Tag
            $('#ImagePreview').removeAttr('src', '');
            // Hide Div
            $("#eventCatDiv").css('display','none');

        });
    });
</script>