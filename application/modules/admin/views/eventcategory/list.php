<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Event Category List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Event Category</a> </li>
      <li class="active"> <strong>Event Category List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('success_message')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('success_message');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->flashdata('error_message')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
  </div>
</div>
<?php }?>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg">  </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Event Category List</h5>
      </div>
      <div class="ibox-content"> <a href="<?= base_url('admin/event/add'); ?>" class="btn btn-success ">Add New</a>
        <?php if(!empty($result)){ ?>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Image</th>
              <th>Feature</th>
              <th>Status</th>
              <th class="center">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=0;
			 foreach($result as $row) { $i++;?>
            <tr class="gradeA">
              <td><?php echo $i; ?></td>
              <td><?php echo $row->name ?></td>
              <td>
              	
			  <?php if(is_file($this->config->item('image_path').'/eventcategory/'.$row->event_id.'/100-100/'.$row->image_name)) { ?>
                <img src="<?= base_url('photo/eventcategory/'.$row->event_id.'/100-100/'.$row->image_name) ;?>" width="50" height="50" class="img-circle imageZoom" id="<?= base_url('photo/eventcategory/'.$row->event_id.'/400-400/'.$row->image_name) ;?>">
                <?php }else{ ?>
                <img src="<?= base_url('photo/eventcategory/images.png');?>" width="50" height="50" class="img-circle imageZoom" id="">
                <?php } ?>
              </td>
              <td>
              <?php
                  if ($row->feature_status == 0) {
                    $check = '';
                  }else{
                    $check = 'checked';
                  }
                ?>
                <input type="checkbox" value="<?= $row->event_id ?>" class="js-switch_feature" <?= $check; ?> />
                </td>
              <td>
              <?php
                  if ($row->status == 0) {
                    $check = '';
                  }else{
                    $check = 'checked';
                  }
                ?>
                <input type="checkbox" value="<?= $row->event_id ?>" class="js-switch" <?= $check; ?> />
                </td>
              <td class="center"><a href="<?php echo base_url()?>admin/event/edit/<?php echo $row->event_id; ?>" title="Edit"><i class="fa fa-pencil-square-o action-btn"></i></a>&nbsp; 
                <!-- <a href="<?php //echo base_url()?>admin/view/<?php //echo $row->log_id; ?>" class="viewNews" data-id="">
                      				<i class="fa fa-eye action-btn text-success" data-toggle="modal" data-target="#myModal"></i></a>-->&nbsp; <a class="event-delete" id="<?php echo $row->event_id; ?>" href="javascript:void(0)" title="Delete"><i class="fa fa-trash-o action-btn text-danger" ></i></a></td>
            </tr>
            <?php }?>
          </tbody>
          <!--<tfoot>
            <tr>
                <th>Rendering engine</th>
                <th>Browser</th>
                <th>Platform(s)</th>
                <th>Engine version</th>
            </tr>
            </tfoot>-->
        </table>
        <?php }else
			{
				echo "No data Avalable";
			}?>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
     /* Init DataTables */
	$('#admintable').DataTable({
		order:[[0,"desc"]],
		pageLength: 25,
	});
	$(".imageZoom").imageTooltip();
	$(document).on('click','.event-delete',function(){
		var ecatid=$(this).attr('id');
		 if(ecatid!=""){
		bootbox.confirm("Are you sure want to delete?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/event/delete/"+ecatid; 
				}
			}); 
		 }
	});

	/****Switcher****/
	// For Single Checkbox
	// var elem = document.querySelector('.js-switch');
	// Switchery(elem, { color: '#1AB394' });
	// For multiple Checkbox
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js-switch_feature'));
		elems.forEach(function(html) {
		var switchery = new Switchery(html,{ color: '#1AB394' });
	});
	$(document).on('click', '.paginate_button a', function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(3) span').hasClass('switchery') && !$(this).find('td:eq(4) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				var elem = this.querySelector('.js-switch_feature');
				Switchery(elem, { color: '#1AB394' });
				/*For Image Tooltip Re-Initialize*/
				$(".imageZoom").imageTooltip();		
			 }
		});
	});
	$(document).on("keyup",".dataTables_filter input", function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(3) span').hasClass('switchery') && !$(this).find('td:eq(4) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				var elem = this.querySelector('.js-switch_feature');
				Switchery(elem, { color: '#1AB394' });
				/*For Image Tooltip Re-Initialize*/
				$(".imageZoom").imageTooltip();		
			 }
		});
	});
	$(document).on("change","#admintable_length select", function(){
		$( "#admintable tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(3) span').hasClass('switchery') && !$(this).find('td:eq(4) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				var elem = this.querySelector('.js-switch_feature');
				Switchery(elem, { color: '#1AB394' });
				/*For Image Tooltip Re-Initialize*/
				$(".imageZoom").imageTooltip();		
			 }
		});
	});
	
	/*****Event Status Change*****/
	$(document).on("change",".js-switch",function(e){
		  var eventId = $(this).attr("value");
		  var eventName = $(this).closest("tr").find("td:eq(1)").html();
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo  base_url('admin/event/enablestatus/'); ?>" + eventId,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  $('#loaderBg2').remove();
						if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(eventName +" event now has been Activated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/event/disablestatus/'); ?>" + eventId ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  $('#loaderBg2').remove();
						if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(eventName +" event now has been Deactivated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});
	
	/*****Event Status Change*****/
	$(document).on("change",".js-switch_feature",function(e){
		  var eventId = $(this).attr("value");
		  var eventName = $(this).closest("tr").find("td:eq(1)").html();
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo  base_url('admin/event/enablefeaturestatus/'); ?>" + eventId,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  $('#loaderBg2').remove();
						if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(eventName +" event should be shown on homepage.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/event/disablefeaturestatus/'); ?>" + eventId ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  $('#loaderBg2').remove();
						if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(eventName +" event should not be shown on homepage.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});
	
});
</script>