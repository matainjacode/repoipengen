<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo (isset($page_title)) ? $page_title : $this->lang->line('admin_tiltle');?></title>
<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/animate.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="passwordBox animated fadeInDown">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox-content">
        <h2 class="font-bold"><?php echo $this->lang->line('f_password'); ?></h2>
        <p> <?php echo $this->lang->line('f_password_msg'); ?> </p>
        <div class="row">
          <div class="col-lg-12"> 
            <?php $attributes = array('class' => 'm-t', 'id' => 'loginFrm');
			echo form_open('admin/forget_password', $attributes);?>
            <div class="text-danger">
            	<?php echo $this->session->flashdata('forgetpwd_ErrMsg'); echo form_error('email'); ?>
            </div>
            <div class="form-group"> 
              <?php $username = array('name' => 'email','class' => 'form-control','placeholder' => $this->lang->line('email')); ?>
              <?= form_input($username); ?>
            </div>
            <?php echo form_submit('email_submit',$this->lang->line('send'),'class="btn btn-primary block full-width m-b"');?> <?php echo form_close();?> 			</div>
        </div>
      </div>
    </div>
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-6"> <?php echo $this->lang->line('admin_tiltle'); ?> </div>
    <div class="col-md-6 text-right"> <small>© <?php echo date('Y');?></small> </div>
  </div>
</div>
</body>
</html>
