<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo (isset($page_title)) ? $page_title : $this->lang->line('admin_tiltle');?></title>
<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/animate.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="loginColumns animated fadeInDown">
  <div class="row">
    <div class="col-md-3">&nbsp;</div>
    <div class="col-md-6">
      <h2 class="font-bold login-msg" style="text-align:center;"><?php echo $this->lang->line('welcome_message'); ?></h2>
      <div class="ibox-content login-from">
        <?php  if($this->session->flashdata('msg')) {?>
        <div class="alert alert-danger display-hide"><?php echo $this->session->flashdata('msg');?>
          <button class="close" data-close="alert"></button>
        </div>
        <?php }?>
        <?php echo form_open('admin/login', array('class' => 'm-t', 'id' => 'loginFrm','method' => 'post' ,'role' =>'form'));?>
        <div class="form-group">
          <?php $username = array('name' => 'email','placeholder'=>$this->lang->line('email'),'class' => 'form-control','required'=>'required'); ?>
          <?= form_input($username); ?>
        </div>
        <div class="form-group">
          <?php $password = array('name' => 'password','placeholder'=>$this->lang->line('password'),'class' => 'form-control' ,'type' => 'password'); ?>
          <?= form_input($password); ?>
        </div>
        <?php echo form_submit('submit','Login','class="btn btn-primary block full-width m-b"');?> <a href="<?php echo base_url()?>admin/forget_password"> <small><?php echo $this->lang->line('f_password')?></small> </a> <?php echo form_close();?> </div>
    </div>
    <div class="col-md-3">&nbsp;</div>
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-3">&nbsp;</div>
    <div class="col-md-3"> <?php echo $this->lang->line('admin_tiltle'); ?> </div>
    <div class="col-md-3 text-right"> <small>© <?php echo date('Y');?></small> </div>
    <div class="col-md-3">&nbsp;</div>
  </div>
</div>
</body>
</html>
