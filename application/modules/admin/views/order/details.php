
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Order Details</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>admin/order/list">Order</a>
                        </li>
                        <li class="active">
                            <strong>Details</strong>
                        </li>
                    </ol>
                </div>

            </div>
            <div class="wrapper wrapper-content animated fadeInRight">

                        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">

                        </div>
                        <div class="ibox-content">
                        
                        <?php  $tprice=0;
						if(!empty($orderdetails)){
							foreach($orderdetails as $order){
							//$tprice=$order->sale_price*$order->quantity;
							?>
                        <div class="row">
                        <div class="col-md-2">
                        <?php 
							if(is_file($this->config->item('image_path').'/product/'.$order->product_id."/450X450/".$order->product_image))
							{ $imageURL = base_url()."photo/product/".$order->product_id."/450X450/".$order->product_image; } 
							else
							{$imageURL = base_url('photo/product/no_product.jpg');}
						?> 
                        <img src="<?php echo $imageURL; ?>"  class="img-responsive" width="100px" height="100px"/>
                        </div>
                        <div class="col-md-5">
                        <div><?php echo $order->name;?><?php echo $order->item_description;?></div>
                        <?php if($order->item_type!='cash_gift'){?>
                        <div>Quantity : <?php echo $order->quantity;?></div>
                        <?php }?>
                        <div>
                        <?php if($order->item_type=='contributed_cash_gift'){?>
                        Contributed amount for this product (<?php echo strtoupper($this->config->item('currency')).' '.number_format($order->sale_price);?>) :
                        <?php }else{?>
                        Price : 
                        <?php }?>
                         <?php
						 switch($order->item_type){
							 case 'contributed_cash_gift':
							            $prce= $order->contribute_amt; 
										break;                  
							 
							 case 'cash_gift':
							 $prce= $order->cash_amt+$order->transaction_amt;
							 break;
							 case 'buy_gift':$prce= $order->price;
							 break;
						 }
						  echo $prce;
						  
						  ?>
                         
                         
                         
                         </div>
                        </div>
                        <div class="col-md-3">
                        	<?php 
								if($order->wishlist_id == 0)
								{
									$address = "";
									if($order->shipping_firstname != "" && $order->shipping_lastname != "")
									{$address .= ucfirst($order->shipping_firstname)." ".ucfirst($order->shipping_lastname);}
									if($order->shipping_address != "")
									{$address .= "<br>".$order->shipping_address;}
									if($order->shipping_city != "")
									{$address .= ",".$order->shipping_city;}
									if($order->shipping_kecamatan != "")
									{$address .= ",".$order->shipping_kecamatan;}
									if($order->shipping_postcode != "")
									{$address .= ",".$order->shipping_postcode;}
									if($order->shipping_contact != "")
									{$address .= "<br> T - ".$order->shipping_contact;}
									echo $address;
								}
                            ?>
                        </div>
                        <div class="col-md-2"><?php echo strtoupper($this->config->item('currency')).' '.number_format($order->subtotal); ?></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <?php }} ?>
                        <div class="row">
                        <div class="col-md-4">  </div>
                        <div class="col-md-6"><b> Total Amount</b> </div>
                        <div class="col-md-2"> <?php echo strtoupper($this->config->item('currency')).' '.number_format($orderdetails[0]->total_product_price) ?> </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                         <div class="row">
                        <div class="col-md-4">  </div>
                        <div class="col-md-6"> <b>Discount Amount</b> </div>
                        <div class="col-md-2"> <?php echo strtoupper($this->config->item('currency')).' '.number_format($orderdetails[0]->discount_amount)  ?> </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                         <div class="row">
                        <div class="col-md-4">  </div>
                        <div class="col-md-6"> <b>Shipping Charge</b>
								<?php 
										if($orderdetails[0]->shipping_method=="onedayservice_jakarta")
										{ echo " (One Day Service - Jakarta)";}
										else
										{ echo " (Free Shipping - Jakarta)";}
								?> </div>
                        <div class="col-md-2"> 
							<?php 
								if($orderdetails[0]->shipping_method=="onedayservice_jakarta")
								{ echo strtoupper($this->config->item('currency')).' '.number_format($orderdetails[0]->shipping_cost);}
								else
								{ echo strtoupper($this->config->item('currency')).' '.number_format($orderdetails[0]->shipping_cost);}
						  ?> </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                         <div class="row">
                        <div class="col-md-4">  </div>
                        <div class="col-md-6"> <b>Gross Total</b> </div>
                        <div class="col-md-2"><?php echo strtoupper($this->config->item('currency')).' '.number_format($orderdetails[0]->total)  ?> </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                       <h4>Order Transaction Status</h4>
                       <div class="hr-line-dashed"></div>
                       
                            <div class="alert alert-success" id="succmsg" style="display:none;">
                           
                           
                            </div>
                        <form class="form-horizontal" id="orderst">
                                <div class="form-group"><label class="col-sm-2 control-label">Remarks </label>
                                         <input type="hidden" value="<?php echo $orderdetails[0]->order_id?>" name="orderid" />
                                         <input type="hidden" value="<?php echo $orderdetails[0]->id?>" name="userid" />
                                    <div class="col-sm-10"><input type="text" name="remark" class="form-control" value="" required="required" /></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Status</label>

                                    <div class="col-sm-10"><select name="status" id="changestatus" class="form-control" required >
                                    <option <?php if($orderdetails[0]->stuts=="processing"){echo 'selected="selected"';} ?> value="processing">Processing</option>
                                    <option <?php if($orderdetails[0]->stuts=="approved"){echo 'selected="selected"';} ?> value="approved">Approved</option>
                                    <option <?php if($orderdetails[0]->stuts=="ready to ship"){echo 'selected="selected"';} ?> value="ready to ship">Ready to Ship</option>
                                    <option <?php if($orderdetails[0]->stuts=="shipping"){echo 'selected="selected"';} ?> value="shipping">Shipping</option>
                                    <option <?php if($orderdetails[0]->stuts=="shipped"){echo 'selected="selected"';} ?> value="shipped">Shipped</option>
                                    <option <?php if($orderdetails[0]->stuts=="canceled"){echo 'selected="selected"';} ?> value="canceled">canceled</option>
                                    <option <?php if($orderdetails[0]->stuts=="failed"){echo 'selected="selected"';} ?> value="failed">Failed payment</option>
                                    </select></div>
                                </div>
                                 <div class="hr-line-dashed"></div>
 
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        
                                        <button type="button" id="changeBtn" class="btn btn-primary">Change status</button>
                                    </div>
                                </div>
                            </form>
                            <div class="hr-line-dashed"></div>
                            <h4>Order Transaction Status History</h4>
                            <div class="hr-line-dashed"></div>
                            <?php if(!empty($orderdetailshistory)){?>
                            <div class="row">
                            <div class="col-md-4"><b>Date</b></div>
                             <div class="col-md-4"><b>Remark</b></div>
                              <div class="col-md-4"><b>Status</b></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <?php foreach($orderdetailshistory as $Hval){?>
                            
                            <div class="row">
                            <div class="col-md-4"><?php echo $Hval->orderchange_date?></div>
                             <div class="col-md-4"><?php echo $Hval->remark?></div>
                              <div class="col-md-4"><?php echo $Hval->status?></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <?php }}?>
                        </div>
                    </div>
                </div>


<script>
$(document).ready(function(){
	$('#changeBtn').on('click',function(){
		var data=$("#orderst").serialize();
		$.ajax({
				method: "POST",
				url: "<?php echo base_url();?>/admin/order/change_status",
				data: data,
				success: function (result) {
					$('#succmsg').show();
					$('#succmsg').html('Update successfully');
				}
			
			});
	});});
</script>













