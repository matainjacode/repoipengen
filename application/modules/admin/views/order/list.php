<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Order List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin")?>">Home</a> </li>
      <li> <a>Order</a> </li>
      <li class="active"> <strong>Order List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('sussess_message')!=""){ ?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class=" float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('sussess_message');?> </div>
  </div>
</div>
<?php } if($this->session->flashdata('error_message')!=""){ ?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class=" float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
  </div>
</div>
<?php	}	?>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class=" float-e-margins admin-flash-msg"> </div>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight" style="z-index: 0;">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title"> </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover " id="admintable">
              <thead>
                <tr>
                  <th>Order Id</th>
                  <th>Client Name</th>
                  <th>Total Amount</th>
                  <th>Order Date</th>
                  <th>Transaction Status</th>
                  <th>Order Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php if(!empty($orderList)) { foreach($orderList as $row){ if($row->transaction_status == "capture"){$t_status = "Success";}else{$t_status = $row->transaction_status;} ?>
                <tr>
                  <td><?php echo $row->orderID; ?></td>
                  <td><?php echo ucfirst($row->client_fname)." ".ucfirst($row->client_lname); ?></td>
                  <td><?php echo strtoupper($this->config->item('currency')).' '.$row->total_amount; ?></td>
                  <td><?php echo date("d M Y  h:i:s",strtotime($row->order_date)); ?></td>
                  <td><?php echo ucfirst($t_status); ?></td>
                  <td><?php echo ucfirst($row->order_status); ?></td>
                  <td>
                  		<a href="<?php echo base_url("admin/order/details/".$row->orderID); ?>" title="Order Details" >
                        	<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>
                        </a>
                  </td>
                </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
    var oTable = $('#admintable').DataTable({
			order:[[0,"desc"]],
			pageLength: 50,
		});
});
</script> 
