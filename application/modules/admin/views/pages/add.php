     <link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet"> 
     
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-lg-10">
            <h2>Add New Page</h2>
            <ol class="breadcrumb">
              <li> <a href="<?php echo base_url();?>">Home</a> </li>
              <li> <a>Pages</a> </li>
              <li class="active"> <strong>Add New Page</strong> </li>
            </ol>
          </div>
          <div class="col-lg-2"> </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
        <?php if($this->session->flashdata('pagesInsertMsg')){?>
            <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins admin-flash-msg">
                  <?php echo $this->session->flashdata('pagesInsertMsg');?>
                  </div>
                </div>
             </div>
            <?php }?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add New Page </h5>
                        </div>
                        <div class="ibox-content">
                        <?php $attributes = array('class' => 'form-horizontal', 'id' => 'pageform','method' => 'post' ,'role' =>'form');

							echo form_open('admin/pages/submit', $attributes);?>
                		<strong></strong>
                        <div class="form-group">
								 
          	<!--Tab Option Panel-->
              <ul class="nav nav-tabs">
              <?php if(!empty($language)){ $a=0; foreach($language as $lang) { if($a==0){$class = "active";}else{$class = "";} ?>
                <li class="<?php echo $class; ?>"><a data-toggle="tab" href="#<?php echo $lang->name; ?>"><?php echo ucfirst($lang->name); ?></a></li>
              <?php $a++;}} ?>
              </ul>
            <!--Tab Body Panel-->
              <div class="tab-content">
              <?php if(!empty($language)){ $a=0; foreach($language as $lang) { if($a==0){$class = "active";}else{$class = "";} ?>
                <div id="<?php echo $lang->name; ?>" class="tab-pane fade in <?php echo $class; ?>">
                    <div style="margin-top: 14px;">
                    	<?= form_input(array('name' => 'langId[]','type' => 'hidden','value' => $lang->language_id )); ?>
                        <div class="form-group">
                                 <?php  $first_name_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Page name','page_name',$first_name_label);?>

                                    <div class="col-sm-10">
                                     <?php $page_name = array('name' => 'page_name[]','class' => 'form-control','placeholder' => 'Page name','required'=>'required','value' => set_value('page_name')); ?>
                    <?= form_input($page_name); ?>
                                    </div>
                                </div>
						<div class="form-group">
                                 <?php  $first_name_label = array('class'=>'col-sm-2 control-label');
								    echo  form_label('Page Description','block_description',$first_name_label);?>
                                    <div class="col-sm-10">
                                     <?php $value = array('name' => 'page_description[]','class' => 'form-control','placeholder' => 'Page Description','required'=>'required','value' =>                                                     set_value('page_description')); ?>

                                <?= form_textarea($value); ?>
                               </div>
                                    </div>
                   </div>
                </div>
              <?php $a++;}} ?>
              </div>
          </div>
                        
                        
                        <div class="hr-line-dashed"></div>  
                                <div class="form-group">
                                     <div class="col-sm-4 col-sm-offset-4">
                                        <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
                                         <button  type="submit" name="submit" class="btn btn-primary" id="fromsubmit" >Save</button>
                                    </div>
                                      
                                </div>
                           <?php echo form_close();?>
                          </div>
                    </div>
                </div>
            </div>
            
        </div>
        <script src="<?php echo base_url();?>assets/js/plugins/summernote/summernote.min.js"></script> 
        
    <!--<script>
        $(document).ready(function(){
			 $('.pagedescription').summernote();
			$('#fromsubmit').click(function(){
           
			var pagescript = $('.note-editable').html();
			//alert(pagescript);
			$('#texteditortext').val(pagescript);
			$('#pageform').submit();
			// event.preventDefault();
			//alert(pagescript);
			});
       });
    </script>-->
