<style type="text/css">
#ui-datepicker-div{
  z-index: 100 !important;
}
  
</style>
<?php 
	$option[""] = "Choose Brand";
	if (!empty($brands)) {
		foreach ($brands as $value) {
		  $option[$value->brand_id] = ucfirst($value->brand_name);
		}
	}
	$categoryOption[""] = "Choose Category";
	if(!empty($cat_list)){
		foreach($cat_list as $list) {
			$categoryOption[$list->categoryID] = ucfirst($list->categoryName);
		}
	}
?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Add product</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Products</a> </li>
      <li class="active"> <strong>Add Product</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('error_msg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('error_msg');?> </div>
  </div>
</div>
<?php }?>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php  $attributes = array("class" => "form-horizontal", "id" => "productform", "name" =>"productform","role"=>"form","autocomplete"=>"off");
               echo form_open_multipart('admin/product/addproduct', $attributes); ?>
  <div class="row">
    <div class="col-md-8 "> 
      <!-- BEGIN SAMPLE FORM PORTLET-->
      <div class="portlet light ">
        <div class="portlet-title">
          <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase"> Products Info</span> </div>
        </div>
        <div class="ibox float-e-margins">
          <div class="">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#General">General</a></li>
              <li><a data-toggle="tab" href="#Data">Data</a></li>
            </ul>
          </div>
        </div>
        <div class="tab-content">
          <div id="General" class="portlet-body form tab-pane fade in active">
            <div class="ibox float-e-margins">
              <ul class="nav nav-tabs">
                <?php $count = 0; foreach($language as $lang){ $count++;?>
                <li class="<?php if($count ==1){echo "active";}?>" ><a data-toggle="tab" href="#<?php echo $lang->name?>" id="<?php echo $lang->language_id?>"><?php echo $lang->name?></a></li>
                <?php } ?>
              </ul>
            </div>
            <div class="tab-content">
              <?php $count = 0; foreach($language as $lang){ $count++;?>
              <div id="<?php echo $lang->name?>" class="portlet-body form tab-pane fade  <?php if($count ==1){echo " in active";}?>">
                <div class="form-body"> <?php echo form_hidden('row', $count); ?> <?php echo form_hidden('language_id[]', $lang->language_id); ?>
                  <div class="form-group"> <?php echo form_label('Product Name');?><span class="text-danger">*</span> <?php echo form_input ('product_name[]','','class="form-control" id="product_name" placeholder="Product Name" required="required"');?><span class="text-danger"><?php echo form_error('product_name'); ?></span> </div>
                  <div class="form-group"> 

                  <?php echo form_label('Short Description',"'for'=>'s_description','class'=>'control-label'");?>
                  <span class="text-danger">*</span> <?php echo form_input ('s_description[]','','class="form-control" id="price" placeholder="Give your short description" required="required"');?><span class="text-danger"><?php echo form_error('s_description'); ?></span> </div>
                  
                  <div class="form-group"> <?php echo form_label('Message',"'for'=>'product_image'");?>
                  <!--Text Paste Here--> 
                  <textarea cols="80" id="editor<?= $count ?>" name="editor[]" rows="10" >
                        
                  </textarea>                             
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div id="Data" class="tab-pane fade">
            <div class="form-group"> <?php echo form_label('SKU');?><span class="text-danger">*</span> <?php echo form_input ('SKU','','class="form-control" id="SKU" placeholder="Stock keeping Unit" required="required"');?><span class="text-danger"><?php echo form_error('SKU'); ?></span> </div>
            <div class="form-group"> <?php echo form_label('Stock','for="stock"','class="control-label"');?><span class="text-danger">*</span>
              <?php $stock=array('1'=>'In Stock','0'=>'Out of Stock');?>
              <?php echo form_dropdown('stock',$stock,'','class="form-control"');?> </div>
            <div class="form-group"> <?php echo form_label('Stock Quantity','for="stock_quantity"');?><span class="text-danger">*</span> <?php echo form_input ('stock_quantity','','class="form-control" id="stock_quantity" placeholder="Stock Quantity" required="required"');?> <span class="text-danger"><?php echo form_error('stock_quantity'); ?></span> </div>
            <div class="form-group"> <?php echo form_label('Price','for="price"','class="control-label"');?><span class="text-danger">*</span> <?php echo form_input ('price','','class="form-control" id="price" placeholder="Price" required="required"');?><span class="text-danger"><?php echo form_error('price'); ?></span> </div>
            <div class="form-group"> <?php echo form_label('Sale Price','for="price"','class="control-label"');?><?php echo form_input ('saleprice','','class="form-control" id="sprice" placeholder="Sale Price"');?></div>
            <div class="form-group">
                <?php echo form_label('Choose Product Brand','for="brand",class="control-label"');?><span class="text-danger">*</span>
                <?php echo form_dropdown('brand',$option,'',['class'=>'form-control brandSelect','required'=>true]); ?>
            </div>
            <div class="form-group">
                <?php echo form_label('Choose Category','class="control-label"');?><span class="text-danger">*</span>
                <?php echo form_dropdown('category',$categoryOption,'',['class'=>'form-control categorySelect','required'=>true]); ?>
                <span class="text-danger"><?php echo form_error('category'); ?></span>
            </div>
            
            <div class="form-group">
            	<?php echo form_label('Start Date','class="control-label font-noraml" for="start_date"');?>
                        <?php echo form_input (array('name'=>'startdate','class'=>'form-control','id'=>'startdate','placeholder'=>'Seal Start Date E.g - YYYY-MM-DD'));?>
            </div>
            <div class="form-group">
            	<?php echo form_label('End Date','class="control-label font-noraml" for="end_date"');?>
                       <?php echo form_input (array('name'=>'enddate','class'=>'form-control','id'=>'enddate','placeholder'=>'Seal End Date E.g - YYYY-MM-DD'));?>
            </div>
			
            <div class="form-group"> <?php echo form_label('Shipping Weight','for="shipping_weight"','class="control-label"');?><?php echo form_input ('shipping_weight','','class="form-control" id="weight" placeholder="Shipping Weight"');?></div>
            <div class="form-group"> <?php echo form_label('Shipping Dimension','for="shipping_dimension"','class="control-label"');?><?php echo form_input ('shipping_dimension','','class="form-control" id="dimension" placeholder="Shipping Dimension"');?></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 ">
      <div class="portlet light ">
        <div class="portlet-title">
          <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Product Images</span> </div>
        </div>
        <div class="portlet-body form">
          <div class="form-body">
            <div class="form-group">
              <div class="col-md-5"> <?php echo form_label('Product Image','for="product_image"','class=" control-label"');?><span class="text-danger">*</span></div>
              <div class="col-md-7"> <?php echo form_button('product_images','Add product Image','data-toggle="modal" data-target="#productsingleimages" class="btn btn-success" id="product_images"');?> </div>
              <div id="dvproductimage" style="margin-top:46px;"> </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="portlet light ">
        <div class="portlet-body form">
          <div class="form-body">
            <div class="form-group">
              <div class="col-md-4"> <?php echo form_label('Additional Images','for="additional_image"','class=" control-label"');?></div>
              <div class="col-md-8"> 
                <!--  <button type="button" data-toggle="modal" data-target="#myModal" class="btn default">Upload</button>--> 
                <?php echo form_button('addition_images','Upload','data-toggle="modal" data-target="#myModal" class="btn default"');?> ` </div>
              <div id="dvPreview" style="margin-top:46px;"> </div>
              <div id="hiddenfield"></div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- <div id="General" class="tab-pane fade in active"> 
          <div class="row">
            <div class="col-md-12 ">
              <div class="portlet light ">
                <div class="form-group"> <?php //echo form_label('Message','for="product_image"');?>
                  <?php  //echo form_textarea('editor','','id="editor" class="form-control editor" placeholder="Type text" rows="3"')  ;?>
                </div>
              </div>
            </div>
          </div>
          </div>-->
  
  <div class="row">
    <div class="col-md-12 ">
      <div class="portlet light ">
        <div class="form-actions"> <?php echo form_submit('submit','Add','class="btn btn-success productSubmit"'); ?>
          <input class="btn default" type="button" onclick="window.location.replace('<?php echo base_url('admin/dashboard');?>')" value="Cancel"/>
        </div>
      </div>
    </div>
  </div>
  <?php echo form_close(); ?> </div>

<!-- Additional product Images-->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" style="width:558px; height:574px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Additional Images</h4>
      </div>
      <div class="modal-body">
        <iframe src="<?php echo base_url();?>uploader/uploader.php" width="503px" height="500px" style="border:0px;" id="uploader"></iframe>
      </div>
    </div>
  </div>
</div>
<!-- product Single images-->
<div id="productsingleimages" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" style="width:558px; height:574px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Product Image</h4>
      </div>
      <div class="modal-body">
        <iframe src="<?php echo base_url();?>uploader/uploader_single.php" width="503px" height="500px" style="border:0px;" id="uploader"></iframe>
      </div>
    </div>
  </div>
</div>
<script>
    CKEDITOR.replace( 'editor1', {
      height: 180
    } );
     CKEDITOR.replace( 'editor2', {
      height: 180
    } );
</script>
<script>
$(document).ready(function() {
	$(".categorySelect, .brandSelect").select2({
			placeholder:"Type Name..",
		});
	
	$('#startdate,#enddate').datepicker({
		minDate: '+0d',
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true,
		dateFormat: "yy-mm-dd",
	});
	
	$('#category_data').focusout(function(){
		var val = $('#category_data').val();
		var cat = $("#categoryList option").filter(function() {return this.value == val;}).data('xyz');
		$('#cat_hide_val').val(cat);
	});
	
    
});
function readURL(input) {
	var fileTypes = ['jpg', 'jpeg', 'png','bmp'];  //acceptable file types
	if (input.files && input.files[0]) {
		  var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
      isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types
		if (isSuccess) { //yes	
		  var reader = new FileReader();
		  reader.onload = function (e) {
			$('#uplode_img').attr('src', e.target.result);
		};
		  reader.readAsDataURL(input.files[0]);
		}else{
			alert('File Type Not Supported.');
		}
	}
}
function productdelete(value)
{
		$("#product_images").removeAttr("disabled");
		$("#"+value).remove();
}
</script> 
