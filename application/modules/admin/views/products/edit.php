<style>
.severimgebutton {
}
.button-server-image {
	float: right;
	margin-top: 20px;
	right: 0;
}
.checkImage {
	float: left;
	margin-top: 10px;
	position: relative;
	width: 25%;
}
.checkImage span {
	left: 5px;
	position: absolute;
	top: 0;
}
</style>
<script>
var base_url= "<?php echo base_url(); ?>";
</script>
<?php 
	$option[""] = "Choose Brand";
	if (!empty($brands)) {
		foreach ($brands as $value) {
		  $option[$value->brand_id] = ucfirst($value->brand_name);
		}
	}
	$categoryOption[""] = "Choose Category";
	if(!empty($cat_list)){
		foreach($cat_list as $list) {
			$categoryOption[$list->categoryID] = ucfirst($list->categoryName);
		}
	}
?>
<div class="page-container"> 
  
  <!-- BEGIN CONTENT -->
  
  <div class="page-content-wrapper"> 
    
    <!-- BEGIN CONTENT BODY --> 
    
    <!-- BEGIN PAGE HEAD-->
    
    <div class="page-head">
      <div class="container"> 
        
        <!-- BEGIN PAGE TITLE -->
        
        <div class="page-title">
          <h1>Edit product </h1>
        </div>
      </div>
    </div>
    
    <!-- END PAGE HEAD--> 
    
    <!-- BEGIN PAGE CONTENT BODY -->
    
    <div class="page-content">
      <div class="container"> 
        
        <!-- BEGIN PAGE BREADCRUMBS -->
        
        <ol class="breadcrumb">
          <li> <a href="<?php echo base_url();?>">Home</a> </li>
          <li> <a>Products</a> </li>
          <li class="active"> <strong>Edit Product</strong> </li>
        </ol>
        
        <!-- END PAGE BREADCRUMBS --> 
        
        <!-- BEGIN PAGE CONTENT INNER -->
        <?php if($this->session->flashdata('error_msg')){?>
        <div class="row admin-list-msg">
          <div class="col-lg-12">
            <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('error_msg');?> </div>
          </div>
        </div>
        <?php }?>
        <div class="page-content-inner">
          <?php  $attributes = array("class" => "form-horizontal", "id" => "productform", "name" =>"productform","role"=>"form","autocomplete"=>"off");

                 echo form_open_multipart('admin/product/editproduct/'.$row->product_id, $attributes); ?>
          <div class="row">
            <div class="col-md-7 "> 
              
              <!-- BEGIN SAMPLE FORM PORTLET-->
              
              <div class="portlet light ">
                <div class="portlet-title">
                  <div class="caption font-red-sunglo"> <i class="icon-settings font-red-sunglo"></i> <span class="caption-subject bold uppercase"> Products Info</span> </div>
                </div>
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#General">General</a></li>
                  <li><a data-toggle="tab" href="#Data">Data</a></li>
                </ul>
                <div class="tab-content">
                  <div id="General" class="portlet-body form tab-pane fade in active">
                    <ul class="nav nav-tabs">
                      <?php $count = 0; foreach($language as $lang){ $count++;?>
                      <li class="<?php if($count ==1){echo "active";}?>" ><a data-toggle="tab" href="#<?php echo $lang->name?>" id="<?php echo $lang->language_id?>"><?php echo $lang->name?></a></li>
                      <?php } ?>
                    </ul>
                    <div class="tab-content">
                      <?php $count = 0; foreach($language as $lang){ $count++;?>
                      <div id="<?php echo $lang->name?>" class="portlet-body form tab-pane fade  <?php if($count ==1){echo " in active";}?>">
                        <div class="form-body"> <?php echo form_hidden('row', $count); ?> <?php echo form_hidden('language_id[]', $lang->language_id); ?>
                          <div class="form-group"> <?php echo form_label('Product Name');?><span class="text-danger">*</span> <?php echo form_input ('product_name[]',$lang->product_name,'class="form-control" id="product_name" placeholder="product name" required="required"');?><span class="text-danger"><?php echo form_error('product_name'); ?></span> </div>
                          <div class="form-group"> <?php echo form_label('Short Description','for="s_description",class="control-label"');?><span class="text-danger">*</span> <?php echo form_input ('s_description[]',$lang->short_description,'class="form-control" id="price" placeholder="Give your short description" required="required"');?><span class="text-danger"><?php echo form_error('s_description'); ?></span> </div>
                          <div class="form-group"> <?php echo form_label('Message','for="product_image"');?>
                          <!--Text Paste Here--> 
                            <textarea cols="80" id="editor<?= $count ?>" name="editor[]" rows="10" >
                               <?php 
                                    echo $lang->message;
                                 ?> 
                          </textarea>
                          </div>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div id="Data" class="tab-pane fade">
                    <div class="form-group"> <?php echo form_label('SKU');?><span class="text-danger">*</span> <?php echo form_input ('SKU',$row->sku,'class="form-control" id="SKU" placeholder="Stock keeping Unit" required="required"');?><span class="text-danger"><?php echo form_error('SKU'); ?></span> </div>
                    <div class="form-group"> <?php echo form_label('Stock','for="stock"','class="control-label"');?><span class="text-danger">*</span>
                      <?php $stock=array('1'=>'In Stock','0'=>'Out of Stock');?>
                      <?php echo form_dropdown('stock',$stock,$row->stock,'class="form-control"');?> </div>
                    <div class="form-group"> <?php echo form_label('Stock Quantity','for="stock_quantity"');?><span class="text-danger">*</span> <?php echo form_input ('stock_quantity',$row->stock_quantity,'class="form-control" id="stock_quantity" placeholder="Stock Quantity" required="required"');?> <span class="text-danger"><?php echo form_error('stock_quantity'); ?></span> </div>
                    <div class="form-group"> <?php echo form_label('Price','for="price"','class="control-label"');?><span class="text-danger">*</span> <?php echo form_input ('price',$row->price,'class="form-control" id="price" placeholder="Price" required="required"');?><span class="text-danger"><?php echo form_error('price'); ?></span> </div>
                    <div class="form-group"> <?php echo form_label('Sale Price','for="saleprice"','class="control-label"');?><span class="text-danger">*</span> <?php echo form_input ('saleprice',$row->sale_price,'class="form-control" id="price" placeholder="Sale Price" required="required"');?><span class="text-danger"><?php echo form_error('saleprice'); ?></span> </div>
                    <div class="form-group"> <?php echo form_label('Choose Product Brand','for="brand",class="control-label"');?><span class="text-danger">*</span> <?php echo form_dropdown('brand',$option,$row->brand_id,['class'=>'form-control brandSelect','required'=>true]); ?> </div>
                    <div class="form-group"> <?php echo form_label('Choose Category','class="control-label"');?><span class="text-danger">*</span> <?php echo form_dropdown('category',$categoryOption,$row->category_id,['class'=>'form-control categorySelect','required'=>true]); ?> <span class="text-danger"><?php echo form_error('category'); ?></span> </div>
                    <div class="form-group"> <?php echo form_label('Start Date','for="start_date"','class="control-label "');?> <?php echo form_input(['name'=>'startdate','class'=>'form-control input-group date','id'=>'startdate','placeholder'=>'Seal Start Date. E.g - YYYY-MM-DD','value'=>$row->sale_start_date]); //,'value'=>$row->sale_start_date ?> </div>
                    <div class="form-group"> <?php echo form_label('End Date','for="end_date"','class="control-label"');?> <?php echo form_input(['name'=>'enddate','class'=>'form-control input-group date','id'=>'enddate','placeholder'=>'Seal End Date. E.g - YYYY-MM-DD','value'=>$row->sale_end_date]); //,'value'=>$row->sale_end_date ?> </div>
                    <div class="form-group"> <?php echo form_label('Shipping Weight','for="shipping_weight"','class="control-label"');?><?php echo form_input ('shipping_weight',$row->shipping_weight,'class="form-control" id="weight" placeholder="Shipping Weight"');?></div>
                    <div class="form-group"> <?php echo form_label('Shipping Dimension','for="shipping_dimension"','class="control-label"');?><?php echo form_input ('shipping_dimension',$row->shipping_dimension,'class="form-control" id="dimension" placeholder="Shipping Dimension"');?></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-5 ">
              <div class="portlet light ">
                <div class="portlet-title">
                  <div class="caption"> <i class="icon-settings font-dark"></i> <span class="caption-subject font-dark sbold uppercase">Product Images</span> </div>
                </div>
                <div class="portlet-body form">
                  <div class="form-body">
                    <div class="form-group">
                      <div class="col-md-4"> <?php echo form_label('Product Image','for="product_image"','class=" control-label"');?><span class="text-danger">*</span></div>
                      <div class="col-md-8"> <?php echo form_button('product_images','Edit product Image','data-toggle="modal" data-target="#productsingleimages" class="btn btn-success" id="product_images"');?> </div>
                      <div id="dvproductimage" style="margin-top:46px;">
                        <?php if($row->product_image != ''){
							  if(is_file($this->config->item('image_path').'/product/'. $row->product_id .
$this->config->item('thumb_size').$row->product_image)){
								
								$imagepath = site_url('photo/product/'. $row->product_id .
$this->config->item('thumb_size').$row->product_image);
								
							}
							else{
								$imagepath = site_url('photo/product/no_product.jpg');
								
								 }
							
							 ?>
                        <img src="<?php echo $imagepath; ?>" style="width:80px;height:60px;"/> </div>
                      <?php }else{ ?>
                      <img src="<?php echo base_url()?>photo/product/no_product.jpg" style="width:80px;height:60px;"/> </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="portlet light ">
              <div class="portlet-body form">
                <div class="form-body">
                  <div class="form-group">
                    <div class="col-md-4"> <?php echo form_label('Additional Images','for="additional_image"','class=" control-label"');?></div>
                    <div class="col-md-8"> 
                      
                      <!--  <button type="button" data-toggle="modal" data-target="#myModal" class="btn default">Upload</button>--> 
                      
                      <?php echo form_button('addition_images','Upload','data-toggle="modal" data-target="#myModal" class="btn default"');?> ` </div>
                    <div id="dvPreview" style="margin-top:46px;">
                      <?php if($galleries){?>
                      <?php foreach($galleries as $gallery) {
						  if(is_file($this->config->item('image_path').'/product/'. $row->product_id .
$this->config->item('thumb_size').$gallery->image_name)){
								
								$imagepath = site_url('photo/product/'. $row->product_id .
$this->config->item('thumb_size').$gallery->image_name);
								
							}
							else{
								$imagepath = site_url('photo/product/no_product.jpg');
								
								 }
						  ?>
                      <div style="position: relative; float: left;  padding: 1px;" id="<?php echo $gallery->img_id ?>">
                      <img src="<?php echo $imagepath;  ?>" style="width:80px;height:60px;"/>
                        <div style="position: absolute; z-index: 1000; top: 0px; right: 7px; color: red;">
                        <a style="color: red" onclick= deleteproductbyid(<?php echo $gallery->img_id ?>,'<?php echo $gallery->image_name ?>',<?php echo $row->product_id ?>);>
                        <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                        </div>
                      </div>
                      <input type="hidden" name="deleteproductsimages[]" id ="delete_exist_value_<?php echo $gallery->img_id ?>" />
                      <?php }}

						else{?>
                      <div style="text-align:center;color:#F00;" class="noproductimage">No Images Found.</div>
                      <?php	}

						?>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 ">
            <div class="portlet light ">
              <div class="form-actions"> <?php echo form_submit('submit','Update','class="btn btn-success productSubmit"'); ?>
                <input class="btn default" type="button" onclick="window.location.replace('<?php echo base_url('admin/dashboard');?>')" value="Cancel"/>
              </div>
            </div>
          </div>
        </div>
        <?php echo form_close(); ?> </div>
      
      <!-- END PAGE CONTENT INNER --> 
      
    </div>
  </div>
  
  <!-- END PAGE CONTENT BODY --> 
  
  <!-- END CONTENT BODY --> 
  
</div>

<!-- END CONTENT --> 

<!-- BEGIN QUICK SIDEBAR --> 

<a class="page-quick-sidebar-toggler" href="javascript:;"> <i class="icon-login"></i> </a> 

<!-- END QUICK SIDEBAR -->

</div>

<!-- Additional product Images-->

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content" style="width:522px; height:574px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Additional Images</h4>
      </div>
      <div class="modal-body">
        <iframe src="<?php echo base_url();?>uploader/upload_edit.php" width="503px" height="500px" style="border:0px;" id="uploader"></iframe>
      </div>
    </div>
  </div>
</div>

<!-- product Single images-->

<div id="productsingleimages" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content" style="width:522px; height:574px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Product Image</h4>
      </div>
      <div class="modal-body">
        <iframe src="<?php echo base_url();?>uploader/uploadproductimageedit.php" width="503px" height="500px" style="border:0px;" id="uploader"></iframe>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>assets/js/plugins/summernote/addproduct_summernote.min.js"></script> 
<script>
    CKEDITOR.replace( 'editor1', {
      height: 180
    } );
    CKEDITOR.replace( 'editor2', {
      height: 180
    } );
</script> 
<script>
$(document).ready(function(e) {
$(".categorySelect, .brandSelect").select2();
$('#startdate,#enddate').datepicker({
    minDate: '+0d',
    todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true,
		dateFormat: "yy-mm-dd",
   
});
$('#category_data').focusout(function(){
    var val = $('#category_data').val();
    var cat = $("#categoryList option").filter(function(){return this.value == val;}).data('xyz');
    $('#cat_hide_val').val(cat);
});
});
function productdelete(value)
{
		$("#product_images").removeAttr("disabled");
		$("#"+value).remove();
}

function deleteproductbyid(value,imageurl,productid)
{
  	var deletevar =[];
    $("#product_images").removeAttr("disabled");
    $("#"+value).remove();
    deletevar.push(value);
    $("#delete_exist_value_"+value).val(deletevar);
	var additionalimageid = 'additionalimageid='+ value +'&imageurl='+imageurl+'&productid='+productid; 
	$.ajax({
				type: "POST",
				url: base_url+'admin/product/additionalImagedelete',
				data: additionalimageid,
				cache: false,
                dataType: 'json',
				success: function(result){
						setTimeout(function(){
						
						if(result.msg == 'unique'){
								console.log('fail');
						}
						if(result.msg == 'success'){
						//window.location.href = base_url+'wishlist/create';
								console.log(result)
						}
						if(result.msg == 'error'){
								console.log(result)
						}
						}, 1000);
			}	


			
		});
}   
</script> 
