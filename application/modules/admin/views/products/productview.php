<style>
.product-details {
	background: #eee none repeat scroll 0 0;
	border: 5px solid #87CEEB;
	color: #000;
	font-size: 13px;
	line-height: 27px;
	margin-bottom: 15px;
	padding-top: 25px;
}
.image-imitation {
    background-color: #f8f8f9;
    padding: 15px 0;
    text-align: center;
}
</style>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Product Info</h5>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="col-md-5">
            <div class="product-images">
				<?php if (!empty($galary)) {foreach ($galary as $value) { ?>
                    <div>
                        <div class="image-imitation">
                        	<?php if(is_file($this->config->item('image_path').'/product/'. $product_id .
$this->config->item('large_thumb_size').$value)){
								
								$imagepath = site_url('photo/product/'. $product_id .
$this->config->item('large_thumb_size').$value);
								
							}
							else{
								$imagepath = site_url('photo/product/no_product.jpg');
								
								 }?>
                        	<img src="<?php echo $imagepath; ?>" width="387" height="430">
                        </div>
                    </div>
                <?php }}else{ ?>
                	<div>
                        <div class="image-imitation">
                        	<img src="<?= site_url('photo/product/no_product.jpg'); ?>" width="387" height="430">
                        </div>
                    </div>
                <?php } ?>
            </div>
          </div>
          <div class="col-md-7">
		
          <?php if(!empty($data)){
					foreach($data as $list){if($list->language_id == '1'){ ?>
            <h2 class="font-bold m-b-xs"> <?= ucfirst($list->product_name) ?> </h2>
            <small><!--Many desktop publishing packages and web page editors now.--></small>
            <div class="m-t-md">
              <h2 class="product-main-price"><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?> <small class="text-muted"><!--Exclude Tax--> Price</small> </h2>
            </div>
            <hr>
            <h4>Short Description</h4>
            <div class="small text-muted"> <?= ucfirst($list->short_description) ?> </div>
            <h4>Sale Start Date</h4>
            <div class="small text-muted"> <?= $list->sale_start_date ?> </div>
            <h4>Sale End Date</h4>
            <div class="small text-muted"> <?= $list->sale_end_date ?> </div>
            <h4>Price</h4>
            <div class="small text-muted"> <?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?> </div>
            <h4>Sale Price</h4>
            <div class="small text-muted"> <?= strtoupper($this->config->item('currency')).' '.number_format($list->sale_price) ?> </div>
            
            <h4>Product Description</h4>
            <div class="small text-muted"> <?= ucfirst($list->message) ?> </div>
            
            <hr>
          <?php }}}else{ ?>
            <div class="col-md-12"> No Data Available </div>
          <?php } ?>
          </div>
        </div>
        <div style="text-align: right;"> <a href="<?= base_url('admin/product/edit/'.$this->uri->segment(4)); ?>" class="btn btn-success">Edit</a> </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){
        $('.product-images').slick({
            dots: true
        });
    });

</script>
