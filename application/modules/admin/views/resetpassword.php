<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo (isset($page_title)) ? $page_title : $this->lang->line('admin_tiltle');?></title>
<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/animate.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="passwordBox animated fadeInDown">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox-content">
        <h2 class="font-bold"><?php echo $this->lang->line('re_password'); ?></h2>
        <div class="row">
          <div class="col-lg-12">
            <?php if($this->session->flashdata('re-msg')) {?>
            <div class="alert alert-danger display-hide"><?php echo $this->session->flashdata('re-msg');?>
              <button class="close" data-close="alert"></button>
            </div>
            <?php }?>
            
            <!-- <form class="m-t" role="form" action="http://webapplayers.com/inspinia_admin-v2.5/index.html">-->
            
            <?php $attributes = array('class' => 'm-t', 'id' => 'loginFrm','method' => 'post' ,'role' =>'form');



				echo form_open('admin/resetpassword/'.$id, $attributes);?>
            <div class="form-group"> 
              
              <!--<input type="email" class="form-control" placeholder="Email address" required="">-->
              
              <?php $pass = array('name' => 'password','class' => 'form-control','placeholder' => $this->lang->line('password'),'required'=>'required'); ?>
              <?= form_input($pass); ?>
            </div>
            <div class="form-group"> 
              
              <!--<input type="email" class="form-control" placeholder="Email address" required="">-->
              
              <?php $re_pass = array('name' => 're_password','class' => 'form-control','placeholder' => $this->lang->line('re_password'),'required'=>'required'); ?>
              <?= form_input($re_pass); ?>
            </div>
            
            <!-- <button type="submit" class="btn btn-primary block full-width m-b">Send</button>--> 
            
            <?php echo form_submit('email_submit',$this->lang->line('send'),'class="btn btn-primary block full-width m-b"');?> <?php echo form_close();?> </div>
        </div>
      </div>
    </div>
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-6"> <?php echo $this->lang->line('company_title'); ?> </div>
    <div class="col-md-6 text-right"> <small>© <?php echo date('Y');?></small> </div>
  </div>
</div>
</body>

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.5/forgot_password.html by HTTrack Website Copier/3.x [XR&CO'2010], Fri, 29 Apr 2016 07:11:13 GMT -->

</html>
