<div class="row wrapper border-bottom white-bg page-heading" >
  <div class="col-lg-10">
    <h2>Email Configuration List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Setting</a> </li>
      <li class="active"> <strong>Email Configuration List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('successMsg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->flashdata('adminDeleteMsg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('adminDeleteMsg');?> </div>
  </div>
</div>
<?php }?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Email Configuration List</h5>
      </div>
      <div class="ibox-content">
      <?php if(empty($email_config)){ ?>
        <div class=""> <a href="<?= base_url('admin/setting/config-add'); ?>" class="btn btn-success ">Add New</a> </div>
       <?php } ?>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Admin Email</th>
              <th>Support Email</th>
              <th class="center">Actions</th>
            </tr>
          </thead>
		<tbody>
        <?php if(!empty($email_config)){ $i = 0; foreach($email_config as $row){ ?>
			<tr>
				<td><?php echo ++$i; ?></td>
				<td><?php echo $row->admin_email; ?></td>
				<td><?php echo $row->info_email; ?></td>
                <td>
                	<a href="<?php echo base_url("admin/setting/config-email-edit/".$row->setting_id); ?>"><i class="fa fa-pencil-square-o fa-fw action-btn"></i></a>
                    <a href="javascript:void(0)" id="<?php echo $row->setting_id; ?>" class="deleteConfig"><i class="fa fa-trash-o fa-fw action-btn text-danger"></i></a>
               </td>
			</tr>
         <?php }} ?>
		</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
		 /* Init DataTables */
	$('#admintable').DataTable({
		//order:[[0,"desc"]],
		//pageLength: 25,
	});
	$(document).on('click','.deleteConfig',function(){
		var configId=$(this).attr('id');
		 if(configId!=""){
			bootbox.confirm("Are you sure you want delete ?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url("admin/setting/config-email-delete/"); ?>" + configId ;
				}
			}); 
		 }
	});
});

		</script>