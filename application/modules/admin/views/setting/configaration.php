<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Email Configuration</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Setting</a> </li>
      <li class="active"> <strong>Email Configuration</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('succeess_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('succeess_message');?> </div>
    </div>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Email Configuration</h5>
        </div>
        <div class="ibox-content">
          <?php 
		  		$attributes = array('class' => 'form-horizontal','id' => 'brandform');
				echo form_open('admin/setting/config-add', $attributes);?>
          <div class="form-group">
            <div class="col-md-3">
            	<?php echo  form_label('Admin Email','admin_email',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
              <?= form_input(array('name' => 'admin_email','type' => 'email','class' => 'form-control','placeholder' => 'Admin Email','required'=>'required','value' => set_value('admin_email'))); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">
            	<?php echo  form_label('Support Email','support_email',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
              <?= form_input(array('name' => 'support_email','type' => 'email','class' => 'form-control','placeholder' => 'Support Email','required'=>'required','value' => set_value('admin_email'))); ?>
            </div>
          </div>

          <div class="hr-line-dashed"></div>
         
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
              <?php echo form_submit('configSubmit','Save','class="btn btn-primary"');?> </div>
          </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
