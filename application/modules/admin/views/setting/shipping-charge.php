<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Shipping Charge</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Setting</a> </li>
      <li class="active"> <strong>Shipping Charge</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('succeess_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('succeess_message');?> </div>
    </div>
  </div>
  <?php }?>
    <?php if($this->session->flashdata('error_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
    </div>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
        <?php if(count($shippingData) > 0){ $title = "Update Shipping Charge"; }else{ $title = "New Shipping Charge"; } ?>
          <h5><?php echo $title ?> </h5> <br>
          <div class="text-danger"><strong>N.B :-</strong> Shipping Charge applicable for "One Day Service" delivery method.</div>
        </div>
        <div class="ibox-content">
        <div>
        <?php if(count($shippingData) == 0){ ?>
         	<div class="col-md-6 col-md-offset-3">
            	<?php echo form_open('admin/setting/shipping-charge-add', array('class' => 'form-horizontal','id' => 'Shippingform'));?>
                  <div class="form-group">
                    <div class="col-md-3">
                        <?php echo  form_label('Shipping Value','',array('class'=>'control-label')); ?>
                    </div>
                    <div class="col-md-9">
                      <?php echo form_input(array('name' => 'shipping_val','class' => 'form-control valinsert','placeholder' => 'Enter Shipping value with % or Rp sign','required'=>'required','value' => set_value('shipping_val'))); ?>
                      <div class="text-danger" id="errorVal"></div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <div class="col-sm-4 col-sm-offset-2">
                      <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
                      <?php echo form_submit('ShippingSubmit','Save',array("class"=>"btn btn-primary", "id"=>"ShippingSubmit"));?> </div>
                  </div>
                  <?php echo form_close();?> 
            </div>
 		<?php }else{ ?>
         	<div class="col-md-6 col-md-offset-3">
            	<?php echo form_open('admin/setting/shipping-charge-add', array('class' => 'form-horizontal','id' => 'Shippingform'));?>
                  <div class="form-group">
                    <div class="col-md-3">
                        <?php echo  form_label('Shipping Value','',array('class'=>'control-label')); ?>
                    </div>
                    <div class="col-md-9">
                      <?php 
					  		if(set_value('shipping_val')) {$value = set_value('shipping_val');}else{$value = strtoupper($shipping_charge);}
					  		echo form_input(array("name"=>"shipping_id", "type"=>"hidden", "value"=>$shipping_id));
					  		echo form_input(array('name' => 'shipping_val','class' => 'form-control valinsert','placeholder' => 'Enter Shipping value with % or Rp sign','required'=>'required','value' => $value)); 
						?>
                      <div class="text-danger" id="errorVal"></div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-2">
                      <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
                      <?php echo form_submit('ShippingUpdate','Update',array("class"=>"btn btn-primary", "id"=>"ShippingUpdate"));?> </div>
                  </div>
                  <?php echo form_close();?> 
            </div>
        <?php } ?>
            <div class="clearfix"></div>
        </div>
        <div>
        	<table class="table table-striped table-bordered table-hover " id="admintable_bak" >
            	<thead>
                	<th>ID</th>
                	<th>Shipping Charge</th>
                	<th>Action</th>
                </thead>
                <tbody>
                <?php if(!empty($shippingData)){ $i=0; foreach($shippingData as $row) { ?>
                    <tr>
                        <td><?php echo ++$i; ?></td>
                        <td><?php echo strtoupper($row->shipping_fees); ?></td>
                        <td><a href="javascript:void(0)" class="shipping-delete" id="<?php echo $row->shipping_id; ?>"><i class="fa fa-trash-o fa-fw action-btn text-danger"></i></a></td>
                    </tr>
                 <?php } } ?>
                </tbody>
            </table>
        </div>
         </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
    $('#admintable').DataTable({});
	$(document).on("click","#ShippingSubmit",function(){
			var shipping_val = $.trim($(".valinsert").val()).split(" ");
			var shipping_val_length = shipping_val.length;
			var inputFirstValue = shipping_val[0];
			var inputlastValue = shipping_val[1];
			if(shipping_val_length == 2)
			{
				if(inputFirstValue == "Rp" || inputFirstValue == "rp" || inputFirstValue == "RP")
				{
					var inputFirstValueCharecter = inputlastValue[0];
					if($.isNumeric(inputlastValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							$("#errorVal").html("");
						}
						else
						{
							$("#errorVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorVal").html("Entire value must be numeric.");
						return false;
					}
				}
				else if(inputlastValue == "%")
				{
					var inputFirstValueCharecter = inputFirstValue[0];
					if($.isNumeric(inputFirstValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							if(inputFirstValue <= 100)
							{
								$("#errorVal").html("");
							}
							else
							{
								$("#errorVal").html("Percentage value not more than 100.");
								return false;
							}
							
						}
						else
						{
							$("#errorVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorVal").html("Entire value must be numeric.");
						return false;
					}
					
				}
				else
				{
					$("#errorVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
					return false;
				}
			}
			else
			{
				$("#errorVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
				return false;
			}
			//return false;
		});
	$(document).on("click","#ShippingUpdate",function(){
			var shipping_val = $.trim($(".valinsert").val()).split(" ");
			var shipping_val_length = shipping_val.length;
			var inputFirstValue = shipping_val[0];
			var inputlastValue = shipping_val[1];
			if(shipping_val_length == 2)
			{
				if(inputFirstValue == "Rp" || inputFirstValue == "rp" || inputFirstValue == "RP")
				{
					var inputFirstValueCharecter = inputlastValue[0];
					if($.isNumeric(inputlastValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							$("#errorVal").html("");
						}
						else
						{
							$("#errorVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorVal").html("Entire value must be numeric.");
						return false;
					}
				}
				else if(inputlastValue == "%")
				{
					var inputFirstValueCharecter = inputFirstValue[0];
					if($.isNumeric(inputFirstValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							if(inputFirstValue <= 100)
							{
								$("#errorVal").html("");
							}
							else
							{
								$("#errorVal").html("Percentage value not more than 100.");
								return false;
							}
						}
						else
						{
							$("#errorVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorVal").html("Entire value must be numeric.");
						return false;
					}
					
				}
				else
				{
					$("#errorVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
					return false;
				}
			}
			else
			{
				$("#errorVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
				return false;
			}
			//return false;
		});
	$(document).on('click','.shipping-delete',function(){
		var shippingID=$(this).attr('id');
		 if(shippingID!=""){
		bootbox.confirm("Are you sure want to delete?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url(); ?>admin/setting/shipping-delete/"+shippingID; 
				}
			}); 
		 }
	});
});
</script>
