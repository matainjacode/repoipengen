  
     <link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet"> 
    <?php if($this->session->flashdata('static_block3')){?>
            <div class="row admin-list-msg">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins admin-delete-falsh-msg">
                  <?php echo $this->session->flashdata('static_block3');?>
                  </div>
                </div>
             </div>
            <?php }?>            

     
        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Block</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li>
                            <a>Static Block</a>
                        </li>
                        <li class="active">
                            <strong>Add Block</strong>
                            
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
        <?php if($this->session->flashdata('static_blockInsertMsg')){?>
            <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins admin-flash-msg">
                  <?php echo $this->session->flashdata('static_blockInsertMsg');?>
                  </div>
                </div>
             </div>
            <?php }?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add Block </h5>
                        </div>
                        <div class="ibox-content">
                            <?php $attributes = array('class' => 'form-horizontal', 'id' => 'blockform','method' => 'post' ,'role' =>'form');
				                echo form_open('admin/static_block/submit', $attributes);?>
                                <strong></strong>
								 <div class="form-group">
								 
          	<!--Tab Option Panel-->
              <ul class="nav nav-tabs">
              <?php if(!empty($language)){ $a=0; foreach($language as $lang) { if($a==0){$class = "active";}else{$class = "";} ?>
                <li class="<?php echo $class; ?>"><a data-toggle="tab" href="#<?php echo $lang->name; ?>"><?php echo ucfirst($lang->name); ?></a></li>
              <?php $a++;}} ?>
              </ul>
            <!--Tab Body Panel-->
              <div class="tab-content">
              <?php if(!empty($language)){ $a=0; foreach($language as $lang) { if($a==0){$class = "active";}else{$class = "";} ?>
                <div id="<?php echo $lang->name; ?>" class="tab-pane fade in <?php echo $class; ?>">
                    <div style="margin-top: 14px;">
                    	<?= form_input(array('name' => 'langId[]','type' => 'hidden','value' => $lang->language_id )); ?>
                        <div class="form-group">
                                 <?php  $first_name_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Block name','block_name',$first_name_label);?>

                                    <div class="col-sm-10">
                                     <?php $block_name = array('name' => 'block_name[]','class' => 'form-control original','id'=>'','placeholder' => 'Block name','required'=>'required','value' => set_value('block_name')); ?>
                    <?= form_input($block_name); ?>
                               </div>
                                    </div>

                                   <?php //plain text area change ?>

                                 <div class="form-group">
                                 <?php  $first_name_label = array('class'=>'col-sm-2 control-label');
								    echo  form_label('Block Description','block_description',$first_name_label);?>
                                    <div class="col-sm-10">
                                     <?php $value = array('name' => 'block_description[]','class' => 'form-control','placeholder' => 'Block Description','required'=>'required','value' =>                                                     set_value('block_description')); ?>

                                <?= form_textarea($value); ?>
                               </div>
                                    </div>  
                   </div>
                </div>
              <?php $a++;}} ?>
              </div>
          </div>
		  
                              <div class="hr-line-dashed"></div>  
                               
                                   <div class="form-group">
                                                 <label class="col-sm-2 control-label">Block Status</label>
                                                 <div class="col-md-3">
                                                <select name="state" id="actionmodel" dir="sate_name" class="form-control">
                                                <option value="select_one"> ---Select---</option>
                                                <option value="1"> Enable</option>
                                                 <option value="0"> Disable</option>
                                          </select>
                                          </div>
                                       </div>

                                <div class="form-group">
                                     <div class="col-sm-4 col-sm-offset-4">
                                        <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
                                         <button  type="submit" name="submit" class="btn btn-primary" id="fromsubmit" >Save</button>
                                    </div>
                                      
                                </div>
                           <?php echo form_close();?>
                         
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
       
        <script src="<?php echo base_url();?>assets/js/plugins/summernote/summernote.min.js"></script> 
        

