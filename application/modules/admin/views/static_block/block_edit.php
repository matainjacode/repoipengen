<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit Block</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url();?>">Home</a>
                </li>
                <li>
                    <a>Static Block</a>
                </li>
                <li class="active">
                    <strong>Edit Block</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
            
<div class="wrapper wrapper-content animated fadeInRight">
        <?php if($this->session->flashdata('pagesEditMsg')){?>
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('pagesEditMsg');?> </div>
    </div>
    </div>
    <?php }?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit Block </h5>
                        </div>
                        <div class="ibox-content">
                            <?php if(!empty($result)){
									foreach($result as $val){
											$attributes = array('class' => 'form-horizontal', 'id' => 'adminform','method' => 'post' ,'role' =>'form');
											echo form_open('admin/static_block/edit/'.$val->block_id, $attributes);
									}
								}
							?>
                            <strong></strong>
								 <div class="form-group">
								 
          	<!--Tab Option Panel-->
              <ul class="nav nav-tabs">
              <?php $classAc="";$j=0;
             if(!empty($result)){ 
             foreach($result as $val){
                 if($j==0){$classAc="active";}else{$classAc="";}?>
        <li class="<?php echo $classAc; ?>"><a data-toggle="tab" href="#<?php echo $val->lang_name;?>"><?php echo ucfirst($val->lang_name);?></a></li>
        <?php $j++;}} ?>
              </ul>
            <!--Tab Body Panel-->
              <div class="tab-content">
               <?php $class="";$i=0;
            if(!empty($result)){
                
             foreach($result as $val){?>
    <?php 
            if($i==0){$class=" in active";}else{$class="";}
            
            ?>
                <div id="<?php echo $val->lang_name;; ?>" class="tab-pane fade in <?php echo $class; ?>">
                    <div style="margin-top: 14px;">
                    	<?= form_input(array('name' => 'language[]','type' => 'hidden','value' => $val->language_id )); ?>
                         <div class="form-group">
             <?php  $page_name_label = array('class'=>'col-sm-2 control-label');
            echo  form_label('Block name','block_name',$page_name_label);?>
    
                <div class="col-sm-10">
                 <?php $block_name = array('name' => 'block_name[]','class' => 'form-control','placeholder' => 'Block name','required'=>'required','value' => $val->block_name );?>
    <?= form_input($block_name); ?>
                </div>
            </div>
          
          				 <div class="form-group">
                         <?php  $first_name_label = array('class'=>'col-sm-2 control-label');
                            echo  form_label('Block Description','block_description',$first_name_label);?>
                            <div class="col-sm-10">
                             
                         <?php $value = array('name' => 'block_description[]','class' => 'form-control','placeholder' => 'Block description','required'=>'required');
            
            echo form_textarea('block_description[]', $val->block_description,  $value); ?>
            
                       </div>
                            </div> 
                   </div>
                </div>
              <?php $i++; }} ?>
              </div>
          </div>
		  
                              <div class="hr-line-dashed"></div>  
                                <?php   if(!empty($result)){
    											$k=0;
										 
									?>                        
									<div class="form-group">
									<label class="col-sm-2 control-label">Block Status</label>
									<div class="col-md-3">
									<select name="state" id="actionmodel" dir="<?php $result[$k]->block_id ?>" class="form-control">
									<?php 
									if($result[$k]->block_status == 1){ ?>
									<option value="1" selected="selected">Enable</option>
									<option value="0">Disable</option>
									<?php }
									else  { ?>
									
									<option value="1" >Enable</option>
									<option value="0" selected="selected">Disable</option>
									
									
									
									<?php } ?>
									
									</select>     
									
									</div>
									</div>
									<?php  } ?>
									<div class="form-group">
									<div class="col-sm-4 col-sm-offset-2">
									<button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/static_block/list_block';">Cancel</button>
									<?php echo form_submit('submit','Save changes','class="btn btn-primary"');?> </div>
									</div>
                           <?php echo form_close();?>
                         
                        </div>
                    </div>
                </div>
            </div>
            
        </div>           
            

