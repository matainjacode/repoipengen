<style>

.error {

color:red;

font-size:13px;

margin-bottom:-15px

}

</style>

<?php if($this->session->flashdata('pagesEditMsg')){?>

            <div class="row admin-list-msg">

                <div class="col-lg-12">

                  <div class="ibox float-e-margins admin-flash-msg">

                  <?php echo $this->session->flashdata('pagesEditMsg');?>

                  </div>

                </div>

             </div>

            <?php }?>

<?php if($this->session->flashdata('static_block1')){?>

            <div class="row admin-list-msg">

                <div class="col-lg-12">

                  <div class="ibox float-e-margins admin-flash-msg">

                  <?php echo $this->session->flashdata('static_block1');?>

                  </div>

                </div>

             </div>

            <?php }?>



<?php if($this->session->flashdata('pagesDeleteMsg')){?>

            <div class="row admin-list-msg">

                <div class="col-lg-12">

                  <div class="ibox float-e-margins admin-delete-falsh-msg">

                  <?php echo $this->session->flashdata('pagesDeleteMsg');?>

                  </div>

                </div>

             </div>

            <?php }?>  

 

 <?php if($this->session->flashdata('blockupdatestate')){?>

            <div class="row admin-list-msg">

                <div class="col-lg-12">

                  <div class="ibox float-e-margins admin-flash-msg">

                  <?php echo $this->session->flashdata('blockupdatestate');?>

                  </div>

                </div>

             </div>

            <?php }?>

            

            

            <?php if($this->session->flashdata('blockupdatestatefail')){?>

            <div class="row admin-list-msg">

                <div class="col-lg-12">

                  <div class="ibox float-e-margins admin-delete-falsh-msg">

                  <?php echo $this->session->flashdata('blockupdatestatefail');?>

                  </div>

                </div>

             </div>

            <?php }?>  

            
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Static Block List</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li>
                            <a>Static Block</a>
                        </li>
                        <li class="active">
                            <strong>Static Block List</strong>
                            
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>

  <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">


      <div class="col-lg-12">

        <div class="ibox float-e-margins">

          <div class="ibox-title">

           

            <button class="btn btn-primary" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/static_block/add';"> Add Static Block</button>

            <div class="ibox-tools"> 

            <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> <a class="dropdown-toggle" data-toggle="dropdown" href="#"></a>

              <a class="close-link"> <i class="fa fa-times"></i> </a> </div>

          </div>

          

          

          

          <div class="ibox-content">

              <table class="table table-striped table-bordered table-hover" id="editable">

            <thead>

            <tr> <th>ID</th>
                <th>NAME</th>
                <th>KEY</th>
                <th>DESCRIPTION</th>
                 <th>STATUS</th>
                <th>Actions</th>
            </tr>

            </thead>

            <tbody>

               <?php foreach($data as $value){ ?>

                <tr>

                <td><?php echo $value['block_id'] ?> </td>

                <td><?php echo $value['block_name'] ?> </td>

                <td><?php echo $value['block_key'] ?></td>

                <td><?php echo $value['block_description']?></td>

               <td>
			  <?php
                  if ($value['block_status'] == 0) {
                    $check = '';
                  }else{
                    $check = 'checked';
                  }
                ?>
                <input type="checkbox" value="<?= $value['block_id'] ?>" class="js-switch" <?= $check; ?> />
                </td>

                

                 <td class="center"><a href="<?php echo base_url()?>admin/static_block/edit/<?php echo $value['block_id']; ?>" title="Edit"><i class="fa fa-pencil-square-o action-btn"></i></a>&nbsp;


                      <a href="<?php echo base_url()?>admin/static_block/delete/<?php echo $value['block_id']; ?>" title="Delete"><i class="fa fa-trash-o action-btn text-danger" ></i></a>             </td>

               

                

                

            </tr>

            <?php } ?>

           

            

            </tbody>

            

            </table>





           

            </div>

        </div>

      </div>

    </div>

  </div>



<script>

        $(document).ready(function(){

           $('#editable').DataTable({

			   "order": [[ 0, "desc" ]],

				"pageLength": 50,});

		});
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
		
		/*******Switcher On-Off*************/
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch')); //console.log();
		elems.forEach(function(data) {
		var switchery = new Switchery(data,{ color: '#1AB394' });
	});
	
	$(document).on("change",".js-switch",function(e){
		  var blockId = $(this).attr("value");
		  var blockName = $(this).closest("tr").find("td:eq(1)").html();
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo base_url('admin/static_block/enablestatus/'); ?>" + blockId ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(blockName +" now has been activated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/static_block/disablestatus/'); ?>" + blockId ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(blockName +" now has been deactivated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});

</script>



