<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo (isset($page_title)) ? $page_title : $this->lang->line('admin_tiltle');?>
<?php //echo $this->lang->line('dashboard');?>
</title>
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/components.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!-- Jquery User Interface-->
<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<!-- Loader CSS-->
<link href="<?php echo base_url();?>assets/css/loader.css" rel="stylesheet" type="text/css" />

<!--Sumer Notes-->
<link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<!-- Morris -->
<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/admin.css" rel="stylesheet">
<!-- slick carousel-->
<link href="<?php echo base_url();?>assets/css/plugins/slick/slick.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/slick/slick-theme.css" rel="stylesheet">
<!-- Switchery -->
<link href="<?php echo base_url();?>assets/css/plugins/switchery/switchery.css" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url();?>assets/css/plugins/select2/select2.min.css" rel="stylesheet">

<!--********************************Js File Start***************************************-->
<script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!-- Data picker -->
<script src="<?php echo base_url('assets/js/plugins/datapicker/bootstrap-datepicker.js');?>"></script>
<script src="http://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
<!-- slick carousel-->
<script src="<?php echo base_url();?>assets/js/plugins/slick/slick.min.js"></script>
<!-- Switchery -->
<script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>
<!--Image Zoom-->
<script src="<?php echo base_url();?>assets/js/plugins/image-tooltip/image-tooltip.js"></script>
</head>

<body>
<div id="wrapper">
<?php 
	$query = $this->db->get_where('admin_login',array('email'=>$this->session->userdata('logged_in'))); 
  $result = $query->result();
  $full_name = "";
  $position = "";
  if (!empty($result)) {
    foreach ($result as $value) {
      $full_name = ucfirst($value->first_name).' '.ucfirst($value->last_name);
      $position = ucfirst($value->position);
    }
  }
  
 
?>
<nav class="navbar-default navbar-static-side" role="navigation">
<div class="sidebar-collapse">
<ul class="nav metismenu" id="side-menu">
<li class="nav-header">
  <div class="dropdown profile-element"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $full_name; ?></strong> </span> <span class="text-muted text-xs block"><?php echo $position; ?> <b class="caret"></b></span> </span> </a> </div>
  <div class="logo-element"> IP+ </div>
</li>
<li class="active"> <a href="<?= base_url('admin'); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a> </li>
<li class="adminList"> <a href="#"><i class="fa fa-user-secret"></i> <span class="nav-label">Admin</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="adminlist">
    <li><a href="<?php echo base_url();?>admin/add">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/list">List</a></li>
  </ul>
</li>
<!--****************************Admin Revenue*****************************************-->
<li class="adminrevenue"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Revenue</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="adminrevenue">
    <li><a href="<?php echo base_url();?>admin/adminrevenue/list">List</a></li>
  </ul>
</li>
<!--**************************Banner Panel**************************-->
<li class="banner"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Banner</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="banner">
    <li><a href="<?php echo base_url();?>admin/banner/add">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/banner/list">List</a></li>
  </ul>
</li>
<!--**************************Blog Panel**************************-->
<li class="blog"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Blog</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="blog">
    <li><a href="<?php echo base_url();?>admin/blog/create">Add Blog</a></li>
    <li><a href="<?php echo base_url();?>admin/blog/list">List</a></li>
  </ul>
</li>
<!--****************************Category Panel**********************************************-->
<li class="category"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Category</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="category">
    <li><a href="<?php echo base_url();?>admin/category/add">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/category/list">List</a></li>
  </ul>
</li>
<!--******************************Coupon Menu************************************************-->
<li class="coupon"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Coupon</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="coupon">
    <li><a href="<?php echo base_url();?>admin/coupon/add">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/coupon/list">List</a></li>
  </ul>
</li>
<!--******************************CIty Menu************************************************--> 
<!--<li class="city"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">City</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="city">
    <li><a href="<?php echo base_url();?>admin/city/add">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/city/list">List</a></li>
  </ul>
</li>--> 
<!--**************************Contribution List**************************-->
<li class="contribute"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Contribution</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="contribute">
    <li><a href="<?php echo base_url();?>admin/contribute/list">List</a></li>
  </ul>
</li>
<!--**************************Event Category Panel**************************-->
<li class="event"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Event Category</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="event">
    <li><a href="<?php echo base_url();?>admin/event/add">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/event/list">List</a></li>
  </ul>
</li>
<!--**************************Order Panel**************************-->
<li class="order"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Order</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="order">
    <li><a href="<?php echo base_url();?>admin/order/list">List</a></li>
  </ul>
</li>

<!--**************************Product Brand Panel**************************-->
<li class="brand"> <a href="#"><i class="fa fa-minus-square"></i> <span class="nav-label">Product Brand</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="brand">
    <li><a href="<?php echo base_url();?>admin/brand/add">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/brand/listing">List</a></li>
  </ul>
</li>
<!--**************************Product Panel**************************-->
<li class="product"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Products</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="product">
    <li><a href="<?php echo base_url();?>admin/product/">Add</a></li>
    <li><a href="<?php echo base_url();?>admin/product/list">List</a></li>
  </ul>
</li>

<!--**************************Setting Panel**************************-->
<li class="setting"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Setting</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="setting">
    <li><a href="<?php echo base_url();?>admin/setting/transaction-fee">Cash Transaction Fee</a></li>
    <li><a href="<?php echo base_url();?>admin/setting/config-email-list">Email Configuration</a></li>
    <li><a href="<?php echo base_url();?>admin/setting/shipping-charge">Shipping Charge</a></li>
  </ul>
</li>
<!--****************************User Panel**********************************************-->
<li class="user"> <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Users</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="user">
    <li><a href="<?php echo base_url();?>admin/user/userlist">List</a></li>
  </ul>
</li>
<!--**************************Wishlist Panel**************************-->
<li class="wishlist"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Wishlist</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse" id="wishlist">
    <li><a href="<?php echo base_url();?>admin/wishlist">List</a></li>
  </ul>
</li>

 <li class="pages"> <a href="#"><i class="fa fa-file"></i> <span class="nav-label">Pages</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="pages">

          <li><a href="<?php echo base_url();?>admin/pages/add">Add</a></li>

          <li><a href="<?php echo base_url();?>admin/pages/pagelist">List</a></li>

        </ul>

      </li>
	  
<li class="static_block"> <a href="#"><i class="fa fa-square" aria-hidden="true"></i><span class="nav-label">Static block</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="block">

          <li><a href="<?php echo base_url();?>admin/static_block/add">Add</a></li>

          <li><a href="<?php echo base_url();?>admin/static_block/list_block">List</a></li>

        </ul>

      </li>	  

</div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
  <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> </div>
    <ul class="nav navbar-top-links navbar-right">
      <!--<li> <span class="m-r-sm"> Total Revenue - <?php //echo $total?> RP</span> </li>-->
      <li> <span class="m-r-sm text-muted welcome-message"><?php echo $this->lang->line('welcome_message');?>.</span> </li>
      <li> <a href="<?php echo base_url()?>admin/logout"> <i class="fa fa-sign-out"></i> Log out </a> </li>
    </ul>
  </nav>
</div>
<script type="text/javascript">

$(document).ready(function(){
		var pgurl = window.location.href.substr(window.location.href.indexOf("/")).split("/");
		var urlVal = pgurl[4];
		/*For Admin Add & List*/
		if(urlVal == "add" || urlVal == "list")
		{
			$(".adminList").addClass("active");
		}
		$('.metismenu').find("."+urlVal).addClass('active');
});

</script>