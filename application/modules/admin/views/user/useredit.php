<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Edit User</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index-2.html">Home</a>
                        </li>
                        <li>
                            <a>User</a>
                        </li>
                        <li class="active">
                            <strong>Edit</strong>
                        </li>
                    </ol>
                </div>

            </div>
            <div class="wrapper wrapper-content animated fadeInRight">

                        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                           
                            
                        </div>
                        <div class="ibox-content">
                        
                        <?php if(validation_errors()!=""){?>
                         <div class="alert alert-danger">
                                    <?php echo validation_errors();?>
                                    </div>
                        <?php }?>
                            <form class="form-horizontal" method="post" action="">
                                <div class="form-group"><label class="col-sm-2 control-label">First Name</label>

                                    <div class="col-sm-10"><input type="text" name="fname" value="<?php echo $result[0]->fname?>" class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Last Name</label>

                                    <div class="col-sm-10"><input type="text" name="lname" value="<?php echo $result[0]->lname?>" class="form-control"></div>
                                </div>
                                 <div class="hr-line-dashed"></div>
                               
                               
                                <div class="form-group"><label class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10"><input type="text" value="<?php echo $result[0]->email?>" name="email" class="form-control" placeholder="placeholder"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                
                         
								<div class="form-group">
                                <label class="col-sm-2 control-label">Is Block</label>
                                    

                                    <div class="col-sm-10">
                                       
                                        <div><label> <input type="radio" name="optionsRadios" <?php if($result[0]->is_block=='yes') echo 'checked="checked"'; ?> id="optionsRadios1" value="yes"> Active </label></div>
                                        <div><label> <input type="radio" name="optionsRadios" id="optionsRadios2" <?php if($result[0]->is_block=='no') echo 'checked="checked"'; ?> value="no"> Inactive </label></div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="<?php echo base_url()?>admin/user/userlist"><button type="button" class="btn btn-white">Cancel</button></a>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

