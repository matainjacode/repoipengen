<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Users List Panel</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin")?>">Home</a> </li>
      <li> <a>User</a> </li>
      <li class="active"> <strong>Users List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('sussess_message')!=""){ ?>
  <div class="row admin-list-msg">
    <div class="col-lg-12">
      <div class=" float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('sussess_message');?>  </div>
    </div>
  </div>
 <?php } if($this->session->flashdata('error_message')!=""){ ?>
 <div class="row admin-list-msg">
    <div class="col-lg-12">
      <div class=" float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
    </div>
  </div>
  <?php	}	?>
  <div class="row admin-list-msg" style="display:none">
    <div class="col-lg-12">
      <div class=" float-e-margins admin-flash-msg"> </div>
    </div>
  </div>
<div class="wrapper wrapper-content animated fadeInRight" style="z-index: 0;">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title"> </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <?php if(!empty($result)){?>
            <table class="table table-striped table-bordered table-hover " id="admintable">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Email</th>
                  <th>Verify</th>
                  <th>User Wishlist</th>
                  <th>User Action</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($result as $value){?>
                <tr class="gradeX">
                  <td><?php echo $value->id?></td>
                  <td><?php echo $value->fname.' '.$value->lname?></td>
                  <td><?php
						
						if(is_file($this->config->item('image_path').'/userimage/'.$value->id.'/thumb/'.$value->profile_pic))
						{
							$imgURL = base_url('photo/userimage/'.$value->id.'/thumb/'.$value->profile_pic);
							$imgBig = base_url('photo/userimage/'.$value->id.'/'.$value->profile_pic);
						}
						else
						{
							$imgURL = base_url('assets/img/userimage.png');
							$imgBig = base_url('assets/img/userimage.png');
						}
					?>
                    <img src="<?php echo $imgURL; ?>" width="50" class="img-circle imageZoom" id="<?php echo $imgBig; ?>"></td>
                  <td><?php echo $value->email?></td>
                  <td><?php echo ($value->verify==""?'No':ucfirst($value->verify))?></td>
                  <td><a href="<?php echo base_url()?>admin/user/wishlist/<?php echo $value->id; ?>">
                    <button class="btn btn-primary">Wishlist</button>
                    </a></td>
                  <td><?php
					  if($value->is_block == 'no') {
						$check = 'checked';
					  }else{
						$check = '';
					  }
					?>
                    <input type="checkbox" value="<?= $value->id ?>" class="js-switch" <?= $check; ?> /></td>
                  <td class="center"><a title="Edit User" href="<?php echo base_url()?>admin/user/edit/<?php echo $value->id; ?>"><i class="fa fa-pencil-square-o action-btn"></i></a>&nbsp;
                    <?php /*?><td class="center"><!--<a title="Edit User" data-toggle="modal" data-target="#GSCCModal" href="<?php //echo base_url()?>admin/user/edit/<?php echo $value->id; ?>"><i class="fa fa-pencil-square-o action-btn"></i></a>&nbsp;--><?php */?>
                    <a title="Reset password" class="open-AddBookDialog" data-toggle="modal" data-id="<?php echo $value->id; ?>" data-target="#GSCCModal" href="javascript:void(0)"><i class="fa fa-refresh" aria-hidden="true"></i></a>&nbsp; <a title="View" href="<?php echo base_url()?>admin/user/view/<?php echo $value->id; ?>" class="viewNews" data-id=""> <i class="fa fa-eye action-btn text-success" data-toggle="modal" data-target="#myModal"></i></a>&nbsp; 
                    
                    <!--<a href="javascript:void(0)"><i class="fa fa-toggle-off" aria-hidden="true"></i>
</a>--> 
                    <a title="Delete" class="userdelete" id="<?php echo $value->id; ?>" href="javascript:void(0)"><i class="fa fa-trash-o action-btn text-danger" ></i></a></td>
                </tr>
                <?php }?>
                  </tfoot>
                
            </table>
            <?php }
					else{
						
						echo 'No data';
					}
					?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="GSCCModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times; </button>
        <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
      </div>
      <div class="modal-body"> <br />
        <form class="form-horizontal" action="<?php echo base_url()?>admin/user/resetpassword" method="post" id="change-password-form">
          <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <input type="hidden" class="form-control" value="" name="uid" id="user-id">
            <div class="col-sm-10">
              <input type="password" class="form-control" value="" name="password" id="password" placeholder="Enter password">
              <div class="error alert-danger" id="passerror"></div>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Re-enter Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" value="" name="cpassword" id="cpassword"  placeholder="ReEnter password">
              <div class="error alert-danger" id="cpasserror"></div>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary subclass" id="updatepass" >Update Password</button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
            /* Init DataTables */
    var oTable = $('#admintable').DataTable({
			order:[[0,"desc"]],
			pageLength: 25,
		});
	/*Image Tooltips*/
	$(".imageZoom").imageTooltip();
	
	/****Switcher****/
	// For Single Checkbox
	// var elem = document.querySelector('.js-switch');
	// Switchery(elem, { color: '#1AB394' });
	// For multiple Checkbox
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
		elems.forEach(function(html) {
		var switchery = new Switchery(html,{ color: '#1AB394' });
	});
	$(document).on('click', '.paginate_button a', function(){
		$( "#admintable tbody tr" ).each(function(){
			 if(!$(this).find('td:eq(6) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				/*Image Tooltips*/
				$(".imageZoom").imageTooltip();
			}
		});
	});
	$(document).on("keyup",".dataTables_filter input", function(){
		$( "#admintable tbody tr" ).each(function(){
			 if(!$(this).find('td:eq(6) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				/*Image Tooltips*/
				$(".imageZoom").imageTooltip();
			}
		});
	});
	$(document).on("change","#admintable_length select", function(){
		$( "#admintable tbody tr" ).each(function(){
			 if(!$(this).find('td:eq(6) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				/*Image Tooltips*/
				$(".imageZoom").imageTooltip();
			}
		});
	});
	
	/*******Switcher End********/
	/*****Status Change*****/
	$(document).on("change",".js-switch",function(e){
		  var eventId = $(this).attr("value");
		  var username = $(this).closest("tr").find("td:eq(1)").html();
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo  base_url('admin/user/isnotblock/'); ?>" + eventId,
				  beforeSend: function(){
						$("body").append(lhtml);
					  },
				  success: function(result){
					  $('#loaderBg2').remove();
						if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(username +" now has been Activated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/user/isblock/'); ?>" + eventId ,
				  beforeSend: function(){
						$("body").append(lhtml);
					  },
				  success: function(result){
					  $('#loaderBg2').remove();
						if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(username +" now has been Deactivated.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});
	
	$(document).on('click','.userdelete',function(){
			var userid=$(this).attr('id');
			 if(userid!=""){
			bootbox.confirm("Are you sure you want delete this User?", function(result) {
					if(result){
						window.location.href = "<?php echo base_url()?>admin/user/delete/"+userid; 
					}
				}); 
			 }
		});
	
	$(document).on("click", ".open-AddBookDialog", function () {
		 var uId = $(this).data('id');
		 $('#user-id').val(uId);
	});
	 
	$(document).on("click", "#updatepass", function () {
	var userid=  $('#user-id').val();
	var pass= $('#password').val();
	var cpass= $('#cpassword').val();
	
	if(pass=="")
	{
	
		$('#passerror').text("Password can not be blank");
		return;
	}
	else if(cpass=="")
	{
		$('#cpasserror').text("Confirm Password can not be blank");
		return;
	}
	else if(pass!=cpass)
	{
		$('#cpasserror').text("Password and confirm password does no match");
		return;
	}
	else
	{
		$('#change-password-form').submit();
	}
	});
	
});
</script>