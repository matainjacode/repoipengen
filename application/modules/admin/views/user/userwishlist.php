

 
<style>
.externalFilter {
    background:#fff;
}
.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all {
    z-index: 1000 !important;
}
.table-responsive {
    min-height: 0.01%;
    overflow-x: -moz-hidden-unscrollable;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
<?php if(isset($userdetails)){foreach($userdetails as $userlist){?>
		
		<h2><?php echo strtoupper($userlist->fname); ?> <?php echo strtoupper($userlist->lname); ?></h2>
		<ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>">Home</a>
                        </li>
						 <li>
                           <?php echo strtoupper($userlist->fname); ?> <?php echo strtoupper($userlist->lname); ?>
                        </li>
                      
                        <li class="active">
                            <strong>Wishlist</strong>
                        </li>
                    </ol>

<?php } } else { ?>
                    <h2>Wishlist</h2>
					<ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>">Home</a>
                        </li>
                      
                        <li class="active">
                            <strong>Wishlist</strong>
                        </li>
                    </ol>


<?php } ?>
                    
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                       <!-- <h5>Basic Data Tables example with responsive plugin</h5>-->
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
					<?php /*?><div class="externalFilter">
					<div class="row">
					<div class="col-md-3">
						<label for="status">Event Date:</label>
						 <div class="form-group">
						 <input type="text" id="datepicker" class="form-control">
						</div>
					</div>
<div class="col-md-3">
<label for="status">Event Date:</label>
						 <div class="form-group">
						 <input type="text" id="datepicker1" class="form-control">
						</div>
					</div>
					<div class="col-md-3 text-center">
					 <div class="form-group">
                      <label for="status">Select Status:</label>
                      <select class="form-control" id="status">
                       <option value="">Status</option>
                        <option value="public">Public</option>
                        <option value="private">Private</option>
                      </select>
					</div>
					

					</div>
					
					
					<div class="col-md-3">
					</div>
					</div>
                    
					</div><?php */?>
                    <div class="ibox-content">
						<div class="row">
                       <?php /*?> <?php if($this->session->flashdata('sussess_message')!=""){ ?>
                                    <div class="alert alert-success">
                                    <?php echo $this->session->flashdata('sussess_message');?>
                                    </div>
						
						<?php
						}
					
					if($this->session->flashdata('error_message')!=""){ ?>
                                    <div class="alert alert-danger">
                                    <?php echo $this->session->flashdata('error_message');?>
                                    </div>
						
						<?php
						}
						?><?php */?>
                        </div>
                        <div class="table-responsive">
                        
                        <?php if(!empty($wishlist)){?>
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
					
                    <thead>
                    <tr>
                    	<th style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; border-bottom: 1px solid rgb(255, 255, 255);"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="position: absolute; background: rgb(255, 255, 255) none repeat scroll 0% 0%; left: 96px;">Event End Date</th>
						<th style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; border-bottom: 1px solid rgb(255, 255, 255);"></th>
						<th style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; border-bottom: 1px solid rgb(255, 255, 255);"></th>
						<th>Event Status</th>
						
                    </tr>
                    
					<tr>
                    	<th>Serial Number</th>
                        <th>User Id</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Event Start Date</th>
                        <th>Event End Date</th>
						<th>Event Date</th>
						<th>Wishlist Image</th>
						<th>Event Status</th>
						
                    </tr>
				
					</thead>
                    <tbody>
					
					<?php foreach($wishlist as $value){?>
                    <tr class="gradeX">
					<td><?php echo $value->id?></td>
                    <td><?php echo $value->uid?></td>
                    <td><?php echo $value->title?></td>
                    <td><?php echo $value->category?></td>
                    <td><?php echo date('d/m/Y', strtotime($value->e_startdate));?></td>
                    <td><?php echo date('d/m/Y', strtotime($value->e_enddate));?></td>
                    <td><?php echo date('d/m/Y', strtotime($value->event_date));?></td>
                    <td><?php if(file_exists('photo/wishlist/'.$value->id.'/100-100/'.$value->wishlist_image)){
							   $imagepathwish = base_url('photo/wishlist/'.$value->id.'/100-100/'.$value->wishlist_image);
								}
								else{ $imagepathwish = base_url('photo/wishlist/event.jpg');}
								?>
					<img src="<?php echo $imagepathwish; ?>" width="36" height="36" class="img-circle">
					</td>
					<td style="text-align:center;"><?php echo strtoupper($value->is_public); ?></td>
					
					</tr>
					<?php } ?>
             
                    </tbody>
					<tfoot>
                    </tfoot>
                    </table>
                    <?php }else{ ?>
						
						 <div class="alert alert-success text-center">
    <strong>No Data</strong> Wishlist is Blank
  </div>
					<?php }
					?>
                        </div>

                    </div>
                </div>
            </div>
            </div>
            
        </div>
               
<div id="GSCCModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
<h4 class="modal-title" id="myModalLabel">Reset Password</h4>
</div>
<div class="modal-body">
<br />
<form class="form-horizontal" action="<?php echo base_url()?>admin/user/resetpassword" method="post" id="change-password-form">
<div class="form-group"><label class="col-sm-2 control-label">Password</label>
<input type="hidden" class="form-control" value="" name="uid" id="user-id">
 <div class="col-sm-10"><input type="password" class="form-control" value="" name="password" id="password" placeholder="Enter password">
 <div class="error alert-danger" id="passerror"></div>
 </div>
</div>
<div class="hr-line-dashed"></div>
<div class="form-group"><label class="col-sm-2 control-label">Re-enter Password</label>

 <div class="col-sm-10"><input type="password" class="form-control" value="" name="cpassword" id="cpassword"  placeholder="ReEnter password">
<div class="error alert-danger" id="cpasserror"></div>
 </div>
</div>
<div class="hr-line-dashed"></div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-primary subclass" id="updatepass" >Update Password</button>
</div>
</div>
</div>
</div>
   <?php /*?> <script src="<?php echo base_url()?>assets/datatabl/js/jquery-1.4.4.min.js" type="text/javascript"></script><?php */?>
       <?php /*?> <script src="<?php echo base_url()?>assets/datatabl/js/jquery.dataTables.js" type="text/javascript"></script><?php */?>

      <?php /*?>  <script src="<?php echo base_url()?>assets/datatabl/js/jquery-ui.js" type="text/javascript"></script><?php */?>

       <?php /*?> <script src="<?php echo base_url()?>assets/datatabl/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script><?php */?>
<script src="<?php echo base_url()?>assets/datatable/js/jquery.jeditable.js"></script>

    <script src="<?php echo base_url()?>assets/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo base_url()?>assets/bootstrapDatepicker/jquery.dataTables.columnFilter.js"></script>

<script>


 

        $(document).ready(function(){
/*var oatable = $('.dataTables-example').DataTable();

	 //var oatable =$('.dataTables-example').dataTable();
		 
			$( "#datepicker,#datepicker1" ).datepicker({onSelect: function() {

oatable.columns(5).search( $(this).val() ).draw();}});

			 $('select#status').change( function() { 
							oatable.columns( 9 ).search( this.value ).draw();
			  });
$("#datepicker").click(function(){
$("#ui-datepicker-div").css("z-index",'100');
})
$("#datepicker1").click(function(){
$("#ui-datepicker-div").css("z-index",'100');
})
*/

$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
                $.datepicker.setDefaults($.datepicker.regional['']);
     $('.dataTables-example').dataTable({"order": [[ 0, "desc" ]]}).columnFilter({ sPlaceHolder: "head:before",
			aoColumns: [ 
						null,
						null,
						null,
						null,
						null,
				    	{type:"date-range" },
						null,
						null,
						{type:"select" },
						null
                    
				]

		});
});

</script>