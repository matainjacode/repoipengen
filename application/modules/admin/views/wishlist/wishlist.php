<style>
.externalFilter {
	background: #fff;
}
.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all {
	z-index: 1000 !important;
}
.table-responsive {
	min-height: 0.01%;
	overflow-x: -moz-hidden-unscrollable;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Wishlist</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Admin</a> </li>
      <li class="active"> <strong>Wishlist</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"></div>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight" style="z-index: 0;">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <div class="ibox-content">
            <div class="row"> </div>
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                  <tr>
                    <th style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; border-bottom: 1px solid rgb(255, 255, 255);"></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th style="position: absolute; background: rgb(255, 255, 255) none repeat scroll 0% 0%; left: 96px;">Event End Date</th>
                    <th style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; border-bottom: 1px solid rgb(255, 255, 255);"></th>
                    <th style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; border-bottom: 1px solid rgb(255, 255, 255);"></th>
                    <th>Event Status</th>
                    <th></th>
                  </tr>
                  <tr>
                    <th>Wishlist ID.</th>
                    <th>Owner Name</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Event Start Date</th>
                    <th>Event End Date</th>
                    <th>Event Date</th>
                    <th>Wishlist Image</th>
                    <th>Event Status</th>
                    <th>Action</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
				  <?php if(!empty($wishlist)){ 
                  		foreach($wishlist as $value){?>
                  <tr class="gradeX">
                    <td><?php echo $value->id;?></td>
                    <td><?php echo ucfirst($value->fname)." ".ucfirst($value->lname)?></td>
                    <td><?php echo $value->title?></td>
                    <td><?php echo $value->category?></td>
                    <td><?php echo date('d/m/Y', strtotime($value->e_startdate));?></td>
                    <td><?php echo date('d/m/Y', strtotime($value->e_enddate));?></td>
                    <td><?php echo date('d/m/Y', strtotime($value->event_date));?></td>
                    <td><?php if(is_file($this->config->item('image_path').'/wishlist/'.$value->id.'/100-100/'.$value->wishlist_image))
				  		{
							$imagepathwish = base_url('photo/wishlist/'.$value->id.'/100-100/'.$value->wishlist_image);
						}
						else
						{
							$imagepathwish = base_url('photo/wishlist/event.jpg');
						}
					?>
                      <img src="<?php echo $imagepathwish; ?>" width="36" height="36" class="img-circle imageZoom"></td>
                    <td style="text-align:center"><?php echo strtoupper($value->is_public);?></td>
                    <td style="padding: 16px;"><a href="<?php echo base_url("admin/wishlist/view/".$value->id); ?>" title="Wishlist View"><i class="fa fa-eye fa-fw fa-2x" style="color:#508DF2"></i></a></td>
                    <td><?php if($value->admin_status == 1){$checkVal = "checked";}else{$checkVal = "";} ?>
                      <input type="checkbox" value="<?= $value->id; ?>" class="js-switch" <?php echo $checkVal ?>  /></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
              <?php }
					else{ ?>
              <div>
                <div class="alert alert-success text-center"> <strong>No Data</strong> Your Wishlist is Blank </div>
              </div>
              <?php }
					?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url()?>assets/datatable/js/jquery.jeditable.js"></script> 
<script src="<?php echo base_url()?>assets/js/plugins/dataTables/datatables.min.js"></script> 
<script src="<?php echo base_url()?>assets/bootstrapDatepicker/jquery.dataTables.columnFilter.js"></script> 
<script>
$(document).ready(function(){
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
	$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
	$.datepicker.setDefaults($.datepicker.regional['']);
	$('.dataTables-example').dataTable({"order": [[ 0, "desc" ]]}).columnFilter({ 
	 		sPlaceHolder: "head:before",
			aoColumns:	[ 
							null,
							null,
							null,
							null,
							null,
							{type:"date-range" },
							null,
							null,
							{type:"select" },
							null
						]
		});
	$(".imageZoom").imageTooltip();
	/*******Switcher On-Off*************/
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch')); //console.log();
		elems.forEach(function(data) {
		var switchery = new Switchery(data,{ color: '#1AB394' });
	});
	/***after Click on Pagination Link*******/
	$(document).on('click', '.paginate_button a', function(){
		$( ".dataTables-example tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				$(".imageZoom").imageTooltip();
			 }
		});
	});
	$(document).on("keyup",".dataTables_filter input", function(){
		$( ".dataTables-example tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				$(".imageZoom").imageTooltip();
			 }
		});
	});
	$(document).on("change",".dataTables_length select", function(){
		$( ".dataTables-example tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				$(".imageZoom").imageTooltip();
			 }
		});
	});
	$(document).on("change",".dandelion_column_filter", function(){
		$( ".dataTables-example tbody tr" ).each(function() {
			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){
				var elem = this.querySelector('.js-switch');
				Switchery(elem, { color: '#1AB394' });
				$(".imageZoom").imageTooltip();
			 }
		});
	});
	
	$(document).on("change",".js-switch",function(e){
		  var wishlistId = $(this).attr("value");
		  var w_name = $(this).closest("tr").find("td:eq(2)").html();
		  if (this.checked) {
			  $.ajax({
				  url:"<?php echo base_url('admin/wishlist/status/'); ?>" + wishlistId + "/1" ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).attr("checked", "checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(w_name +" wishlist now has been Activate.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).removeAttr("checked");}
					  }
				  });
		  }else{
			  $.ajax({
				  url:"<?php echo base_url('admin/wishlist/status/'); ?>"  + wishlistId + "/0" ,
				  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
				  success: function(result){
					  	$('#loaderBg2').remove();
					  	if($.trim(result) != 0)
						{
							$(this).removeAttr("checked");
							$(".admin-list-msg").css("display","");
							$(".admin-flash-msg").html(w_name +" wishlist now has been Deactivate.");
							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
						}
						else
						{$(this).attr("checked", "checked");}
					  }
				  });
		  };
	});
});
</script>