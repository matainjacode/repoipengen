<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Index extends MX_Controller
	{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model(array('login_model','dashboard_model')); 
	}
	public function index()
	{
		if (!$this->session->userdata('logged_in')){redirect('marchent/login');}else{redirect('marchent/dashboard');}
	}

	function login()
	{ 
	$data["page_title"] = "Marchent login";
		if (!$this->session->userdata('logged_in'))
		{
			if(isset($_POST['submit']))
			{ 
				  $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
	
				  $this->form_validation->set_rules('password', 'Password', 'required');
	
					if($this->form_validation->run() == TRUE)
					{
						$username = $this->input->post('email');
						$password = $this->input->post('password');
						$result   = $this->login_model->login($username, $password);
						if(!empty($result))
						{
							if($result[0]->marchent_status != 0)
							{
								$this->session->set_userdata('logged_in',$result[0]->marchent_email);
								redirect('marchent/dashboard');
							}
							else
							{
								$this->session->set_flashdata('msg','Access Denied by Administrator!');
								redirect("marchent/login");
							}
						}
						else
						{
							$this->session->set_flashdata('msg','Email or Password is invalid!');
							$data["page_title"] = "Marchent Log-In || iPengen";
							$this->load->view('marchent/login',$data);	
						}
	
					}
					else
					{
						$this->session->set_flashdata('msg','Invalid Email or Password is required!');
						$data["page_title"] = "Marchent Log-In || iPengen";
						$this->load->view('marchent/login',$data);
					}
				}
			else
			{ 
				$data["page_title"] = "Marchent Log-In || iPengen";
				$this->load->view('marchent/login',$data);
			}
		}
		else
		{
			redirect('marchent/dashboard');
		}
	 }

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		redirect('marchent/login');
	}

	function forget_password()
	{
		$data["page_title"] = "Forget password";
		if(isset($_POST['email_submit']))
		{
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if($this->form_validation->run() == TRUE)
			{
				$cusd=$this->login_model->forgotemail($this->input->post('email'));
					if(!empty($cusd))
					{
						$this->email->initialize($this->config->item('smtp_info'));
						$email_to = $this->input->post('email');
						$subject="[Ipengen] Change Password";	//subject
						
						$data['username'] = ucfirst($cusd->marchent_first_name).' '.ucfirst($cusd->marchent_last_name);
						$data['imgURL'] = base_url('');
						$data['link'] = base_url('marchent/resetpassword/'.base64_encode($email_to));
						$message = $this->load->view('e_template/password-change',$data, true);
						echo $message;
						$this->email->to($email_to);
						$this->email->from($this->config->item('email_form'), $this->config->item('wbsite_name'));
						$this->email->subject($subject);
						$this->email->message($message);
						/*if($this->email->send())
						{
						echo 'success';
							//$this->session->set_flashdata('msg','Thank You! Please check your mail to reset password.');
							//redirect('marchent/login');
						}
						else
						{
							echo 'Faild';
							//$this->session->set_flashdata('forgetpwd_ErrMsg','Oops..!!! Something Wrong..Please try again.');
//							redirect('marchent/forget_password');
						}
						echo $this->email->print_debugger();*/
	
						
					}
					else
					{
						$this->session->set_flashdata('msg','Email is not registered!');
						redirect('marchent/login'); 
					}
			}
			else
			{
				$data["page_title"] = "Marchent Forget Password | iPengen";
				$this->load->view('marchent/fotgotpassword',$data);
			}
		}
		else
		{
			$data["page_title"] = "Marchent Forget Password | iPengen";
			$this->load->view('marchent/fotgotpassword',$data);
		}
	}

    function resetpassword($log_email)
	{
		$data["page_title"] = "reset password";
		if(!empty($_POST))
		{
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('re_password', 'Re-enter password', 'required|matches[password]');
			if($this->form_validation->run() == TRUE)
			{
				$password=$this->input->post('password');
				$total_result=$this->login_model->admin_exist();
				if($total_result)
				{
					foreach($total_result as $result_singal)
					{
						if($log_email !== NULL || $log_email != '')
						 $this->db->update('ig_marchent_login',array('marchent_password'=>md5($this->config->item('email_verification_salt').$password)),array('marchent_email'=>base64_decode($log_email)));
						 $this->session->set_flashdata('msg','Password Change successfully');
							redirect('marchent/login');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('re-msg','Password is not matched.');
				redirect('marchent/resetpassword/'.$log_id);}
			}
		else
		{
			$this->load->view('marchent/resetpassword',array('id'=>$log_email,"page_title"=>"Admin Password Reset || iPengen"));
		}

	}	

	public function dashboard()
	{
		$data["page_title"] = "Dashboard";
		$data["user_count"] = $this->dashboard_model->registered_user_count();
		$data["total_order_count"] = $this->dashboard_model->total_order_count();
		$data["best_selling_product"] = $this->dashboard_model->best_selling_product();
		$data["wishlist_count"] = $this->dashboard_model->wishlist_create_count();
		$data["latest_order"] = $this->dashboard_model->latest_order();
		$data["latest_gift"] = $this->dashboard_model->latest_gift();
		$data["revenue_count"] = $this->dashboard_model->revenue_count();
		$this->load->template('site/index',$data);
	}



		



	}



?>