<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	public function login($user,$pass) {
		//$this->db->insert('groups', $data);
		$value = $this->db->get_where('ig_marchent_login',array(
															'marchent_email' => $user,
															'marchent_password' => md5($this->config->item('email_verification_salt').$pass),
														 )
										);
		if($value->num_rows()>0)
		return $value->result();
		else
		 return array();
		}
	public function forgotemail($email)
	{
		$result = $this->db->get_where('ig_marchent_login',array('marchent_email' => $email));
		
		if($result->num_rows()>0)
		return $result->row();
		else
		 return false;
	}	
	public function admin_exist()
	{$result = $this->db->get('ig_marchent_login');
	return $result->result();
	}
	 public function enableAdmin()
		 {
			 $email=$this->session->userdata('logged_in');   
			
			 $this->db->select('*');
			 $this->db->from('ig_marchent_login');
		     $this->db->where('marchent_email',$email);
		     $query = $this->db->get();
		      return $query->row();
			 }
}
?>