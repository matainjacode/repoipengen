<div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                               <!-- <span class="label label-success pull-right">Monthly</span>-->
                                <h5>Wishlist users</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">40 886,200 </h1>
                               <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                                <small>Total users</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <!--<span class="label label-info pull-right">Annual</span>-->
                                <h5>Wishlist created</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">275,800</h1>
                                <!--<div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>-->
                                <small>Total wishlist</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <!--<span class="label label-primary pull-right">Today</span>-->
                                <h5>Sales</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">12</h1>
                                <!--<div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>-->
                                <small>Total Sales</small>
                            </div>
                        </div>
                    </div>
                    <?php /*?><div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <!--<span class="label label-danger pull-right"> This Month</span>-->
                                <h5>Bestselling Products</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $best_selling_product; ?></h1>
                                <!--<div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>-->
                                <small>Product</small>
                            </div>
                        </div>
            	</div><?php */?>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                               <!-- <span class="label label-success pull-right">Monthly</span>-->
                                <h5>Revenue</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"> 2500</h1>
                               <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                                <small>Total Revenue</small>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Monthly</span>
                                <h5>Best Selling Products</h5>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover no-margins">
                                	<thead>
                                		<tr>
                                			<th>Image</th>
                                			<th>Product Name</th>
                                			<th>Product Price</th>
                                			<th>SKU</th>
                                			<th>Sale Price</th>
                                		</tr>
                                	</thead>
                                    <tbody>
                                    
                                    <?php if(!empty($best_selling_product)){ foreach($best_selling_product as $row){ if($row->product_count >= 10){
										$symbolRemove = preg_replace('/[^A-Za-z0-9\-]/', '-', $row->name);
										$string = str_replace(' ', '-', $symbolRemove);
									if(is_file($this->config->item('image_path').'/product/'.$row->product_id.$this->config->item('thumb_size').$row->product_image))
									{ $imageURL = base_url('photo/product/'.$row->product_id.$this->config->item('thumb_size').$row->product_image); }
									else
									{ $imageURL = base_url('photo/product/no_product.jpg');}
									?>
                                    	<tr onClick="window.location.href='<?php echo base_url()."p/".$row->product_id."-".$string ?>'" style="cursor:pointer">
                                    		<td><img src="<?= $imageURL ?>" alt="<?= $row->name ?>" width="35" class="img-responsive"></td>
                                    		<td><?php echo ucwords($row->name); ?></td>
                                    		<td><?php echo strtoupper($this->config->item('currency')).' '.number_format($row->price); ?></td>
                                    		<td><?php echo $row->sku; ?></td>
                                    		<td><?php echo strtoupper($this->config->item('currency')).' '.number_format($row->sale_price); ?></td>
                                    	</tr>
                                      <?php }}}else{ ?>
                                      	<tr>
                                    		<td colspan="5"><h3>No Product Available</h3></td>
                                    	</tr>
                                      <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="row">
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Latest Orders</h5>
                                <div class="ibox-tools">
                                   <!--Text Here-->
                                </div>
                            </div>
                            <div class="ibox-content">
                            <?php if(!empty($latest_order)){?>
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>User</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                     <?php foreach($latest_order as $lorder){?>
                                    <tr>
                                        <td><small><a href="<?php echo base_url();?>admin/order/details/<?php echo $lorder->order_id; ?>"><?php echo ucfirst($lorder->status);?></a></small></td>
                                        <td><i class="fa fa-clock-o"></i> <?php echo $lorder->date_added;?></td>
                                        <td><?php echo ucfirst($lorder->payment_firstname);?></td>
                                        <td class="text-navy"> <?php echo strtoupper($this->config->item('currency')).' '.number_format($lorder->total);?></td>
                                    </tr>
                                    <?php } ?>
                                 
                                    </tbody>
                                </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Cash Gift</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content"> 
                            <?php if(!empty($latest_gift)){ ?>
                                <ul class="todo-list m-t small-list">
                                <?php foreach($latest_gift as $gift){
									   $url = base_url().'~'.$gift->url;
									?>
                                    <li>
                                        <!--<a href="#" class="check-link"></a>-->
                                        <span class="m-l-xs "><a href="<?php echo $url; ?>"><?php echo strtoupper($gift->title)?></a><?php echo ' - '.strtoupper($gift->fname.' '.$gift->lname); ?></span>

                                    </li>
                                    <?php }?>
                               
                                </ul>
                                 <?php }?>
                            </div>
                        </div>
                    </div>
        </div>
        
        </div>