<div class="footer">
  <div> <strong>Copyright</strong> Ipengen &copy; <?php echo date("Y");?> </div>
</div>
</div>
</div>
<!-- Mainly scripts --> 

<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script> 
<script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script> 
<script src="<?php echo base_url('assets/bootstrapDatepicker/date-euro.js') ?>" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/js/plugins/jeditable/jquery.jeditable.js"></script> 
<!-- Peity --> 
<script src="<?php echo base_url();?>assets/js/plugins/peity/jquery.peity.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/demo/peity-demo.js"></script> 
<!-- Custom and plugin javascript --> 
<script src="<?php echo base_url();?>assets/js/inspinia.js"></script> 
<script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script> 
<!-- jQuery UI --> 
<!--<script src="<?php //echo base_url();?>assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>-->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<!-- Jvectormap --> 
<script src="<?php echo base_url();?>assets/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 

<!-- Sparkline --> 
<script src="<?php echo base_url();?>assets/js/plugins/sparkline/jquery.sparkline.min.js"></script> 
<!-- Sparkline demo data  --> 
<script src="<?php echo base_url();?>assets/js/demo/sparkline-demo.js"></script> 
<!-- ChartJS--> 
<script src="<?php echo base_url();?>assets/js/plugins/chartJs/Chart.min.js"></script> 
<script src="<?php echo base_url();?>assets/frontend/js/bootstrap_confirm.js"></script> 
<script src="<?php echo base_url();?>assets/frontend/js/bootbox.js"></script> 

</body>
</html>