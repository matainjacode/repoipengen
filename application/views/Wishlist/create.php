<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key= AIzaSyAog-FDBbkmoWJrDnPhTdyLJcLGh8XcuB4"></script>
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="<?php echo base_url()?>assets/js/placepicker.js"></script>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker();
	});
</script>
<script>
var autocomplete = [];
var els = [];
function initialize() {
    add_auto();
    /* options = {
      language: 'en-GB',
      types: ['(cities)'],
      componentRestrictions: { country: "uk" }
    }*/
}
function add_auto(){
    autocomplete.length = null;
    els.length = null;
    var $controls = $('#controls input[type=text]');
 //alert($controls);
    $controls.each(function(i,el){
        els[i] = el;
        autocomplete[i] = new google.maps.places.Autocomplete(el);
        google.maps.event.addListener(autocomplete[i], 'place_changed', function() {
           // infobox.close();
            var place = autocomplete[i].getPlace();
            if (!place.geometry) {
                return;
            }
        });
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style>
.imgloaderwait {
    border-radius: 2px;
    left: 32%;
    padding: 10px;
    position: absolute;
    top: 30%;
}
.imgwaitprocess {
    background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 9999;
}
</style>
<?php $catval =(isset ($_GET['cat']) && ($_GET['cat']) != '') ? $_GET['cat'] : '' ;?>

<div id="all">
<div id="content">
  <div class="container">
    <div class="row">
      <section>
      <div class="wizard">
        <div class="wizard-inner">
          <div class="connecting-line"></div>
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"> <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1"> <span class="round-tab"> <i class="fa fa-star" aria-hidden="true"></i> </span> </a>
              <div class="tabtittle"> <?php echo $this->lang->line("your_event"); ?></div>
            </li>
            <li role="presentation" class="disabled" id="tab2"> <a href="" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2"> <span class="round-tab"> <i class="fa fa-pencil" aria-hidden="true"></i> </span> </a>
              <div class="tabtittle"><?php echo $this->lang->line("personalize"); ?></div>
            </li>
            <li role="presentation" class="disabled" id="tab3"> <a href="" data-toggle="tab" aria-controls="complete" role="tab" title="Complete"> <span class="round-tab"> <i class="fa fa-check" aria-hidden="true"></i> </span> </a>
              <div class="tabtittle"> <?php echo $this->lang->line("ready"); ?></div>
            </li>
          </ul>
        </div>
        <div class="tab-content">
          <div class="tab-pane active" role="tabpanel" id="step1"> 
            <!--Titleheader-->
            <div class="Titleheader"> <span class="txt-header"><?php echo $this->lang->line("your_event"); ?></span> <span class="line"></span> </div>
            <!--eof Titleheader-->
            <?php
                        $data = array('name'=>'wishlistform','id'=>'wishlistform');
                        echo form_open('wishlist/create_wishlist',$data);
                   	 ?>
            <div class="row">
              <div class="col-md-6 listField"> 
                <!--<input type="hidden" class="lastinsertid" id="lastinsertid" value="">-->
                
                <input type="hidden" class="wishid" id="wishid" value="">
                <input type="hidden" class="userid" id="userid" value="<?php echo $this->session->userdata['log_in']['user_id']?>">
                <input type="hidden" class="usetotwish" id="usetotwish" value="<?php echo $totalwishlist;?>">
                <?php  echo form_input ('wishlistname',set_value('wishlistname'),'placeholder="'.$this->lang->line("w_name_placeholder").'" 
							class="form-control" id="wishlistname"'); ?>
                <!--<label id="wishlistname-error" class="error" for="wishlistname" style="display:none"></label> -->
                <div class="css-error">
                  <?= form_error('wishlistname'); ?>
                </div>
              </div>
              <div class="col-md-3 listField">
                <select class="form-control" name="Event Category" id="eventcategory">
                  <option selected><?php echo $this->lang->line("w_event_cat"); ?></option>
                  <?php 
            foreach($groups as $row)
            { 
			$check = ($catval == $row->name) ? ' selected="selected"' : '';
              echo '<option value="'.$row->name.'" '.$check.'>'.$row->name.'</option>';
		
            }
            ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 listField"><?php echo $this->lang->line("w_date"); ?> :<br>
                <?php  echo form_input ('eventdate',set_value('eventdate'),'placeholder="'.$this->lang->line("w_date").'" 
							class="form-control" id="eventdate"'); ?>
                <div class="css-error">
                  <?= form_error('eventdate'); ?>
                </div>
              </div>
              <div class="col-md-6 listField"><?php echo $this->lang->line("w_end_date"); ?> :
                <button type="button" class="btn btn-info btn-xs" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Max 60 days after Event Date"> <i class="fa fa-question" aria-hidden="true"></i> </button>
                <br>
                <?php  echo form_input ('eventenddate',set_value('eventenddate'),'placeholder="'.$this->lang->line("w_end_date").'" 
							class="form-control" id="eventenddate"'); ?>
                <div class="css-error">
                  <?= form_error('eventenddate'); ?>
                </div>
                <br />
                - <?php echo $this->lang->line("w_warning_info"); ?>---</div>
            </div>
            <div class="row">
              <div class="col-md-12 listField url">
                <p><?php echo $this->lang->line("w_p_url"); ?> :</p>
                <div class="">
                  <div class="total-form">
                    <div class="col-md-5" style="padding:0px">
                      <div class="urllink"><?php echo base_url();?>~</div>
                    </div>
                    <div class="col-md-6 lodimag" style="padding:0px">
                      <?php  echo form_input ('seturl',set_value('seturl'),'placeholder="'.$this->lang->line("w_p_url").'" 
							class="form-control mod" id="seturl"'); ?>
                    </div>
                    <div class="col-md-1 lodimg" > <img src="<?php echo base_url().'files/loader/loader.gif'?>" id="loding" style="display:none" /> <img src="<?php echo base_url().'files/loader/check.png'?>" id="checkloding" style="display:none" /> <img src="<?php echo base_url().'files/loader/cross.png'?>" id="crossloding" style="display:none" /> </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
            <p></p>
            <div class="checkbox">
              <label>
              <h4> <?php echo form_checkbox('cashgift', 'yes','','id="cashgift"'); ?> <?php echo $this->lang->line("w_receive_cash_gift"); ?></h4>
              </label>
              <p><?php echo $this->lang->line("w_receive_info_1"); ?><br>
                <span class="text-danger"><?php echo $this->lang->line("w_receive_info_2"); ?></span></p>
            </div>
            <div class="row"> <!--form col 1-->
              <div class="col-md-6">
                <h4><?php echo $this->lang->line("w_gift_sent"); ?></h4>
                <div class="row">
                  <div class="col-xs-6 listform">
                    <?php  echo form_input ('firstname',set_value('firstname'),'placeholder="'.$this->lang->line("f_name").'" 
							class="form-control" id="firstname"'); ?>
                    <div class="css-error">
                      <?= form_error('firstname'); ?>
                    </div>
                  </div>
                  <div class="col-xs-6 listform">
                    <?php  echo form_input ('lastname',set_value('lastname'),'placeholder="'.$this->lang->line("l_name").'" 
							class="form-control" id="lastname"'); ?>
                    <div class="css-error">
                      <?= form_error('lastname'); ?>
                    </div>
                  </div>
                </div>
                <div class="listform">
                  <?php if ($status == true){ ?>
                  <?php 

							?>
                  <?php echo $this->lang->line("w__prev_event_add"); ?> :
                  <select class="form-control" name="Event" onchange="getvalue(this.value)">
                    <option selected><?php echo $this->lang->line("w_event_choose"); ?></option>
                    <?php 
                            foreach($all_wishlist as $row)
                            { 
                            echo '<option value="'.$row->title.'" onchange="getvalue(this.value)">'.$row->title.'</option>';
                            }
                            ?>
                  </select>
                  <?php } ?>
                </div>
                <div class="listform">
                  <?php  echo form_input ('addr',set_value('address'),'placeholder="'.$this->lang->line("w_event_add").'" 
							class="form-control" id="addr"'); ?>
                  <div class="css-error">
                    <?= form_error('addr'); ?>
                  </div>
                </div>
                <div class="listform">
                  <?php  echo form_input ('addr2',set_value('address2'),'placeholder="'.$this->lang->line("w_event_add_2").'" 
							class="form-control" id="addr2"'); ?>
                </div>
                <div class="listform">
                  <?php  echo form_input ('pcode',set_value('postcode'),'placeholder="'.$this->lang->line("w_event_postcode").'" 
							class="form-control" id="pcode" size="10"'); ?>
                  <div class="css-error">
                    <?= form_error('pcode'); ?>
                  </div>
                </div>
                <div class="listform">
                  <?php  echo form_input ('dist',set_value('district'),'placeholder="'.$this->lang->line("w_event_district").'" 
							class="form-control" id="dist" size="20"'); ?>
                </div>
                <div class="listform">
                  <?php echo form_input ('city','JAKARTA RAYA','class="form-control" id="city" readonly'); ?>
                  <div class="css-error">
                    <?= form_error('city'); ?>
                  </div>
                </div>
                <div class="listform">
                  <?php  echo form_input ('mobileno',set_value('mobile'),'placeholder="'.$this->lang->line("w_event_contact").'" 
							class="form-control" id="mobileno"'); ?>
                  <div class="css-error">
                    <?= form_error('mobile'); ?>
                  </div>
                </div>
              </div>
              <!--form col 1--> 
              <!--form col 2-->
              <div class="col-md-6">
                <h4><?php echo $this->lang->line("privacy"); ?></h4>
                <div class="radio">
                  <label> <?php echo form_radio('privacy', 'public', 'checked', 'id=privacy');?> <?php echo $this->lang->line("w_event_privacy"); ?> <strong><?php echo $this->lang->line("public"); ?></strong> </label>
                </div>
                <div class="checkbox">
                  <label> <?php echo form_checkbox('shiprivacy', 'yes','','id = "shiprivacy"'); ?> <?php echo $this->lang->line("w_event_add_show"); ?> </label>
                </div>
                <div class="radio">
                  <label>
                  <?php echo form_radio('privacy', 'private', '', 'id=password');?> <?php echo $this->lang->line("w_event_visitor_pwd"); ?>
                  <div class="formlist">
                    <?php  echo form_password ('visitpassword',set_value('visitpassword'),'class="form-control" id="visitpassword"'); ?>
                    <div id="visitpassword-error" class="error" for="visitpassword" style="display:none"></div>
                    <div class="css-error">
                      <?= form_error('visitpassword'); ?>
                    </div>
                    <span class="text-danger"> * <em> <?php echo $this->lang->line("w_event_visitor_pwd_warning"); ?> </em> </span></div>
                  </label>
                </div>
                <?php //echo form_submit('submit','Save and continue','class="btn btn-primary next-step" id="submit"'); ?>
                <button type="button" class="btn btn-primary btn-lg next-step savecontinue" id="scontinue"><?php echo $this->lang->line("w_event_save_btn"); ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                <button type="button" class="btn btn-primary btn-info-full next-step editcontinue" id="updatecontinue" style="display:none"><?php echo $this->lang->line("w_event_update_btn"); ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                <p>&nbsp; </p>
              </div>
            </div>
            <?= form_fieldset_close(); ?>
            <?= form_close(); ?>
          </div>
          <!--eof form col 2--> 
          <!--eof Form Section 1--> 
          <!--Form Section 2-->
          <div class="tab-pane" role="tabpanel" id="step2">
            <div id="img" class="modal fade" role="dialog">
              <div class="modal-dialog"> 
                <!-- Modal content--> 
              </div>
            </div>
            <!--Titleheader-->
            <div class="Titleheader"> <span class="txt-header"><?php echo $this->lang->line("w_personalize"); ?></span> <span class="line"></span> </div>
            <!--eof Titleheader-->
            <?php
    $data = array('name'=>'personalizeform','id'=>'personalizeform');
    echo form_open_multipart('wishlist/create_personalize',$data);
    ?>
            <div class="row">
              <div class="col-md-4">
                <div class="listForm">
                  <div style="margin: 7% auto 0px; display: table; position: relative;" id="imageMain"> 
                    <!-- begin_picedit_box --> 
                    <!-- end_picedit_box -->
                    <input type="hidden" value="" id="wid" name="lastwid"/>
                    <input type="hidden" value="" id="imagecropdata" name="imagecropname" class="imagecropclass"/>
                    <?php $imagepath= base_url().'photo/wishlist/no-event.png';?>
                    <img style="width:365px" class="imgreviewcrop" id="uplode_img" src="<?php echo $imagepath; ?>" />
                    <!--<iframe src="<?php //echo $imagepath; ?>" width="365px" height="235px" style="border:0px;" id="uplode_img" class="imgreviewcrop"></iframe>-->
                    <div id="file-upload-cont">
                      <input type="file" name="wisimage" id="wisimage" size="20" />
                      <div id="my-button"><?php echo $this->lang->line("w_img_upload"); ?></div>
                    </div>
                    <!-- Modal --> 
                    <!--<div class="text-danger">** Max image size 2 MB under the image box</div>-->
                  </div>
                </div>
              </div>
              <div class="col-md-8"> <?php echo $this->lang->line("w_note"); ?> : <br>
                <div class="listForm">
                  <?php
					$data = array(
						  'name'        => 'guest_note',
						  'class'       => 'form-control',
						  'id'          => 'guest_note',
						  'rows'        => '3',
						  'cols'        => '',
						  'maxlength'   => '250',
						);
					  echo form_textarea($data);
					  echo'<div class="panelNT pull-right cractercount">250</div>';
					?>
                  <span class="text-danger"><em><?php echo $this->lang->line("w_rsvp_info"); ?> </em> </span></div>
                <div class="listForm"><?php echo $this->lang->line("w_e_date"); ?><br>
                  <div class='input-group date' id='datetimepicker1'>
                    <?php  echo form_input ('eventpdate',set_value('eventpdate'),'placeholder="'.$this->lang->line("w_e_date").'" 
							class="form-control" id="eventpdate" data-date-format="DD-MM-YYYY hh:mm A" required'); ?>
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                </div>
                <div class="listForm"  id="controls"><?php echo $this->lang->line("w_e_add"); ?>:<br>
                  <!--<textarea name="Event Address" cols="" rows="3" class="form-control"></textarea>-->
                  <input id="from" name="event_address" class="form-control locationg" type="text" value="<?php echo (isset($_POST['event_address'])?$_POST['event_address']:'') ?>">
                </div>
                <div class="listform">
                  <ul class="list-inline">
                    <li>
                      <button type="button" class="btn btn-default prev-step previous"><?php echo $this->lang->line("previous"); ?></button>
                    </li>
                    <li>
                      <button type="button" class="btn btn-info next-step skipletter"><?php echo $this->lang->line("skip"); ?></button>
                    </li>
                    <li>
                      <?php /*?><?php echo form_submit('submit','Save and Finish','class="btn btn-primary btn-info-full next-step" id="submit"'); ?><?php */?>
                      <button type="button" class="btn btn-primary btn-info-full next-step personalize"><?php echo $this->lang->line("w_e_save_finish"); ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                    </li>
                  </ul>
                </div>
              </div>
              <!--eof col md 12--> 
            </div>
            <?= form_fieldset_close(); ?>
            <?= form_close(); ?>
          </div>
          <!--eof Form Section 2--> 
          <!--Form Section 3-->
          <div class="tab-pane" role="tabpanel" id="complete"> 
            <!--Titleheader-->
            <div class="Titleheader"> <span class="txt-header"><?php echo $this->lang->line("w_almost_ready"); ?></span> <span class="line"></span> </div>
            <!--eof Titleheader-->
            
            <div class="row">
              <div class="col-md-12">
                <p class="text-center"><?php echo $this->lang->line("w_add_item"); ?></p>
                <div class="col-md-4">
                <div class="listField text-center">
                  <button type="button" class="btn btn-primary btn-lg" id="btnRedirectWishlist"><i class="fa fa-heart" aria-hidden="true"></i> <?php echo $this->lang->line("w_my_wishlist"); ?></button>
                  <input type="hidden" id="hidWishlistUrl" name="hidWishlistUrl" value="" />
                  <p></p>
                </div>
                </div>
                <!--Titleheader-->
                <!--<div class="Titleheader"> <span class="txt-header"><?php echo $this->lang->line("w_browse_gift"); ?></span> <span class="line"></span> </div>-->
                <!--eof Titleheader-->
               <div class="col-md-8">
                <span class="txt-header"><?php echo $this->lang->line("w_browse_gift"); ?></span>
                <div class="row">
                  <div class="col-sm-3">
                    <?php $count=0; ?>
                    <?php if(!empty($maincategory)){
        foreach ($maincategory as $mcat ){
			
			$urlN=base_url().'search';
			echo '<a href="'.$urlN.'">'.$mcat->name."</a>".'<br>';
			$count++;
			?>
                    <?php if ($count % 12 == 0):?>
                  </div>
                  <div class="col-sm-3">
                    <?php endif;?>
                    <?php } }?>
                  </div>
                </div>
              </div>  
              </div>
            </div>
          </div>
        </div>
        </section>
      </div>
    </div>
    <!-- /.container --> 
  </div>
  <!-- /#content --> 
</div>
<script>
$(document).ready(function() {
	
	 /*$('#eventdate').datepicker({
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					calendarWeeks: true,
					autoclose: true,
					format: "dd/mm/yyyy",
		 });*/
		 
		 
	$("#imgInp").change(function(){
    readURL(this);
}); 
    
});  
function getvalue(selectedValue)
 {
    $.ajax({
        url: '<?php echo base_url();?>wishlist/get_wishlist_address',
        type: 'POST',
        data: {option : selectedValue},
		dataType : "json",
		//contentType : "application/json",
        success: function(result) {
		
			$.each(result, function (i,j) {
				
				var name =j['name'];
				var sname = name.split(" ");
				var fname = sname[0];
				var lname = sname[1];
				
				var address =j['street_address'];
				var adr = address.split(",");
				var adr1 = adr[0];
				var adr2 = adr[1];
				
				$('#firstname').val(fname);
				$('#lastname').val(lname);
				$('#addr').val(adr1);
				$('#addr2').val(adr2);
				$('#pcode').val(j['zip']);
				$('#dist').val(j['district']);
				$('#mobileno').val(j['ph_no']);
				//$('#city').val(j['city']);
				
			});
		
		}
		
    });
}
/*$("#wisimage").change(function(){
        readURL(this);
    });
	
function readURL(input) {
	var fileTypes = ['jpg', 'jpeg', 'png','bmp'];  //acceptable file types
	var maxsize = "2097152";
	var filesize = input.files[0].size;
	
		if (input.files && input.files[0]) {
			 var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
			 isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types
			if (isSuccess) { //yes
				if(maxsize > filesize)
				{	
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#uplode_img').attr('src', e.target.result);
					};
					reader.readAsDataURL(input.files[0]);
				}
				else
				{
					$("#wisimage").notify('Image Size should not be more than 2MB. ');
				}
				
			}else{
				$("#wisimage").notify('File Type Not Supported.');
			}
		}
	
}
*/
$("#btnRedirectWishlist").on('click',function(){
	//var url = $("#hidWishlistUrl").val();
	window.location.href = "<?php echo base_url("my-wishlist"); ?>";;
});
</script> 
<script src="<?php echo base_url();?>assets/js/cropbox.js"></script> 
<script type="text/javascript">



$(document).ready(function() {
	
	$('#guest_note').keyup( function() {
			var value = $(this).val();
			
			if (value.length == 0) {
			$(".cractercount").html(0);
			return;
			}
			else
			{
			var regex = /\s+/gi;
			var wordCount = value.trim().replace(regex, ' ').split(' ').length;
			var totalChars = value.length;
			var charCount = value.trim().length;
			var charCountNoSpace = value.replace(regex, '').length;
			$(".cractercount").html(250-charCount);
			}
});
	

/* Date Range Code Start*/

/*	$.datepicker.setDefaults({
		changeMonth: true,
		changeYear: false,
		format: 'dd/mm/yy'
	});*/
	
/*	$("#datepicker").datepicker({
      minDate : +30,
      maxDate : +90,
      dateFormat : "yy-mm-dd",
    });*/
	
	$("#eventdate").datepicker({
    format: "dd-mm-yyyy",
    //weekStart: 1,
   
    autoclose: 1,
    todayHighlight: 1,
    //startView: 2,
    minView: 2,
    startDate : 'now',
    endDate : new Date('2050-09-08'),
}).on('changeDate', function (selected) {
	$('#eventenddate').datepicker('remove');
	$('#eventenddate').val('');
    var minDate = new Date(selected.date.valueOf());
	//var date2 = $('#eventdate').datepicker('getDate', '+60d');
	var date2 =  new Date(selected.date.valueOf());
	
	 date2.setDate(date2.getDate()+60);
	
    $('#eventenddate').datepicker({clearBtn: true,format: "dd-mm-yyyy",autoclose: 1,	startDate: minDate,	endDate:date2,});
});

});
	

</script>
<style>
.listForm input#wisimage {
    height: 42px;
    opacity: 0;
    position: relative;
    width: 100%;
    z-index: 100;
	cursor: pointer;
}
#my-button {
    /*background-color: #3F8BC9;*/
	border-radius: 5px;
    border: 0 solid black;
    padding: 10px 15px;
    position: absolute;
    text-align: center;
    top: 0;
    z-index: 1;
	width:100%;
}
.form-control.error {
    border-color: #ff7272;
}
#file-upload-cont {
    background: #50c9cf none repeat scroll 0 0;
	
    color: #fff;
    position: relative;
}
</style>
