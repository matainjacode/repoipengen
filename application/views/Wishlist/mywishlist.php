
<div id="content">
  <div class="container white">
    <div class="row"> 
      <!--Side Bar-->
      <div class="col-md-3 nopadding"> 
        <!--Original sidebar-->
        <?php $this->load->view('common/leftsidebar')?>
        <!--Eof original sidebar--> 
      </div>
      <!-- Right Column-->
      <div class="col-md-9">
        <div class="dashboard-header"><?php echo $this->lang->line("my_wishlist"); ?></div>
        <div class="allrows" id="removein">
          <?php if(!empty($my_wishlist)){ ?>
          <table class="table table-condensed ">
            <thead>
              <tr>
                <td>&nbsp;</td>
                <td><?php echo $this->lang->line("wishlist_title"); ?></td>
                <td><?php echo $this->lang->line("wishlist_action"); ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($my_wishlist as $my_wishlists){?>
              <?php $urlSlug=$my_wishlists->url; ?>
              <?php if($my_wishlists->wishlist_image !=''){
			        if(is_file($this->config->item('image_path')."wishlist/".$my_wishlists->id."/100-100/".$my_wishlists->wishlist_image)){
					$imagepath= base_url()."photo/wishlist/".$my_wishlists->id."/100-100/".$my_wishlists->wishlist_image;
					}else{
			        $imagepath= base_url().'photo/wishlist/no-event.png';
			      	}}else{ 
					$imagepath= base_url()."photo/wishlist/no-event.png"; 
					}
				?>
              <tr>
                <td><a href="<?php echo getWishlistUrl($urlSlug);?>"><img src="<?php echo $imagepath; ?>" class="img-rounded img-responsive userphoto"></a></td>
                <td><div class="listform wtitle"> <a href="<?php echo getWishlistUrl($urlSlug);?>"><?php echo $my_wishlists->title?></a>
                    <div class="msgprivacy" id="msgprivacy<?php echo $my_wishlists->id?>"></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="listform"><?php echo $this->lang->line("created"); ?> : <strong>
                    <?php $date=date_create($my_wishlists->created); echo date_format($date,'d/m/Y'); ?>
                    </strong> , <?php echo $this->lang->line("end"); ?> : <span class="text-danger"><strong>
                    <?php $date=date_create($my_wishlists->e_enddate);
                   echo date_format($date,'d/m/Y'); ?>
                    </strong></span></div>
                  <div class="listform"><?php echo $this->lang->line("privacy"); ?> : <span class="text-danger" id="text-danger<?php echo $my_wishlists->id;?>"><?php echo ucfirst($my_wishlists->is_public);?></span> &nbsp;
                    <button type="submit" class="btn btn-xs btn-success clschange" role="button" data-toggle="collapse" href="#Privacy<?php echo $my_wishlists->id;?>" data-id="<?php echo $my_wishlists->is_public;?>" aria-expanded="false" aria-controls="Privacy"><?php echo $this->lang->line("change_privacy"); ?></button>
                    <div class="collapse" id="Privacy<?php echo $my_wishlists->id;?>">
                      <div class="alert alert-info" role="alert">
                        <input type="hidden" name="wshid" value="<?php echo $my_wishlists->id;?>" />
                        <div class="checkbox">
                          <label>
                            <input type="radio"  id='publicSelect<?php echo $my_wishlists->id;?>' name="setprivacy" value="public" <?php echo ($my_wishlists->is_public == 'public' ? 'checked' : "");?>>
                            <?php echo $this->lang->line("public"); ?> </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input  type="radio" id='privateSelectp<?php echo $my_wishlists->id;?>' name="setprivacy" value="private" <?php echo ($my_wishlists->is_public=='private' ? 'checked' : "");?>>
                            <i class="fa fa-lock" aria-hidden="true"></i> <?php echo $this->lang->line("private"); ?>
                            <?php //echo ucfirst($my_wishlists->is_public);?>
                          </label>
                          <br>
                          <div class="col-xs-12 col-md-6 listform">
                            <input name="visitor password" type="password" value="" maxlength="15" class="form-control blankpwd" id="error<?php echo $my_wishlists->id;?>" >
                            <span class="text-danger"> * <em><?php echo $this->lang->line("warning_notify"); ?> </em> </span></div>
                          </label>
                        </div>
                        <img src="<?php echo base_url().'files/loader/loader.gif'?>" id="loding" style="display:none" />
                        <p class="text-right">
                          <button type="button" class="btn btn-primary change"><?php echo $this->lang->line("save_privacy_btn"); ?></button>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="listform"><a class="btn btn-warning btn-xs" role="button" data-toggle="collapse" href="#transferEmail<?php echo $my_wishlists->id;?>" aria-expanded="false" aria-controls="transferEmail"> <i class="fa fa-exchange" aria-hidden="true"></i> <?php echo $this->lang->line("friend_transfer"); ?> </a>
                    <div class="collapse" id="transferEmail<?php echo $my_wishlists->id;?>">
                      <div class="alert alert-warning" role="alert">
                        <div class="col-xs-12 col-md-6 listform">
                          <input type="hidden" name="wshid" value="<?php echo $my_wishlists->id;?>" />
                          <input type="text" value="" class="form-control" id="sendmail<?php echo $my_wishlists->id;?>" placeholder="<?php echo $this->lang->line("transfer_email"); ?>">
                        </div>
                        <button type="submit" class="btn btn-info transfer"><?php echo $this->lang->line("transfer"); ?></button>
                        <div class="col-md-1 lodimg" > <img src="<?php echo base_url().'files/loader/loader.gif'?>" id="loding" style="display:none" /> </div>
                      </div>
                    </div>
                  </div></td>
                <td><div class="listform"><a href="<?php echo getWishlistUrl($urlSlug);?>" data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="View"><?php echo $this->lang->line("view"); ?></a> </div>
                  <div class="listform"><a href="<?php echo base_url();?>wishlist/edit/<?php echo $my_wishlists->id; ?>/<?php echo $urlSlug; ?>" data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Edit"><?php echo $this->lang->line("edit"); ?></a></div></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php  } else { ?>
          
            <?php echo $this->lang->line("empty_wishlist"); ?> </div>
          
          <?php } ?>
          <?php echo $this->pagination->create_links(); ?> </div>
        <!--eof allrows--> 
      </div>
      <!--eof infobox for Dashboard--> 
      
    </div>
    <!--Eof Right Column--> 
  </div>
</div>
<!--container White-->
</div>
<script>
	$(document).ready(function(){
		
		
		
		$('.change').on('click',function(){
			var pass,pk;
			var wishid=$(this).parent().parent().find('input[type=hidden]').val();
			var is_public=$(this).parent().parent().find('input[type=radio]:checked').val();
			
			if(is_public=='private')
			{
				pass=$(this).parent().parent().find('input[type=password]').val();
				
				if( pass!="" )
				  {  pk=1; }else{ pk=0; } 
		    }else{ pass="";pk=1;$("#error"+wishid).val("")}
				
			if(is_public=='public')
			{
				$('#loding').css({'display':'block'});
			}	
			
				if(pk==1){
				$('#loding').css({'display':'block'});	
				var dataString = 'wishlistid='+ wishid + '&is_public='+ is_public + '&password='+ pass ;
				
		
	    $.ajax({
			url: '<?php echo base_url();?>wishlist/changeprivacy',
			type: 'POST',
			data: dataString,
			cache: false,
			dataType: 'json',
			
        success: function(result) {
		
				//console.log(result.privacy);
				if(result.msg == 'success'){
				$('#text-danger'+wishid).text(result.privacy);
				$('#loding').css({'display':'none'});
				$("#removein").find(".in").removeClass("in");
				$("#msgprivacy"+wishid).html("<span class='green'>successfully updated</span>");
                $("#error"+wishid).val('')
				
				}
		}
	
		});
		}else{
			
			$("#error"+wishid).addClass("error");
			
			return false;
		}
		});
		
		$('.clschange').on('click',function(){
			
			//$("#removein").find(".in").removeClass("in");
			
			var ispivate = $(this).data('id');
			
	       var pid=$(this).next().find('input[type=hidden]').val();
			var publicselect=$('#publicSelect'+pid).val();
			var privateSelectp=$('#privateSelectp'+pid).val();
				if(publicselect == ispivate){
					
				$('#publicSelect'+pid).attr('checked', 'checked')
				}
				if(privateSelectp == ispivate){
					
				$('#privateSelectp'+pid).attr('checked', 'checked')
				}
			
		});
		
		
		$('.transfer').on('click',function(){
			var email;
			var wishidj=$(this).parent().parent().parent().find('input[type=hidden]').val();
			var is_email=$(this).parent().parent().find('input[type=text]').val();
			var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
			if(is_email !='' && pattern.test(is_email))
			{
				var dataString = 'wishlistid='+ wishidj + '&is_mail='+ is_email ;
				 $('#loding').css({'display':'block'});
				$.ajax({
				url: '<?php echo base_url();?>wishlist/wishlisttransfer',
				type: 'POST',
				data: dataString,
				cache: false,
				dataType: 'json',
				
						success: function(result) 
						{
					
							if(result.msg == 'success')
								{
							        $("#msgprivacy"+wishidj).html("<span class='green'>This wishlist is successfully transferred to "+is_email+"</span>");
									$('#loding').css({'display':'none'});
									$("#transferEmail"+wishidj).removeClass("in");
							
							   }
						}
	
		             });
				
				
			}else{
								
				$("#sendmail"+wishidj).addClass("error")
				
			}
		});
	 });

	</script> 
