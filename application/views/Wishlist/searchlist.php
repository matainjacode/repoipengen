<style>
#content #checkout .table tbody tr td img, #content #basket .table tbody tr td img, #content #customer-order .table tbody tr td img {
    width: 50% !important;
}
div#admintable_wrapper .row:first-child{ display:none;}
.dataTables_info {
    display: none;
}
table.dataTable thead .sorting_asc::after {
    content: "";
}
table.dataTable thead .sorting::after {
    content: "";
    opacity: 0;
}
</style>
<div id="content">
  <div class="container white">
    <div class="linerows" id="basket">
      <h3><?= $this->lang->line("search_list") ?> </h3>
      <?php if(count($result) > 10){ ?>
      <table class="table table-condensed" id="admintable">
        <thead>
          <tr>
            <th ><?= $this->lang->line("wishlist") ?></th>
            <th >&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        <?php 
			if(!empty($result)){ foreach($result as $row){ 
			if(is_file($this->config->item("image_path")."wishlist/".$row["id"]."/100-100/".$row["image"]))
			{$imagePath = base_url()."photo/wishlist/".$row["id"]."/100-100/".$row["image"];}
			elseif(is_file($this->config->item('image_path').'/product/'.$row["id"].$this->config->item('thumb_size').$row["image"]))
			{$imagePath = base_url("photo/product/".$row["id"].$this->config->item('thumb_size').$row["image"]);}
			else
			{$imagePath = base_url("assets/img/no_image.png");}
			if($row["url"] != "")
			{ $url = base_url("~".$row["url"]); }
			else
			{ $url = base_url("p/".$row["id"]."-".slugify($row["title"])); }
			$owner_name = $row["fname"]." ".$row["lname"];
		?>
          <tr>
            <td width="25%"><a href="<?= $url ?>" target="_blank"> <img src="<?= $imagePath; ?>" alt="<?= $row["title"]; ?>" class="img-responsive"> </a></td>
            <td><a href="<?= $url ?>" target="_blank"><?= ucwords($row["title"]); ?></a> <br> 
            	<?php if($row["fname"] != ""){  echo "Owner : ".ucwords($owner_name); } ?> 
            </td>
			<!--<td><input type="number" value="2" class="form-control"></td>
            <td>Rp 123000</td>
            <td>Rp 246000</td>
            <td><a href="#"><i class="fa fa-trash-o"></i></a></td>-->          
			</tr>
         <?php } }else{ ?>
         	<tr>
            <td colspan="2"><h3 align="center"> <?= '"'.$searchkey.'" '.$this->lang->line("not_found_pls_try") ?> </h3></td>
			</tr>
         <?php }  ?>
        </tbody>
        <!--<tfoot>
          <tr>
            <th colspan="4" bgcolor="#99CCCC" class="text-right">Total</th>
            <th colspan="2" bgcolor="#99CCCC">Rp 772400</th>
          </tr>
        </tfoot>-->
      </table>
      <?php }else{ ?>
      <table class="table table-condensed">
        <thead>
          <tr>
            <th ><?= $this->lang->line("wishlist") ?></th>
            <th >&nbsp;</th>
            <th >&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        <?php 
			if(!empty($result)){ foreach($result as $row){ 
			if(is_file($this->config->item("image_path")."wishlist/".$row["id"]."/100-100/".$row["image"]))
			{$imagePath = base_url()."photo/wishlist/".$row["id"]."/100-100/".$row["image"];}
			elseif(is_file($this->config->item('image_path').'/product/'.$row["id"].$this->config->item('thumb_size').$row["image"]))
			{$imagePath = base_url("photo/product/".$row["id"].$this->config->item('thumb_size').$row["image"]);}
			else
			{$imagePath = base_url("assets/img/no_image.png");}
			if($row["url"] != "")
			{ $url = base_url("~".$row["url"]); }
			else
			{ $url = base_url("p/".$row["id"]."-".slugify($row["title"])); }
			$owner_name = $row["fname"]." ".$row["lname"];
		?>
          <tr>
            <td width="25%">
            		<a href="<?= $url ?>" target="_blank"> <img src="<?= $imagePath; ?>" alt="<?= $row["title"]; ?>" class="img-responsive"> </a>
            </td>
            <td width="60%"><a href="<?= $url ?>" target="_blank"><?= ucwords($row["title"]); ?></a> <br> 
            	<?php if($row["fname"] != ""){  echo "Owner : ".ucwords($owner_name); } ?> 
                <?php if(isset($row["price"]) != ""){ echo $row["short_description"]; } ?>
            </td>
			<!--<td><input type="number" value="2" class="form-control"></td>--> 
            <td align="right"><?php if(isset($row["price"]) != "") {echo $this->config->item("currency")." ". $row["price"];}else{ echo "&nbsp;";} ?></td>
            <!--<td>Rp 246000</td>
            <td><a href="#"><i class="fa fa-trash-o"></i></a></td>-->          
			</tr>
         <?php } }else{ ?>
         	<tr>
            <td colspan="3"><h3 align="center"> <?= '"'.$searchkey.'" '.$this->lang->line("not_found_pls_try") ?> </h3></td>
			</tr>
         <?php }  ?>
        </tbody>
        <!--<tfoot>
          <tr>
            <th colspan="4" bgcolor="#99CCCC" class="text-right">Total</th>
            <th colspan="2" bgcolor="#99CCCC">Rp 772400</th>
          </tr>
        </tfoot>-->
      </table>
      <?php } ?>
      <!-- /.table-responsive -->
           
    </div>
  </div>
</div>
<script>
    $('#admintable').DataTable();
</script>