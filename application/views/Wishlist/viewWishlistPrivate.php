<?php 
//print_r($my_wishlist);die();
/*if (file_exists('./photo/wishlist/'.$my_wishlist->id.'/700-700/'.$my_wishlist->wishlist_image)) {
		echo "file has";
}*/
	if($my_wishlist->e_startdate!=''){
		$sDate = new DateTime($my_wishlist->e_startdate);
		$start['date']=$sDate->format('dS F o');
		$start['time']=$sDate->format('H.i a');
	}else{
		$start['date']="Not Specified";
		$start['time']="Not Specified";
	}
	if($my_wishlist->event_date!=''){
		$esDate = new DateTime($my_wishlist->event_date);
		$estart['date']=$esDate->format('dS F o');
		$estart['time']=$esDate->format('H.i a');
	}else{
		$estart['date']="Not Specified";
		$estart['time']="Not Specified";
	}
	if($my_wishlist->e_enddate!=''){
		$eDate = new DateTime($my_wishlist->e_enddate);
		$end['date']=$eDate->format('dS F o');
		$end['time']=$eDate->format('H.i a');
	}else{
		$end['date']="Not Specified";
		$end['time']="Not Specified";
	}
?>
<div id="all">

        <div id="content">
            <div class="container white">
<div class="row">

<div class="usertitle"><div class="wishlisttitle"><?php echo $my_wishlist->title; ?></div></div>

</div>            
<!--User Photo and Information-->
<div class="row bottomArea">
 <div class="item-title text-center text-uppercase" style="color:#626262;margin:15px 0px;"><b><?php echo $this->lang->line("private_wishlist") ?></b></div>
  <div class="col-md-6">
  <div class="added-box">
  <p class="view-password"><?php echo $this->lang->line("wishlist_password_info") ?></p>
   <div class="row">
   <form method='post' name='' action=''>
   <div class="col-md-9">
   <?php if(isset($error)){ ?>
	   
   <input type='password' name='wishPass' id='wishPass' class='form-control border-color-mod textError' placeholder="<?php echo $this->lang->line("password"); ?>"/>
	<?php }else{ ?>
   <input type='password' name='wishPass' id='wishPass' class='form-control border-color-mod' placeholder="<?php echo $this->lang->line("password"); ?>"/>
   <?php } ?>
   </div>
    <div class="col-md-3">
    <input type='submit' name='wishPassSubmit' id='wishPassSubmit' class="btn btn-primary btn-info-full btn-modify" value='<?php echo $this->lang->line("proceed") ?>'/>
    </div>
   </form>
   
   </div> 
   </div>           
   </div>
  
  
  
  
  
  
<div class="col-md-6">
<div class="added-box">
  <p class="view-password"><?php echo $this->lang->line("password_resquest") ?></p>
  <form method="post" action="" name=''>
<div class="row">
   <div class="col-md-9 form-group">
    <?php if(isset($errorFname)){ ?>
    <input type="text" class="form-control border-color-mod textError" placeholder="<?php echo $this->lang->line("f_name"); ?>" name='fname'>
	<?php }else{ ?>
    <input type="text" class="form-control border-color-mod" placeholder="<?php echo $this->lang->line("f_name"); ?>" name='fname'>
   <?php } ?>
   
   </div>
    
   
   
   </div>
    <div class="row">
   <div class="col-md-9 form-group">
   <input type="text" class="form-control border-color-mod" placeholder="<?php echo $this->lang->line("l_name"); ?>" name='lname'>
   </div>
   </div>
   <div class="row">
   <div class="col-md-9 form-group">
   <?php if(isset($errorReqEmail)){ ?>
   <input type="email" class="form-control border-color-mod textError" placeholder="<?php echo $this->lang->line("email"); ?>" name='reqemail'>
	<?php }else{ ?>
   <input type="email" class="form-control border-color-mod" placeholder="<?php echo $this->lang->line("email"); ?>" name='reqemail'>
   <?php } ?>
   
   </div>
    <div class="col-md-3">
    <input type='submit' name='reqPass' id='reqPass' class="btn btn-primary btn-info-full btn-modify" value='<?php echo $this->lang->line("resquest") ?>'/>
    </div>
   
   
   </div>
   <div class="row">
   <div class="col-md-9">
   <?php if(isset($notificationEmail)){ echo "<h4>".$notificationEmail."</h4>"; } ?>

   </div>
   </div>
   </form>
    </div>
  

 
  </div>
</div>

<!--<div class="row lightbg">
 
  <div class="col-md-6">
  <div class="imgbox ">
  <h4>Enter Password to view the Wishlist</h4>
	<form method='post' name='' action=''>
    	<input type='text' name='wishPass' id='wishPass' class='form-control' action=''/>
        <input type='submit' name='wishPassSubmit' id='wishPassSubmit' class="btn btn-primary btn-info-full" value='Submit'/>
    </form>
  </div>       
   </div>
  
<div class="col-md-6">

<div class="infobox">
<div class="wishlisteventinfo">
<form method="POST" action="">
<p>Name: <input type="text" name="Name" size="20" class='form-control'></p>
<p>Email: <input type="text" name="Email" size="20" class='form-control'></p>
<p><input type='submit' name='reqPass' id='reqPass' class="btn btn-primary btn-info-full" value='Request for Password'/></p>
</form>

         
 </div>
    
    </div>
  

 
  </div>
</div>-->
<!--eof User Photo and Information-->                   
</div></div><!-- /.container -->         
</div>
<script type="text/javascript">
function initMap(myLatLng,address) {
       // var myLatLng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: address
        });
  
  
}

function addMarker(map){
 $.each(map,function(index,value){
  var lat = value['lat'];
  var long = value['long'];
  var add = value['address'];
  
  var myLatLng = {lat: lat, lng: long};
  var address = value['address'];
  var hidVal = lat +','+long;
  $("#mapLatlong").val(hidVal);
  initMap(myLatLng,address);
 }) 
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDavhJIxC8MxS_fW7b3Dd3CEAGEkmRSn4g&callback=initMap"></script>       
<script>

var mapArr = [];
//Function to covert address to Latitude and Longitude
var getLocation =  function(address) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
      	var latitude = results[0].geometry.location.lat(); 
      	var longitude = results[0].geometry.location.lng();
	  	mapArr.push({
            lat: latitude, 
            long:  longitude,
			 address:address
        });
		
		addMarker(mapArr);
      } 
  }); 
}

<?php if($my_wishlist->wishlist_address!=''){ ?>
	getLocation('<?php echo $my_wishlist->wishlist_address; ?>');
<?php } else{ ?>
	getLocation('kolkata,India');
<?php }?>

$(".listform").find("#map").click(function(){
 var hidden = $("#mapLatlong").val();
 //window.location.href = "http://maps.google.com/maps?q="+hidden;
  window.open("http://maps.google.com/maps?q="+hidden,'_blank');
});

</script>
<!-- Magnific Popup core CSS file -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
<!-- Magnific Popup core JS file -->
<script>
$(document).ready(function(){
	$('.image-link').magnificPopup({type:'image'});
	$('#addRecipient').on('click',function(){
		var dataString = 'uid='+<?php echo $loginUserArray['user_id'] ?>+'&tag="addRecipient"&wid='+<?php echo $my_wishlist->id;?>+'&wishUid='+<?php echo $my_wishlist->uid;?>;
		$.ajax({
				   type: "POST",
				   url: "<?php echo base_url();?>wishlist/addRecipient",
				   data: dataString,
				   cache: false,
				   success: function(result){
					   //alert(result);die();
						if(result=='success'){
							//$("#addRecipient").addClass('displayNone'); 
							/*$("#removeRecipient").css('display','block');*/
							$("#addRecipient").css('display','none'); 
							$("#removeRecipient").css('display','block');
						}
				   }
				});
	});
	$('#removeRecipient').on('click',function(){
		var dataString = 'uid='+<?php echo $loginUserArray['user_id'] ?>+'&tag="removeRecipient"&wid='+<?php echo $my_wishlist->id;?>+'&wishUid='+<?php echo $my_wishlist->uid;?>;
		$.ajax({
				   type: "POST",
				   url: "<?php echo base_url();?>wishlist/removeRecipient",
				   data: dataString,
				   cache: false,
				   success: function(result){
					   //alert(result);die();
					   	if(result=='success'){
							$("#removeRecipient").css('display','none');
							//$("#removeRecipient").css('display','none');   
							$("#addRecipient").css('display','block');
						}
				   }
				});
	});
	
});

</script>

