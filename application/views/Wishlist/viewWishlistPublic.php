<style>
.wishlistblock {
	color: #4491a2;
	font-family: "Pacifico", cursive;
	font-size: 20px;
	font-weight: 700;
	text-align: center;
	width: 100%;
	color: #F00;
}
</style>
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/bootstrap-tagsinput.css">
<?php
	if($my_wishlist->e_startdate!=''){
		$sDate = new DateTime($my_wishlist->e_startdate);
		$start['date']=$sDate->format('dS F o');
		$start['time']=$sDate->format('H.i a');
	}else{
		$start['date']=$this->lang->line("not_specified");
		$start['time']=$this->lang->line("not_specified");
	}
	if($my_wishlist->event_date!=''){
		$esDate = new DateTime($my_wishlist->event_date);
		$estart['date']=$esDate->format('dS F o');
		$estart['time']=$esDate->format('H.i a');
	}else{
		$estart['date']=$this->lang->line("not_specified");
		$estart['time']=$this->lang->line("not_specified");
	}
	if($my_wishlist->e_enddate!=''){
		$eDate = new DateTime($my_wishlist->e_enddate);
		$end['date']=$eDate->format('dS F o');
		$end['time']=$eDate->format('H.i a');
	}else{
		$end['date']=$this->lang->line("not_specified");
		$end['time']=$this->lang->line("not_specified");
	}
	$tmount=0;
	
	
	if(is_file($this->config->item('image_path').'userimage/'.$my_wishlist->uid.'/'.$wishlist_owner->profile_pic)){
		$imagepath = 'photo/userimage/'.$my_wishlist->uid.'/'.$wishlist_owner->profile_pic;
	 }
     else{
		$imagepath = 'assets/img/userimage.png';
	 }
	 
?>
<div id="all">
  <div id="content">
    <div class="container white"> 
      <?php if($loginUserArray['user_id']!=$my_wishlist->uid){ ?>
      <div class="row topbutton">
        <div class="col-md-8 rowgap">
          <table width="100%" cellspacing="5" cellpadding="5" border="0">
            <tbody>
              <tr>
                <td width="85" valign="middle" align="left"><a href="<?php echo $wishlist_owner_url ?>"><img src="<?php echo $imagepath ?>" class="img-circle img-responsive userphoto" width="80"></a></td>
                <td valign="middle" align="left"><strong><a href="<?php echo $wishlist_owner_url ?>"><?php echo strtoupper($wishlist_owner->fname).' '.strtoupper($wishlist_owner->lname) ?></a></strong></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <?php }else{ $url=base_url().'~'.$my_wishlist->url ?>
      
      <div style="clear:both;"></div>
      <?php }?>
      <div class="row usertitle">
        <div class="wishlisttitle"><?php echo $my_wishlist->title; ?></div>
        <div class="wishlistblock"><?php echo ($my_wishlist->admin_status==0? $this->lang->line("wishlist_Block") :''); ?></div>
      </div>
      
      <!--User Photo and Information-->
      <div class="row lightbg">
        <div class="col-md-6">
          <div class="imgbox">
            <input type="hidden" id="wisid" name="wisid" value="<?php echo $my_wishlist->id; ?>" />
            <?php if($my_wishlist->wishlist_image!='' && file_exists('./photo/wishlist/'.$my_wishlist->id.'/700-700/'.$my_wishlist->wishlist_image)){ ?>
            <a href="<?php echo base_url();?>photo/wishlist/<?php echo $my_wishlist->id;?>/<?php echo $my_wishlist->wishlist_image;?>" class="image-link"><img src="<?php echo base_url();?>photo/wishlist/<?php echo $my_wishlist->id;?>/700-700/<?php echo $my_wishlist->wishlist_image;?>" class="bg-image magnify"></a>
            <?php }else{ ?>
            <img src="<?php echo base_url();?>photo/wishlist/no-event.png" class="bg-image">
            <?php } ?>
            <input type="hidden" id="mapLatlong" value=""/>
          </div>
        </div>
        <div class="col-md-6">
          <div class="wishlistinfo pull-right"> <span class="label label-danger"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $this->lang->line("end"); ?> : <?php echo $end['date']; ?></span></div>
          <div class="infobox">
            <div class="wishlisteventinfo">
              <div class="listform">
                <h5><?php echo $this->lang->line("event_info"); ?></h5>
                <?php echo $this->lang->line("w_e_date"); ?> : <?php echo $estart['date']; ?>. <?php echo $this->lang->line("time"); ?> : <?php echo $estart['time']; ?></div>
              <div class="listform">
                <h5><?php echo $this->lang->line("special_note"); ?></h5>
                <?php echo $my_wishlist->rsvp_info?$my_wishlist->rsvp_info : $this->lang->line("not_specified"); ?></div>
              <div class="listform">
                <h5><?php echo $this->lang->line("event_address"); ?></h5>
                
                <?php echo $my_wishlist->wishlist_address?$my_wishlist->wishlist_address : $this->lang->line("not_specified"); ?> </div>
              <?php if($my_wishlist->is_show_shipping_address=='yes'){ ?>
              <div class="listform">
                <h5><?php echo $this->lang->line("event_address") ?></h5>
                <?php echo !empty($my_wishlist_shipping)?$my_wishlist_shipping->name."<br/>".$my_wishlist_shipping->street_address."<br/>".$my_wishlist_shipping->zip."<br/>".$my_wishlist_shipping->district."<br/>".$my_wishlist_shipping->city."<br/>".$my_wishlist_shipping->ph_no : $this->lang->line("not_specified"); ?> </div>
              <?php } ?>
              <?php if(isset($my_wishlist->wishlist_address) && $my_wishlist->wishlist_address!=''){ ?>
              <div class="listform">
                <div id="map"></div>
                <input type='hidden' id='latDiv' value=''>
                <input type='hidden' id='longDiv' value=''>
               </div>
               <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <!--eof User Photo and Information-->
       <?php if($loginUserArray['user_id']==$my_wishlist->uid){ $url=base_url().'~'.$my_wishlist->url; ?>
      <div class="row row-eq-height">
        <div class="col-md-6 text-center red-bg"> Share Wishlist :
          <div class="text-right" style="float: right; padding-right: 10px; margin: -4px 20px;">
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style"> <a  class="a2a_dd" href="https://www.addtoany.com/share"></a> <a href="http://www.facebook.com/sharer.php?u=<?php echo $url;?>" title="Facebook share" class="a2a_button_facebook a2a_counter"></a> <a href="http://twitter.com/share?url=<?php echo $url;?>&amp;text=Share popup on &amp;hashtags=ipengen" title="Twitter share" class="a2a_button_twitter"></a> <a href="mailto:?subject=Ipengen Wishlist&amp;body=Check out this site <?php echo $url;?>." title="Share by Email" class="a2a_button_google_plus a2a_counter"></a> </div>
            <script async src="https://static.addtoany.com/menu/page.js"></script> 
          </div>
        </div>
        <div class="col-md-6 text-center bg-info">
        <div class="rowgap"> <a href="<?php echo site_url('shop'); ?>" class="btn btn-primary btn-lg"><i class="fa fa-plus" aria-hidden="true"></i><?php echo $this->lang->line("more_gift_wishlist") ?> </a> </div>
      </div>
      </div>
      <?php } ?>
      <!--Gift Cash-->
      <div class="row">
        <div class="allrows">
          <input type="hidden" value="<?= $wishlistID ?>" id="wishlistId">
          <?php if($loginUserArray['user_id']!=$my_wishlist->uid){ ?>
          <div class="item-title text-center"><?php echo $this->lang->line("send_cash_gift") ?></div>
          <div class="item-desc text-center"><?php echo $this->lang->line("send_cash_gift_instead") ?></div>
          <div class="form-inline text-center">
            <div class="form-group">
              <label class="sr-only" for="exampleInputAmount"><?php echo $this->lang->line("amount_info") ?></label>
              <div class="input-group">
                <div class="input-group-addon"><?php echo $this->config->item('currency'); ?></div>
                <input type="text" class="form-control input-lg amnt" id="exampleInputAmount" placeholder="<?php echo $this->lang->line("amount") ?>">
              </div>
            </div>
            <button class="btn btn-info btn-lg" id="send_gift_cash" data-wishlist-id="<?php echo $my_wishlist->id; ?>"><?php echo $this->lang->line("send_cash_gift") ?></button>
            <h5 class="msgBox"></h5>
          </div>
        </div>
      </div>
      <?php }else{ ?>
      <!--eof Gift Cash-->
      
      <div class="row">
        <div class="allrows">
          <div class="item-title text-center"><?php echo $this->lang->line("cash_gift_receive")?></div>
          <table class="table table-condensed">
            <thead>
              <tr>
                <th><?php echo $this->lang->line("table_order") ?> </th>
                <th><?php echo $this->lang->line("table_date") ?> </th>
                <th><?php echo $this->lang->line("table_total") ?> </th>
                <th><?php echo $this->lang->line("table_status") ?> </th>
              </tr>
            </thead>
            <tbody>
              <?php  if(!empty($my_wishlist_cashgift)) { foreach($my_wishlist_cashgift as $row){ ?>
              <tr>
                <td># <?php echo $row->order_id; ?> : <a href="<?php echo base_url("~".$row->url) ?>"> <?php echo $row->title ?> </a> - <?php echo $this->lang->line("cash_gift_from") ?> <a href="<?php echo base_url("u/".$row->user_id) ?>"><?php echo ucfirst($row->fname)." ".ucfirst($row->lname);  ?></a></td>
                <td><?php echo date("d/m/Y",strtotime($row->transaction_time)) ?></td>
                <td>Rp <?php echo number_format($row->gross_amount, 2) ?></td>
                <td><span class="label label-info">
                  <?php 
							//if($row->transaction_status == "capture") 
							//{ echo "Success";}
							echo ucfirst($row->transaction_status);
						?>
                  </span></td>
              </tr>
              <?php }}else{ ?>
              <tr>
                <td colspan="4"><h4 align="center"><?php echo $this->lang->line("no_data_found") ?></h4></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row text-center bg-info">
        <div class="col-md-8 col-sm-8"></div>
        <div class="col-md-4 col-sm-4 text-right listform">
          <label for="FILTER"></label>
          <select name="FILTER" id="FILTER" class="form-control">
            <option value=""><?php echo $this->lang->line("filter_by") ?></option>
            <option value="latest"><?php echo $this->lang->line("latest") ?></option>
            <option value="natz"><?php echo $this->lang->line("Name_A_Z") ?></option>
            <option value="nzta"><?php echo $this->lang->line("Name_Z_A") ?></option>
            <option value="phtl"><?php echo $this->lang->line("Price_High_to_Low") ?></option>
            <option value="plth"><?php echo $this->lang->line("Price_Low_to_High") ?></option>
          </select>
        </div>
      </div>
      <?php } ?>
      <div id='productListDiv'> 
        <!--Product Listing Here--> 
      </div>
      <?php if($loginUserArray['user_id']==$my_wishlist->uid){ ?>
      <div class="row text-center bg-info">
        <div class=""> <a href="<?php echo site_url('shop'); ?>" class="btn btn-primary btn-lg"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo $this->lang->line("more_gift_wishlist") ?></a> </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <!-- /.container --> 
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:440px;"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $this->lang->line("success"); ?></h4>
      </div>
      <div class="modal-body">
        <table class="table modalTbl">
        </table>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-xs-3 pull-left"><a href="<?php echo site_url("cart/view"); ?>">
            <button class="btn btn-info"><?php echo $this->lang->line("view_cart"); ?></button>
            </a></div>
          <div class="col-xs-6 pull-right"><a href="<?php echo site_url("checkout/index"); ?>">
            <button class="btn btn-success"><?php echo $this->lang->line("checkout"); ?></button>
            </a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url()?>assets/frontend/js/bootstrap-tagsinput.min.js"></script> 
<script type="text/javascript">
function initMap(myLatLng,address) {
       // var myLatLng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: address
        });
  
  
}

function addMarker(map){
 $.each(map,function(index,value){
  var lat = value['lat'];
  var long = value['long'];
  var add = value['address'];
  
  var myLatLng = {lat: lat, lng: long};
  var address = value['address'];
  var hidVal = lat +','+long;
  $("#mapLatlong").val(encodeURIComponent(address));
  initMap(myLatLng,address);
 }) 
}
</script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDavhJIxC8MxS_fW7b3Dd3CEAGEkmRSn4g&callback=initMap"></script> 
<script>

var mapArr = [];
//Function to covert address to Latitude and Longitude
var getLocation =  function(address) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
      	var latitude = results[0].geometry.location.lat(); 
      	var longitude = results[0].geometry.location.lng();
	  	mapArr.push({
            lat: latitude, 
            long:  longitude,
			 address:address
        });
		
		addMarker(mapArr);
      } 
  }); 
}


<?php if($my_wishlist->wishlist_address!=''){ ?>
	getLocation('<?php echo $my_wishlist->wishlist_address; ?>');
<?php } else{ ?>
	getLocation('kolkata,India');
<?php }?>

$(".listform").find("#map").click(function(){
 var hidden = $("#mapLatlong").val();
 //window.location.href = "http://maps.google.com/maps?q="+hidden;
  window.open("http://maps.google.com/maps?q="+hidden,'_blank');
});

</script> 
<!-- Magnific Popup core CSS file -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
<!-- Magnific Popup core JS file --> 
<script>
$(document).ready(function(){
	
	var changeVal = "";
	var wishlistid = "";
		/*Onload Variable*/
		"use strict";
		changeVal = jQuery(this).val();
		wishlistid = jQuery("#wishlistId").val();
		getWishlistProduct(changeVal, wishlistid);
		/*End */
		
	jQuery(document).on("change","#FILTER",function(){
		changeVal = jQuery(this).val();
		wishlistid = jQuery("#wishlistId").val();
		getWishlistProduct(changeVal, wishlistid);
	});
	jQuery(document).on("click",".taginputInit",function(){ $("#emailall").tagsinput('refresh'); });
	$('.image-link').magnificPopup({type:'image'});
	$(document).on('click','#addRecipient',function(){
		var dataString = 'uid='+<?php echo $loginUserArray['user_id'] ?>+'&tag="addRecipient"&wid='+<?php echo $my_wishlist->id;?>+'&wishUid='+<?php echo $my_wishlist->uid;?>;
		$.ajax({
				   type: "POST",
				   url: "<?php echo base_url();?>wishlist/addRecipient",
				   data: dataString,
				   cache: false,
				   success: function(result){
					   //alert(result);die();
						if(result=='success'){
							//$("#addRecipient").addClass('displayNone'); 
							/*$("#removeRecipient").css('display','block');*/
							$("#addRecipient").css('display','none'); 
							$("#removeRecipient").css('display','block');
						}
				   }
				});
	});
	$(document).on('click','#removeRecipient',function(){
		var dataString = 'uid='+<?php echo $loginUserArray['user_id'] ?>+'&tag="removeRecipient"&wid='+<?php echo $my_wishlist->id;?>+'&wishUid='+<?php echo $my_wishlist->uid;?>;
		$.ajax({
				   type: "POST",
				   url: "<?php echo base_url();?>wishlist/removeRecipient",
				   data: dataString,
				   cache: false,
				   success: function(result){
					   //alert(result);die();
					   	if(result=='success'){
							$("#removeRecipient").css('display','none');
							//$("#removeRecipient").css('display','none');   
							$("#addRecipient").css('display','block');
						}
				   }
				});
	});
	
	$(document).on("click","#send_gift_cash",function(){ 
				var wishlistId=$(this).attr("data-wishlist-id");
				var type = 'cash_gift';
				var amount = $("#exampleInputAmount").val();
				var product_name = "Cash Gift";
				var dataString = 'wid='+wishlistId+'&product_name='+product_name+'&amount='+amount+'&type='+type;
				//alert(dataString);
				if(amount!=''){ //alert(amount);
					$.ajax({
					   type: "POST",
					   url: "<?php echo base_url();?>cart",
					   data: dataString,
					   dataType: 'json',
					   success: function(result){
						  if(result){
							  							  console.log(result);


							  $("#msgBox").html("You have successfully added item to cart!");
							  $("#msgBox").show().delay(2000).queue(function(n) {
									  $(this).hide(); n();
									});
									
						  	  $('#dLabel').html('<i class="fa fa-shopping-cart cart"></i>'+ result['count'] +'<span class="caret"></span>');
							  $(".modal-body > table.modalTbl").html("");
							  if(result['item']['type'] == 'buy_gift'){
								  if(result['item']['wishlist_id']!=0){
									var title = "BUY GIFT - "+(result['item']['name']).toUpperCase()+" FOR WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
									var img = result['item']['image'];
								  }else{
									var title = (result['item']['name']).toUpperCase();
									var img = result['item']['image'];
								  }
							  }else if(result['item']['type'] == 'cash_gift'){
								var title = "CASH GIFT - "+(result['item']['wishlist_name']).toUpperCase() + " (Cash Transaction Fees -"+result['item']['transaction_fees']+")";
								var img = result['item']['no_image'];
							  }else{
								var title = "CONTRIBUTION - "+(result['item']['name']).toUpperCase()+" FROM WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
								var img = result['item']['no_image'];  
								  
							  }
									  var content = "<tr><td><img src='"+img+"' width='50' height='67'> </a></td><td><p>"+result['item']['qty']+"<br />"+title+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+result['item']['price']+"</strong></p></td></tr>";
							  $(".modal-body > table.modalTbl").append(content);
							  $("#myModal").modal();
							  $.ajax({
							   type: "POST",
							   url: "<?php echo base_url();?>cart/info",
							   dataType: 'json',
							   success: function(res){ 
							    $("ul.minicart > div.text-center").html("");
							    $("table.mini-cart").html("");
								  var total = 0;
								  $.each(res,function(i,k){
									  if(k.type == 'buy_gift'){
										  if(k.wishlist_id!=0){
											var name = "BUY GIFT - "+(k.name).toUpperCase()+" FOR WISHLIST "+(k.wishlist_name).toUpperCase();
											var image = k.image;
										  }else{
											var name = (k.name).toUpperCase();
											var image = k.image;
										  }
									  }else if(k.type == 'cash_gift'){
										var name = "CASH GIFT - "+(k.wishlist_name).toUpperCase();
 	  									var image = k.no_image;
									  }else{
										var name = "CONTRIBUTION - "+(k.name).toUpperCase()+" FROM WISHLIST "+(k.wishlist_name).toUpperCase();
 	  									var image = k.no_image;  
										  
									  }
									  var html = "<tr><td><img src='"+image+"' width='50' height='67'> </a></td><td><p>"+k.qty+"<br />"+name+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+k.price+"</strong></p></td><td><button type='button' class='close remvItem' aria-label='Close' data-row-id="+k.rowid+"><span aria-hidden='true'>&times;</span></button></td></tr>";

									  $("table.mini-cart").append(html);
									  total = total + k.subtotal;
								  });
								  var htm = "<li role='separator' class='divider'></li><div class='text-right'><h5 id='subtotal'>SUBTOTAL : <?php echo $this->config->item('currency'); ?>"+" "+total+"</h5></div><li role='separator' class='divider'></li><div class='row'><div class='col-md-6'><div class='pull-left'><a href='<?php echo site_url("cart/view"); ?>'><button type='button' class='btn btn-info btn-sm'><i class='fa fa-shopping-cart' aria-hidden='true'></i> VIEW</button></a></div></div><div class='col-md-6'><div class='pull-right'><a href='<?php echo site_url("checkout"); ?>'><button type='button' class='btn btn-success btn-sm'>CHECKOUT <i class='fa fa-caret-right' aria-hidden='true'></i></button></a></div></div> </div>";
								  $(".total").html(htm);
								}
							});
						  }
						}
					});
				}
				else{$("#send_gift_cash").notify(" Please enter an amount!"); }
			});
			
	$(document).on("click",".contributeBtn",function(){ 
				var wishlistId=$(this).attr("data-wishlist-id");
				var type = 'contributed_cash_gift';
				var product_id = $(this).attr("data-product-id");
				var contribute_amount = $("#contributeInputAmount_"+product_id).val();
				var product_name = $(this).attr("data-product-name");
				var dataString = 'wid='+wishlistId+'&product_id='+product_id+'&product_name='+product_name+'&contribute_amount='+contribute_amount+'&type='+type;
				
				var camount=$(this).parent().find("input[name='contrbutetotal']" ).val();
				var pprice=$(this).parent().find("input[name='pro_price']" ).val();
				if(contribute_amount!=''){ 
				     if(parseInt(pprice)>=parseInt(contribute_amount)+parseInt(camount)){
						  $(this).parent().parent().parent().prev().hide();				
						  $.ajax({
					   type: "POST",
					   url: "<?php echo base_url();?>cart",
					   data: dataString,
					   dataType: 'json',
					   success: function(result){
						  if(result){
							  $("#msgBox").html("You have successfully added item to cart!");
							  $("#msgBox").show().delay(2000).queue(function(n) {
									  $(this).hide(); n();
									});
						  	  $('#dLabel').html('<i class="fa fa-shopping-cart cart"></i>'+ result['count'] +'<span class="caret"></span>');
							  $(".modal-body > table.modalTbl").html("");
							  if(result['item']['type'] == 'buy_gift'){
								  if(result['item']['wishlist_id']!=0){
									var title = "BUY GIFT - "+(result['item']['name']).toUpperCase()+" FOR WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
									var img = result['item']['image'];
								  }else{
									var title = (result['item']['name']).toUpperCase();
									var img = result['item']['image'];
								  }
							  }else if(result['item']['type'] == 'cash_gift'){
								var title = "CASH GIFT - "+(result['item']['wishlist_name']).toUpperCase();
								var img = result['item']['no_image'];
							  }else{
								var title = "CONTRIBUTION - "+(result['item']['name']).toUpperCase()+" FROM WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
								var img = result['item']['no_image'];  
								  
							  }
									  var content = "<tr><td><img src='"+img+"' width='50' height='67'> </a></td><td><p>"+result['item']['qty']+"<br />"+title+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+result['item']['price']+"</strong></p></td></tr>";
							  $(".modal-body > table.modalTbl").append(content);	  
							  $("#myModal").modal();
							  $.ajax({
							   type: "POST",
							   url: "<?php echo base_url();?>cart/info",
							   dataType: 'json',
							   success: function(res){ 
							    $("ul.minicart > div.text-center").html("");
							    $("table.mini-cart").html("");
								  var total = 0;
								  $.each(res,function(i,k){
									  if(k.type == 'buy_gift'){
										  if(k.wishlist_id!=0){
											var name = "BUY GIFT - "+(k.name).toUpperCase()+" FOR WISHLIST "+(k.wishlist_name).toUpperCase();
											var image = k.image;
										  }else{
											var name = (k.name).toUpperCase();
											var image = k.image;
										  }
									  }else if(k.type == 'cash_gift'){
										var name = "CASH GIFT - "+(k.wishlist_name).toUpperCase();
 	  									var image = k.no_image;
									  }else{
										var name = "CONTRIBUTION - "+(k.name).toUpperCase()+" FROM WISHLIST "+(k.wishlist_name).toUpperCase();
 	  									var image = k.no_image;  
										  
									  }
									  var html = "<tr><td><img src='"+image+"' width='50' height='67'> </a></td><td><p>"+k.qty+"<br />"+name+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+k.price+"</strong></p></td><td><button type='button' class='close remvItem' aria-label='Close' data-row-id="+k.rowid+"><span aria-hidden='true'>&times;</span></button></td></tr>";

									  $("table.mini-cart").append(html);
									  total = total + k.subtotal;
								  });
								  var htm = "<li role='separator' class='divider'></li><div class='text-right'><h5 id='subtotal'>SUBTOTAL : <?php echo $this->config->item('currency'); ?>"+" "+total+"</h5></div><li role='separator' class='divider'></li><div class='row'><div class='col-md-6'><div class='pull-left'><a href='<?php echo site_url("cart/view"); ?>'><button type='button' class='btn btn-info btn-sm'><i class='fa fa-shopping-cart' aria-hidden='true'></i> VIEW</button></a></div></div><div class='col-md-6'><div class='pull-right'><a href='<?php echo site_url("checkout"); ?>'><button type='button' class='btn btn-success btn-sm'>CHECKOUT <i class='fa fa-caret-right' aria-hidden='true'></i></button></a></div></div> </div>";
								  $(".total").html(htm);
								}
							});
							  
							  
						  }
						}
					});
					 }else{
						$(this).parent().parent().find('.contributeBtn').notify("Please input lesser amount than the contribution money left!");
					 }
				}
				else{ 
				$(this).parent().parent().find('.contributeBtn').notify(" Please enter an amount!");
				 }
			});
	$(document).on("click",".button_buy_gift",function(){ 
			var wishlistId=$(this).attr("data-wishlist-id");
			var quantity = 1; 
			var product_id = $(this).attr("data-product-id");
			var product_name = $(this).attr("data-product-name");
			var stock_quantity = $(this).attr("data-product-stock");
			var product_type = 'buy_gift';
			var dataString = 'wid='+wishlistId+'&product_id='+product_id+'&product_name='+product_name+'&quantity='+quantity+'&type='+product_type;
			
			if(stock_quantity >= quantity){

				$(this).parent().next().hide();
				$.ajax({
				   type: "POST",
				   url: "<?php echo base_url();?>cart",
				   data: dataString,
				   dataType: 'json',
				   success: function(result){
					  if(result){
						  $("#msgBox").html("You have successfully added item to cart!");
						  $("#msgBox").show().delay(2000).queue(function(n) {
								  $(this).hide(); n();
								});
						  $('#dLabel').html('<i class="fa fa-shopping-cart cart"></i>'+ result['count'] +'<span class="caret"></span>');
						  $(".modal-body > table.modalTbl").html("");
						  if(result['item']['type'] == 'buy_gift'){
							  if(result['item']['wishlist_id']!=0){
								var title = "BUY GIFT - "+(result['item']['name']).toUpperCase()+" FOR WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
								var img = result['item']['image'];
							  }else{
								var title = (result['item']['name']).toUpperCase();
								var img = result['item']['image'];
							  }
						  }else if(result['item']['type'] == 'cash_gift'){
							var title = "CASH GIFT - "+(result['item']['wishlist_name']).toUpperCase();
							var img = result['item']['no_image'];
						  }else{
							var title = "CONTRIBUTION - "+(result['item']['name']).toUpperCase()+" FROM WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
							var img = result['item']['no_image'];  
							  
						  }
								  var content = "<tr><td><img src='"+img+"' width='50' height='67'> </a></td><td><p>"+result['item']['qty']+"<br />"+title+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+result['item']['price']+"</strong></p></td></tr>";
						  $(".modal-body > table.modalTbl").append(content);
						  $("#myModal").modal();
						  $.ajax({
						   type: "POST",
						   url: "<?php echo base_url();?>cart/info",
						   dataType: 'json',
						   success: function(res){ console.log(res); 
							$("ul.minicart > div.text-center").html("");
							$("table.mini-cart").html("");
							  var total = 0;
							  $.each(res,function(i,k){
								  if(k.type == 'buy_gift'){
									  if(k.wishlist_id!=0){
										var name = "BUY GIFT - "+(k.name).toUpperCase()+" FOR WISHLIST "+(k.wishlist_name).toUpperCase();
										var image = k.image;
									  }else{
										var name = (k.name).toUpperCase();
										var image = k.image;
									  }
								  }else if(k.type == 'cash_gift'){
									var name = "CASH GIFT - "+(k.wishlist_name).toUpperCase();
									var image = k.no_image;
								  }else{
									var name = "CONTRIBUTION - "+(k.name).toUpperCase()+" FROM WISHLIST "+(k.wishlist_name).toUpperCase();
									var image = k.no_image;  
									  
								  }
								  var html = "<tr><td><img src='"+image+"' width='50' height='67'> </a></td><td><p>"+k.qty+"<br />"+name+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+k.price+"</strong></p></td><td><button type='button' class='close remvItem' aria-label='Close' data-row-id="+k.rowid+"><span aria-hidden='true'>&times;</span></button></td></tr>";

								  $("table.mini-cart").append(html);
								  total = total + k.subtotal;
							  });
							  var htm = "<li role='separator' class='divider'></li><div class='text-right'><h5 id='subtotal'>SUBTOTAL : <?php echo $this->config->item('currency'); ?>"+" "+total+"</h5></div><li role='separator' class='divider'></li><div class='row'><div class='col-md-6'><div class='pull-left'><a href='<?php echo site_url("cart/view"); ?>'><button type='button' class='btn btn-info btn-sm'><i class='fa fa-shopping-cart' aria-hidden='true'></i> VIEW</button></a></div></div><div class='col-md-6'><div class='pull-right'><a href='<?php echo site_url("checkout"); ?>'><button type='button' class='btn btn-success btn-sm'>CHECKOUT <i class='fa fa-caret-right' aria-hidden='true'></i></button></a></div></div> </div>";
							  $(".total").html(htm);
							  
							}
						});
					  }
					}
				});
			}
			else{ $(".button_buy_gift").notify("Out of Stock."); }
		});
			
			
		$(document).on("click",".Invitecls",function(){
		//	alert($('#emailall').val());
			var notifyID = $(this).attr("id");
		    var wisid=$('#wisid').val();
			var proId=$(this).parent().find("input[name='pro_id']" ).val();
			var emailall=new Array();
			$("#emailall option").each(function(){
    			if(isEmail($(this).val()))
				{
					
					emailall.push($(this).val());
				}
			
			});
			if(emailall.length!=0){
				$.ajax({
					   type: "POST",
					   url: "<?php echo base_url();?>wishlist/sendmail",
					   data: {emailall:emailall,wisid:wisid,proid:proId},
					   success: function(result){
						   if($.trim(result) == 1)
						   {
							   $("#"+notifyID).notify("Contribution Email send Successfully.","success");
						   }
						   else
						   {
							   $("#"+notifyID).notify("Sorry,  something wrong went..Try again.","error");
						   }
					   }
				});
			}

		});
		
		function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
	
	
	$('.emlcls').on('keypress',"input[type='text']", function() {
		
    var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
    if(!re) {
        //$('#error').show();
		
    } else {
       // $('#error').hide();
	   
    }
})
	$('input').on('beforeItemAdd', function(event) {
		//alert('fdsfd');
  // event.item: contains the item
  // event.cancel: set to true to prevent the item getting added
});
	
	
	
	$(document).on("keydown",'.amnt',function (e) {
  var key = e.charCode || e.keyCode || 0;            
        return (
            key == 8 || 
            key == 9 ||
            key == 46 ||
            (key >= 37 && key <= 40) ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105)
  ); 
 });
});
function getWishlistProduct(val, id)
{
	"use strict";
	$.ajax({
			type:"POST",
			url:"<?php echo base_url() ?>wishlist/getwishlistproduct",
			data:{"shortVal":val,"wishlistid":id},
			success: function(data){
					if($.trim(data) != "")
					{ $("#productListDiv").html(data); }
					else
					{ $("#productListDiv").html(""); }
				}
		});
}	

</script> 
