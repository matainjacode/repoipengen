<div id="all">

        <div id="content">
            <div class="container white">
            <br/>
				 <?php if($this->session->flashdata("errorMsg")){ ?>
                            	<div class="alert alert-danger"><?php echo $this->session->flashdata("errorMsg"); ?> </div>
                            <?php } ?>
                            
                            <?php
								if($this->session->flashdata("quantity"))
								{
									$msg = $this->session->flashdata("quantity");
									if(!empty($msg))
									{
										foreach($msg as $r=>$v)
										{ 
							?>
                               		<div class="alert alert-danger"><?php echo $v ?></div>
                             <?php
										}
									}
								}
							?>
                            
                <div class="linerows" id="basket">

              		<form action="<?php echo site_url('cart/update'); ?>" method="post" />

                            <h3><?= $this->lang->line("shopping_cart") ?></h3>
                            
                                <?php if(isset($products) && !empty($products)){ ?>  
                                                     
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th colspan="2"><?= $this->lang->line("product") ?></th>
                                            <th><?= $this->lang->line("quantity") ?></th>
                                            <th><?= $this->lang->line("price") ?></th>
                                            <th colspan="2"><?= $this->lang->line("table_total") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php 
											foreach($products as $product){
												echo form_hidden('cart[' . $product['id'] . '][id]', $product['id']);
												echo form_hidden('cart[' . $product['id'] . '][rowid]', $product['rowid']);
												echo form_hidden('cart[' . $product['id'] . '][name]', $product['name']);
												echo form_hidden('cart[' . $product['id'] . '][price]', $product['price']);
												echo form_hidden('cart[' . $product['id'] . '][qty]', $product['qty']);	
										?>
                                        	<tr>
                                                <td>
                                                        <?php if(isset($product['product_image']) && $product['product_image']!=''){ ?>
                                                        <a href="<?php echo $product['product_url']; ?>"><img src="<?php echo $product['image'] ?>" alt="<?php //echo $product['name'] ?>"></a>
                                                        <?php }else{ ?>
                                                        <img src="<?php echo $product['no_image'] ?>" alt="<?php echo $product['name'] ?>">
                                                        <?php } ?>
                                                </td>
                                           
                                                <?php if($product['type'] == 'buy_gift'){ 
													if(isset($product['wishlist_id']) && $product['wishlist_id']!=0){
												 ?>			
                                                 <td><?= $this->lang->line("buygift") ?> - <a href="<?php echo $product['product_url']; ?>"><?php echo $product['name'] ?></a><br> for Wishlist <a href="<?php echo site_url($product['wishlist_url'])
												 ; ?>"><?php echo $product['wishlist_name']?> </a>
                                                 <?php if($product['qty'] > $product['stock_quantity']){ ?>
                                                 <span style="color:red;">***</span>
												 <?php } ?>
                                                 </td>
                                                 <?php }else { ?>
                                                 <td><a href="<?php echo $product['product_url']; ?>"><?php echo $product['name'] ?><?php if($product['qty'] > $product['stock_quantity']){ ?><span style="color:red;">***</span>
												 <?php } ?></a></td>
                                                 <?php } ?>

												<td>
                                                    <input type="number" name="<?php echo 'cart['. $product['id'] .'][qty]' ?>" value="<?php echo $product['qty'] ?>" class="form-control pquantity" style="width:50px;">
                                                </td>
                                                
                                                <td><?php echo $this->config->item('currency').' '.$product['price'] ?></td>
                                                <td><?php echo $this->config->item('currency').' '.$product['subtotal'] ?></td>
                                                <?php } else if($product['type'] == 'cash_gift'){ ?>
                                             	<td>	
                                                  <?= $this->lang->line("cashgift") ?> - <a href="<?php echo site_url($product['wishlist_url'])
												 ; ?>"><?php echo $product['wishlist_name']?></a>(transaction fee is <?php echo $product['transaction_fees']; ?>)  </td>
												<td></td>
												<td><?php echo $this->config->item('currency').' '. ((isset($product['product_sale_price']) && $product['product_sale_price']!=0) ? $product['product_sale_price'] : 0 )?></td>
                                                <td><?php echo $this->config->item('currency').' '. ((isset($product['price']) && $product['price']!=0) ? $product['price'] : 0) ?></td>
                                                <?php } else{ ?>
                                                <td> <?= $this->lang->line("contribution_text") ?> - <a href="<?php echo $product['product_url']; ?>"><?php echo $product['name'] ?></a> from Wishlist <a href="<?php echo site_url($product['wishlist_url'])
												 ; ?>"><?php echo $product['wishlist_name']?></a>   </td>
												<td></td>
                                                <td><?php echo $this->config->item('currency').' '.$product['product_sale_price'] ?></td>
                                                <td><?php echo $this->config->item('currency').' '.$product['price'] ?></td>
                                                
                                                <?php } ?>
                                                
                                                <td><a href="<?php echo site_url('cart/removeItem/'.$product['rowid']) ?>"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                        	</tr>
                                           
										<?php } ?>
                                      
                                    </tbody>
                                    <tfoot>
                                        <tr id="discountHeader" <?php echo (isset($discount) && $discount!='') ? '' : "style='display:none;'" ?>>
                                            <th colspan="4" bgcolor="#f5f5f5" class="text-right discount_txt"><?= $this->lang->line("discount") ?> <?php echo (isset($coupon_code) && $coupon_code!='') ? "(" .$coupon_code. ")" : '' ?></th>
                                            <th colspan="2" bgcolor="#f5f5f5" id="discountAmt"><?php 											echo (isset($discount) && $discount!='') ? $this->config->item('currency').' '.$discount : '' ?></th>
                                        </tr>
                                        <tr>
                                            <th colspan="4" bgcolor="#99CCCC" class="text-right"><?= $this->lang->line("table_total") ?></th>
                                            <th colspan="2" bgcolor="#99CCCC" id="grossAmt"><?php echo $total ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                             
                            <!-- /.table-responsive -->
                            

 <div class="row box-footer">
 <!--Coupon Box-->
<div class="col-md-4">
<div class="couponMsg" style="color:red;"></div>
<div class=" alert alert-warning">
<?= $this->lang->line("coupon_code") ?> 
<div class="input-group listform">
   <input type="text" class="form-control" name="coupon" id="coupon" value="<?php echo (isset($coupon_code) && $coupon_code!='') ? $coupon_code : '' ?>">
   <span class="input-group-btn">
    <button class="btn btn-primary" type="button" id="couponBtn"><i class="fa fa-gift"></i></button>
      </span>
        </div></div>  
        </div>  <!--EOF Coupon Box-->
 
 <div class="col-md-4 text-right">
 <div class="listform"><button type="submit" class="btn btn-info" id="updateCart"><i class="fa fa-refresh"></i> <?= $this->lang->line("update_basket") ?></button></div>
 </div>                                    

 <div class="col-md-4 text-right">
<!-- <div class="listform"><a href="<?php echo site_url("checkout"); ?>"> <button class="btn btn-warning">Proceed to checkout <i class="fa fa-chevron-right"></i ></button></a></div>-->
 <div class="listform"><a href="<?php echo site_url("checkout"); ?>" class="btn btn-warning"><?= $this->lang->line("proceed_to_checkout") ?> <i class="fa fa-chevron-right"></i ></a></div>
  </div>      
  </div><!--rows-->
 </form>                         

  <?php  }else{ ?>
    <p><?= $this->lang->line("empty_cart_msg") ?></p>
  <?php } ?>                       

                    

</div><!-- /.col-md-12 --></div><!-- /.container --></div></div>

<script type="text/javascript">
	$(document).ready(function(e) {
		
        $("#couponBtn").click(function(e) {
            var coupon = $.trim($("#coupon").val());
			var total_product_price = <?php echo $total_without_discount; ?>;
			if(coupon!=''){
				$.ajax({
					   type: "POST",
					   url: "<?php echo base_url();?>cart/coupon",
					   data: {coupon:coupon,total_product_price:total_product_price},
					   dataType: 'json',
					   success: function(result){ console.log(result);
						   var discountNum = result.discount.match(/\d+/);
						   //console.log(discountNum);
						   if(discountNum!=null){
							  $(".couponMsg").html("");
							   $(".discount_txt").html("Discount ("+coupon+")");
							   $("#discountHeader").show();
							   $("#discountAmt").html(result.discount);
							   $("#grossAmt").html(result.gross_amount);
							   $("#coupon").val(coupon);
						   }else{
							   if($("#discountHeader").is(':visible')){
								  $("#discountHeader").hide();
								}
							   $(".couponMsg").html("Invalid Coupon Code!");
							   $("#grossAmt").html('<?php echo $total ?>');
							   
							}
						   
						}
					});
			}
        });
		
		var discount_coupon = $("#coupon").val();
		if(discount_coupon!=''){
			$("#couponBtn").trigger('click');
		}
		
    });
</script>