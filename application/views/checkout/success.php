<div id="all">

        <div id="content">
            <div class="container white">
            
            
                   <!--Review--><!-- /.EOF Review-->
<!--Payment-->
  <div class="linerows">
  <h3><?= $this->lang->line("thank_you") ?> !!</h3>
  <p><strong><?= $this->lang->line("order_number_info") ?> <span class="text-primary"># <?php echo $order_id; ?></span> </strong></p>
 
   <div class="linerows">
     
     <p><?= $this->lang->line("email_confirm_text") ?></p>
     <h3><?= $this->lang->line("share_glory_gifting") ?></h3>
     <div id="basket"><table class="table table-condensed" >
                                 <thead>
                                     <tr>
                                         <th colspan="2"><?= $this->lang->line("your_order") ?></th>
                                         <th><?= $this->lang->line("price") ?></th>
                                         <th><?= $this->lang->line("table_total") ?></th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                 <?php ?>
                                 	<?php foreach($order as $ord){ ?>
                                     <tr>
                                         <td>
                                         	<?php if($ord->item_type != 'cash_gift'){ ?>
                                             <a href="<?php echo $ord->product_url ?>">
                                                 <img src="<?php echo $ord->image_url ?>" alt="<?php echo $ord->name ?>">
                                             </a>
                                             <?php }else{ ?>
                                             <a href="javascript:void(0)">
                                                 <img src="<?php echo $ord->no_image; ?>">
                                             </a>
                                             
                                             <?php } ?>
                                         </td>
                                         <td><?php echo $ord->item_description ?> <?php echo ($ord->item_type!='cash_gift') ? ('x'.$ord->quantity) : '' ?> <!--<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-facebook-square" aria-hidden="true"></i> Share</button>-->
                                         
                                        <a class="btn btn-primary btn-xs" href="http://www.facebook.com/sharer.php?u=
										<?php echo ($ord->item_type!='cash_gift') ? $ord->product_url : $ord->wishlist_url; ?>" title="Facebook share" target="_blank"><i class="fa fa-facebook"></i><?= $this->lang->line("share") ?></a>
                                          
                                         </td>
                                         <td><?php 
											$price = ($ord->item_type != 'cash_gift') ? ($this->config->item('currency').' '.number_format($ord->price)) : '';
										 echo $price ?></td>
                                         <td>
                                         	<?php if($ord->item_type != 'contributed_cash_gift'){ 
													$amt =  number_format($ord->subtotal);
												}else{
													$amt =  number_format($ord->contribute_amt);
												}
												 echo $this->config->item('currency').' '.$amt
											?>
                                         </td>
                                     </tr>
                                     <?php } ?>
                                 </tbody>
                                 <tfoot>
                                 	 <tr id="discountHeader" <?php echo (isset($ord->discount_amount) && $ord->discount_amount!=0) ? '' : "style='display:none;'" ?>>
                                        <th colspan="3" bgcolor="#f5f5f5" class="text-right"><?= $this->lang->line("discount") ?><?php echo (isset($ord->coupon_code) && $ord->coupon_code!='') ? "(".$ord->coupon_code.")" : '' ?></th>
                                        <th bgcolor="#f5f5f5" id="discountAmt"><?php echo (isset($ord->discount_amount) && $ord->discount_amount!='') ? $this->config->item('currency').' '.$ord->discount_amount : '' ?></th>
                                    </tr>
                                    <?php if($shipping_method == 'onedayservice_jakarta'){?>
                                    <tr id="shippingMethodHeader">
                                        <th colspan="3" bgcolor="#f5f5f5" class="text-right"><?= $this->lang->line("shipping_charge") ?> <?php echo (isset($shipping_charge) && $shipping_charge!='') ? "(".$shipping_charge.")" : '' ?></th>
                                        <th bgcolor="#f5f5f5" id="shippingAmt"><?php echo (isset($shipping_charge) && $shipping_charge!='') ? $shipping_charge : '' ?></th>
                                    </tr>
                                    
                                    <?php }?>
                                     <tr>
                                         <th colspan="3" bgcolor="#99CCCC" class="text-right"><?= $this->lang->line("table_total") ?></th>
                                         <th bgcolor="#99CCCC"><?php echo $this->config->item('currency').' '.number_format($ord->total); ?></th>
                                     </tr>
                                 </tfoot>
                             </table></div>
    
   </div>  
   </div>                                  
 <!-- /eof Payment -->

                           
                           

                            
                        
                    
                    <!-- /.box -->



               

    </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


        <!-- *** FOOTER ***
 _________________________________________________________ -->
        
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
       
        <!-- *** COPYRIGHT END *** -->



</div>