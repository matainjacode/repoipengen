<!DOCTYPE html>
<html lang="<?php if(isset($lang_code)){ echo $lang_code;}else{	echo "en";	}?>" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="iPengen Wishlist">
<meta name="keywords" content="">
<title>
<?php if(isset($page_title)){	echo $page_title;}else{	echo "Ipengen";	}?>
</title>
<meta name="keywords" content="">
<meta property="og:image" content="<?php if(isset($wishlistimg)){ echo $wishlistimg;}else{	echo "";}?>" />
<meta property="og:url" content="<?php if(isset($wishlisturl)){ echo $wishlisturl;}else{	echo "";}?>" />
<meta property="og:title" content="<?php if(isset($wishlisttitle)){ echo $wishlisttitle;}else{	echo "";}?>" />
<meta property="og:description" content="<?php if(isset($wishlistdes)){ echo $wishlistdes;}else{	echo "";}?>" />
<meta property="fb:app_id" content="<?php if(isset($wishlistAppId)){ echo $wishlistAppId;}else{	echo "";}?>" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<!-- styles -->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<link href="<?= base_url();?>assets/frontend/css/font-awesome.css" rel="stylesheet">
<link href="<?= base_url();?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url();?>assets/frontend/css/animate.min.css" rel="stylesheet">
<link href="<?= base_url();?>assets/frontend/css/owl.carousel.css" rel="stylesheet">
<link href="<?= base_url();?>assets/frontend/css/owl.theme.css" rel="stylesheet">
<link href="<?= base_url();?>assets/frontend/css/jqueryformvalidation.css" rel="stylesheet">
<link href="<?= base_url();?>assets/frontend/css/font.css" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
<link href="<?= base_url();?>assets/frontend/css/normalize.css" rel="stylesheet">
<link href="<?= base_url();?>assets/frontend/css/demo.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<!--Datatable CSS-->
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.fancybox.pack.js"></script>

<!-- theme stylesheet -->
<link href="<?= base_url();?>assets/frontend/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="<?= base_url();?>assets/frontend/css/custom.css" rel="stylesheet">
<?php /*?><script src="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>
    <script src="<?= base_url();?>/assets/frontend/cropbox/nanobar.js"></script><?php */?>
<script src="<?= base_url();?>assets/frontend/cropbox/nanobar.js"></script>
<!--Datatable JS-->
<script src="<?= base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script>
       var base_url = "<?php echo base_url(); ?>";
	   var currency = "<?php echo $this->config->item('currency'); ?>";
	   
    </script>
<link rel="shortcut icon" href="<?= base_url();?>/assets/frontend/img/favicon.ico">
<style>
.loader {
	background: rgba(174, 168, 168, 0.36) none repeat scroll 0 0;
	border: 3px solid #1ec2e7;
	height: 100%;
	padding: 20%;
	position: fixed;
	text-align: center;
	width: 100%;
	z-index: 500;
}
.img1loader {
	background: rgba(174, 168, 168, 0.36) none repeat scroll 0 0;
	border: 3px solid #1ec2e7;
	height: 25%;
	left: 40%;
	padding: 8% 15px 28px 13px;
	position: absolute;
	text-align: center;
	top: 28%;
	width: 117px;
	z-index: 500;
}
.forgetpwddiv {
	border: 1px solid #cfcbcb;
	padding: 9px;
}
.bar {
	background: #07effb none repeat scroll 0 0;
}
.ui-widget-content {
	z-index: 9999;
}
.result {
	background-color: #FFF;
	display: block;
	left: 1px;
	position: absolute;
	top: 34px;
	width: 87.1%;
	z-index: 9999;/*padding:10px;*/
}
.resultP {
	position: relative;
}
.dropCl {
	padding: 5px 16px;
	border-bottom: 1px solid #eee;
	color: #000;
}
.dropCl:hover {
	background-color: #ccc;
	color: white;
	padding: 5px 16px;
}
.dropCl a {
	text-decoration: none;
	color: #000;
	display: block;
}
.dropCl a:hover {
	text-decoration: none;
	color: #FFF;
	background-color: #333;
	display: block;
}
.result{
	max-height:400px;
	overflow-y:auto;
}


.langSelect {
    -moz-appearance: none;
	-webkit-appearance:none;
    background: rgba(255, 253, 253, 0) none repeat scroll 0 0;
    border: 0 none !important;
    border-radius: 3px;
	cursor:pointer;
	padding-left: 5px;
    position: relative;
    text-align: left;
    width: 70px;
    z-index: 2;
}
.langSpan{
	width: 65px;
	background-color:#FFFFFF;
	position:relative;
	z-index:0;
}

.langSpan::before {
    background: rgba(0, 0, 0, 0) url("http://staging.ipengen.com/assets/img/dropdown.png") no-repeat scroll center center;
    content: "";
    height: 18px;
    position: absolute;
    right: -17px;
    top: 0;
    width: 63px;
    z-index: 0;
}
</style>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?4JRBtKf2EyhJlaQyeA3naD6iNNihbvWO";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
</head>

<body>

<!-- *** NAVBAR *** 
 _________________________________________________________ -->

<div class="loader" style="display:none;" ><Img src="<?php echo base_url().'assets/img/ipengen-loader.gif'; ?>" style="width: 100px;"/></div>

<!--Modal-->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="Login"><?= $this->lang->line("customer")." ".$this->lang->line("login") ?></h4>
      </div>
      <div class="modal-body">
        <?php
							$datalog = array('name'=>'loginformmodal','id'=>'loginformmodal');
							echo form_open('user/login',$datalog);
                  		  ?>
        <div class="error autherrormodal" style="display:none"><?= $this->lang->line("u_p_invalid") ?></div>
        <div class="form-group">
          <input type="text" class="form-control" name="emailmodal" id="emailmodal" placeholder="<?= $this->lang->line("email") ?>">
        </div>
        <div class="form-group">
          <input type="password" class="form-control" name="passwordmodal" id="passwordmodal" placeholder="<?= $this->lang->line("password") ?>">
        </div>
        <p class="text-center">
          <button type="submit" class="btn btn-primary modallogin"><i class="fa fa-sign-in"></i> <?= $this->lang->line("login") ?></button>
        </p>
        </form>
        <p class="text-center text-muted"><?= $this->lang->line("not_register") ?> </p>
        <p class="text-center text-muted"><a href="#"><strong><?= $this->lang->line("register_now") ?> </strong></a>! <?= $this->lang->line("register_now_info") ?></p>
      </div>
    </div>
  </div>
</div>

<!--Eof Modal--> 

                <!--<div class="">
                    <div class="">-->

<!--Modal-->
<div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="signup" aria-hidden="true">
  <div class="modal-dialog totwidth"> 
    
    <!--<div class="">
                    <div class="">-->
    <div class="container logwhite">
      <div class="col-md-6 ipengenblue hidden-xs hidden-sm">
        <div class="infobox text-center text-white">
          <p>&nbsp;</p>
          <p><img src="<?= base_url();?>/assets/frontend/img/logo-small.png" width="142" height="50"> </p>
          <?php echo $this->lang->line("signup_text"); ?>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><br>
          </p>
          <p>&nbsp;</p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="infobox">
          <div class="listform">
            <h4><?php echo $this->lang->line("already_reagister"); ?></h4>
          </div>
          <div class="row">
            <input type="hidden" id="setcat" value="" />
            <?php
                        $data = array('name'=>'loginform','id'=>'loginform');
                        echo form_open('user/login',$data); ?>
            <div class="col-md-6">
              <div class="error autherror" style="display:none"><?= $this->lang->line("u_p_invalid") ?></div>
              <div class="listform">
                <?php
  					  $data1 = array(
									'name'          => 'logemail',
									'id'            => 'logemail',
									'value'         => '',
                                    'type'          => 'email',
                                    'placeholder'   => $this->lang->line("email") 
									);

						echo form_input($data1); ?>
              </div>
              <div class="listform">
                <?php
     				 $passdata = array(
									'name'          => 'logpass',
									'id'            => 'logpass',
									'value'         => '',
                                    'type'          => 'password',
                                    'placeholder'   => $this->lang->line("password") 
									);

						echo form_input($passdata); ?>
              </div>
              <div class="listform">
                <button type="submit" class="btn btn-primary loginBtn"><?php echo $this->lang->line("login"); ?></button>
                <a href="javascript:void(0)" class="forgetPwdlink"><?php echo $this->lang->line("f_password"); ?></a></div>
            </div>
            <?php echo form_close(); ?>
            <?php /*?><div class="img1loader" style="display:none" ><Img src="<?php echo base_url().'assets/img/ballloder.gif'; ?>" style="width: 50px;"/></div><?php */?>
            <div class="col-md-6">

              <div class="listform">
              		<a href="<?= $login_url ?>" > <img class="loginUrl" src="<?= base_url();?>/assets/frontend/img/ZW4QC.png" width="202" height="46"></a>
              </div>

            </div>
          </div>
          <!--subtitle-->
          <div class="subTitle"> <span class="txt"><i class="fa fa-heart-o" aria-hidden="true"></i></span> <span class="line"></span> </div>
          <div style="display:none;background: rgb(221, 245, 211) none repeat scroll 0% 0%; color: rgb(79, 181, 93); padding: 9px 26px 1px;" class="forgetpwdmsg">
            <p><?= $this->lang->line("email_sent") ?> </p>
          </div>
          <!-------------------------------------Start Forget Password Section-------------------------------------------------->
          <div class="forgetpwddiv" style="display:none">
            <div class="listform">
              <h4><?php echo $this->lang->line("enter_your_mail") ?></h4>
            </div>
            <?php
                        $forgetpwd = array('name'=>'forgetpwdform','id'=>'forgetpwdform');
                        echo form_open('user/porgetpwd',$forgetpwd); ?>
            <div class="listform">
              <?php
	 $fdata = array(
                    'name'          => 'forgetemail',
                    'id'            => 'forgetemail',
                    'value'         => '',
                   	'type'			=> 'email', 
                    'placeholder'   => $this->lang->line("email"), 
					'style'			=> 'width: 58%'
                    );
    
        echo form_input($fdata).''; ?>
            </div>
            <div class="listform">
              <input type="submit" class="btn btn-primary forgetpwdbtn" value="<?php echo $this->lang->line("retrive_password") ?>">
            </div>
            <?php echo form_close(); ?> </div>
          
          <!-------------------------------------End Forget Password Section----------------------------------------------------> 
          <!--eof subtitle-->
          <div class="registrationdiv">
            <div class="listform">
              <h4><?php echo $this->lang->line("register_with_email") ?></h4>
            </div>
            <div class="row">
              <?php
        $data = array('name'=>'registrationform','id'=>'registrationform');
        echo form_open('user/signup',$data);
     ?>
              <div class="col-md-6">
                <div class="listform">
                  <?php
        $data1 = array(
                    'name'          => 'regemail',
                    'id'            => 'regemail',
                    'value'         => '',
                    'type'          => 'email',
                    'placeholder'   => $this->lang->line("email") 
                    );
    
        echo form_input($data1).' <br>';
        ?>
                  <label id="regemail-error12" style="display:none;color:#F20909"><?= $this->lang->line("unique_email") ?></label>
                </div>
                <div class="listform">
                  <?php 
        $passdata = array(
                    'name'          => 'password',
                    'id'            => 'regpassword',
                    'value'         => '',
                    'type'          => 'password',
                    'placeholder'   => $this->lang->line("password") 
                    );
    
        echo form_input($passdata);
    ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="listform">
                  <?php $fdata = array(
                    'name'          => 'fname',
                    'id'            => 'fname',
                    'value'         => '',
                    'placeholder'   => $this->lang->line("f_name") 
                    );
    
        echo form_input($fdata).''; ?>
                </div>
                <div class="listform">
                  <?php $ldata = array(
                    'name'          => 'lname',
                    'id'            => 'lname',
                    'value'         => '',
                    'placeholder'   => $this->lang->line("l_name") 
                    );
    
        echo form_input($ldata); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="listform">
                  <table width="100%" border="0" cellspacing="5" cellpadding="10">
                    <tr>
                      <td colspan="2" align="left" valign="middle"><h4><?php echo $this->lang->line("agrement") ?></h4>
                        <div class="listform"> <?php echo $this->lang->line("agrement_part1") ?> <a href="<?php echo base_url("term-condition"); ?>" target="_blank"><?php echo $this->lang->line("terms_conditions") ?></a> <?php echo $this->lang->line("agrement_part2") ?>.</div></td>
                    </tr>
                    <tr>
                      <td width="50%" align="left" valign="middle"><button type="submit" class="btn btn-primary btn-lg wishlist" dir="wishlist"><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo $this->lang->line("create_wishlist") ?></button>
                        <button type="button" class="btn btn-info btn-lg buygifts" dir="buygifts"><i class="fa fa-gift" aria-hidden="true"></i><?php echo $this->lang->line("buy_gift") ?></button></td>
                  </table>
                </div>
              </div>
              <?php
        echo form_close();
     ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--  </div>
                </div>--> 
  </div>
</div>
<!--Eof Modal-->
<?php if(!$this->input->cookie('login',TRUE) && $this->uri->segment(1) == ""){ ?>
<!--VIDEO BOX-->

<?php /*?><div id="videobox" style="z-index: -99; width: 100%; height: auto" class=" videobox hidden-xs hidden-sm">
  <div id="scroll-btn" class="scroll-nav"> 
    <!--<img src="img/scroll.png" width="149" height="149" class="miniscroll fadeInOut">--> 
    <i class="fa fa-angle-double-down" aria-hidden="true"></i> </div>
  <video id="teaser" autoplay>
    <!--<source src="<?= base_url();?>/assets/frontend/img/video-teaser.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
    <source src="<?= base_url();?>/assets/frontend/img/video-teaser.ogv" type='video/ogg; codecs="theora, vorbis"'>-->
    </object>
  </video>
</div><?php */?>

<!--EOF VIDEO BOX-->
<?php } ?>
<!--<div class="navbar navbar-default yamm" role="navigation" id="navbar" data-spy="affix" data-offset-top="900" data-offset-bottom="200">-->
<div class="navbar navbar-default yamm" role="navigation" id="navbar" data-spy="affix">
  <div class="container">
    <div class="navbar-header"> 
    <a class="navbar-brand home" href="<?= base_url();?>" data-animate-hover="bounce"> 
    <img src="<?= base_url();?>/assets/frontend/img/logo.png" alt="iPengen Logo" class="hidden-xs hidden-sm"> 
    <img src="<?= base_url();?>/assets/frontend/img/logo-small.png" alt="iPengen Logo" class="visible-xs visible-sm"><span class="sr-only">iPengen</span> </a>
      <div class="navbar-buttons">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation"> <span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i> </button>
        
         <a class="navbar-toggle btn-top" role="button" data-target="#" href="<?php echo site_url("cart/view"); ?>"> <i class="fa fa-shopping-cart"></i> </a> 
		 <?php if(!$this->session->userdata('log_in')){ ?>
        <a href="#" data-toggle="modal" data-target="#signup-modal" class="navbar-toggle"> <i class="fa fa-user" aria-hidden="true"></i> </a> 
        <?php }else{ ?>
        <button type="button" class="navbar-toggle btn-top" data-toggle="collapse" data-target="#user"> <span class="sr-only">Toggle navigation</span> <i class="fa fa-user"></i> </button>
        <?php } ?>
        </div>
    </div>
    
    <!--/.navbar-header -->
    <div class="navbar-collapse collapse" id="navigation">
      <ul class="nav navbar-nav navbar-left">
        <li class="dropdown yamm-fw" > <a id="redirctli" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo $this->lang->line("browse_menu"); ?> </a>
          <ul class="dropdown-menu">
            <li>
              <div class="yamm-content">
                <div class="row">
                  <div class="col-md-9">
                    <?php 
									  $lanuagearray="";
									  $language_id="";
									  $language_name="";
									  $lanuagearray=$this->session->userdata('site_lang');
									 	  if(!empty($lanuagearray)){
										  $language_id=$lanuagearray['laguage_id'];
										  $language_name=$lanuagearray['laguage_name'];
									  }else{
										  $language_id=1;
									  }
                                      $this->db->select('ipengen_category.id,ipengen_category.parent_id,ig_category_details.name');
	$this->db->from('ipengen_category');
	$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
	$this->db->where(array('ipengen_category.parent_id'=>0,'ig_category_details.lang'=>$language_id));
	$query=$this->db->get();
  $mresult = $query->result();
  $m=1;
  if(!empty($mresult)){
			 foreach($mresult as $value){
				 if($m==1){
					 echo '<div class="row"><div class="col-sm-4">';
				 }else{
					 echo '<div class="col-sm-4">';
				 }
				 $m++;
				    echo '<h5>'.$value->name.'</h5>';
				  $str2="";
					$stval="";
					$this->db->select('ipengen_category.id,ipengen_category.parent_id,ig_category_details.name');
					$this->db->from('ipengen_category');
					$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
					$this->db->where(array('ipengen_category.parent_id'=>$value->id,'ig_category_details.lang'=>$language_id));
					$query=$this->db->get();
					$result = $query->result();
					if(!empty($result)){
					
					echo '<ul>';
					foreach($result as $value){
					
					//str_replace("world","Peter","Hello world!")
					echo '<li><a href="'.base_url().'search/'.$value->id.'-'.str_replace(' ','-',strtolower($value->name)).'" id="'.$value->id.'">'.$value->name.'</a>';
					
					//$str2.=$stval=submenulest($value->id);
					if($stval=="")
					{
						echo '</li>';
					}
					
					
					}
					echo '</ul></div>';
					}
				
				 
				 if($m==4){
					echo '</div>';
					 $m=1;
				 }
				 
				 
			 }
  }
 
 if($m>1){echo '</div></div>';}
  ?>
                  </div>
                  <div class="col-md-3">
                    <div class="banner"> <a href="#"> <img src="<?= base_url();?>/assets/frontend/img/banner.jpg" class="img img-responsive" alt=""> </a> </div>
                    <div class="banner"> <a href="#"> <img src="<?= base_url();?>/assets/frontend/img/banner2.jpg" class="img img-responsive" alt=""> </a> </div>
                  </div>
                </div>
              </div>
              <!-- /.yamm-content --> 
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <?php if(isset($searchkey) != ""){ $searchVal = $searchkey;}else{$searchVal = "";} ?>
     <?php if($this->session->userdata('log_in')){ 
	 $sessArr = $this->session->userdata('log_in');
	 $sessUserName = isset($sessArr['user_name']) ? $sessArr['user_name'] : '' ;
	 	if(strlen($sessUserName) == 0)
		{
			$query = $this->db->get_where('ig_user',array('id' => $sessArr['user_id']))->row();
			$userName = $query->user_id;
		}
		else
		{$userName = $sessArr['user_name']; }
	 ?>
    <div class="navbar-collapse collapse hidden-md" id="user">
      <ul class="nav navbar-nav navbar-left user">
      <li><a href="<?php echo base_url().'u/'.$userName; ?>"><?php echo $this->lang->line("profile"); ?></a></li>
      
        <li class="dropdown-header"><?php echo $this->lang->line("greeting_text"); ?>, <?php echo strtoupper($this->session->userdata['log_in']['fname']).' '.strtoupper($this->session->userdata['log_in']['lname']); ?></li>
        	<li><a href="<?php echo base_url();?>create-a-wishlist"><?php echo $this->lang->line("create_wishlist"); ?></a></li>
            <li><a href="<?php echo base_url();?>dashboard"><?php echo $this->lang->line("dashboard"); ?></a></li>
            <li><a href="<?php echo base_url();?>my-wishlist"><?php echo $this->lang->line("my_wishlist"); ?></a></li>
            <li><a href="<?= base_url();?>transaction"><?php echo $this->lang->line("transaction"); ?></a></li>
            <li><a href="<?php echo base_url().'u/'.$userName; ?>"><?php echo $this->lang->line("profile"); ?></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo base_url();?>user/logout"><?php echo $this->lang->line("logout"); ?></a></li>
            </ul>
    </div>
  <?php } ?> 
    <div class="hidden-xs topsearch col-md-4 col-sm-3 nopadding resultP">
      <!--<form role="search">-->
		<?= form_open("search-list",array("method"=>"get")); ?>
        <div class="input-group" id="shgrop">
          <input type="text" id="searchwishlist_off" name="searchdata" class="form-control" placeholder="<?php echo $this->lang->line("search_placeholder"); ?>" value="<?php echo $searchVal; ?>" autocomplete="off">
          <!--<div id="result" class="result"></div>-->
          <span class="input-group-btn">
          <button class="btn btn-primary" name="searchbutton" value="search" id="searchBtn"><i class="fa fa-search"></i></button>
          </span> </div>
		<?= form_close(); ?>   
      <!--</form>-->
    </div>
    
    <!--/.nav-collapse -->
    <div class="navbar-buttons">
      <ul class="menu menutop list-inline text-right hidden-xs">
        <?php if($this->session->userdata('log_in')){ 
		
		
		?>
        <li class="dropdown"><a id="UserAccount" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> <span class="caret"></span></a> 
          <!--if user has login-->
          <ul class="dropdown-menu usertop" aria-labelledby="UserAccount">
            <li class="dropdown-header"><?php echo $this->lang->line("greeting_text"); ?>, <?php echo strtoupper($this->session->userdata['log_in']['fname']).' '.strtoupper($this->session->userdata['log_in']['lname']); ?></li>
            <li><a href="<?php echo base_url();?>create-a-wishlist"><?php echo $this->lang->line("create_wishlist"); ?></a></li>
            <li><a href="<?php echo base_url();?>dashboard"><?php echo $this->lang->line("dashboard"); ?></a></li>
            <li><a href="<?php echo base_url();?>my-wishlist"><?php echo $this->lang->line("my_wishlist"); ?></a></li>
            <li><a href="<?= base_url();?>transaction"><?php echo $this->lang->line("transaction"); ?></a></li>
            <li><a href="<?= base_url().'u/'.$userName; ?>"><?php echo $this->lang->line("profile"); ?></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo base_url();?>user/logout"><?php echo $this->lang->line("logout"); ?></a></li>
          </ul>
          <!--eof if user has login--> 
        </li>
        <?php }else {?>
        <a href="#" data-toggle="modal" data-target="#signup-modal"><?php echo $this->lang->line("login-register"); ?></a>
        <?php }?>
        
        <!--Top Mini Cart-->
        <?php $this->load->library('cart'); ?>
        <li class="dropdown"> <a id="dLabel" data-target="#" href="<?php echo site_url("cart/view"); ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-shopping-cart cart"></i>
          <?php
							$cart = $this->cart->contents(); 
							$count = count($this->cart->contents());
							if($count!=0){echo $count;}
						?>
          <span class="caret"></span> </a>
          <?php
						if($count == 0 ){
					?>
          <ul class="dropdown-menu dropdown-menu-right minicart" aria-labelledby="dLabel">
            <div class="text-center"><?php echo $this->lang->line("empty_cart_msg"); ?></div>
            <table class="table table-hover mini-cart">
            </table>
            <div class="total"></div>
          </ul>
          <?php }else{ ?>
          <ul class="dropdown-menu dropdown-menu-right minicart" aria-labelledby="dLabel">
            <table class="table table-hover mini-cart">
              <?php $i=1;
							$photopath = $this->config->item('image_display_path');
							$thumb_size= $this->config->item('thumb_size');
							$no_image = $photopath."product/cash_gift.png";
							foreach ($cart as $item){ 
								$this->db->select('ig_products.product_image'); 
								$this->db->from('ig_products');
								$this->db->where('ig_products.product_id',$item['id']);
								$query = $this->db->get();
								if($query->num_rows() > 0){
									foreach($query->result() as $row){
										$image = $photopath."product/".$item['id'].$thumb_size.$row->product_image;
									}
								}
								
								if($item['wishlist_id'] != 0){
									$this->db->select('*');
									$this->db->from('ig_wishlist');
									$this->db->where('id',$item['wishlist_id']);
									$query = $this->db->get();
									$result = $query->row();
									if(!empty($result))
									{
										$wishlist_name = isset($result->title)? $result->title : "";
										$wishlist_url = isset($result->url) ? '~'.$result->url : "";
									}
								}
							?>
              <?php echo form_hidden($i.'[rowid]', $item['rowid']); ?>
              <tr>
                <?php if($item['type'] == 'buy_gift'){ ?>
                <td><a class="pull-left" href="#"> <img src="<?php echo $image; ?>" width="50" height="67"> </a></td>
                <?php }else{ ?>
                <td><a class="pull-left" href="#"> <img src="<?php echo $no_image; ?>" width="50" height="67"> </a></td>
                <?php } ?>
                <td><p><?php echo $item['qty'] ?><br />
                    <?php if($item['type'] == 'buy_gift'){ 
							 		if(isset($item['wishlist_id']) && $item['wishlist_id']!=0){
							 ?>
                    <?= $this->lang->line("buygift") ?> - <?php echo strtoupper($item['name']) ?><?= $this->lang->line("for_wishlist") ?>  <?php echo strtoupper($wishlist_name) ?>
                    <?php }else{ echo strtoupper($item['name']) ;  }?>
                    <?php }else if($item['type'] == 'cash_gift'){ ?>
                    <?= $this->lang->line("cashgift") ?> - <?php echo strtoupper($wishlist_name) ?>
                    <?php }else{ ?>
                    <?= $this->lang->line("contribution_text") ?> - <?php echo strtoupper($item['name']) ?> <?= $this->lang->line("form_wishlist") ?> <?php echo strtoupper($wishlist_name); ?>
                    <?php } ?>
                  </p>
                  <p><strong><?php echo $this->config->item('currency')." ". $item['price']; ?></strong></p></td>
                <td><button type="button" class="close remvItem" aria-label="Close" data-row-id="<?php echo $item['rowid']; ?>"><span aria-hidden="true">&times;</span></button></td>
              </tr>
              <?php $i++; ?>
              <?php 
							}	
							$grand_total = $this->cart->total();
						 ?>
            </table>
            <div class="total">
              <li role="separator" class="divider"></li>
              <div class="text-right">
                <h5 id="subtotal"><?= $this->lang->line("subtotal") ?> : <?php echo $this->config->item('currency')." ".$grand_total; ?> </h5>
              </div>
              <li role="separator" class="divider"></li>
              <div class="row">
                <div class="col-md-6">
                  <div class="pull-left"><a href="<?php echo site_url("cart/view"); ?>">
                    <button type="button" class="btn btn-info btn-sm"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> <?= $this->lang->line("view") ?> </button>
                    </a></div>
                </div>
                <div class="col-md-6">
                  <div class="pull-right"><a href="<?php echo site_url("checkout"); ?>">
                    <button type="button" class="btn btn-success btn-sm"> <?= $this->lang->line("checkout") ?> <i class="fa fa-caret-right" aria-hidden="true"></i> </button>
                    </a></div>
                </div>
              </div>
            </div>
          </ul>
          <?php } ?>
        </li>
        <!--eof Top Mini Cart-->
        <!--<li><a href="#" class="eyeview"><i class="fa fa-eye" aria-hidden="true"></i><?php //echo $this->lang->line("eyeview"); ?></a></li>-->
        <li>
        <span class="langSpan">
          <select onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;" class="langSelect">
            <option value="english" <?php if($language_name == 'english') echo 'selected="selected"'; ?>>EN</option>
            <option value="bahasa" <?php if($language_name == 'bahasa') echo 'selected="selected"'; ?>>ID</option>
          </select>
        </span>
        </li>
      </ul>
      
      <!--/.nav-collapse --> 
    </div>
    
    <!--mobile search bar-->
    <div id="search" class="hidden-lg hidden-md hidden-sm">
      <?= form_open("search-list",array("method"=>"get")); ?>
        <div class="input-group" id="mobshpro">
          <input type="text" class="form-control" name="searchdata" id="shwislit_bak" placeholder="<?php echo $this->lang->line("search_placeholder"); ?>" value="<?php echo $searchVal; ?>" autocomplete="off">
          <div id="result1" class="result"></div>
          <span class="input-group-btn">
          <button class="btn btn-primary" name="searchbutton" value="search" id="searchBtnlit"><i class="fa fa-search"></i></button>
          </span> </div>
      <?= form_close(); ?>
    </div>
    <!--eof mobile search bar--> 
    
  </div>
  <!-- /.container --> 
</div>
<!-- /#navbar --> 

<!-- /#navbar -->

<div id="all">
<script type="text/javascript">
	  $(document).ready(function(e) {
       		
		$("table.mini-cart").on('click','.remvItem',function() {
            var row_id = $(this).attr('data-row-id'); //alert(row_id);
			if(row_id){
				$.ajax({
					   url: "<?php echo site_url("cart/removeItem/") ?>"+row_id,
					   success: function(result){
						  console.log(result);
						  if(result == 0){
							  setTimeout(function () {
									$('#dLabel').html('<i class="fa fa-shopping-cart cart"></i><span class="caret"></span>');
									$("ul.minicart").html("");
									$("ul.minicart").html("<div class='text-center'> Shopping Cart is empty! </div><table class='table table-hover mini-cart'></table>");
									$.ajax({
											   type: "POST",
											   url: "<?php echo base_url();?>cart/destroy",
											   dataType: 'json',
											   success: function(flag){ console.log(flag);}
											});  
									
								}, 100);
								
						  }
						  else{
							$('#dLabel').html('<i class="fa fa-shopping-cart cart"></i>'+ result +'<span class="caret"></span>');
							$.ajax({
							   type: "POST",
							   url: "<?php echo base_url();?>cart/info",
							   dataType: 'json',
							   success: function(res){ //alert(res);
							    $("ul.minicart > div.text-center").html("");
							    $("table.mini-cart").html("");
								  var total = 0;
								  console.log(res);
								  $.each(res,function(i,k){
									  
									  var html = "<tr><td><a class='pull-left' href='#'> <img src='"+k.image+"' width='50' height='67'> </a></td><td><p>"+k.qty+"<br />"+k.name+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+k.price+"</strong></p></td><td><button type='button' class='close remvItem' aria-label='Close' data-row-id="+k.rowid+"><span aria-hidden='true'>&times;</span></button></td></tr>";
									 
									  $("table.mini-cart").append(html);
									  total = total + k.subtotal;
								  });
								  var htm = "<li role='separator' class='divider'></li><div class='text-right'><h5 id='subtotal'>SUBTOTAL : <?php echo $this->config->item('currency'); ?>"+" "+total+"</h5></div><li role='separator' class='divider'></li><div class='row'><div class='col-md-6'><div class='pull-left'><a href='<?php echo site_url("cart/view"); ?>'><button type='button' class='btn btn-info btn-sm'><i class='fa fa-shopping-cart' aria-hidden='true'></i> VIEW</button></a></div></div><div class='col-md-6'><div class='pull-right'><a href='<?php echo site_url("checkout"); ?>'><button type='button' class='btn btn-success btn-sm'>CHECKOUT <i class='fa fa-caret-right' aria-hidden='true'></i></button></a></div></div> </div>";
								  $(".total").html(htm);
								}
							});  
							}
						}
					});
			}else{
				return false;	
			}
        });
		
	});
	
	</script> 
<script>
/*$(function() {*/
$(document).ready(function() {
	
	var pageURL = $(location). attr("href");	
	var dataString = 'lastUrl='+ pageURL;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>user/storeSessionUrl",
		data: dataString,
		cache: false,
		success: function(html)
		{
			console.log(html);
		}
    });
	
var timer;
	$("#searchwishlist").keyup(function() 
	{ 
	var searchid = $(this).val();
	var dataString = 'search='+ searchid;
	if(searchid!='')
	{
		  clearTimeout(timer);  
		timer =setTimeout(function(){
		$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>site/search",
		data: dataString,
		cache: false,
		success: function(html)
		{
			if(html!=""){
		
		$("#result").html(html).show();
		
			}else{
				 $("#result").html('');
				$('#shgrop').notify("Sorry,  Wishlist or product is not found");
				//alert('Sorry,  Wishlist or product is not found');
			}
		}
		});
		}, 1000);
	}return false;    
	});
	
$(document).on("click","#searchBtn", function(){
	var searchVal = $("#searchwishlist_off").val();
	if(searchVal == "")
	{
		$('#searchwishlist_off').notify("Sorry,  Wishlist Owner Name or Product Name required." , "error");
		return false;
	}
	else
	{return true;}
});
$(document).on("click","#searchBtnlit", function(){
	var shwislit = $("#shwislit_bak").val();
	if(shwislit == "")
	{
		$('#shwislit_bak').notify("Enter Wishlist Owner or Product Name." , "error");
		return false;
	}
	else
	{return true;}
});

$("#shwislit").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
	clearTimeout(timer); 
	timer =setTimeout(function(){
    $.ajax({
    type: "POST",
    url: "<?php echo base_url()?>site/search",
    data: dataString,
    cache: false,
    success: function(html)
    {
		if(html!=""){
	
    $("#result1").html(html).show();
	
	}else{
		$("#result1").html('');
		$('#mobshpro').notify("Sorry,  Wishlist or product is not found");
			//alert('Sorry,  Wishlist or product is not found');
		}
    }
    });
	}, 1000);
}return false;    
});

jQuery("#result").on("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).on("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
/*Language Switcher File Design Change*/
	/*$(".langSelect").wrap("<span class='select-wrapper'></span>");
	$(".langSelect").after("<span class='holder'></span>");
	$(".langSelect").change(function(){
		var selectedOption = $(this).find(":selected").text();
		$(this).next(".holder").text(selectedOption);
	}).trigger('change');*/
/*END*/

});
$('#redirctli').on('click',function(){
	/*window.location=$(this).find('a').attr('href');*/
	window.location='<?php echo site_url('shop')?>';
});

/*$('.loginUrl').on('click',function(){
	var pageURL = $(location). attr("href");	
	var dataString = 'lastUrl='+ pageURL;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>user/storeSessionUrl",
		data: dataString,
		cache: false,
		success: function(html)
		{
			console.log(html);
		}
    });
});*/
</script> 
