<style>
/*.dashboradhref {
    padding: 9px 160px 8px 0;
}
.mywishhref {
    padding: 9px 157px 8px 0;
}
.createwishhref {
    padding: 9px 136px 8px 0;
}
.profilepass {
    padding: 7px 140px 8px 0;
} 
.profileedit {
    padding: 7px 198px 8px 0;
}*/
.menu-content a {
	color: #fff;
}
.menu-content a li {
	padding-left: 16px;
}
.nav-side-menu ul .sub-menu li.active, .nav-side-menu li .sub-menu li.active {
	font-weight: 100;
	font-weight: 100;
	background: rgb(26,76,83);
}
.nav-side-menu ul .sub-menu li.active a, .nav-side-menu li .sub-menu li.active a {
	font-weight: 100;
}
</style>
<?php

 if($this->session->userdata['log_in'] && !empty($this->session->userdata['log_in']['profile_pic']) && is_file($this->config->item('image_path').'userimage/'.$this->session->userdata['log_in']['user_id'].'/'.$this->session->userdata['log_in']['profile_pic'])){
		$imagepath = 'photo/userimage/'.$this->session->userdata['log_in']['user_id'].'/'.$this->session->userdata['log_in']['profile_pic'];
	   }
	
      else{
			$imagepath = 'assets/img/userimage.png';
		  }
		  

 		$this->db->select('wallet_ammount');
		$this->db->from('ig_wallet');
		$this->db->where('receipent_uid',$this->session->userdata['log_in']['user_id']);
		$query=$this->db->get();
        $walletvalue = $query->result();
		if(!empty ($walletvalue))
		{
		foreach($walletvalue as $walletvalue)
		{
			$walletvalue->wallet_ammount;
		}
		}
       $value = (isset($walletvalue->wallet_ammount)) ? $walletvalue->wallet_ammount : 0 ;
	   
	    $this->db->select('count(wallet_id) as new');
		$this->db->from('ig_wallet_transaction');
		$this->db->join('ig_wishlist', 'ig_wishlist.id = ig_wallet_transaction.wishlist_id');
		$this->db->where('ig_wishlist.uid',$this->session->userdata['log_in']['user_id']);
		$this->db->where('ig_wallet_transaction.is_new','yes');
		$query=$this->db->get();
        $count =  $query->result();
			if(!empty ($count))
			{
			foreach($count as $countw)
			{
			$totalcount = $countw->new;
			}
			}
?>
<div class="nav-side-menu">
  <div class="brand">
    <table width="100%" border="0" cellspacing="5" cellpadding="5">
      <tr>
        <td width="85" align="left" valign="middle"><img src="<?php  echo base_url().$imagepath; ?> " width="80" class="img-circle img-responsive userphoto"></td>
        <?php if(isset($this->session->userdata['log_in']['fname']) && isset($this->session->userdata['log_in']['lname'])){ ?>
        <td align="left" valign="middle"><?php echo $this->lang->line("greeting_text"); ?>, <strong><?php echo strtoupper($this->session->userdata['log_in']['fname']).' '.strtoupper($this->session->userdata['log_in']['lname']); ?></strong><?php /*?> <?php echo $this->lang->line("wallet"); ?> (<?php echo $this->config->item('currency'); ?> <?php echo number_format($value,2);?>) <?php */?></td>
        <?php }else{?>
        <td align="left" valign="middle"><?php echo $this->lang->line("greeting_text"); ?>, <strong><!--Put First And Last Name--></strong></td>
        <?php } ?>
      </tr>
    </table>
  </div>
  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  <div class="menu-list">
    <ul id="menu-content" class="menu-content collapse out">
      <a href="<?= base_url();?>dashboard">
      <li class="dashboard"> <i class="fa fa-dashboard fa-lg"></i> <?php echo $this->lang->line("dashboard"); ?> </li>
      </a> 
      <a href="<?= base_url();?>create-a-wishlist" >
      <li onclick="location.href = base_url+'create-a-wishlist';" class="create-a-wishlist"> <i class="fa fa-pencil fa-lg"></i> <?php echo $this->lang->line("create_wishlist"); ?> </li>
      </a> <a href="<?= base_url(); ?>my-wishlist">
      <li class="my-wishlist"> <i class="fa fa-heart fa-lg"></i> <?php echo $this->lang->line("my_wishlist"); ?> </li>
      </a> <a href="<?= base_url(); ?>reminder">
      <li class="reminder"> <i class="fa fa-calendar fa-lg"></i><?php echo $this->lang->line("event_reminder"); ?> </li>
      </a> <a href="<?= base_url(); ?>surprise">
      <li  class="surprise"> <i class="fa fa-gift fa-lg"></i> <?php echo $this->lang->line("surprise_box"); ?> <span class="label label-danger"><?php echo $this->lang->line("new"); ?></span> </li>
      </a> <a href="javascript:void(0)">
      <li  data-toggle="collapse" data-target="#cashgift" class="collapsed cashgift"> <i class="fa fa-money fa-lg"></i> <?php echo $this->lang->line("cash_gift"); ?>
        <?php if (isset ($totalcount) && $totalcount>0){?>
        <span class="label label-danger"><?php echo $this->lang->line("new"); ?></span>
        <?php } ?>
        <span class="arrow"></span> </li>
      </a>
      <ul class="sub-menu collapse giftcash" id="cashgift">
        <a href="<?= base_url(); ?>my-cash-gift-list" class="">
        <li class=""><?php echo $this->lang->line("cash_gift_receive"); ?></li>
        </a> <a href="<?= base_url();?>bank-account" class="">
        <li><?php echo $this->lang->line("bank_account_details"); ?></li>
        </a>
      </ul>
      <!--<a href="<?= base_url();?>recipient">
      <li class="recipient"> <i class="fa fa-users fa-lg"></i> <?php //echo $this->lang->line("recipient_list"); ?> </li>
      </a>--> 
      <a href="<?= base_url();?>transaction">
      <li class="transaction"> <i class="fa fa-list fa-lg"></i><?php echo $this->lang->line("transaction"); ?> / <?php echo $this->lang->line("order"); ?> </li>
      </a> 
      <!--<li>
                  <a href="#">
                  <i class="fa fa-chain fa-lg"></i> <?php //echo $this->lang->line("contribution"); ?></a>
                </li>
-->
      
      <li data-toggle="collapse" data-target="#profile" class="collapsed myprofile user"> 
      <a href="javascript:void(0)"><i class="fa fa-user fa-lg"></i> <?php echo $this->lang->line("profile_text"); ?> <span class="arrow"></span></a> 
      </li>
      <ul class="sub-menu collapse myprofileul " id="profile">
        <a href="<?php echo base_url();?>user/profile" class="">
        <li><?php echo $this->lang->line("view"); ?>/<?php echo $this->lang->line("edit"); ?></li>
        </a> <a href="<?php echo base_url();?>user/passwordsetting" class="">
        <li><?php echo $this->lang->line("pwd_change"); ?></li>
        </a>
      </ul>
      <a href="<?php echo base_url();?>user/logout">
      <li> <i class="fa fa-sign-out fa-lg"></i> <?php echo $this->lang->line("sign_out"); ?> </li>
      </a>
    </ul>
  </div>
</div>
<script type="text/javascript">

/*if(window.location.href == base_url+"wishlist/mywishlist"){
  $(".mywish").addClass("active");
}
if(window.location.href == base_url+"wishlist/create"){
  $(".createwish").addClass("active");
}
if(window.location.href == base_url+"user/passwordsetting"){
  $(".profilepasshref").addClass("active");
  $(".myprofile").removeClass("collapsed");
  $(".myprofileul").addClass("in");
}
if(window.location.href == base_url+"user/profile"){
  $(".profileedithref").addClass("active");
  $(".myprofile").removeClass("collapsed");
  $(".myprofileul").addClass("in");
}*/
$(document).ready(function(){
	var pgurl = window.location.href.substr(window.location.href.indexOf("/")).split("/");
	var urlClass = pgurl[3];
	$(document).find("."+urlClass).addClass("active");
	if(urlClass == "my-cash-gift-list" || urlClass == "bank-account")
	{
		$(document).find(".cashgift").addClass("active").removeClass("collapsed");
		$(".giftcash").addClass("in");
	}
	if(urlClass == "user")
	{
		$(document).find(".user").addClass("active").removeClass("collapsed");
		$(".myprofileul").addClass("in");
	}
});
/*if(window.location.href == base_url+"user/passwordsetting"){
  $(".profilepasshref").addClass("active");
  $(".myprofile").removeClass("collapsed");
  $(".myprofileul").addClass("in");
}
if(window.location.href == base_url+"user/profile"){
  $(".profileedithref").addClass("active");
  $(".myprofile").removeClass("collapsed");
  $(".myprofileul").addClass("in");
}
if(window.location.href == base_url+"gift"){
  $(".profilepasshref").addClass("active");
  $(".cashgift").removeClass("collapsed");
  $(".giftcash").addClass("in");
}
if(window.location.href == base_url+"bank-account"){
  $(".profileedithref").addClass("active");
  $(".cashgift").removeClass("collapsed");
  $(".giftcash").addClass("in");
}*/
function mywishlist(){
				window.location.href = base_url+"my-wishlist";
	}

</script> 
