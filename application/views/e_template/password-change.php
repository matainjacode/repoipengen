<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Password Change</title>
<style>
@charset "utf-8";
/* CSS Document */

body{ font-family:Roboto; font-size:12px; color:#333; padding:0px; margin:0px;}

.wrapper{ width:870px; margin:auto;}
.mainContenerBg{ background-color:#F9F9F9; padding:15px;}
.logoBg{ padding:5px; text-align:center;}
.dearText{ font-size:15px; font-weight:bold; color:#333;}
.contText{ font-size:13px; color:#666; line-height:22px; text-indent:50px;}
.contText2{ font-size:13px; color:#333; line-height:22px; font-weight:normal;}
.container{ padding:0px; margin:0px;}
.contWBg{ background-color:#FFF; padding:20px;}
.yoursSincerely{ font-size:13px; font-weight:bold; color:#666; letter-spacing:1px;}
.yoursSincerelyName{ font-size:13px; color:#333;}

.headingText{ font-size:22px; color:#135D02; font-weight:bold; padding-bottom:5px;}
.providerBlock{ margin-top:10px;}

.jobTitel{ font-size:18px; color:#135D02; font-weight:bold; border-bottom:1px solid #E1E1E1; padding-bottom:5px;}
.jobTitel a{ font-size:18px; color:#135D02; font-weight:bold; text-decoration:none;}
.jobTitel a:hover{ color:#26A40B;}


.proBoxLeft{ width:44%; float:left; padding:0px 20px 15px 20px; background-color:#FFF; border-right:1px solid #EDEDED; min-height:200px; margin-top:15px;}
.proBoxRight{ width:44%; float:right; padding:0px 20px 15px 20px; background-color:#FFF; min-height:200px; margin-top:15px;}

.headingText2{ font-size:16px; color:#135D02; font-weight:bold; padding-bottom:5px;}

.proSmallBoxL{ width:30%; float:left;}
.proSmallBoxR{ width:70%; float:right;}

.smallHText{ font-size:14px; font-weight:bold; color:#333;}
.smallText{ font-size:14px; color:#666;}
.smallText a{ font-size:14px; color:#666; text-decoration:underline;}
.smallText a:hover{ color:#135D02; text-decoration:none;}

.marTop10{ margin-top:10px;}

.button{ background-color:#135D02; border-radius:5px; padding:10px 15px; color:#FFF; font-size:15px; border:0px; cursor:pointer; font-weight:bold;}
.button:hover{ background-color:#178200;}

.checkMail{ font-size:15px; color:#135D02;}
.contBox{ width:96%; padding:2%; background-color:#FFF;}

.emailConfBoxL{ width:30%; float:left;}
.emailConfBoxR{ width:70%; float:left;}

.elitedutyT{ font-size:13px; color:#135D02; font-weight:bold; letter-spacing:1px;}

.date{ font-size:14px; font-weight:bold; color:#333;}
.date2{ font-size:14px; font-weight:bold; color:#135D02; font-style:italic;}

.userName{ font-size:16px; color:#333333; font-weight:bold;}





.floatLeft{ float:left;}
.floatRight{ float:right;}
.clearBoth{ clear:both;}
</style>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
</head>
<body>
<div class="wrapper">
  <div class="mainContenerBg">
    <div class="logoBg" align="center"><a href="#"><img src="<?php echo $imgURL ?>" width="231" height="69" alt="iPengen Logo"></a></div>
    <div class="headingText"><?= $this->lang->line("password_change") ?></div>
    <div class="container contWBg">
      <p class="dearText"><?= $this->lang->line("dear") ?>, <?php echo $username; ?> </p>
      <p class="checkMail"><?= $this->lang->line("pwd_reset_info") ?></p>
      <p class="contText2"> <?= $this->lang->line("pwd_reset_info_1") ?> <a href="<?php echo $link; ?>" target="_blank"><?= $this->lang->line("link")?></a>. </p>
      <p class="contText2"><strong><?= $this->lang->line("or") ?> </strong> <?= $this->lang->line("copy") ?> : <a style="text-decoration:none"><?php echo $link; ?></a> </p>
      <p class="contText2"><strong> <?= $this->lang->line("info") ?>:</strong><?= $this->lang->line("pwd_lenth_info") ?> </p>
      <p class="checkMail"><strong> <?= $this->lang->line("note") ?>:</strong> <?= $this->lang->line("pwd_reset_info_2") ?></p>
    </div>
    <div class="yoursSincerely">Yours Sincerely</div>
    <div class="elitedutyT">iPengen Team</div>
  </div>
</div>
</body>
</html>
