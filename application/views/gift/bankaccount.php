<div id="content">
  <div class="container white">
    <div class="row"> 
      <!--Side Bar-->
      <div class="col-md-3 nopadding">
        <?php $this->load->view('common/leftsidebar')?>
        
        <!--Eof original sidebar--> 
      </div>
      <!-- Right Column-->
      <div class="col-md-9">
        <div class="dashboard-header"><?php echo $this->lang->line("cash_gift_bank_title"); ?></div>
        <div class="allrows">
          <p class="text-muted"><?php echo $this->lang->line("cash_gift_bank_info_1"); ?> <a href="<?php echo base_url("contact-us"); ?>"><?php echo $this->lang->line("contact_us"); ?></a>.</p>
          <hr>
          <?php    $data = array('name'=>'bank-account','id'=>'bank-account','method'=>'post');
           echo form_open('bank-account',$data);					
                   	 ?>
          <div class="listform">
            <?php  echo form_input ('account_holder_name',set_value('account_holder_name'),'placeholder="'.$this->lang->line("cash_gift_bank_AC").'" class="form-control" id="account-holder-name" required="required"'); ?>
          </div>
          <div class="listform">
            <?php  echo form_input ('account_no',set_value('account_no'),'placeholder="'.$this->lang->line("cash_gift_bank_AC_no").'" class="form-control" id="account-no" required="required"'); ?>
          </div>
          <div class="listform">
            <?php  echo form_input ('bank_name',set_value('bank_name'),'placeholder="'.$this->lang->line("cash_gift_bank_AC_name").'" class="form-control" id="bank-name" required="required"'); ?>
          </div>
          <div class="listForm">
            <?php
$data = array(
      'name'        => 'bank_details',
	  'class'       => 'form-control',
      'id'          => 'bank_details',
      'rows'        => '4',
      'cols'        => '',
	  'maxlength'   => '250',
	  'placeholder'=>$this->lang->line("cash_gift_bank_AC_detail"),
	  'required'=>'required'
    );
  echo form_textarea($data);
  ?>
            <span class="text-danger"><em><?php echo $this->lang->line("cash_gift_bank_info_2"); ?> </em> </span></div>
          <button type="submit" class="btn btn-primary btn-info-full next-step"><?php echo $this->lang->line("cash_gift_save_btn"); ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
          <?= form_close(); ?>
          <!--EOF if no bank detail set yet-->
          
          <p></p>
          
          <!--BANK DETAILS ALREADY SET--> 
          
        </div>
        <!--eof allrows--> 
      </div>
      <!--eof infobox for Dashboard--> 
      
    </div>
    <!--Eof Right Column--> 
  </div>
</div>
