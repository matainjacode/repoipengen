<div id="content">
            <div class="container white">
            <div class="row">
<!--Side Bar-->  <div class="col-md-3 nopadding">
<?php $this->load->view('common/leftsidebar')?>

<!--Eof original sidebar-->
</div><!-- Right Column-->
<div class="col-md-9">
<div class="dashboard-header"><?= $this->lang->line("cash_gift_bank_title") ?></div>
<div class="allrows">
 
 <hr>
<p></p>
     
<?php if($this->session->flashdata('successmessage')){ ?>
<div class="alert alert-success msg" style="margin-top:10px;"><?php echo $this->session->flashdata('successmessage'); ?></div>
<?php }else if($this->session->flashdata('warningmessage')){ ?>
<div class="alert alert-danger msg" style="margin-top:10px;"><?php echo $this->session->flashdata('warningmessage'); ?></div>

 <?php } ?>       
<!--BANK DETAILS ALREADY SET-->
 <?php    $data = array('name'=>'edit-bank-account','id'=>'edit-bank-account','method'=>'post');
           echo form_open('gift/edit_bank_account',$data);					
                   	 ?>
<div class="listform">
<label for="Account_Name"> <?= $this->lang->line("cash_gift_bank_AC") ?></label>
<div class="input-group">
 <?php  echo form_input ('account_holder_name',$result->account_holder_name,'placeholder="'.$this->lang->line("cash_gift_bank_AC").'" disabled="disabled" class="form-control" id="account-name" required="required"'); ?>
  <span class="input-group-btn">
   <button type="button" class="btn btn-link edit-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
   </span>                
</div></div>


<div class="listform">
 <label for="Account_No"><?= $this->lang->line("account") ?> #</label>
<div class="input-group">
 <?php  echo form_input ('account_no',$result->bank_account_no,'placeholder="'.$this->lang->line("bank_ac_no").'" class="form-control" id="account-no" disabled="disabled" required="required" '); ?>
  <span class="input-group-btn">
   <button type="button" class="btn btn-link edit-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
   </span>                
</div></div>


<div class="listform">
 <label for="Bank"><?= $this->lang->line("bank") ?></label>
<div class="input-group">
  <?php  echo form_input ('bank_name',$result->bank_name,'placeholder="'.$this->lang->line("cash_gift_bank_AC_name").'" class="form-control" id="bank-name" disabled="disabled" required="required"'); ?>
  <span class="input-group-btn">
   <button type="button" class="btn btn-link edit-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
   </span>                
</div></div>


<div class="listform">
<label for="Bank_details"><?= $this->lang->line("cash_gift_bank_AC_detail") ?></label>
<div class="input-group">
<?php
$data = array(
      'name'        => 'bank_details',
	  'class'       => 'form-control',
      'id'          => 'bank_details',
      'rows'        => '4',
      'cols'        => '',
	  'maxlength'   => '250',
	  'placeholder'=>'Bank Details',
	  'value'=>$result->bank_details,
	  'disabled'=>'disabled',
	  'required'=>'required'
    );
  echo form_textarea($data);
  ?>
  <span class="input-group-btn">
   <button type="button" class="btn btn-link edit-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
   </span>                
</div></div>
<button type="submit" class="btn btn-primary btn-info-full next-step">Update<i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
<?= form_close(); ?>


<!--BANK DETAILS ALREADY SET-->

</div><!--eof allrows-->
</div><!--eof infobox for Dashboard-->


</div><!--Eof Right Column-->
</div>
            
            </div>
            <script>
			$(document).ready(function(){
				$('.msg').delay(5000).fadeOut('slow');
				$('.edit-link').on('click',function(){
					$(this).parent().parent().find(':disabled').prop( "disabled", false );
				});
				
				});
			</script>