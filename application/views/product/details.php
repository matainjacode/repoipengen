<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<?php
	$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	$url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	
	
	
?>
<div id="all">
  <div id="content">
    <div class="container white">
      <div class="col-md-12"> 
        
        <!--<h4>LADIES</h4>-->
        <hr>
      </div>
      <div class="col-md-3 hidden-xs hidden-sm hidden-md"> 
        <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ --> 
        <!--Side Navbar-->
        <div class="sidemenu">
          <div class="sidemenutitle"><?php echo $this->lang->line("categories"); ?></div>
          <div class="sidemenulist" id="sidemenuid"> <?php print_r($html_category);?> </div>
          
          <!--<div class="sidemenulist">
                      <ul>
                        <li><a href="category.html">MEN42</a>
                          <ul>
                            <li><a href="category.html">T-shirts</a></li>
                            <li><a href="category.html">Shirts</a></li>
                            <li><a href="category.html">Pants</a></li>
                            <li><a href="category.html">Accessories</a></li>
                          </ul>
                        </li>
                        <li><a href="category.html">LADIES123</a>
                          <ul>
                            <li><a href="category.html">T-shirts</a></li>
                            <li><a href="category.html">Skirts</a></li>
                            <li><a href="category.html">Pants</a></li>
                            <li><a href="category.html">Accessories</a></li>
                          </ul>
                        </li>
                        <li><a href="category.html">KIDS11</a>
                          <ul>
                            <li><a href="category.html">T-shirts</a></li>
                            <li><a href="category.html">Skirts</a></li>
                            <li><a href="category.html">Pants</a></li>
                            <li><a href="category.html">Accessories</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div>--> 
        </div>
        <!--eof side navebar-->
        <div class="sidemenu">
          <div class="sidemenutitle"><?php echo $this->lang->line("brands"); ?> <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> <?php echo $this->lang->line("clear"); ?></a></div>
          <div class="sidemenulist" id="brandserchid">
            <div class="form-group brandsclass">
              <?php
                     
                   if(!empty($result)){
                           foreach($result as $val){
							   ?>
              <div class="checkbox">
                <label>
                  <input value="<?php echo $val->brand_id;?>" name="brand[]" type="checkbox">
                  <?php echo $val->brand_name;?> </label>
              </div>
              <?php
                               
                           }
                           }
                      
                       ?>
            </div>
            <button class="btn btn-default btn-sm btn-primary" id="bandseach"><i class="fa fa-pencil"></i> <?php echo $this->lang->line("apply"); ?></button>
          </div>
        </div>
        <div class="sidemenu">
          <div class="sidemenutitle"><?php echo $this->lang->line("price_range"); ?> <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> <?php echo $this->lang->line("clear"); ?></a></div>
          <div class="sidemenulist">
            <div class="col-sm-2 col-xs-12 pull-left" style="margin-top:10px; padding-left:0px;">
              <p style="color:#135b02;font-size:12px;"><span id="amount111" style="background:#eee;border:solid 1px #ddd;padding:2px 2px; margin-right:3px;border-radius:5px;">1</span></p>
            </div>
            <div class="col-sm-8 col-xs-12" style="padding:0px;margin-top:12px;"> 
              
              <!--<p id="amount"></p>-->
              
              <div id="slider-range"></div>
              <input type="hidden" id="amount1" name="job_budget1" value="10">
              <input type="hidden" id="amount2" name="job_budget" value="30">
            </div>
            <div class="col-sm-2 col-xs-12 pull-right text-right" style="margin-top:10px;">
              <p style="color:#135b02;font-size:12px;"> <span id="amount222" style="background:#eee;border:solid 1px #ddd;padding:2px 2px; margin-left:0px;border-radius:5px;">1</span></p>
            </div>
          </div>
        </div>
        
        <!-- *** MENUS AND FILTERS END *** -->
        
        <div class="banner"> <a href="#"> <img src="<?php echo base_url();?>assets/frontend/img/banner.jpg" alt="sales 2014" class="img-responsive"> </a> </div>
      </div>
      <div class="col-md-9">
        <div class="detailproducttitle"><?php echo $Details[0]->product_name;?></div>
        <div class="row" id="productMain">
          <div class="col-sm-6">
            <div id="mainImage">
              <?php 
							
							
							 
							
							if(!empty($productIMG)){
								foreach($productIMG as $imgpath){
									
							
							 ?>
              <a class="image-link" id='mainImg' href="javascript:void(0)<?php  //echo $originalpath.$imgpath;?>" data-toggle="modal" data-target="#imagelightbox"> <img src="<?php  echo $medpath.$imgpath;?>" alt="" class="img-responsive" > </a>
              <?php
							break;
							
							 } 
							}
							
							else
							{
								?>
              <img src="<?php  echo $no_image_path;?>" alt="" class="img-responsive">
              <?php
							}
							?>
              <?php 
							//if(!empty($productIMG)){

									
							
							 ?>
            </div>
            <div class="ribbon sale">
              <?php if (isset($Details[0]->sale_price) && ($Details[0]->sale_price < $Details[0]->price)){?>
              <div class="theribbon"><?php echo $this->lang->line("sale"); ?></div>
              <?php } ?>
              <div class="ribbon-background"></div>
            </div>
            <!-- /.ribbon -->
            
            <div class="ribbon new">
              <div class="theribbon"><?php echo $this->lang->line("new"); ?></div>
              <div class="ribbon-background"></div>
            </div>
            <!-- /.ribbon --> 
            <!--Img Thumbnails-->
            
            <div class="row maindetailthumbs" id="thumbs">
              <div id="main">
                <ul class="gallery clearfix">
                  <?php 
							
							if(!$no_image)
							{
							if(!empty($productIMG)){
								foreach($productIMG as $imgpath){
									
							 ?>
                  <div class="col-xs-4"> <a href="<?php echo $medpath.$imgpath;?>" class="thumb "> <img src="<?php  echo $thumpath.$imgpath;?>" alt="" class="img-responsive"> </a> </div>
                  <?php /*?><li class="prodIMMg"><a href="<?php echo $medpath.$imgpath;?>" rel="prettyPhoto[gallery1]" title=""><img src="<?php echo $medpath.$imgpath;?>" /></a></li><?php */?>
                  <?php } 
							}
							}
							?>
                </ul>
              </div>
            </div>
            <!--EOF Img Thumbnails-->
            <?php /*?><img src="<?php  echo $medpath.$productIMG[0];?>" width="387" height="85" usemap="#image_map" border="0">

<map name="image_map">
<?php 	foreach($productIMG as $imgpath){?>
<area shape="rect" coords="6,11,72,73" href="<?php  echo $medpath.$imgpath;?>" rel="prettyPhoto[image_map]">
<?php }} ?>

</map> 
           <?php */?>
          </div>
          <div class="col-sm-6">
            <div class="box"> 
              <!--social sharing-->
              <div class="social">
                <h4><?php echo $this->lang->line("share_it") ?> :</h4>
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style"> <a  class="a2a_dd" href="https://www.addtoany.com/share"></a> <a href="http://www.facebook.com/sharer.php?u=<?php echo $url;?>" title="Facebook share" class="a2a_button_facebook"></a> <a href="http://twitter.com/share?url=<?php echo $url;?>&amp;text=Share popup on &amp;hashtags=ipengen" title="Twitter share" class="a2a_button_twitter"></a> <a href="mailto:?subject=Ipengen Wishlist&amp;body=Check out this site <?php echo $url;?>." title="Share by Email" class="a2a_button_google_plus"></a> </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
                <p>
                  <?php /*?><a class="external facebook customer share" href="http://www.facebook.com/sharer.php?u=<?php echo $url;?>" title="Facebook share" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a class="external twitter customer share" href="http://twitter.com/share?url=<?php echo $url;?>&amp;text=Share popup on &amp;hashtags=ipengen" title="Twitter share" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a class="external gplus google_plus customer share" href="https://plus.google.com/share?url=<?php echo $url;?>" title="Google Plus Share" target="_blank"><i class="fa fa-google-plus"></i></a>
                                <a href="mailto:?subject=Ipengen Wishlist&amp;body=Check out this site <?php echo $url;?>." title="Share by Email"><i class="fa fa-envelope"></i></a><?php */?>
                  <!--<a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                                    <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>--> 
                </p>
              </div>
              <?php $dt= date('Y-m-d');
									 $dt1=date_create($Details[0]->sale_start_date);
									 $dt1=date_format($dt1,"Y-m-d");
									 $dt2=date_create($Details[0]->sale_end_date);
									 $dt2=date_format($dt2,"Y-m-d");
										if(($dt<=$dt2)||($dt<=$dt1)){
										$exp=1;
										}else{
										$exp=0;
										}
 								?>
              <!--Eof Social Sharing-->
              <div class="detailproducttitle"><?php echo $Details[0]->product_name;?></div>
              <p class="price"><?php echo $this->config->item('currency'); ?>
                <?php if($exp==1){
							    echo number_format($Details[0]->sale_price);
							   }else{
								    echo number_format($Details[0]->price);
							   }?>
              </p>
              <p class="goToDescription"><a href="#details" class="scroll-to"><?php echo $this->lang->line("goDescription") ?></a>
              
              <hr>
              <!--If user is not login or registered , please hide "ADD TO YOUR WISHLIST" and ADD TO YOUR WISHLIST" Section. Add a Register or Login Button : <a href="#"data-toggle="modal" class="btn btn-info" data-target="#login-modal">Login | Register</a>
                             
                             --HIDE THIS SECTION IF USER IS NOT REGISTERED / LOGIN----> 
              
              <i class="fa fa-heart" aria-hidden="true"></i> <?php echo $this->lang->line("add_wishlist") ?><span id="j-wishlist-num" class="wishlist-num"> (<?php echo $wishlistCount;?> Adds)</span>
              <?php if(!empty($loginUserArray['user_id'])){
								
								 ?>
              <h5><?php echo $this->lang->line("add_to_your_wishlist") ?> </h5>
              <div class="listform">
                <select name="Wishlist" id="Wishlist" class="form-control">
                  <option selected value=''><?php echo $this->lang->line("select_Your_Wishlist") ?></option>
                  <?php 
									if(!empty($myWishlist)){
                                    	foreach($myWishlist as $val){
											echo "<option value='".$val->id."'>" .$val->title. "</option>";	
										}
									}
										
									?>
                </select>
                <!--<a href="basket.html" class="btn btn-info" id='addPrdctToWshlst'><i class="fa fa-heart"></i> Add to wishlist</a>-->
                <button class="btn btn-info" id='addPrdctToWshlst'><i class="fa fa-heart"></i> <?php echo $this->lang->line("add_wishlist") ?></button>
                <h5 id='notificationDiv'></h5>
              </div>
              <hr>
              <h5><?php echo $this->lang->line("or_buy") ?></h5>
              <?php /*?><div class="listform">
                                <select name="recipientList" id="recipientList" class="form-control">
                                    <option selected>Select Your Wishlist</option>
                                     <?php 
									 if(!empty($recipientWishlist)){
                                    	foreach($recipientWishlist as $val){
											echo "<option value='".$val[0]->wid."'>" .ucfirst($val[0]->fname).'-'.$val[0]->title. "</option>";	
										}
									 }
									?>
                                </select>
                                </div><?php */?>
              <div class="form-inline">
                <div class="form-group">
                  <label class="sr-only" for="quantity"><?php echo $this->lang->line("quantity") ?></label>
                  <div class="input-group">
                    <div class="input-group-addon"><?php echo $this->lang->line("qty") ?></div>
                    <select class="form-control" name="quantity" id="quantity">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                  <button class="btn btn-primary" id="button_buy_gift" data-product-id="<?php echo $Details[0]->product_id ?>" data-product-name="<?php echo $Details[0]->product_name  ?>" data-product-stock="<?php echo $Details[0]->stock_quantity ?>" ><i class="fa fa-shopping-cart"></i> <?php echo $this->lang->line("buy_gift"); ?></button>
                  <h5 id="msgBox"></h5>
                </div>
              </div>
              <h5 id="cartMsgDiv"></h5>
              <?php //}else{
	 ?>
              <?php /*?><div>
     <button class="btn btn-info" ><?php echo $this->lang->line("comming_Soon") ?>...</button>
     </div><?php */?>
              <?php
                                   //  }
 
 
                                 } else{ ?>
              <a data-target="#signup-modal" class="btn btn-info" data-toggle="modal" href="#"><?php echo $this->lang->line("login-register") ?></a>
              <?php }?>
              <!---- EOF HIDE THIS SECTION IF USER IS NOT REGISTERED / LOGIN---->
              </p>
            </div>
          </div>
        </div>
        <div class="box" id="details"> <?php echo $Details[0]->message;?> 
          <!--<p>
                            <h4>Product details</h4>
                            <p>White lace top, woven, has a round neck, short sleeves, has knitted lining attached</p>
                            <h4>Material & care</h4>
                            <ul>
                                <li>Polyester</li>
                                <li>Machine wash</li>
                            </ul>
                            <h4>Size & Fit</h4>
                            <ul>
                                <li>Regular fit</li>
                                <li>The model (height 5'8" and chest 33") is wearing a size S</li>
                            </ul>

                            <hr>
                            <div class="social">
                                <h4>Share it to your friends</h4>
                                <p>
                                <a class="external facebook customer share" href="http://www.facebook.com/sharer.php?u=<?php echo $url;?>" title="Facebook share" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a class="external twitter customer share" href="http://twitter.com/share?url=<?php echo $url;?>&amp;text=Share popup on &amp;hashtags=ipengen" title="Twitter share" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a class="external gplus google_plus customer share" href="https://plus.google.com/share?url=<?php echo $url;?>" title="Google Plus Share" target="_blank"><i class="fa fa-google-plus"></i></a>
                                <a class="external pinterest customer share" href="http://pinterest.com/pin/create/button/?url=<?php echo $url;?>&description=[title]" title="Pinterest Share" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                <a href="mailto:?subject=Ipengen Wishlist&amp;body=Check out this site <?php echo $url;?>." class="email" title="Share by Email"><i class="fa fa-envelope"></i></a>
                                   
                                </p>
                            </div>--> 
          
        </div>
        <div class="row same-height-row">
          <div class="col-md-3 col-sm-6">
            <div class="box same-height">
              <h3><?php echo $this->lang->line("product_like"); ?></h3>
            </div>
          </div>
          <?php if(!empty($likethiscategoryProduct)){
						   foreach($likethiscategoryProduct as $val){
								 ?>
          <div class="col-md-3 col-sm-6">
            <div class="product same-height">
              <div class="flip-container">
                <div class="flipper">
                  <div class="front"> <a href="<?php echo $val['product_url']; ?>"> <img src="<?php echo $val['imgurl'];?>" alt="" class="img-responsive"> </a> </div>
                  <div class="back"> <a href="<?php echo $val['product_url']; ?>"> <img src="<?php echo $val['imgurl'];?>" alt="" class="img-responsive"> </a> </div>
                </div>
              </div>
              <a href="<?php echo $val['product_url']; ?>" class="invisible"> <img src="<?php echo $val['imgurl'];?>" alt="" class="img-responsive"> </a>
              <div class="text">
                <h3><a href="<?php echo $val['product_url']; ?>"><?php echo $val['product_name'];?></a></h3>
                <p class="price"><?php echo $this->config->item('currency'); ?> <?php echo 
									number_format($val['sale_price']);?></p>
              </div>
            </div>
            <!-- /.product --> 
          </div>
          <?php }}else{
						  ?>
          <div class="col-md-3 col-sm-6">
            <div class="box same-height">
              <h3><?php echo $this->lang->line("no_product"); ?></h3>
            </div>
          </div>
          <?php
					  }?>
        </div>
        <div class="row same-height-row">
          <div class="col-md-3 col-sm-6">
            <div class="box same-height newheight">
              <h3><?php echo $this->lang->line("recent_product"); ?></h3>
            </div>
          </div>
          <?php if (!empty ($productViewRecently)){?>
          <?php foreach ($productViewRecently as $recentview){
						
							?>
          <div class="col-md-3 col-sm-6">
            <div class="product same-height newheight">
              <div class="flip-container">
                <div class="flipper">
                  <div class="front"> <a href="<?php echo $recentview['product_url'] ?>"> <img src="<?php echo $recentview['imgurl'] ?>" class="img-responsive"> </a> </div>
                  <div class="back"> <a href="<?php echo $recentview['product_url'] ?>"> <img src="<?php echo $recentview['imgurl'] ?>" class="img-responsive"> </a> </div>
                </div>
              </div>
              <a href="<?php echo $recentview['product_url'] ?>" class="invisible"> <img src="<?php echo $recentview['imgurl'] ?>" class="img-responsive"> </a>
              <div class="text">
                <h3><a href="<?php echo $recentview['product_url'] ?>"><?php echo $recentview['product_name']?></a></h3>
                <p class="price"><?php echo $this->config->item('currency'); ?> <?php echo number_format($recentview['sale_price']);?></p>
              </div>
            </div>
            <!-- /.product --> 
          </div>
          <?php } }else { 
						?>
          <div class="col-md-3 col-sm-6">
            <div class="box same-height">
              <h3><?php echo $this->lang->line("no_product"); ?></h3>
            </div>
          </div>
          <?php
						
									
						
						
						}?>
        </div>
      </div>
      <!-- /.col-md-9 --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /#content -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:440px;"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="border:none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line("success"); ?></h4>
        </div>
        <div class="modal-body">
          <table class="table modalTbl">
          </table>
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col-xs-3 pull-left"><a href="<?php echo site_url("cart/view"); ?>">
              <button class="btn btn-info"><?php echo $this->lang->line("view_cart"); ?></button>
              </a></div>
            <div class="col-xs-6 pull-right"><a href="<?php echo site_url("checkout/index"); ?>">
              <button class="btn btn-success"><?php echo $this->lang->line("checkout"); ?></button>
              </a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="addwishModal" role="dialog">
    <div class="modal-dialog wisgdig"> 
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" id="crossbtnw" data-dismiss="modal">&times;</button>
          <h5 class="modal-title"><i class="fa fa-check-circle-o wishcls" aria-hidden="true"></i> <?php echo $this->lang->line("new_wishlist_item"); ?></h5>
        </div>
        <div class="modal-body text-right"> <a class="btn btn-primary" id="whislink" href=""><?php echo $this->lang->line("view_Wishlist"); ?><i class="fa fa-chevron-right" aria-hidden="true"></i> </a> <a  class="btn btn-primary"  href="<?php echo base_url()?>search"><?php echo $this->lang->line("continue_shop"); ?><i class="fa fa-chevron-right" aria-hidden="true"></i> </a> </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="imagelightbox" role="dialog">
    <div class="modal-dialog totwidth1"> 
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <script src="<?php echo base_url()?>assets/frontend/js/jssor.slider-21.1.5.min.js" type="text/javascript"></script> 
          <script type="text/javascript">
         jssor_1_slider_init = function() {
            
            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,$Zoom:1,$Easing:{$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad},$Opacity:2},
              {$Duration:1000,$Zoom:11,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Zoom:1,$Rotate:1,$During:{$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
              {$Duration:1000,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
              {$Duration:1200,x:0.5,$Cols:2,$Zoom:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:4,$Cols:2,$Zoom:11,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Left:$Jease$.$Swing,$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
              {$Duration:1000,x:-4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
              {$Duration:1200,x:-0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Left:$Jease$.$Swing,$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
              {$Duration:1000,x:4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
              {$Duration:1200,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
              {$Duration:1000,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.7}},
              {$Duration:1200,x:-4,y:2,$Rows:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Row:28},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
              {$Duration:1200,x:1,y:2,$Cols:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:19},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.8}}
            ];
            
            var jssor_1_options = {
              $AutoPlay: false,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Rows: 2,
                $Cols: 6,
                $SpacingX: 14,
                $SpacingY: 12,
                $Orientation: 2,
                $Align: 156
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1100);
                    refSize = Math.max(refSize, 300);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            //responsive code end
        };
		
		
        
    </script>
          <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 960px; height: 480px; overflow: hidden; visibility: hidden;"> 
            <!-- Loading Screen -->
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
              <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
              <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 240px; width: 720px; height: 480px; overflow: hidden;">
              <?php 
        if(!empty($productIMG)){
								foreach($productIMG as $imgpath){
									
							
							 ?>
              <div data-p="150.00" style="display: none;" class="slideimage"> <img data-u="image" src="<?php  echo base_url().'photo/product/'. $Details[0]->product_id.'/800X800/'.$imgpath;?>" /> <img data-u="thumb" src="<?php  echo $medpath.$imgpath;?>" /> </div>
              <?php }}?>
            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort01-99-66" style="position:absolute;left:0px;top:0px;width:240px;height:480px; border:1px solid #ccc;" data-autocenter="2"> 
              <!-- Thumbnail Item Skin Begin -->
              <div data-u="slides" style="cursor: default;">
                <div data-u="prototype" class="p">
                  <div class="w">
                    <div data-u="thumbnailtemplate" class="t"></div>
                  </div>
                  <div class="c"></div>
                </div>
              </div>
              <!-- Thumbnail Item Skin End --> 
            </div>
            <!-- Arrow Navigator --> 
            <span data-u="arrowleft" class="jssora05l" style="top:158px;left:248px;width:40px;height:40px;" data-autocenter="2"></span> <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;" data-autocenter="2"></span> </div>
          <script type="text/javascript">jssor_1_slider_init();</script> 
        </div>
      </div>
    </div>
  </div>
  
  <!-- *** FOOTER ***
 _________________________________________________________ --> 
  
  <!-- /#footer --> 
  
  <!-- *** FOOTER END *** --> 
  
  <!-- *** COPYRIGHT ***
 _________________________________________________________ --> 
  
  <!-- *** COPYRIGHT END *** --> 
  
</div>

<!-- Magnific Popup core CSS file -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
<script>
	//$.noConflict();
		jQuery(document).ready(function(){
			
			
			
			$('#crossbtnw').on('click',function(){
				$('#addwishModal').hide();
				
			});
			
			$( "#slider-range" ).slider({
						range: true,
						min: 0,
						max: 10000,
						values: [ 100,1000 ],
								slide: function( event, ui ) {
								$( "#amount111" ).html( "$" + ui.values[ 0 ]);
								$( "#amount222" ).html( "$" + ui.values[ 1] );
								$( "#amount1" ).val(ui.values[ 0 ]);
								$( "#amount2" ).val(ui.values[ 1 ]);
								},
								stop: function( event, ui ) {
									//window.location.href=baseUrl+'/search';
									var priceRange=$( "#amount1" ).val()+'-'+$( "#amount2" ).val();
									hash_val2='#price='+priceRange
									
									window.location.href="<?php echo base_url().'search';?>"+hash_val2;
									

								}
								});
								$( "#amount111" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ));
								$( "#amount222" ).html( "$" + $( "#slider-range" ).slider( "values", 1 ));
			
			$('#bandseach').click(function(){
				
				var brand1='';
				
				$('input[name="brand[]"]:checked').each(function() {
				
				brand1+=this.value+',';
				});
				brand1=brand1.slice(0, -1)
				hash_val2='#brand='+brand1;
				window.location.href="<?php echo base_url().'search';?>"+hash_val2;

			});
			$('#sidemenuid a').click(function(){
				hash_val2='#categoryid='+$(this).attr('id');
				window.location.href="<?php echo base_url().'search';?>"+hash_val2;
			});	

			jQuery("#addPrdctToWshlst").click(function(e){
				e.preventDefault();
				pid='<?php echo $Details[0]->product_id?>';
				uid='<?php echo $loginUserArray['user_id'];?>';
				var wishlistId=jQuery("#Wishlist").val();
				if(uid!='' && pid!='' && wishlistId!=''){
					var dataString = 'wid='+wishlistId+'&tag="addProductToWishlist"&uid='+uid+'&pid='+pid;
					$.ajax({
				   type: "POST",
				   url: "<?php echo base_url();?>product/addProductToWishlistAjax",
				   data: dataString,
				   cache: false,
				   success: function(result){
						   $("#notificationDiv").html('');
						   $("#whislink").attr('href',result);
						$("#addwishModal").css('display','block');
				   }
				});
				}
			});
			$('#mainImg').click(function(){
				/*var zoomImage=$('#thumbs').find('.active').attr('href');
				   if(zoomImage){
						lastUrl=zoomImage.split('/');
					   	lastChar=lastUrl.pop();
						$('.image-link').attr('href','<?php echo $originalpath; ?>'+lastChar);
				   }*/
			});
				
			//$('.image-link').magnificPopup({type:'image'});
			
			
			
			
			
		});
	</script> 
<script>
  
  /**
   * jQuery function to prevent default anchor event and take the href * and the title to make a share pupup
   *
   * @param  {[object]} e           [Mouse event]
   * @param  {[integer]} intWidth   [Popup width defalut 500]
   * @param  {[integer]} intHeight  [Popup height defalut 400]
   * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
   */
  $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
    
    // Prevent default anchor event
    e.preventDefault();
    
    // Set values for window
    intWidth = intWidth || '500';
    intHeight = intHeight || '400';
    strResize = (blnResize ? 'yes' : 'no');

    // Set title and open popup with focus on it
    var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
        strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,            
        objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
  }
  
  /* ================================================== */
  
  /*$(document).ready(function ($) {
    $('.customer.share').on("click", function(e) {
      $(this).customerPopup(e);
    });
  });*/
  </script> 
<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery(document).on("click","#button_buy_gift",function(){
		
					var wishlistId=$("#recipientList").val();
					var quantity = parseInt($("#quantity").val()); 
					var product_id = $(this).attr("data-product-id");
					var product_name = $(this).attr("data-product-name");
					var stock_quantity = parseInt($(this).attr("data-product-stock"));
					var product_type = 'buy_gift';
					var dataString = 'wid='+wishlistId+'&product_id='+product_id+'&product_name='+product_name+'&quantity='+quantity+'&type='+product_type;
					if(stock_quantity < quantity)
					{ $("#quantity").notify("Product out of stock.","error"); }
					else
					{
						$.ajax({
						   type: "POST",
						   url: "<?php echo base_url();?>cart",
						   data: dataString,
						   dataType: 'json',
						   success: function(result){
							  if(result){
								  $("#msgBox").html("You have successfully added item to cart!");
								  $("#msgBox").show().delay(2000).queue(function(n) {
										  $(this).hide(); n();
										});
								  $('#dLabel').html('<i class="fa fa-shopping-cart cart"></i>'+ result['count'] +'<span class="caret"></span>');
								  $(".modal-body > table.modalTbl").html("");
								  if(result['item']['type'] == 'buy_gift'){
									  if(result['item']['wishlist_id']!=0){
										var title = "BUY GIFT - "+(result['item']['name']).toUpperCase()+" FOR WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
										var img = result['item']['image'];
									  }else{
										var title = (result['item']['name']).toUpperCase();
										var img = result['item']['image'];
									  }
								  }else if(result['item']['type'] == 'cash_gift'){
									var title = "CASH GIFT - "+(result['item']['wishlist_name']).toUpperCase();
									var img = result['item']['no_image'];
								  }else{
									var title = "CONTRIBUTION - "+(result['item']['name']).toUpperCase()+" FROM WISHLIST "+(result['item']['wishlist_name']).toUpperCase();
									var img = result['item']['no_image'];  
									  
								  }
										  var content = "<tr><td><img src='"+img+"' width='50' height='67'> </a></td><td><p>"+result['item']['qty']+"<br />"+title+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+result['item']['price']+"</strong></p></td></tr>";
								  $(".modal-body > table.modalTbl").append(content);
								  $("#myModal").modal();
								  $.ajax({
								   type: "POST",
								   url: "<?php echo base_url();?>cart/info",
								   dataType: 'json',
								   success: function(res){ console.log(res); 
									$("ul.minicart > div.text-center").html("");
									$("table.mini-cart").html("");
									  var total = 0;
									  $.each(res,function(i,k){
										  if(k.type == 'buy_gift'){
											  if(k.wishlist_id!=0){
												var name = "BUY GIFT - "+(k.name).toUpperCase()+" FOR WISHLIST "+(k.wishlist_name).toUpperCase();
												var image = k.image;
											  }else{
												var name = (k.name).toUpperCase();
												var image = k.image;
											  }
										  }else if(k.type == 'cash_gift'){
											var name = "CASH GIFT - "+(k.wishlist_name).toUpperCase();
											var image = k.no_image;
										  }else{
											var name = "CONTRIBUTION - "+(k.name).toUpperCase()+" FROM WISHLIST "+(k.wishlist_name).toUpperCase();
											var image = k.no_image;  
											  
										  }
										  var html = "<tr><td><img src='"+image+"' width='50' height='67'> </a></td><td><p>"+k.qty+"<br />"+name+"</p><p><strong><?php echo $this->config->item('currency'); ?>"+" "+k.price+"</strong></p></td><td><button type='button' class='close remvItem' aria-label='Close' data-row-id="+k.rowid+"><span aria-hidden='true'>&times;</span></button></td></tr>";
	
										  $("table.mini-cart").append(html);
										  total = total + k.subtotal;
									  });
									  
									  var htm = "<li role='separator' class='divider'></li><div class='text-right'><h5 id='subtotal'>SUBTOTAL : <?php echo $this->config->item('currency'); ?>"+" "+total+"</h5></div><li role='separator' class='divider'></li><div class='row'><div class='col-md-6'><div class='pull-left'><a href='<?php echo site_url("cart/view"); ?>'><button type='button' class='btn btn-info btn-sm'><i class='fa fa-shopping-cart' aria-hidden='true'></i> VIEW</button></a></div></div><div class='col-md-6'><div class='pull-right'><a href='<?php echo site_url("checkout"); ?>'><button type='button' class='btn btn-success btn-sm'>CHECKOUT <i class='fa fa-caret-right' aria-hidden='true'></i></button></a></div></div> </div>";
									  $(".total").html(htm);
									  
									}
								});
							  }
							}
						});
					}
					
					
					//$("area[rel^='prettyPhoto']").prettyPhoto();
				});
	jQuery(document).on("change","#quantity",function(){
			var inputval = jQuery(this).val();
			var product_id = $("#button_buy_gift").data("product-id");
			jQuery.ajax({
					type:"POST",
					url:"<?php echo base_url('quantitycheck'); ?>",
					data:{"val":inputval,"p_id":product_id},
					success: function(result){
							if(jQuery.trim(result) == 0)
							{ jQuery("#quantity").notify("Product out of stock.","error");}
						}
				});
		});
	
});
			
</script>