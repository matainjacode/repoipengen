<style>.ui-widget-content{z-index:2 !important}</style>
<script src="<?php echo base_url();?>assets/frontend/js/product_search.js"></script>
<div id="content">
            <div class="container white">
                <div class="col-md-12">
                    <span>
                    <h4><?php 
					
					$catidarray="";
					$catid="";
					$divcat="";
					$pnam="";
					$catidarray=explode('-',$this->uri->segment(2));
					$c=0;
					if(!empty($catidarray)){
						if($catidarray[0]!=""){
					foreach($catidarray as $val){
						
						if($c==0)
						{
							$catid=$val;
							?>
                           
                            
                            <?php 
							}else{
								$pnam.= strtoupper($val).' ';
							}
							$c++;
						
					}
					$divcat='<button class="btn btn-xs btn-success category" id="cat'.$catid.'"><i class="fa fa-times-circle"></i> '.$pnam.'</button>';
						}
					}
					?></h4>
                     <input type="hidden" name="catidfirst" id="catidfirst" value="<?php echo $catid?>" />
                    </span>
                    <hr>
                </div>
                
                <div style="display:none;" class="searchGlobalJs none">
  <div class="hostName"><?php echo base_url();?></div>
    <div class="categoryid"></div>
  <div class="brand"></div>
  <div class="price_range"></div>
  <div class="id"></div>
  <div class="rShort"></div>

  <div class="villId"></div>
   <div class="hashVal"><?php echo $_SERVER['REQUEST_URI'];?></div>

</div>
              <div class="col-md-3 hidden-xs hidden-sm hidden-md">
                    <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
                                    <!--Side Navbar-->
                  <div class="sidemenu">
                  <div class="sidemenutitle"><?php echo $this->lang->line("categories"); ?></div>
                
                    <div class="sidemenulist" id="sidemenuid">
                    <?php print_r($html_category);?>
                      
                    </div>
                    </div>
                <!--eof side navebar-->
                <div class="sidemenu">
                  <div class="sidemenutitle"><?php echo $this->lang->line("brands"); ?> <button id="removebrand" class="btn btn-xs btn-danger pull-right" ><i class="fa fa-times-circle"></i> <?php echo $this->lang->line("clear"); ?></button></div>
                
                    <div class="sidemenulist" id="brandserchid">
                      <div class="form-group brandsclass">
					   <?php
                     
                   if(!empty($result)){
                           foreach($result as $val){
							   ?>
                                <div class="checkbox">
                                    <label>
                                        <input value="<?php echo $val->brand_id;?>" name="brand[]" type="checkbox"><?php echo $val->brand_name;?>
                                    </label>
                                </div>
                               <?php
                               
                           }
                           }
                      
                       ?>                                 
                                </div>

                                <button class="btn btn-default btn-sm btn-primary" id="bandseach"><i class="fa fa-pencil"></i> <?php echo $this->lang->line("apply"); ?></button>

                    
                    </div>
                    </div>

                    
                    <div class="sidemenu">
                  <div class="sidemenutitle"><?php echo $this->lang->line("price_range"); ?> <button id="price-range" class="btn btn-xs btn-danger pull-right" ><i class="fa fa-times-circle"></i> <?php echo $this->lang->line("clear"); ?></button></div>
                
                    <div class="sidemenulist">
                        <div class="col-sm-2 col-xs-12 pull-left" style="margin-top:10px; padding-left:0px;">
                        <p style="color:#135b02;font-size:12px;"><span id="amount111" style="background:#eee;border:solid 1px #ddd;padding:2px 2px; margin-right:3px;border-radius:5px;">1</span></p>
                        </div>
                        <div class="col-sm-8 col-xs-12" style="padding:0px;margin-top:12px;"> 
                        
                        <!--<p id="amount"></p>-->
                        
                        <div id="slider-range"></div>
                        <input type="hidden" id="amount1" name="job_budget1" value="10">
                        <input type="hidden" id="amount2" name="job_budget" value="30">
                        </div>
                        <div class="col-sm-2 col-xs-12 pull-right text-right" style="margin-top:10px;">
                        <p style="color:#135b02;font-size:12px;"> <span id="amount222" style="background:#eee;border:solid 1px #ddd;padding:2px 2px; margin-left:0px;border-radius:5px;">1</span></p>
                        </div>
                    </div>
                    </div>
  

                  <!-- *** MENUS AND FILTERS END *** -->

                  <div class="banner">
                        <a href="#">
                            <img src="<?php echo base_url();?>assets/frontend/img/banner.jpg" alt="sales 2014" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="col-md-9">
                    <div id="searchall" class="searchall"><?php if($divcat!="")echo $divcat; ?></div>

                    <div class="categorybox info-bar" id="ctagrybox">
                        <div class="row">
                        <input type="hidden" name="count-limit" id="count-limit" value="12" />
                            <div class="col-sm-12 col-md-4 products-showing">
                                <?php echo $this->lang->line("product_show_1"); ?> <strong id="disp-count"></strong> <?php echo $this->lang->line("product_show_2"); ?> <strong id="total-count"></strong> <?php echo $this->lang->line("product_show_3"); ?>
                            </div>

                            <div class="col-sm-12 col-md-8  products-number-sort">
                                <div class="row">
                                    <form class="form-inline">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-number" id="product-show">
                                                <strong><?php echo $this->lang->line("show"); ?></strong>  <a id="12" href="javascript:void(0)" class="btn btn-default btn-sm btn-primary">12</a>  <a href="javascript:void(0)" id="24" class="btn btn-default btn-sm">24</a>  <a href="javascript:void(0)" id="0" class="btn btn-default btn-sm"><?php echo $this->lang->line("all"); ?></a> <?php echo $this->lang->line("product_show_3"); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-sort-by">
                                                <strong><?php echo $this->lang->line("sort_by"); ?></strong>
                                                <select name="sort-by" id="sort-by" class="form-control">
                                                    <option value="pricelow"><?php echo $this->lang->line("Price_Low_to_High"); ?></option>
                                                    <option value="pricehigh"><?php echo $this->lang->line("Price_High_to_Low"); ?></option>
                                                    <option value="nameasc"><?php echo $this->lang->line("Name_A_Z"); ?></option>
                                                    <option value="namedesc"><?php echo $this->lang->line("Name_Z_A"); ?></option>
                                                    <option value="salseFirst"><?php echo $this->lang->line("sales_first"); ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="alert alert-danger prod-msg" style="display:none;"> Sorry! No product available. </div>
                    <div class="row products" id="allproductid">
 
 
                    </div>
                    <!-- /.products -->

                    <div class="pages">

                       <!-- <p class="loadMore">
                            <a href="#" class="btn btn-primary btn-lg"><i class="fa fa-chevron-down"></i> Load more</a>
                        </p>-->

                        <ul class="pagination" id="paginationid">
                        
                        </ul>
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
		<script>
		$(document).ready(function() {
		
								
				});
					
		</script>