<style>
.overlap {
    background: rgba(0, 0, 0, 0.3) none repeat scroll 0 0;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
<div id="all">
  <div id="content">
    <div class="container white">
      <div class="col-md-12"> 
        <hr>
      </div>
      <div class="col-md-12"> 
        <?php foreach($blogData as $value){ ?>
          <!--blog image-->
			<?php  if($value->display_status==1){
						if(is_file($this->config->item("image_path")."blog/".$value->blog_id."/".$value->blog_image))
						{ $imgPath = base_url("photo/blog/".$value->blog_id."/".$value->blog_image); }
						else
						{ $imgPath = base_url("assets/img/no_image.png");}
					?>
                    <div class="col-md-6" onClick="window.open('<?= base_url("blog-view/".$value->blog_id) ?>');" style="cursor:pointer">	
                    <img src="<?= $imgPath ?>" class="img-responsive" width="500" height="500"/>
                    </div>
                    <?php }else{?>
                    <div class="col-md-6">
                    <iframe class="img-responsive" width="500" height="500" src="<?= $value->blog_link_video ?>"></iframe>
                    </div>
                     
                    <?php }?>
                  	<div class="col-md-6">
					<div class="wishlisttitle"><?php echo $value->blog_title ?></div>
                    <hr />
                    <div><?php echo $value->blog_description ?></div>
                    </div>
                             <!--eof blog image--> 
          
         
        </div>
        <!--Eof First Column Article--> 
        <?php } ?>
      </div>
      
      
      <!-- /.col-md-9 --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /#content -->
</div>
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
