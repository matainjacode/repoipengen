<style>
/*.overlap {
    background: rgba(0, 0, 0, 0.3) none repeat scroll 0 0;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content 
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button 
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.modal-content iframe{
        margin: 0 auto;
        display: block;
    }*/
</style>
        <div id="content">

            <div class="container-fluid nopadding">
                <div id="sliderhome">
                    <div id="main-slider">
                        <div class="item">
                           <img class="img-responsive" src="<?= base_url();?>/assets/frontend/img/slider1.jpg" alt="">
                           
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="<?= base_url();?>/assets/frontend/img/slider2.jpg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="<?= base_url();?>/assets/frontend/img/slider3.jpg" alt="">
                        </div>
                    </div>
                    <!-- /#main-slider -->
                </div>
              
            </div>
<!--category Boxes--> 

<!--<div class="container-fluid" data-animate="fadeInDown">-->
<div class="container-fluid front-cat-blue hidden-lg hidden-md raleway">
<?php echo $this->lang->line("category_info"); ?>
</div>
<div class="container-fluid">
<!--mel-->

<!--item 1-->
<div class="row row-eq-height">
<div class="col-md-4 front-cat-blue hidden-xs hidden-sm caticon">
<div class="cat-into-text raleway">
   <?php echo $this->lang->line("category_info"); ?>
 </div>       

        </div><!--eof col-md-4 -->

<div class="col-md-8 col-xs-12 nopadding">    
<?php if (!empty($eventcategory)){ ?>
<?php foreach ($eventcategory as $ecat){?>
    
<div class="col-xs-6 col-sm-3"> 
<div class="cat-item cat-img">
	 <img src="<?php echo $this->config->item('image_display_path')?>eventcategory/<?php echo $ecat->id;?>/400-400/<?php echo $ecat->image_name;?>">
     
		<?php if($this->session->userdata('log_in')){ ?>
       	 <a class="overlink" href="<?= base_url();?>create-a-wishlist?cat=<?php echo $ecat->event_name ?>">
            <div class="link-btn">
            <?php echo $ecat->event_name;?>
            </div>
        </a>
        <?php } else{ ?>
        
       <a  class="overlink" data-toggle="modal" data-ctname="<?php echo $ecat->event_name ?>" href="#"><div class="link-btn" id="link-btn"><?php echo $ecat->event_name ?></div></a> <!--data-target="#signup-modal"-->
        <?php }?>
        
		
            
		</div><!--eof div item-->
        </div>        
        
<?php } }?>


</div><!--row equal-->



</div><!--category-->                              
</div><!--eof col-md-8 nopadding-->                           
</div>
<!--eof category Boxes-->           
            

            <!-- *** HOT PRODUCT SLIDESHOW ***
 _________________________________________________________ -->
            <div id="hot">

                <div class="header-product">
                    <div class="container">
                        <div class="col-md-12">
                           <div class="raleway"><?php echo $this->lang->line("gift_section"); ?></div>
                        </div>
                    </div>
                </div>
 <!-- *** HOT PRODUCT GRID*** -->
    <div class="container-fluid nopadding">
    <!--item 1-->

    <?php if (!empty($featuredproduct)){ ?>    
    <?php foreach ($featuredproduct as $fproducts){
		
		$photopath = $this->config->item('image_display_path');
		$photopath_absolute = $this->config->item('image_path');
		$thumb_size= $this->config->item('thumb_size');
		$medium_thumb_size= $this->config->item('medium_thumb_size');
		
		$photopath_is_check =$photopath_absolute."product/".$fproducts->product_id;
		$thumpath = $photopath."product/".$fproducts->product_id.$medium_thumb_size;
		
		if(is_file($this->config->item('image_path').'/product/'.$fproducts->product_id.$medium_thumb_size.$fproducts->product_image)){
		$imgpurl=$this->config->item('image_display_path').'product/'.$fproducts->product_id.$medium_thumb_size.$fproducts->product_image ;
		}  else{
		$imgpurl= $this->config->item('image_display_path').'product/no_product.jpg';
		}
		
		
		?>
    
        <div class="col-xs-6 col-sm-3">
        <div class="productitem item-img">

            <img src="<?php echo $imgpurl;?>">
        
        
         <a class="overlay" href="<?php echo $this->Product_model->getProductUrl_name($fproducts->product_id,$fproducts->product_name)?>">
                <div class="description">
                 <p>
                     <?php echo $fproducts->product_name;?>
                  </p>
            </div>
            </a>
        </div><!--eof div item-->
        </div><!--eof item 1 -->
        
    <?php } }?>
    </div> 
    <!--Eof HOT PRODUCT-->
                
                <!-- /.container -->

            </div>
            <!-- /#hot -->

            <!-- *** HOT END *** -->

            <!-- *** BLOG HOMEPAGE ***
 _________________________________________________________ -->

            <div class="box text-center paddremve">
                <div class="header-product">
                    <div class="container">
                        <div class="col-md-12">
                           <div class="wishlisttitle"><?php echo $this->lang->line("wishlist_testimonial"); ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">

                <div class="col-md-12">

                    <div id="blog-homepage" class="row">
                    <?php if(!empty($blogdata)){ ?>
                    <!--First Column Article-->
                    <?php foreach($blogdata as $value){ 
						$blogViewUrl = '"'.base_url().'"';
					?>
                    <div class="col-md-6" >
                    <!--blog image-->
                    
                    <?php if($value->display_status==1){
						if(is_file($this->config->item("image_path")."blog/".$value->blog_id."/".$value->blog_image))
						{ $imgPath = base_url("photo/blog/".$value->blog_id."/".$value->blog_image); }
						else
						{ $imgPath = base_url("assets/img/no_image.png");}
					?>
                    <div class="col-xs-4" onClick="window.open('<?= base_url("blog-view/".$value->blog_id) ?>');" style="cursor:pointer">	
                    <img src="<?= $imgPath ?>" class="img-responsive"/>
                    </div>
                    <?php }else{?>
                    <div class="col-xs-4" style="padding: 0px;">
                    <iframe src="<?= $value->blog_link_video ?>" class="img-responsive"></iframe>
                    <div class="overlap" id="myBtn"></div>
                    </div>
                    <!--<div id="myModal" class="modal">
                      <div class="modal-content">
                        <span class="close" style="font-family:arial">x</span>
                        <div>
                        <iframe src="<? //= $value->blog_link_video ?>"  width="560" height="315" frameborder="0" allowfullscreen></iframe>
                        </div>
                      </div>
                    
                    </div>-->
                    <?php }?>
                    
                    <!--eof blog image-->
                    
                    <!--blog content-->
                    <div class="col-xs-8" onClick="window.open('<?= base_url("blog/~".$value->blog_id."-".$value->blog_auto_title) ?>');" style="cursor:pointer">
                    <div class="post"><h4><?=$value->blog_title?></h4>
                                
                                <hr>
                                <p class="intro"><?php
								
								$string = strip_tags($value->blog_description);

								if (strlen($string) > 200) {
								
									// truncate string
									$stringCut = substr($string, 0, 200);
								
									// make sure it ends in a word so assassinate doesn't become ass...
									$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'.base_url("blog/~".$value->blog_id."-".$value->blog_auto_title) .'">Read More</a>'; 
								}
								echo $string;

								
								?>
                                
                                </p>
                                
                    </p></div>
                      </div><!--eof blog content-->
                     </div> <!--Eof First Column Article-->
                    <?php }} ?>
                    <!--2nd Column Article-->
                    <?php /*?><div class="col-md-6">
                    <!--blog image-->
                    <div class="col-xs-4"> <iframe src="//www.youtube.com/embed/zpOULjyy-n8?rel=0" class="img-responsive"></iframe><?php /*?><img src="<?= base_url();?>/assets/frontend/img/MBS-6.jpg" class="img-responsive"><?php ?></div>
                    <!--eof blog image-->
                        <!--blog content-->
                    <div class="col-xs-8">
                    <div class="post">
                                <h4><a href="post.html">Who is who - example blog post</a></h4>
                                <p class="author-category">By <a href="#">John Slim</a> in <a href="">About Minimal</a>
                                </p>
                                <hr>
                                <p class="intro">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean
                                    ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                                
                                </p>
                           </div>
                      </div><!--eof blog content-->
                      </div><?php */?><!--Eof 2nd Column Article-->
                    </div>
                    <!-- /#blog-homepage -->
                </div>
            </div>
            <!-- /.container -->

            <!-- *** BLOG HOMEPAGE END *** -->


        </div>   
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
$(document).on("click",".link-btn", function(){
    var cat = $(this).html();
    //alert(cat);
	$.ajax({
		 type:"POST",
		 url:"<?php echo base_url("seturl"); ?>",
		 data:{"catName":cat},
		 success: function(res){
				 if($.trim(res) == "1")
				 { $("#signup-modal").modal("show");}
			 }
		});
});

</script>
