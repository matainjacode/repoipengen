        <div id="content">

            <?php /*?><div class="container-fluid nopadding">
                <div id="sliderhome">
                    <div id="main-slider">
                        <div class="item">
                           <img class="img-responsive" src="<?= base_url();?>/assets/frontend/img/slider1.jpg" alt="">
                           
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="<?= base_url();?>/assets/frontend/img/slider2.jpg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="<?= base_url();?>/assets/frontend/img/slider3.jpg" alt="">
                        </div>
                    </div>
                    <!-- /#main-slider -->
                </div>
              
            </div><?php */?>
<!--category Boxes--> 

<!--<div class="container-fluid" data-animate="fadeInDown">-->
<div class="container-fluid front-cat-blue hidden-lg hidden-md raleway">
<?php echo $this->lang->line("category_info"); ?>
</div>
<div class="container-fluid">
<!--mel-->

<!--item 1-->
<div class="row row-eq-height">
<div class="col-md-4 front-cat-blue hidden-xs hidden-sm caticon">
<div class="cat-into-text raleway">
   <?php echo $this->lang->line("category_info"); ?>
 </div>       

        </div><!--eof col-md-4 -->

<div class="col-md-8 col-xs-12 nopadding">    
<?php if (!empty($eventcategory)){ ?>
<?php foreach ($eventcategory as $ecat){?>
    
<div class="col-xs-6 col-sm-3"> 
<div class="cat-item cat-img">
	 <img src="<?php echo $this->config->item('image_display_path')?>eventcategory/<?php echo $ecat->id;?>/400-400/<?php echo $ecat->image_name;?>">
     
		<?php if($this->session->userdata('log_in')){ ?>
       	 <a class="overlink" href="<?= base_url();?>create-a-wishlist?cat=<?php echo $ecat->event_name ?>">
            <div class="link-btn">
            <?php echo $ecat->event_name;?>
            </div>
        </a>
        <?php } else{ ?>
        
       <a data-target="#signup-modal" class="overlink" data-toggle="modal" data-ctname="<?php echo $ecat->event_name ?>" href="#"><div class="link-btn" id="link-btn"><?php echo $ecat->event_name ?></div></a>
        <?php }?>
        
		
            
		</div><!--eof div item-->
        </div>        
        
<?php } }?>


</div><!--row equal-->



</div><!--category-->                              
</div><!--eof col-md-8 nopadding-->                           
</div>
<!--eof category Boxes-->           
            

            <!-- *** HOT PRODUCT SLIDESHOW ***
 _________________________________________________________ -->
            <div id="hot">

                <div class="header-product">
                    <div class="container">
                        <div class="col-md-12">
                           <div class="raleway"><?php echo $this->lang->line("gift_section"); ?></div>
                        </div>
                    </div>
                </div>
 <!-- *** HOT PRODUCT GRID*** -->
    <div class="container-fluid nopadding">
    <!--item 1-->

    <?php if (!empty($featuredproduct)){ ?>    
    <?php foreach ($featuredproduct as $fproducts){
		
		$photopath = $this->config->item('image_display_path');
		$photopath_absolute = $this->config->item('image_path');
		$thumb_size= $this->config->item('thumb_size');
		$medium_thumb_size= $this->config->item('medium_thumb_size');
		
		$photopath_is_check =$photopath_absolute."product/".$fproducts->product_id;
		$thumpath = $photopath."product/".$fproducts->product_id.$medium_thumb_size;
		
		if(is_file($this->config->item('image_path').'/product/'.$fproducts->product_id.$medium_thumb_size.$fproducts->product_image)){
		$imgpurl=$this->config->item('image_display_path').'product/'.$fproducts->product_id.$medium_thumb_size.$fproducts->product_image ;
		}  else{
		$imgpurl= $this->config->item('image_display_path').'product/no_product.jpg';
		}
		
		
		?>
    
        <div class="col-xs-6 col-sm-3">
        <div class="productitem item-img">

            <img src="<?php echo $imgpurl;?>">
        
        
         <a class="overlay" href="<?php echo $this->Product_model->getProductUrl_name($fproducts->product_id,$fproducts->product_name)?>">
                <div class="description">
                 <p>
                     <?php echo $fproducts->product_name;?>
                  </p>
            </div>
            </a>
        </div><!--eof div item-->
        </div><!--eof item 1 -->
        
    <?php } }?>
    </div> 
    <!--Eof HOT PRODUCT-->
                
                <!-- /.container -->

            </div>
            <!-- /#hot -->

            <!-- *** HOT END *** -->

            <!-- *** BLOG HOMEPAGE ***
 _________________________________________________________ -->

            <div class="box text-center paddremve">
                <div class="header-product">
                    <div class="container">
                        <div class="col-md-12">
                           <div class="wishlisttitle"><?php echo $this->lang->line("wishlist_testimonial"); ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">

                <div class="col-md-12">

                    <div id="blog-homepage" class="row">
                    <?php if(!empty($blogdata)){ ?>
                    <!--First Column Article-->
                    <?php foreach($blogdata as $value){ ?>
                    <div class="col-md-6">
                    <!--blog image-->
                    <div class="col-xs-4">
                    <?php if($value->display_status==1){?>
                    <img src="<?= base_url();?>photo/blog/<?=$value->blog_id?>/<?=$value->blog_image?>"
                     class="img-responsive"/>
                    <?php }else{?>
                    <iframe src="<?=$value->blog_link_video?>" class="img-responsive"></iframe>
                    <?php }?>
                    </div>
                    <!--eof blog image-->
                    
                    <!--blog content-->
                    <div class="col-xs-8">
                    <div class="post"><h4><?=$value->blog_title?></h4>
                                
                                <hr>
                                <p class="intro"><?=$value->blog_description;?></p>
                                
                    </p></div>
                      </div><!--eof blog content-->
                     </div> <!--Eof First Column Article-->
                    <?php }} ?>
                    <!--2nd Column Article-->
                    <?php /*?><div class="col-md-6">
                    <!--blog image-->
                    <div class="col-xs-4"> <iframe src="//www.youtube.com/embed/zpOULjyy-n8?rel=0" class="img-responsive"></iframe><?php /*?><img src="<?= base_url();?>/assets/frontend/img/MBS-6.jpg" class="img-responsive"><?php ?></div>
                    <!--eof blog image-->
                        <!--blog content-->
                    <div class="col-xs-8">
                    <div class="post">
                                <h4><a href="post.html">Who is who - example blog post</a></h4>
                                <p class="author-category">By <a href="#">John Slim</a> in <a href="">About Minimal</a>
                                </p>
                                <hr>
                                <p class="intro">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean
                                    ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                                
                                </p>
                           </div>
                      </div><!--eof blog content-->
                      </div><?php */?><!--Eof 2nd Column Article-->
                    </div>
                    <!-- /#blog-homepage -->
                </div>
            </div>
            <!-- /.container -->

            <!-- *** BLOG HOMEPAGE END *** -->


        </div>        