<div id="content">
  <div class="container white">
    <div class="row"> 
      <!--Side Bar-->
      <div class="col-md-3 nopadding">
        <?php $this->load->view('common/leftsidebar')?>
        
        <!--Eof original sidebar--> 
      </div>
      
      <!-- Right Column-->
      <div class="col-md-9">
		<div class="alert alert-success msg-box" style="display:none; margin-top:10px;">
      		<?= $this->lang->line("success_email_sent") ?>
      	</div>
        <div class="dashboard-header"><?php echo $this->lang->line("serprise_title"); ?></div>
        <div class="allrows">
          <p class="text-muted"><?php echo $this->lang->line("serprise_info"); ?> <a href="<?php echo base_url("contact-us"); ?>"><?php echo $this->lang->line("contact_us"); ?></a>.</p>
          <?php if ( !empty($surpriselist)){ ?>
          <table class="table table-condensed">
            <thead>
              <tr>
                <th><?php echo $this->lang->line("table_order"); ?>
                  </th>
                <th><?php echo $this->lang->line("table_date"); ?>
                  </th>
                <th><?php echo $this->lang->line("table_total"); ?>
                  </th>
                <th></th>
                <th><?php echo $this->lang->line("table_action"); ?>
                  </th>
              </tr>
            </thead>
            <tbody>
            
            <?php // if ( !empty($surpriselist)){
				
				foreach ($surpriselist as $surprise){
				
				    $sDate = new DateTime($surprise->transaction_time);
					$order['date']=$sDate->format('d/m/Y');
				
				?>
            
              <tr>
                <td><a href="<?= base_url(); ?>surprise/deatils/<?php echo $surprise->order_id;?>"># <?php echo $surprise->order_id;?></a> : 
                <?= $this->lang->line("gift_bought_by") ?> <a href="<?= $surprise->user_url;?>"><?php echo $surprise->fname.' '.$surprise->lname;?></a>
                <td><?php echo $order['date'];?></td>
                <td><?php echo $this->config->item('currency'); ?> <?php echo number_format($surprise->gross_amount)?></td>
                <!--<td><span class="label label-info"><?php echo ucfirst($surprise->transaction_status) ?></span></td>-->
                <td><button type="submit" class="btn btn-info btn-sm thncls" data-sender-id='<?php echo $surprise->id;  ?>'><i class="fa fa-gift" aria-hidden="true"></i> <?= $this->lang->line("say_thank_you") ?></button></td>
                <td><a href="<?= base_url(); ?>surprise/deatils/<?php echo $surprise->order_id;?>" class="btn btn-primary btn-sm"> <?= $this->lang->line("view") ?> </a></td>
              </tr>
              
              
             <?php } ?> 
              
            </tbody>
          </table>
          <?php }else{ 
          echo $this->lang->line("empty_supprisebox");
           } ?> 
        </div>
        <!--eof allrows--> 
      </div>
      <!--eof infobox for Dashboard--> 
      
    </div>
    <!--Eof Right Column--> 
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$('.thncls').on('click',function(){
			var sender_user_id = $(this).attr('data-sender-id');
			$.ajax({
				url: '<?php echo base_url();?>surprise/email_notification',
				type: 'POST',
				data:{
						receiverID:sender_user_id 
					 },
				cache: false,
				success:function(result){
					if($.trim(result)==1){
						$('.msg-box').show().delay(5000).fadeOut('slow');
					}
				}
			
			});
	});  
});
</script>