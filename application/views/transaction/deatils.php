<div id="content">
            <div class="container white">
            <div class="row">
<!--Side Bar-->  <div class="col-md-3 nopadding">
<?php $this->load->view('common/leftsidebar')?>

<!--Eof original sidebar-->
</div><!-- Right Column-->
<div class="col-md-9">
<?php //print_r($orderdetails); ?>
<div class="dashboard-header"><?= $this->lang->line("transaction")."/".$this->lang->line("order") ?></div>


        <div class="allrows">
		<?php if (!empty ($orderdetails)) 
			{ 
			//print_r($orderdetails);
				foreach($orderdetails as $allorder)
				{
					$orderid = $allorder->order_id;
					$fname = $allorder->fname;
					$lname = $allorder->lname;
					$email = $allorder->email;
				}
			}
		?>
                    <h4><?= $ths->lang->line("table_order") ?> #<?php echo (isset ($orderid)) ? $orderid : '' ?> </h4>
                                <!--<p>You send gift for <a href="mywishlistname">My Birthday
                                                Wishlist</a> on for <strong>22/06/2013</strong> and is currently <strong>Being prepared</strong>.-->                        </p>
                            <hr>
                                <div class="row addresses">
                                    <div class="col-md-6">
                                        <h5><?= $ths->lang->line("sender") ?> </h5>
                                        <p><?php echo (isset($fname) ? $fname : '').' '.(isset($lname) ? $lname : '') ?><br>
                                            <?php echo (isset($email) ? $email : '') ; ?>
                                            <br>
                                      <?php $sender_add = strtoupper($order->payment_firstname.','.$order->payment_lastname.','.$order->payment_address_1.','.$order->payment_address_2.','.$order->payment_city.','.$order->payment_postcode) ?>
                                      <input type="hidden" name="sender_address" value="<?php echo $sender_add ?>" />
                                       </p>
                                  </div>
                                    <div class="col-md-6">
                                        <h5><?= $ths->lang->line("shipping_address") ?></h5>
                                        <p><?php echo (isset($fname) ? $fname : '').' '.(isset($lname) ? $lname : '') ?><br>
        <?php echo (isset($email) ? $email : '') ; ?><br>
        <?php $shipping_add = strtoupper($order->shipping_firstname.','.$order->shipping_lastname.','.$order->shipping_address.','.$order->shipping_city.','.$order->shipping_kecamatan.','.$order->shipping_postcode) ?>
                                      <input type="hidden" name="sender_shipping_address" value="<?php echo $shipping_add ?>" /></p>
                                  </div>
                    </div>
                            <hr>
                            
                            
                            
                            
                                <div class="table table-condensed" id="basket">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2"> <?= $ths->lang->line("product") ?> </th>
                                                <th><?= $ths->lang->line("table_total") ?></th>
                                            </tr>
                                        </thead>
											<?php $sum = 0;
											if (!empty ($orderdetails)) {
                                            foreach($orderdetails as $allorder){ ?>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo $allorder->pslug ?>">
                                                        <img src="<?php echo $allorder->imgpurl ?>" alt="<?php echo $allorder->name?>" class="thumbnail">
                                                    </a>
                                                </td>
                                                <td><?php echo $allorder->item_description?>
                                                <?php if ($allorder->item_type == 'buy_gift'){?>
                                                <p><?= $ths->lang->line("qty") ?> : <?php echo $allorder->quantity?></p>
                                                
                     
                                                <p><?= $ths->lang->line("price") ?> : <?php echo $this->config->item('currency'); ?> <?php echo ($allorder->item_type == 'contributed_cash_gift') ? number_format($allorder->contribute_amt) : number_format($allorder->price)?></p>
                                                <?php } ?>
                                                </td>
                                                <td><?php echo $this->config->item('currency'); ?> 
												<?php if($allorder->item_type == 'buy_gift')
												{
													echo number_format(($allorder->price * $allorder->quantity)); 
												}elseif($allorder->item_type == 'contributed_cash_gift'){
													echo  number_format($allorder->contribute_amt);
												}else { echo number_format($allorder->price);} ?></td>
                                            </tr>
                                            
                                        </tbody>
                                         <?php }}?>
                                        <tfoot>
                                            <tr>
                                                <th colspan="2" class="text-right"><?= $this->lang->line("sub_total") ?></th>
                                                <th><?php echo $this->config->item('currency').' '.number_format($allorder->total_product_price);?></th>
                                            </tr>
                                            <?php if($allorder->discount_amount != 0){ ?>
                                            <tr>
                                              <th colspan="2" class="text-right"><?= $this->lang->line("discount")." ".$this->lang->line("code") ?>  <?php echo '('.$allorder->coupon_code.')' ?> </th>
                                              <th><?php echo $this->config->item('currency').' '.$allorder->discount_amount ?></th>
                                            </tr>
                                            <?php } 
												if($allorder->shipping_method =='onedayservice_jakarta'){
											?>
                                            <tr>
                                              <th colspan="2" class="text-right"><?= $this->lang->line("shipping") ?> </th>
                                              <th><?php echo $this->config->item('currency').' '.$allorder->shipping_cost ?></th>
                                            </tr>
                                            <?php } ?>
                                            <tr>
                                              <th colspan="2" class="text-right"><?= $this->lang->line("table_total") ?></th>
                                              <th><?php echo $this->config->item('currency').' '.$allorder->total ?></th>
                                            </tr>
                                            <tr>
                                              <th colspan="2" class="text-right">&nbsp;</th>
                                              <th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                       
                                    </table>
        
                                </div>
                                <!-- /.table-responsive -->
        
                                
        
                  </div>
 

   


</div><!--eof infobox for Dashboard-->
</div><!--Eof Right Column-->
</div>
            
            </div>