<div id="content">
  <div class="container white">
    <div class="row"> 
      <!--Side Bar-->
      <div class="col-md-3 nopadding">
        <?php $this->load->view('common/leftsidebar')?>
        
        <!--Eof original sidebar--> 
      </div>
      <!-- Right Column-->
      <div class="col-md-9">
        <div class="dashboard-header"><?php echo $this->lang->line("transaction"); ?> / <?php echo $this->lang->line("order"); ?> </div>
        <div class="allrows">
          <p class="text-muted"><?php echo $this->lang->line("transaction_info"); ?> <a href="<?php echo base_url("contact-us"); ?>"><?php echo $this->lang->line("contact_us"); ?></a>.</p>
          <?php if(!empty($paymentdetails)){?>
          <table class="table table-condensed">
            <thead>
              <tr>
                <th><?php echo $this->lang->line("table_order"); ?>
                  </td>
                <th><?php echo $this->lang->line("table_date"); ?>
                  </td>
                <th><?php echo $this->lang->line("table_total"); ?>
                  </td>
                <th><?php echo $this->lang->line("table_status"); ?>
                  </td>
                <th><?php echo $this->lang->line("table_action"); ?>
                  </td>
              </tr>
            </thead>
            <tbody>
              <?php foreach($paymentdetails as $payments){

					$sDate = new DateTime($payments->transaction_time);
					$order['date']=$sDate->format('d/m/Y');

				  ?>

              <tr>
                <td><a href="<?= base_url(); ?>transaction/deatils/<?php echo $payments->order_id ?>"># <?php echo $payments->order_id ?></a> </td>
                <td><?php echo $order['date'];?></td>
                <td><?php echo $this->config->item('currency'); ?> <?php echo number_format($payments->gross_amount)?></td>
                <td><span class="label label-info"><?php echo $payments->transaction_status?></span></td>
                <td><a href="<?= base_url(); ?>transaction/deatils/<?php echo $payments->order_id ?>" class="btn btn-primary btn-sm"><?php echo $this->lang->line("view"); ?></a></td>
              </tr>
              <?php }?>
            </tbody>
          </table>
          <?php }else{ 
		  echo $this->lang->line("empty_order"); 
		  }?>
           <?php echo $this->pagination->create_links(); ?>
        </div>
        <!--eof allrows--> 
      </div>
      <!--eof infobox for Dashboard--> 
      
    </div>
    <!--Eof Right Column--> 
  </div>
</div>
