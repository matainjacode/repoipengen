<div id="content">
            <div class="container white">
            <div class="row">
<!--Side Bar-->  <div class="col-md-3 nopadding">
<!--Original sidebar-->
<?php $this->load->view('common/leftsidebar')?>
<!--Eof original sidebar-->
</div><!-- Right Column-->
<div class="col-md-9">
<div class="infobox">
<h3><?php echo $this->lang->line("greeting_info"); ?></h3>


  <div class="col-xs-6 col-sm-3"><div class="dashbutton"><a href="<?php echo base_url().'create-a-wishlist'; ?>"><div class="dashbutton-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div><?php echo $this->lang->line("create_wishlist"); ?></a></div></div>
  <div class="col-xs-6 col-sm-3"><div class="dashbutton"><a href="<?php echo base_url().'my-wishlist'; ?>"><div class="dashbutton-icon"><i class="fa fa-heart" aria-hidden="true"></i></div><?php echo $this->lang->line("my_wishlist"); ?></a></div></div>
  <div class="clearfix visible-xs-block"></div>
  <div class="col-xs-6 col-sm-3"><div class="dashbutton"><a href=""><div class="dashbutton-icon"><i class="fa fa-search-plus" aria-hidden="true"></i></div><?php echo $this->lang->line("wishlist_find"); ?></a></div></div>
  <div class="col-xs-6 col-sm-3"><div class="dashbutton"><a href="<?php echo base_url().'search'; ?>"><div class="dashbutton-icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></div><?php echo $this->lang->line("browse_catalog"); ?></a></div></div>
  


<span class="clearfix"></span>
<div class="allrows">

      <div class="panel panel-ipengen">
  <div class="panel-heading">
    <span class="panel-title"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $this->lang->line("notifications"); ?></span>
  </div>
  <div class="newspanel">
  <ul class="list-group">
  <?php
  if(!empty($wishlist_notification)){
	  foreach($wishlist_notification as $notification){
		 // print_r($notification);
	  
	  ?>
      <li class="list-group-item"><small class="text-muted"><i class="fa fa-clock-o"></i> 
	  <?php
				$date=date_create($notification->notification_date);
				echo date_format($date,"d/m/Y H:i:s");
				if($notification->notification_type == "Invite")
				{ echo ' <span class="label label-danger">Contribution</span>'; }
		?> 
		
	  </small><br>
     	<?php
			/* Code for Notification Sender name */
			if($notification->uuid != $notification->sender_uid){
				if($uid == $notification->uuid)
				{ 
					echo '<a href="'.$notification->getuserprofileurl.'" class="userID" id="'.$notification->sender_uid.'">'.ucwords($notification->fname.'  '.$notification->lname).'</a>';
				}else if($uid == $notification->sender_uid){
					echo "You";
				}
	 		 }else{ echo "You"; }
			/* Code for Notification context */
			if($notification->notification_type == "Invite")
			{ echo " invited you to contribute "; }
			elseif($notification->notification_type == "reply")
			{ echo " Thanks for the gift "; }
			elseif($notification->notification_type == "Cashgift")
			{ echo " Contributed Cash Gift : "; }
			elseif($notification->notification_type == "ContributedCashGift")
			{   if($uid == $notification->uuid)
				{ 
					echo ' Contributed ';
				}else if($uid == $notification->sender_uid){
					echo " Contribute ";
				}
			}
			elseif($notification->notification_type == "Buygift")
			{
				if($notification->uuid != $notification->sender_uid){
					if($uid == $notification->uuid)
					{ 
						echo " bought you a gift from your wishlist "; 
					}
					else if($uid == $notification->sender_uid)
					{
						$receiver = isset($notification->receiver_name) ? $notification->receiver_name :"";
						echo " bought a gift for ".$receiver." 's wishlist : ";
					}
				}else{
					echo " bought a product ".$notification->product_title;
				}
			}
			/* Code for cash shown */
			if($notification->notification_type == "Cashgift")
			{ 
				if($uid == $notification->uuid){
					$wishText = "your wishlist : ";
				}else if($uid == $notification->sender_uid){
					$wishText =  isset($notification->receiver_name) ? ucwords($notification->receiver_name)."'s Wishlist : " : "";
				}
				
				echo $this->config->item("currency").' '.number_format($notification->notification_amount,2)." to ".$wishText;
			}
			
			if($notification->notification_type == "ContributedCashGift")
			{ 
				if($uid == $notification->uuid){
					echo $this->config->item("currency").' '.number_format($notification->notification_amount,2)." for ". ucwords($notification->product_title). " in Wishlist ";
				}else if($uid == $notification->sender_uid){
					$receiver = isset($notification->receiver_name) ? $notification->receiver_name : "";
					echo $this->config->item("currency").' '.number_format($notification->notification_amount,2)." for ". ucwords($notification->product_title). " in ". $receiver." 's Wishlist ";
				}
				
			}
			/* Code for Product Name view for Invite */
			if($notification->notification_type == "Invite")
			{ echo $notification->product_title." in wishlist : ";}
			/* Code for Wishlist Name if Notification type "Bye Gift"*/
			if($notification->notification_type == "Buygift" || $notification->notification_type == "Invite" || $notification->notification_type == "Cashgift" || $notification->notification_type == "ContributedCashGift")
			{ echo '<a href="'.getWishlistUrl($notification->url).'">'.$notification->title.'</a>';}
			
			
    	?>  
      </li>
      <?php
			  }
		  }
		  else
		  {
			  echo '<li class="list-group-item">'. $this->lang->line("empty_notifications") .'</li>';
		  }
	  ?>

          </ul>
</div>
</div>
</div>
   
<div class="allrows">
      <div class="panel panel-ipengen">
  <div class="panel-heading">
    <span class="panel-title"><i class="fa fa-heart" aria-hidden="true"></i> <?php echo $this->lang->line("your_wishlist"); ?></span>
  </div>
 <div id="removein" class="">
  <?php if(!empty($my_wishlist)){ ?>

<table class="table table-condensed ">
      <thead>
        <tr>
          <td>&nbsp;</td>
          <td><?php echo $this->lang->line("wishlist_title"); ?></td>         
          <td><?php echo $this->lang->line("wishlist_action"); ?></td>         
          
        </tr>
      </thead>
      
         <tbody>
            
			<?php foreach ($my_wishlist as $my_wishlists){?>          
                

        <?php 
		$urlSlug=$my_wishlists->url;
		?>

        
         <?php 
			if(is_file($this->config->item("image_path")."wishlist/".$my_wishlists->id."/".$my_wishlists->wishlist_image))
			{ $imagepath = base_url('photo/wishlist/'.$my_wishlists->id."/".$my_wishlists->wishlist_image); }
			else
			{ $imagepath = base_url('photo/wishlist/event.jpg'); }
		?>
        

        <tr>
          <td><a href="<?php echo getWishlistUrl($urlSlug);?>"><img src="<?php echo $imagepath; ?>" class="img-rounded img-responsive userphoto"></a></td>
          <td>
          <div class="listform">
          <a href="<?php echo getWishlistUrl($urlSlug);?>"><?php echo $my_wishlists->title?></a>
          <div class="msgprivacy" id="msgprivacy<?php echo $my_wishlists->id?>"></div>
          <div class="clearfix"></div>
          </div>
          <div class="listform"><?php echo $this->lang->line("created"); ?> : <strong><?php $date=date_create($my_wishlists->created); echo date_format($date,'d/m/Y'); ?></strong> , <?php echo $this->lang->line("end"); ?> : <span class="text-danger"><strong><?php $date=date_create($my_wishlists->e_enddate);
                   echo date_format($date,'d/m/Y'); ?></strong></span></div>
          <div class="listform"><?php echo $this->lang->line("privacy"); ?> : <span class="text-danger" id="text-danger<?php echo $my_wishlists->id;?>"><?php echo ucfirst($my_wishlists->is_public);?></span> &nbsp;<button type="submit" class="btn btn-xs btn-success clschange" role="button" data-toggle="collapse" href="#Privacy<?php echo $my_wishlists->id;?>" data-id="<?php echo $my_wishlists->is_public;?>" aria-expanded="false" aria-controls="Privacy"><?php echo $this->lang->line("change_privacy"); ?></button>

<div class="collapse" id="Privacy<?php echo $my_wishlists->id;?>">
<div class="alert alert-info" role="alert">
<input type="hidden" name="wshid" value="<?php echo $my_wishlists->id;?>" />

<div class="checkbox">
<label>
<input type="radio"  id='publicSelect<?php echo $my_wishlists->id;?>' name="setprivacy" value="public" <?php echo ($my_wishlists->is_public == 'public' ? 'checked' : "");?>> <?php echo $this->lang->line("public"); ?>
</label></div>

<div class="checkbox">
<label>  
<input  type="radio" id='privateSelectp<?php echo $my_wishlists->id;?>' name="setprivacy" value="private" <?php echo ($my_wishlists->is_public=='private' ? 'checked' : "");?>>
<i class="fa fa-lock" aria-hidden="true"></i> <?php echo $this->lang->line("private"); ?><?php //echo ucfirst($my_wishlists->is_public);?></label><br>

<div class="col-xs-12 col-md-6 listform">

<input name="visitor password" type="password" value="" maxlength="15" class="form-control blankpwd" id="error<?php echo $my_wishlists->id;?>" >


<span class="text-danger"> * <em><?php echo $this->lang->line("warning_notify"); ?> </em> </span></div>
</label>
</div>

<img src="<?php echo base_url().'files/loader/loader.gif'?>" id="loding" style="display:none" />

<p class="text-right"><button type="button" class="btn btn-primary change"><?php echo $this->lang->line("save_privacy_btn"); ?></button></p>
</div>
</div>
</div>

<div class="listform"><a class="btn btn-warning btn-xs" role="button" data-toggle="collapse" href="#transferEmail<?php echo $my_wishlists->id;?>" aria-expanded="false" aria-controls="transferEmail">
<i class="fa fa-exchange" aria-hidden="true"></i> <?php echo $this->lang->line("friend_transfer"); ?>
</a>
<div class="collapse" id="transferEmail<?php echo $my_wishlists->id;?>">
<div class="alert alert-warning" role="alert">
<div class="col-xs-12 col-md-6 listform">
<input type="hidden" name="wshid" value="<?php echo $my_wishlists->id;?>" />
<input type="text" value="" class="form-control" id="sendmail<?php echo $my_wishlists->id;?>" placeholder="<?php echo $this->lang->line("transfer_email"); ?>"></div> 
<button type="submit" class="btn btn-info transfer"><?php echo $this->lang->line("transfer"); ?></button>
<div class="col-md-1 lodimg" >                           
<img src="<?php echo base_url().'files/loader/loader.gif'?>" id="loding" style="display:none" />
</div>
</div>
</div>
</div>

 </td>
          
<td><div class="listform"><a href="<?php echo getWishlistUrl($urlSlug);?>" data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="View"><?php echo $this->lang->line("view"); ?></a> </div>
 <div class="listform"><a href="<?php echo base_url();?>wishlist/edit/<?php echo $my_wishlists->id; ?>/<?php // echo $urlSlug; ?>" data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Edit"><?php echo $this->lang->line("edit"); ?></a></div> 
</td>
        </tr>
        
        
        
     <?php } ?>
        </tbody>
    </table>

<?php  } else { ?>
    <div><div class="alert alert-success text-center">
    <?php echo $this->lang->line("empty_wishlist"); ?>
  </div></div>
<?php } ?>
  
    </div>
    </div>
    </div>
    
    </div><!--eof infobox for Dashboard-->
    
    
    </div><!--Eof Right Column-->
    </div>
    
    </div><!--container White-->
    </div>
    
       <script>
	$(document).ready(function(){
		
		
		
		$('.change').on('click',function(){
			var pass,pk;
			var wishid=$(this).parent().parent().find('input[type=hidden]').val();
			//alert(wishid);
			var is_public=$(this).parent().parent().find('input[type=radio]:checked').val();
			
			if(is_public=='private')
			{
				pass=$(this).parent().parent().find('input[type=password]').val();
				
				if( pass!="" )
				  {  pk=1; }else{ pk=0; } 
		    }else{ pass="";pk=1;$("#error"+wishid).val("")}
				
			if(is_public=='public')
			{
				$('#loding').css({'display':'block'});
			}	
			
				if(pk==1){
				$('#loding').css({'display':'block'});	
				var dataString = 'wishlistid='+ wishid + '&is_public='+ is_public + '&password='+ pass ;
				
		
	    $.ajax({
			url: '<?php echo base_url();?>wishlist/changeprivacy',
			type: 'POST',
			data: dataString,
			cache: false,
			dataType: 'json',
			
        success: function(result) {
		
				//console.log(result.privacy);
				if(result.msg == 'success'){
				$('#text-danger'+wishid).text(result.privacy);
				$('#loding').css({'display':'none'});
				$("#removein").find(".in").removeClass("in");
				$("#msgprivacy"+wishid).html("<span class='green'>successfully updated</span>");
                $("#error"+wishid).val('')
				
				}
		}
	
		});
		}else{
			
			$("#error"+wishid).addClass("error");
			
			return false;
		}
		});
		
		$('.clschange').on('click',function(){
			
			//$("#removein").find(".in").removeClass("in");
			
			var ispivate = $(this).data('id');
			
	       var pid=$(this).next().find('input[type=hidden]').val();
			var publicselect=$('#publicSelect'+pid).val();
			var privateSelectp=$('#privateSelectp'+pid).val();
				if(publicselect == ispivate){
					
				$('#publicSelect'+pid).attr('checked', 'checked')
				}
				if(privateSelectp == ispivate){
					
				$('#privateSelectp'+pid).attr('checked', 'checked')
				}
			
		});
		
		
		$('.transfer').on('click',function(){
			var email;
			var wishidj=$(this).parent().parent().parent().find('input[type=hidden]').val();
			var is_email=$(this).parent().parent().find('input[type=text]').val();
			var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
			if(is_email !='' && pattern.test(is_email))
			{
				var dataString = 'wishlistid='+ wishidj + '&is_mail='+ is_email ;
				 $('#loding').css({'display':'block'});
				$.ajax({
				url: '<?php echo base_url();?>wishlist/wishlisttransfer',
				type: 'POST',
				data: dataString,
				cache: false,
				dataType: 'json',
				
						success: function(result) 
						{
					
							if(result.msg == 'success')
								{
							        $("#msgprivacy"+wishidj).html("<span class='green'>This wishlist is successfully transferred to "+is_email+"</span>");
									$('#loding').css({'display':'none'});
									$("#transferEmail"+wishidj).removeClass("in");
							
							   }
						}
	
		             });
				
				
			}else{
								
				$("#sendmail"+wishidj).addClass("error")
				
			}
		});
		
		$('.thncls').on('click',function(){
			var sender_user_id = $(".userID").attr('id');
			var notification_id=$(this).attr('id');
			$.ajax({
				url: '<?php echo base_url();?>user/notification',
				type: 'POST',
				data:{
						notification_id:notification_id,
						receiverID:sender_user_id 
					 },
				cache: false,
				success:function(result){
					if($.trim(result)=="1"){
						//$("#"+notifyID).notify("Contribution Email send Successfully.","success");
						 location.reload();
					}
					else
					{
						//$("#"+notifyID).notify("Sorry,  something wrong went..Try again.","error");
					}
				}
			
			});
			});
	 });

	</script>