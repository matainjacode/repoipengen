<script type="text/javascript" src="<?= base_url();?>/assets/frontend/cropbox/hammer.js"></script>
<script type="text/javascript" src="<?= base_url();?>/assets/frontend/cropbox/jquery.mousewheel.js"></script>
<link href="<?php echo base_url();?>assets/frontend/cropbox/style.css" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url();?>/assets/frontend/css/bootstrap-datetimepicker.css" />
<script src="<?= base_url();?>/assets/frontend/cropbox/cropbox.js"></script>
<script type="text/javascript">

	var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };
 </script>
<style>
@import url("//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc2/css/bootstrap-glyphicons.css");
.uploadImage {
	background: rgb(20, 154, 171) none repeat scroll 0 0;
	color: rgb(255, 255, 255);
	cursor: pointer;
	font-size: 13px;
	padding: 15px;
	text-align: center;
	width: 100%;
}
.imgloader {
	background: rgba(174, 168, 168, 0.36) none repeat scroll 0 0;
	border: 3px solid #1ec2e7;
	height: 100%;
	padding: 20%;
	position: fixed;
	text-align: center;
	width: 100%;
	top: 0;
	z-index: 500;
}
.cropfile {
	background: rgb(80, 201, 207) none repeat scroll 0% 0%;
	color: rgb(255, 255, 255);
	width: 100%;
	border-radius: 0px;
	border: medium none;
	font-size: 16px;
}
.fnameerror {
	color: #C00;
}
.lnameerror {
	color: #C00;
}
#btnCrop {
	background: rgb(80, 201, 207) none repeat scroll 0 0;
	border: medium none;
	color: #fff;
	font-size: 16px;
	height: 25px;
	width: 75px;
}
.imageBox {
	width:100%;
	background-size:588px auto !important;
	
}
#userid {
	border: 1px solid #ccc;
}
.user_name {
	background: #eee none repeat scroll 0 0;
	border: 1px solid #eee;
	padding: 6px;
	width: 100%;
}
</style>

<div class="imgloader" style="display:none"> <Img src="<?php echo base_url().'assets/img/ipengen-loader.gif'; ?>" style="width: 100px;"/> </div>
<div id="all">
<div id="content">
  <div class="container white">
    <div class="row"> 
      <!--Side Bar-->
      <div class="col-md-3 nopadding"> 
        <!--Original sidebar-->
        <?php $this->load->view('common/leftsidebar')?>
        <!--Eof original sidebar--> 
      </div>
      <!-- Right Column-->
      <div class="col-md-9">
        <div class="dashboard-header"><?php echo $this->lang->line("profile_text"); ?></div>
        <?php foreach($userdata as $userdatalist){
					$changepasswordform = array('name'=>'edituserprofile','id'=>'edituserprofile');
					echo form_open('user/login',$changepasswordform);
		?>
        <div class="linerows">
          <div class="row">
            <?php 
    if(isset($userdatalist->profile_pic) && is_file($this->config->item('image_path').'userimage/'.$userdatalist->id.'/'.$userdatalist->profile_pic)){
   		 $imagePath= base_url().'photo/userimage/'.$userdatalist->id.'/'.$userdatalist->profile_pic;
    
    }
    else{
    	 $imagePath= base_url().'assets/img/userimage.png';
    }?>
            <div class="col-xs-6 col-md-4">
              <div class="cropped"> <img src="<?php echo $imagePath; ?>" style="width:500px;" class="img-thumbnail"> </div>
              <span class="btn btn-default btn-file cropfile" style=""><span class="fileinput-new"><?php echo $this->lang->line("select_image"); ?></span>
              <input type="file" id="file" name="...">
              </span> </div>
            <div class="col-xs-12 col-md-8">
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("f_name"); ?></label>
                <div class="input-group">
                  <?php $data = array(
                                      'name'        => 'fname',
                                      'id'          => 'firstname',
                                      'value'       => $userdatalist->fname,
                                      'placeholder'   => $this->lang->line("f_name"),
                                      'class'        => 'form-control',
                                    );
                      echo form_input($data); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("l_name"); ?></label>
                <div class="input-group">
                  <?php $data = array(
                                      'name'        => 'lname',
                                      'id'          => 'lastname',
                                      'value'       => $userdatalist->lname,
                                      'placeholder'   => $this->lang->line("l_name"), 
                                      'class'        => 'form-control',
                                    );
                      echo form_input($data); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("email"); ?></label>
                <div class="input-group">
                  <?php $data = array(
                                      'name'        => 'email',
                                      'id'          => 'email',
                                      'value'       => $userdatalist->email,
                                      'placeholder' => $this->lang->line("email"), 
                                      'class'       => 'form-control',
									  'type'        => 'email',
									  'style'       => 'width:96%'
                                    );
                      echo form_input($data); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"></i></button>
                  </span> </div>
              </div>
              <!--eof listform-->
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("w_event_contact"); ?></label>
                <div class="input-group">
                  <?php $mobdata = array(
                                      'name'        => 'mobile',
                                      'id'          => 'mobile',
                                      'value'       => $userdatalist->mobile,
                                      'placeholder' => $this->lang->line("w_event_contact"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($mobdata); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("dob"); ?></label>
                <div class="row">
                  <div class="col-sm-6">
                    <div class='form-groups input-group date' id='dateofbirth'>
                      <input type='text' class="form-control" id="dob" value="<?php echo $userdatalist->dob; ?>"/>
                      <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="input-group">
                      <button type="button" class="btn btn-link dobbtn"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("user_name"); ?></label>
                <div class="input-group"> <span class="user_name form-control"><?php echo $userdatalist->user_id; ?> </span> <span class="input-group-btn">
                  <button type="button" class="btn btn-link" data-toggle="modal" data-target="#useridModal"><i class="fa fa-pencil" aria-hidden="true" ></i></button>
                  </span> </div>
              </div>
            </div>
          </div>
          <!--eof row-->
          <hr>
          <!--Social media-->
          <h4><?php echo $this->lang->line("social_media"); ?></h4>
          <div class="input-group listform"> <span class="input-group-addon" id="fb">https://www.facebook.com/</span>
            <?php $fbdata = array(
                                      'name'        => 'facebook',
                                      'id'          => 'facebook',
                                      'value'       => str_replace('https://www.facebook.com/',' ',$userdatalist->facebook),
                                      'placeholder' => $this->lang->line("facebook"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($fbdata); ?>
            <span class="input-group-btn">
            <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
            </span> </div>
          <div class="input-group listform"> <span class="input-group-addon" id="ig">https://www.instagram.com/</span>
            <?php $igdata = array(
                                      'name'        => 'instagram',
                                      'id'          => 'instagram',
                                      'value'       => str_replace('https://www.instagram.com/',' ',$userdatalist->instagram),
                                      'placeholder' => $this->lang->line("instagram"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($igdata); ?>
            <span class="input-group-btn">
            <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
            </span> </div>
          <div class="input-group listform"> <span class="input-group-addon" id="twitter">https://twitter.com/</span>
            <?php $twitterdata = array(
                                      'name'        => 'tw',
                                      'id'          => 'tw',
                                      'value'       => str_replace('https://twitter.com/',' ',$userdatalist->twitter),
                                      'placeholder' => $this->lang->line("twitter"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($twitterdata); ?>
            <span class="input-group-btn">
            <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
            </span> </div>
          <?php }if(!empty($useraddressdata[0])){ ?>
          
          <!--EOF Social media-->
          <hr>
          <div class="linerows">
            <h4><?php echo $this->lang->line("shipping_address"); ?></h4>
            <div class="listform form-horizontal">
              <label for="Account_Name"><?php echo $this->lang->line("w_event_add"); ?></label>
              <div class="input-group">
                <?php $addressrdata = array(
                                      'name'        => 'address1',
                                      'id'          => 'address1',
                                      'value'       => $useraddressdata[0]->street_address,
                                      'placeholder' => $this->lang->line("w_event_add"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($addressrdata); ?>
                <span class="input-group-btn">
                <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                </span> </div>
            </div>
            <!--eof listform-->
            
            <div class="listform form-horizontal">
              <label for="Account_Name"><?php echo $this->lang->line("w_event_add_2"); ?></label>
              <div class="input-group">
                <?php $address2rdata = array(
                                      'name'        => 'address2',
                                      'id'          => 'address2',
                                      'value'       => $useraddressdata[0]->land_mark,
                                      'placeholder' => $this->lang->line("w_event_add_2"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($address2rdata); ?>
                <span class="input-group-btn">
                <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                </span> </div>
            </div>
            <!--eof listform-->
            
            <div class="listform form-horizontal">
              <label for="Account_Name"><?php echo $this->lang->line("kecamatan"); ?></label>
              <div class="input-group">
                <?php $kecamatandata = array(
                                      'name'        => 'kecamatan',
                                      'id'          => 'kecamatan',
                                      'value'       => $useraddressdata[0]->district,
                                      'placeholder' => $this->lang->line("kecamatan"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($kecamatandata); ?>
                <span class="input-group-btn">
                <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                </span> </div>
            </div>
            <!--eof listform-->
            
            <div class="row">
              <div class="listform form-horizontal col-md-6">
                <label for="Account_Name"> city </label>
                <div class="input-group">
                  <?php 
						   $options = array(
						   '' => $this->lang->line("w_event_city"),
                           '1507' => 'Aceh',
                           '1508' => 'Bali',
                           '1509' => 'Banten',
                           '1510' => 'Bengkulu',
                           '1511' => 'BoDeTaBek',
                           '1512' => 'Gorontalo',
                           '1513' => 'Jakarta Raya',
                           '1514' => 'Jambi',
                           '1515' => 'Jawa Barat',
                           '1516' => 'Jawa Tengah',
                           '1517' => 'Jawa Timur',
                           '1518' => 'Kalimantan Barat',
                           '1519' => 'Kalimantan Selatan',
                           '1520' => 'Kalimantan Tengah',
                           '1521' => 'Kalimantan Timur',
                           '1522' => 'Kepulauan Bangka Belitung',
                           '1523' => 'Lampung',
                           '1524' => 'Maluku',
                           '1525' => 'Maluku Utara',
                           '1526' => 'Nusa Tenggara Barat',
                           '1527' => 'Nusa Tenggara Timur',
                           '1528' => 'Papua',
                           '1529' => 'Riau',
                           '1530' => 'Sulawesi Selatan',
                           '1531' => 'Sulawesi Tengah',
                           '1532' => 'Sulawesi Tenggara',
                           '1533' => 'Sulawesi Utara',
                           '1534' => 'Sumatera Barat',
                           '1535' => 'Sumatera Selatan',
                           '1536' => 'Sumatera Utara',
                           '1537' => 'Yogyakarta',
							);
							//echo form_dropdown('city', $options,$useraddressdata[0]->city, 'class="form-control" id="city"'); 
							echo form_input ('city','Jakarta Raya','class="form-control" id="city" readonly'); 
						   ?>
                </div>
              </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal col-md-6">
                <label for="Account_Name"><?php echo $this->lang->line("w_event_postcode"); ?></label>
                <div class="input-group">
                  <?php $postcodedata = array(
                                      'name'        => 'postcode',
                                      'id'          => 'postcode',
                                      'value'       =>  $useraddressdata[0]->zip,
                                      'placeholder' => $this->lang->line("w_event_postcode"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($postcodedata); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
              <!--eof listform--> 
              
            </div>
            <!--eof listform-->
            
            <?php } else{ ?>
            <!--EOF Social media-->
            <hr>
            <div class="linerows">
              <h4><?php echo $this->lang->line("shipping_address"); ?></h4>
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("w_event_add"); ?></label>
                <div class="input-group">
                  <?php $addressrdata = array(
                                      'name'        => 'address1',
                                      'id'          => 'address1',
                                      'value'       => '',
                                      'placeholder' => $this->lang->line("w_event_add"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($addressrdata); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("w_event_add_2"); ?></label>
                <div class="input-group">
                  <?php $address2rdata = array(
                                      'name'        => 'address2',
                                      'id'          => 'address2',
                                      'value'       => '',
                                      'placeholder' => $this->lang->line("w_event_add_2"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($address2rdata); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("kecamatan"); ?><</label>
                <div class="input-group">
                  <?php $kecamatandata = array(
                                      'name'        => 'kecamatan',
                                      'id'          => 'kecamatan',
                                      'value'       => '',
                                      'placeholder' => $this->lang->line("kecamatan"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($kecamatandata); ?>
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
              <!--eof listform-->
              
              <div class="row">
                <div class="listform form-horizontal col-md-6">
                  <label for="Account_Name"><?php echo $this->lang->line("kecamatan"); ?></label>
                  <div class="input-group">
                    <?php 
						   $options = array(
						   '' => $this->lang->line("w_event_city"),
                           '1507' => 'Aceh',
                           '1508' => 'Bali',
                           '1509' => 'Banten',
                           '1510' => 'Bengkulu',
                           '1511' => 'BoDeTaBek',
                           '1512' => 'Gorontalo',
                           '1513' => 'Jakarta Raya',
                           '1514' => 'Jambi',
                           '1515' => 'Jawa Barat',
                           '1516' => 'Jawa Tengah',
                           '1517' => 'Jawa Timur',
                           '1518' => 'Kalimantan Barat',
                           '1519' => 'Kalimantan Selatan',
                           '1520' => 'Kalimantan Tengah',
                           '1521' => 'Kalimantan Timur',
                           '1522' => 'Kepulauan Bangka Belitung',
                           '1523' => 'Lampung',
                           '1524' => 'Maluku',
                           '1525' => 'Maluku Utara',
                           '1526' => 'Nusa Tenggara Barat',
                           '1527' => 'Nusa Tenggara Timur',
                           '1528' => 'Papua',
                           '1529' => 'Riau',
                           '1530' => 'Sulawesi Selatan',
                           '1531' => 'Sulawesi Tengah',
                           '1532' => 'Sulawesi Tenggara',
                           '1533' => 'Sulawesi Utara',
                           '1534' => 'Sumatera Barat',
                           '1535' => 'Sumatera Selatan',
                           '1536' => 'Sumatera Utara',
                           '1537' => 'Yogyakarta',
							);
							echo form_dropdown('city', $options,'', 'class="form-control" id="city"'); 
						   ?>
                  </div>
                </div>
                <!--eof listform-->
                
                <div class="listform form-horizontal col-md-6">
                  <label for="Account_Name"><?php echo $this->lang->line("w_event_postcode"); ?></label>
                  <div class="input-group">
                    <?php $postcodedata = array(
                                      'name'        => 'postcode',
                                      'id'          => 'postcode',
                                      'value'       =>  '',
                                      'placeholder' => $this->lang->line("w_event_postcode"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($postcodedata); ?>
                    <span class="input-group-btn">
                    <button type="button" class="btn btn-link"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    </span> </div>
                </div>
                <!--eof listform--> 
                
              </div>
              <!--eof listform-->
              
              <?php } ?>
              <div class="row">
                <div class="col-md-4 listField">
                  <input type="hidden" class="imagehidden" value="" />
                  <input type="submit" class="btn btn-primary userupdateBtn btn-block" value="<?php echo $this->lang->line("save_profile"); ?>" style="height:50px" />
                </div>
              </div>
            </div>
            <!--eof Row--> 
            
          </div>
          <?php  echo form_close(); ?>
        </div>
        <!--eof infobox for Dashboard--> 
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog"> 
            
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("picture_crop"); ?></h4>
              </div>
              <div class="modal-body">
                <div class="imageBox" style="width:100%; background-position:center">
                  <div class="thumbBox"></div>
                  <div class="spinner" style="display: none">Loading...</div>
                </div>
                <div class="action">
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="button" id="btnCrop" value="<?php echo $this->lang->line("crop"); ?>" class="" data-dismiss="modal" style="">
                    </div>
                    <div class="col-sm-6 text-right">
                      <input type="button" id="btnZoomIn" value="+" style="">
                      <input type="button" id="btnZoomOut" value="-" style="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="useridModal" class="modal fade" role="dialog">
          <div class="modal-dialog"> 
            
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("user_name_change_text"); ?></h4>
              </div>
              <div class="modal-body">
                <div class="listform form-horizontal">
                  <label for="Account_Name"><?php echo $this->lang->line("user_name"); ?></label>
                  <div class="input-group">
                    <?php $useriddata = array(
                                      'name'        => 'user_id',
                                      'id'          => 'user_id',
                                      'value'       => $userdatalist->user_id,
                                      'placeholder' => $this->lang->line("user_name"), 
                                      'class'       => 'form-control',
									  'type'        => 'text'
                                    );
                      echo form_input($useriddata); ?>
                    <span class="errorusername" style="color:#F00"></span> <span class="input-group-btn">
                    <button type="button" class="btn btn-primary btnuserupdate"><?php echo $this->lang->line("update"); ?></button>
                    </span> </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Eof Right Column--> 
    </div>
  </div>
  <!--container White--> 
</div>
<script type="text/javascript">

		$("input" ).each(function() {
				if($(this).val() != ""){
				$(this).attr( "disabled","disabled" );
				}
		});
		
		$("#user_id").removeAttr("disabled");
		$(".input-group span .btn-link").click(function(){

  $(this).parent().parent().find(".form-control").removeAttr("disabled");
  
  });

$(".dobbtn").click(function(){

    $("#dob").removeAttr("disabled");
	
  
  });
	$(".userupdateBtn").removeAttr("disabled");
	$("#btnCrop").removeAttr("disabled");
	$("#btnZoomIn").removeAttr("disabled");
	$("#btnZoomOut").removeAttr("disabled");
	/*******Date Time Picker********/
	var firstOpen = true;
	$(function () {
		$('#dateofbirth').datetimepicker({
			minDate: '01/01/1950',
			viewMode: 'years',
			format: 'DD/MM/YYYY'
		});
	});
	/*******Date Time Picker End********/
    window.onload = function() {
        var options =
        {
            imageBox: '.imageBox',
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
        var cropper;
        document.querySelector('#file').addEventListener('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                options.imgSrc = e.target.result;
                cropper = new cropbox(options); 
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        document.querySelector('#btnCrop').addEventListener('click', function(){
			
			$(".cropped").empty();
            var img = cropper.getDataURL()
            document.querySelector('.cropped').innerHTML += '<img src="'+img+'" style="width:500px;" class="img-thumbnail">';
			$(".imagehidden").val(img);
			$('#myModal').modal('hide');
        })
        document.querySelector('#btnZoomIn').addEventListener('click', function(){
            cropper.zoomIn();
        })
        document.querySelector('#btnZoomOut').addEventListener('click', function(){
            cropper.zoomOut();
        })
		$("input#file").change(function () {
      $("#myModal").modal('show');
   });    
};
</script>