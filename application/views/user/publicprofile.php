<div id="content">
  <div class="container white"> 
    <!--User Name-->
    <div class="row bluewhiteheader raleway"><?php echo $userName; ?></div>
    <!--EOF User Name-->
    <div class="row"> 
      <!--Column 1 user-->
      <div class="col-md-3"> <img src="<?php echo $profileImage; ?>" class="img-thumbnail"> </div>
      <!--Eof Column 1 user--> 
      
      <!--Column 2 user-->
      <div class="col-md-3">
        <ul class="list-inline ">
          <li>
            <h3><i class="fa fa-facebook-square" aria-hidden="true"></i></h3>
          </li>
          <li>
            <h3><i class="fa fa-twitter" aria-hidden="true"></i></h3>
          </li>
          <li>
            <h3><i class="fa fa-instagram" aria-hidden="true"></i></h3>
          </li>
        </ul>
        <ul class="list-unstyled">
          <li class="text-danger"><?php echo $this->lang->line("member_since"); ?> : <?php echo $profileCreationDate; ?></li>
          <li><?php echo $this->lang->line("total_wishlist"); ?> : <strong><?php echo count($wishlist); ?></strong></li>
          <li><?php echo $this->lang->line("total_gift"); ?> :<strong> <?php echo count($wishlist_gift) ?></strong></li>
          <li><?php echo $this->lang->line("share_btn"); ?> : insert social icons </li>
        </ul>
      </div>
      <!--Eof Column 2 user--> 
      
      <!--Column 2 user details-->
      <div class="col-md-4">
        <ul class="list-inline ">
          <li><img src="<?php echo base_url('assets/'); ?>badges/snowflake.png" width="76" height="76" alt="Newbie"></li>
          <li><img src="<?php echo base_url('assets/'); ?>badges/users.png" width="76" height="76" alt="share"></li>
          <li><img src="<?php echo base_url('assets/'); ?>badges/bronze.png" width="76" height="76" alt="Bronze"></li>
        </ul>
      </div>
      <!--EOF Column 2 user details--> 
    </div>
    <!--Eof Row-->
    
    <div class="row bluewhiteheader raleway"></div>
    <div class="row"> 
      <!--Column Wishlist-->
      <div class="col-md-8">
        <h3><?php echo $this->lang->line("wishlist"); ?></h3>
        <!--wishlist table-->
        <table class="table table-condensed ">
          <thead>
          </thead>
          <tbody>
          <?php if(!empty($wishlist)){ foreach($wishlist as $list){ 
				if(is_file($this->config->item("image_path")."wishlist/".$list->id."/".$list->wishlist_image))
				{ $wishlistImage = base_url('photo/wishlist/'.$list->id."/".$list->wishlist_image); }
				else
				{ $wishlistImage = base_url('photo/wishlist/event.jpg'); }
		  ?>
            <tr>
              <td><a href="<?php echo getWishlistUrl($list->url);?>"><img src="<?php echo $wishlistImage; ?>" class="img-rounded img-responsive userphoto"></a></td>
              <td>
              	<div class="listform"><a href="<?php echo getWishlistUrl($list->url);?>"><?php echo ucwords($list->title) ?></a></div>
                <div class="listform"><?php echo $this->lang->line("created"); ?> : <strong><?php echo date("d/m/Y",strtotime($list->e_startdate)); ?></strong> , <?php echo $this->lang->line("end"); ?> : <span class="text-danger"><strong><?php echo date("d/m/Y",strtotime($list->e_enddate)); ?></strong></span></div>
              </td>
            </tr>
            <?php }} ?>
          </tbody>
        </table>
        <!--EOF wishlist table-->
        <hr>
        <!--Email User-->
        <h3><i class="fa fa-comments-o" aria-hidden="true"></i> <?php echo $this->lang->line("send_msg"); ?></h3>
        <p><?php echo $this->lang->line("send_msg_info"); ?>.</p>
        <form class="form-horizontal" action="" id="msgfrm">
          <div class="form-group" style="display:none;">
            <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $this->lang->line("email"); ?></label>
            <div class="col-sm-10">
              <input type="email" class="form-control" value="<?php echo $userEmail;?>" id="inputEmail3" placeholder="<?php echo $this->lang->line("email"); ?>" name="email" required="required">
            </div>
          </div>
          <div class="form-group">
            <label for="Message" class="col-sm-2 control-label"><?php echo $this->lang->line("message"); ?></label>
            <div class="col-sm-10">
              <textarea name="message" required cols="" rows="4" class="form-control" id="msgtxt"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="button" id="msgsnt" class="btn btn-info"><?php echo $this->lang->line("send_msg"); ?></button>
            </div>
          </div>
        </form>
        
        <!--EOF Email User--> 
      </div>
      <!--EOF Column Wishlist--> 
      
      <!--Column Wishlist Products received-->
      <div class="col-md-4">
      <?php if(count($wishlist_gift) != 0) { ?>
        <h3> <?php echo count($wishlist_gift)." ".$this->lang->line("recent_gift"); ?></h3>
      <?php } ?>
        <!--Item grid-->
        <?php if(!empty($wishlist_gift)){ foreach($wishlist_gift as $row){ 
			if(is_file($this->config->item('image_path').'/product/'.$row->product_id.$this->config->item('thumb_size').$row->product_image))
			{ $imageURL = base_url('photo/product/'.$row->product_id.$this->config->item('thumb_size').$row->product_image); }
			else
			{  $imageURL = base_url('photo/product/no_product.jpg'); }
		?>
        <div class="col-sm-6 user-gift">
          <div class="item-img"><img src="<?php echo $imageURL; ?>"></div>
          <div a href="#" class="product-cat-desc">
            <div class="product-cat-name"><?php echo $row->name; ?></div>
            <div class="product-cat-price"><?php echo $this->config->item("currency")." ".number_format($row->price, 2) ?></div>
          </div>
          </a> </div>
          <?php } }else{ ?>
        <!--<div class="col-sm-6 user-gift">
          <div class="item-img"><img src="<?php echo base_url('photo/product/no_product.jpg'); ?>"></div>
          <div a href="#" class="product-cat-desc">
            <div class="product-cat-name">No Product Available</div>
            <div class="product-cat-price"><?php echo $this->config->item("currency")." 0.00" ?></div>
          </div>-->
          <?php } ?>
          </a> </div>
          
        <!--eof item grid--> 
        
        <!--Item grid-->
        <!--<div class="col-sm-6 user-gift">
          <div class="item-img"><img src="http://cdn3.bigcommerce.com/s-dbwm0/products/664/images/1233/Moet_and_Chandon_Rose_Black_Unfurl_Gift_Tin__49899.1449696491.1280.1280.jpg?c=2"></div>
          <div a href="#" class="product-cat-desc">
            <div class="product-cat-name">Lorem ipsum dolor sit amet</div>
            <div class="product-cat-price">Rp 350,000</div>
          </div>
          </a> </div>-->
        <!--eof item grid-->
        
        <!--<div class="col-sm-6 user-gift">
          <div class="item-img"><img src="http://images.cultfurniture.com/images/products/zoom/1380014777-46368400.jpg"></div>
          <div a href="#" class="product-cat-desc">
            <div class="product-cat-name">Lorem ipsum dolor sit amet consectetur adipisicing elit</div>
            <div class="product-cat-price">Rp 350,000</div>
          </div>
          </a> -->
          <!-- /.ribbon -->
          <!--<div class="ribbon gift">
            <div class="theribbon">GIFT</div>
            <div class="ribbon-background"></div>
          </div>
        </div>-->
        <!--eof item grid--> 
        
      </div>
      <!--Column Wishlist Products received--> 
    </div>
    <!--Eof Row--> 
    
  </div>
</div>
<script>
$(document).ready(function(){
	$('#msgsnt').on('click',function(){
		var msgtxt=$('#msgtxt').val();
		var inputEmail3=$('#inputEmail3').val();
		/*if(inputEmail3==""){
			$("#inputEmail3").css("border-color" ,"red");
			return false;
		}else{
			if(validateEmail(inputEmail3)){
			}else{
				$("#inputEmail3").css("border-color" ,"red");
				$('#inputEmail3').notify("Emailid is not valid!");
			   return false;
			}
			
		}*/
		if(msgtxt==""){
			$("#msgtxt").css("border-color" ,"red");
			return false;
		}
		
		var dataString=$('#msgfrm').serialize(); 
		$.ajax({
				type: "POST",

				//url: "http://localhost/ipengen/wishlist/create_wishlist",

				url: base_url+'user/send_message',

				data: dataString,
				cache: false,
               
				success: function(result){
					
					if(result='success'){
					$('#msgtxt').val("");
		         // $('#inputEmail3').val("");
				 $('#msgsnt').notify("Mail sent successfully", "success");
					}
					
					
				}
		});
	});
	function validateEmail(email) {
		var filter = "/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/";
		if (filter.test(email)) 
		{ return true; }
		else 
		{ return false; }
	}
});
</script>