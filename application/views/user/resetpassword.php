<div class="container">
  <div id="all">
    <div id="content">
      <div class="container white">
        <div class="row">
          <div class="col-md-12">
            <div class="dashboard-header"><?= $this->lang->line("choose_new_password") ?></div>
            <?php
					$changepasswordform = array('name'=>'resetpassword','id'=>'resetpassword');
					echo form_open('',$changepasswordform);
                ?>
            <div class="row allrows">
              <div class="col-sm-12 ">
                <div class="alert alert-success msg" style="display:none"> <strong><?= $this->lang->line("success") ?>!</strong><?= $this->lang->line("information_updated") ?> </div>
              </div>
              <div class="col-sm-12 ">
                <h3> <?= $this->lang->line("password") ?> </h3>
              </div>
              <div class="col-md-8 listField"><?= $this->lang->line("new_pwd") ?> :
                <?php $newdata = array(
                                          'name'        => 'password',
                                          'id'          => 'password',
                                          'value'       => '',
										  'type'	    => 'password',
                                          'placeholder' => $this->lang->line("new_pwd"),
                                          'class'       => 'form-control',
                                        );
                          echo form_input($newdata); ?>
                <span class="lnameerror"></span> </div>
              <div class="col-md-8 listField"><?= $this->lang->line("confirm_new_pwd") ?> :
                <?php $condata = array(
                                          'name'        => 'cpassword',
                                          'id'          => 'cpassword',
                                          'value'       => '',
										  'type'	    => 'password',
                                          'placeholder' => $this->lang->line("confirm_new_pwd"),
                                          'class'       => 'form-control',
                                        );
                          echo form_input($condata); ?>
                <span class="lnameerror"></span> </div>
            </div>
            <div class="row allrows">
              <div class="col-md-6 listField">
                <input type="hidden" class="imagehidden" value="" />
                <input type="submit" class="btn btn-primary" value="Change Password" />
                <?php
					echo form_close();
           ?>
              </div>
            </div>
          </div>
          <!--eof infobox for Dashboard--> 
          
        </div>
        <!--Eof Right Column--> 
      </div>
    </div>
  </div>
</div>
