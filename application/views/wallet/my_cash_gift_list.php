<div id="all">
  <div id="content">
    <div class="container white">
      <div class="row">
        <div class="col-md-3 nopadding"><!--Side Bar--> 
          <!--Original sidebar-->
          <?php $this->load->view('common/leftsidebar')?>
          <!--Eof original sidebar--> 
        </div>
        <!-- Right Column-->
        <div class="col-md-9">
          <div class="dashboard-header"><?php echo $this->lang->line("cash_gift_title"); ?></div>
          <div class="allrows">
            <p class="text-muted"><?php echo $this->lang->line("cash_gift_info"); ?> <a href="<?php echo base_url("contact-us"); ?>"><?php echo $this->lang->line("contact_us"); ?></a>.</p>
            <?php if(!empty($cashgiftlist)){?>
            <table class="table table-condensed">
              <thead>
                <tr>
                  <th><?php echo $this->lang->line("table_order"); ?>
                    </td>
                  <th><?php echo $this->lang->line("table_date"); ?>
                    </td>
                  <th><?php echo $this->lang->line("table_total"); ?>
                    </td>
                  <th><?php echo $this->lang->line("table_status"); ?>
                    </td>
                  <th><?php echo $this->lang->line("table_action"); ?>
                    </td>
                </tr>
              </thead>
              <tbody>
                <?php 
									  foreach ($cashgiftlist as $list){?>
                <tr>
                  <td># <?php echo $list->order_id;?> : <?php echo $this->lang->line("cash_gift_from_my"); ?> : <a href="<?php echo base_url();?>~<?php echo $list->url; ?>"><?php echo $list->title;?></a> - <?php echo $this->lang->line("cash_gift_from"); ?> <a href="<?php echo base_url();?>u/<?php echo $list->user_id;?>"><?php echo $list->fname;?> <?php echo $list->lname;?></a></td>
                  <?php
                                        $sDate = new DateTime($list->date_added);
                                        $end['date']=$sDate->format('d-m-y');
                                        ?>
                  <td><?php echo $end['date'];?></td>
                  <td><?php echo $this->config->item("currency")." ".number_format($list->amount);?></td>
                  <td><span class="label label-success"><?php echo ucfirst($list->wallet_status);?></span></td>
                  <td><a href="<?php echo base_url();?>wallet/details/<?php echo $list->wishlist_id; ?>" class="btn btn-primary btn-sm"><?php echo $this->lang->line("view"); ?></a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php }
			
			else{ 
				echo $this->lang->line('empty_wallet');
				}?>
          </div>
          <!--eof allrows--> 
        </div>
        <!--eof infobox for Dashboard--> 
        
      </div>
      <!--Eof Right Column--> 
    </div>
  </div>
  <!--container White--> 
</div>
