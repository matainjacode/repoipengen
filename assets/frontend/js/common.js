$(function() {
	
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#loginform").validate({
    // Specify validation rules
    rules: {
      logemail: {
        required: true,
        email: true
      },
      logpass: {
        required: true,
        minlength: 4
      }
    },
    // Specify validation error messages
    messages: {
      logpass: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      logemail: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      return false;

    }
  });

 $("#resetpassword").validate({
    // Specify validation rules
    rules: {
      password: {
        required: true,
        minlength: 4
      },
      cpassword: {
        required: true,
        minlength: 4,
		equalTo: "#password"
      }
    },
    // Specify validation error messages
    messages: {
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      cpassword: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      return true;

    }
  });


  $("#loginformmodal").validate({
    // Specify validation rules
    rules: {
      emailmodal: {
        required: true,
        email: true
      },
      passwordmodal: {
        required: true,
        minlength: 4
      }
    },
    // Specify validation error messages
    messages: {
      passwordmodal: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      emailmodal: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      return false;

    }
  });

  $("#registrationform").validate({
    // Specify validation rules
    rules: {
      fname:  "required",
      lname : "required",
      regemail: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 5
      }
    },
    // Specify validation error messages
    messages: {
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      regemail: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      return false;
    }
  });
  $("#changepasswordform").validate({
    // Specify validation rules
    rules: {
      cpasssword: {
        required: true,
		 minlength: 6
        
      },
      newpassword: {
        required: true,
        minlength: 6,
	},
	conpassword: {
        required: true,
        minlength: 6,
		equalTo: "#newpassword"
      }
    },
    // Specify validation error messages
    messages: {
      cpasssword: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
	newpassword: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
	conpassword: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      return false;

    }
  });
  
  $("#wishlistform").validate({
    // Specify validation rules
    rules: {
      firstname:  "required",
      lastname : "required",
        wishlistname : "required",
		category : "required",
	   eventdate : "required",
	   eventenddate : "required",
	   seturl : "required",
	   addr : "required",
	   pcode : "required",
	   eventcategory : "required",
      visitpassword: {
        minlength: 3
      }
    },
    // Specify validation error messages
    messages: {
		//firstname: "Please enter your first name",
		lastname: "Please enter your lastname name",
		wishlistname: "Please enter your wishlist name",
		eventdate: "Please enter your event start date",
		eventenddate: "Please enter your event end date",
		addr: "Please enter your address",
		pcode: "Please enter your pincode",
      visitpassword: {
        
        minlength: "Your password must be at least 3 characters long"
      },
      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      return false;
    }
  });
  
  $("#personalizeform").validate({
    // Specify validation rules
    rules: {
	    eventdate : "required",
	  
      visitpassword: {
        minlength: 3
      }
    },
    // Specify validation error messages
    messages: {
		addr: "Please enter your address",
		pcode: "Please enter your pincode",
      visitpassword: {
        
        minlength: "Your password must be at least 3 characters long"
      },
      
    },
    submitHandler: function(form) {
      return false;
    }
  });
});

var getval;

//--------------------forgetpwd for validation------------
$("#forgetpwdform").validate({
    // Specify validation rules
    rules: {
      forgetemail: {
        required: true,
        email: true
      }
    },
    // Specify validation error messages
    messages: {
      forgetemail: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      return false;

    }
  });


$("#edituserprofile").validate({
    // Specify validation rules
    rules: {
      email: {
        required: true,
        email: true
      },
	 mobile: {
			minlength:9,
			maxlength:10,
			number: true
      },
	   postcode: {
			number: true
      },
	address1: {
				required: true,
				}
    },
    // Specify validation error messages
    messages: {
      
       email: "Please enter a valid email address",
	   mobile:"Enter your mobile no",
	   postcode:"Enter your mobile no",
		address1: "Please Add the address"
		
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
		
	  
      return false;

    }
  });
  
// Ajax User step1..................
$(document).on('click','.savecontinue',function(event){
	
	var privacy = $('input[name=privacy]:checked', '#wishlistform').val()

	var strname = $("#wishlistname").val();
	
	if ( privacy == 'private')
	{
		var visitpassword = $('#visitpassword').val();
		var length = $('#wishlistform input[type="password"]').val().length
		if (visitpassword == '')
		{
			$("#visitpassword-error").css("display","block");
			$("#visitpassword-error").text("Please enter your Password");
			
		}
		if (length > 15)
		{
			$("#visitpassword-error").css("display","block");
			$("#visitpassword-error").text("Password must be less than 15 chars");
			
		}
		
		if (length <= 15 && visitpassword != '')
		{
			if($("#wishlistform").valid() == true)
		{
		event.preventDefault();

		var wishlistname = $('#wishlistname').val();
		var eventdate = $('#eventdate').val();
		var eventenddate = $('#eventenddate').val();
		var cashgift = $('input[name=cashgift]:checked', '#wishlistform').val();
		
		var firstname = $('#firstname').val();
		var lastname = $('#lastname').val();
		var addr = $('#addr').val();
		var addr2 = $('#addr2').val();
		
		var pcode = $('#pcode').val();
		var dist = $('#dist').val();
		var city = $('#city').val();
		var mobileno = $('#mobileno').val();
		
		var privacy = $('input[name=privacy]:checked', '#wishlistform').val()
		var newshiprivacy = $('input[name=shiprivacy]:checked', '#wishlistform').val()
		//var password = $('#password').val();
		var visitpassword = $('#visitpassword').val();
		var category = $('#eventcategory').val();
		var url =  $('#seturl').val();
		
	   
var dataString = 'wishlistname='+ wishlistname + '&eventdate='+ eventdate + '&eventenddate='+ eventenddate + '&cashgift='+ cashgift +'&firstname='+ firstname + '&lastname='+ lastname + '&addr='+ addr + '&addr2='+ addr2 +'&pcode='+ pcode + '&dist='+ dist + '&city='+ city + '&mobileno='+ mobileno +'&privacy='+ privacy + '&shiprivacy='+ newshiprivacy + '&visitpassword='+ visitpassword + '&category='+ category + '&url='+ url;

        $('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);
		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				//url: "http://localhost/ipengen/wishlist/create_wishlist",
				url: base_url+'wishlist/create_wishlist',
				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
						// $('#load').css('display','none');
						$('html, body').animate({ scrollTop: 0 }, 0);
						 $('#loaderBg2').remove();
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    $('#firstname-error').append(result.firstname.message);
                     $('#firstname-error').css('display','block');
                    $('#lastname-error').append(result.lastname.message);
                     $('#lastname-error').css('display','block');
					 $('#wishlistname-error').append(result.lastname.message);
                     $('#wishlistname-error').css('display','block');
					 $('#eventdate-error').append(result.eventdate.message);
                     $('#eventdate-error').css('display','block'); 
					 $('#eventenddate-error').append(result.eventenddate.message);
                     $('#eventenddate-error').css('display','block');
					  $('#addr-error').append(result.addr.message);
                     $('#addr-error').css('display','block'); 
					 $('#pcode-error').append(result.pcode.message);
                     $('#pcode-error').css('display','block');
                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					console.log(result);
					
					$('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled " )
					$( "#step2" ).addClass( "active" )
				    $( "#tab2" ).removeClass( "disabled" )
					$( "#tab2" ).addClass( "active " )
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               	
			}	
		});
		
		return false;
         }
		}
		
	}
	else
	 { 

    if($("#wishlistform").valid() == true)
		{
		event.preventDefault();
		$form = $(this);

    var wishlistname = $('#wishlistname').val();
    var eventdate = $('#eventdate').val();
    var eventenddate = $('#eventenddate').val();
    var cashgift = $('input[name=cashgift]:checked', '#wishlistform').val();
	
	var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var addr = $('#addr').val();
    var addr2 = $('#addr2').val();
	
	var pcode = $('#pcode').val();
    var dist = $('#dist').val();
    var city = $('#city').val();
    var mobileno = $('#mobileno').val();
	
   var privacy = $('input[name=privacy]:checked', '#wishlistform').val()
    var newshiprivacy = $('input[name=shiprivacy]:checked', '#wishlistform').val()
    //var password = $('#password').val();
	var category = $('#eventcategory').val();
    var visitpassword = $('#visitpassword').val();
	var url =  $('#seturl').val();

var dataString = 'wishlistname='+ wishlistname + '&eventdate='+ eventdate + '&eventenddate='+ eventenddate + '&cashgift='+ cashgift +'&firstname='+ firstname + '&lastname='+ lastname + '&addr='+ addr + '&addr2='+ addr2 +'&pcode='+ pcode + '&dist='+ dist + '&city='+ city + '&mobileno='+ mobileno +'&privacy='+ privacy + '&shiprivacy='+ newshiprivacy + '&visitpassword='+ visitpassword + '&category='+ category + '&url='+ url;

        //$('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);
		// AJAX Code To Submit Form.
		
		$.ajax({
				type: "POST",

				//url: "http://localhost/ipengen/wishlist/create_wishlist",

				url: base_url+'wishlist/create_wishlist',

				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
					     $('html, body').animate({ scrollTop: 0 }, 0);
						 $('#loaderBg2').remove();
						 $("#wishid").val(result.getwishid);
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    
                    $('#lastname-error').append(result.lastname.message);
                     $('#lastname-error').css('display','block');
					 $('#wishlistname-error').append(result.lastname.message);
                     $('#wishlistname-error').css('display','block');
					 $('#eventdate-error').append(result.eventdate.message);
                     $('#eventdate-error').css('display','block'); 
					 $('#eventenddate-error').append(result.eventenddate.message);
                     $('#eventenddate-error').css('display','block');
					  $('#addr-error').append(result.addr.message);
                     $('#addr-error').css('display','block'); 
					 $('#pcode-error').append(result.pcode.message);
                     $('#pcode-error').css('display','block');
                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					console.log(result)
					 $form.attr('action' , 'jjj' );
					 $('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled " )
					$( "#step2" ).addClass( "active" )
				     $( "#tab2" ).removeClass( "disabled" )
					$( "#tab2" ).addClass( "active " )
					
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               
			}	


			
		});
		
		return false;
         }else{ 
         return false;
         }
     }
	 
	
  });
  
// Ajax User step1edit..................
$(document).on('click','.editcontinue',function(event){

	var privacy = $('input[name=privacy]:checked', '#wishlistform').val()
	
	var strname = $("#wishlistname").val();
	
	if ( privacy == 'private')
	{
		var visitpassword = $('#visitpassword').val();
		var visitpasswordblanck = $('#visitpasswordblanck').val();
		var length = $('#wishlistform input[type="password"]').val().length
		if (visitpassword == '')
		{
			var visitpasswordblanckhid = $('#hidpassdeta').val();
			var vpass = visitpasswordblanckhid;
			
		}
		if (visitpasswordblanck == '')
		{
			$("#visitpassword-error").css("display","block");
			$("#visitpassword-error").text("Input your password");
			
		}
		if (visitpassword != ''){
			

		if (length > 15)
		{
			$("#visitpassword-error").css("display","block");
			$("#visitpassword-error").text("Password must be less than 15 chars");
			
		}
		if (length <3)
		{
			$("#visitpassword-error").css("display","block");
			$("#visitpassword-error").text("Password must be gretter than 3 chars");
			
		}
	  }

		if (visitpasswordblanck != ''){

		if (length > 15)
		{
			$("#visitpassword-error").css("display","block");
			$("#visitpassword-error").text("Password must be less than 15 chars");
			
		}
		if (length <3)
		{
			$("#visitpassword-error").css("display","block");
			$("#visitpassword-error").text("Password must be gretter than 3 chars");
			
		}
	  }
		
		if ((length <= 15 && visitpasswordblanck != '') || (length <= 15 && visitpassword != '')|| visitpassword == '')
		{
			if($("#wishlistform").valid() == true)
		{
		event.preventDefault();

		var wishlistname = $('#wishlistname').val();
		var eventdate = $('#eventdate').val();
		var eventenddate = $('#eventenddate').val();
		 var cashgift = $('input[name=cashgift]:checked', '#wishlistform').val();
		
		var firstname = $('#firstname').val();
		var lastname = $('#lastname').val();
		var addr = $('#addr').val();
		var addr2 = $('#addr2').val();
		
		var pcode = $('#pcode').val();
		var dist = $('#dist').val();
		var city = $('#city').val();
		var mobileno = $('#mobileno').val();
		
		var privacy = $('input[name=privacy]:checked', '#wishlistform').val();
		var newshiprivacy = $('input[name=shiprivacy]:checked', '#wishlistform').val()
		//var password = $('#password').val();
		var vpass = ($('#visitpasswordblanck').val()) ? $('#visitpasswordblanck').val() : $('#visitpassword').val();
		var visitpassword = vpass;
		var category = $('#eventcategory').val();
		var wishid = $('#wishid').val();
		var url =  $('#seturl').val();
		var shiprivacy =  $('#shiprivacy').val();
		//var lastinsertid = $('#lastinsertid').val();
	   
var dataString = 'wishlistname='+ wishlistname + '&eventdate='+ eventdate + '&eventenddate='+ eventenddate + '&cashgift='+ cashgift +'&firstname='+ firstname + '&lastname='+ lastname + '&addr='+ addr + '&addr2='+ addr2 +'&pcode='+ pcode + '&dist='+ dist + '&city='+ city + '&mobileno='+ mobileno +'&privacy='+ privacy + '&shiprivacy='+ newshiprivacy + '&visitpassword='+ visitpassword + '&category='+ category + '&wishid='+ wishid + '&url='+ url + '&shiprivacy='+ shiprivacy;

        $('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);
		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",

				//url: "http://localhost/ipengen/wishlist/create_wishlist",

				url: base_url+'wishlist/edit_wishlist',

				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
						// $('#load').css('display','none');
						$('html, body').animate({ scrollTop: 0 }, 0);
						 $('#loaderBg2').remove();
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    $('#firstname-error').append(result.firstname.message);
                     $('#firstname-error').css('display','block');
                    $('#lastname-error').append(result.lastname.message);
                     $('#lastname-error').css('display','block');
					 $('#wishlistname-error').append(result.lastname.message);
                     $('#wishlistname-error').css('display','block');
					 $('#eventdate-error').append(result.eventdate.message);
                     $('#eventdate-error').css('display','block'); 
					 $('#eventenddate-error').append(result.eventenddate.message);
                     $('#eventenddate-error').css('display','block');
					  $('#addr-error').append(result.addr.message);
                     $('#addr-error').css('display','block'); 
					 $('#pcode-error').append(result.pcode.message);
                     $('#pcode-error').css('display','block');
                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					console.log(result)
					 $('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled " )
					$( "#step2" ).addClass( "active" )
				     $( "#tab2" ).removeClass( "disabled" )
					$( "#tab2" ).addClass( "active " )
					
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               
			}	


			
		});
		
		return false;
         }
		}
		
	}
	else
	 { 

    if($("#wishlistform").valid() == true)
		{
		event.preventDefault();

    var wishlistname = $('#wishlistname').val();
    var eventdate = $('#eventdate').val();
    var eventenddate = $('#eventenddate').val();
     var cashgift = $('input[name=cashgift]:checked', '#wishlistform').val();
	
	var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var addr = $('#addr').val();
    var addr2 = $('#addr2').val();
	
	var pcode = $('#pcode').val();
    var dist = $('#dist').val();
    var city = $('#city').val();
    var mobileno = $('#mobileno').val();
	
    var privacy = $('input[name=privacy]:checked', '#wishlistform').val();
   var newshiprivacy = $('input[name=shiprivacy]:checked', '#wishlistform').val()
    //var password = $('#password').val();
    var visitpassword = $('#visitpassword').val();
	var category = $('#eventcategory').val();
	var wishid = $('#wishid').val();
	var url =  $('#seturl').val();
	var shiprivacy =  $('#shiprivacy').val();
	//var lastinsertid = $('#lastinsertid').val();
	   
var dataString = 'wishlistname='+ wishlistname + '&eventdate='+ eventdate + '&eventenddate='+ eventenddate + '&cashgift='+ cashgift +'&firstname='+ firstname + '&lastname='+ lastname + '&addr='+ addr + '&addr2='+ addr2 +'&pcode='+ pcode + '&dist='+ dist + '&city='+ city + '&mobileno='+ mobileno +'&privacy='+ privacy + '&shiprivacy='+ newshiprivacy +  '&visitpassword='+ visitpassword + '&category='+ category + '&wishid='+ wishid + '&url='+ url + '&shiprivacy='+ shiprivacy;

        //$('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);
		// AJAX Code To Submit Form.
		
		$.ajax({
				type: "POST",

				//url: "http://localhost/ipengen/wishlist/create_wishlist",

				url: base_url+'wishlist/edit_wishlist',

				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
					$('html, body').animate({ scrollTop: 0 }, 0);
						 $('#loaderBg2').remove();
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    
                    $('#lastname-error').append(result.lastname.message);
                     $('#lastname-error').css('display','block');
					 $('#wishlistname-error').append(result.lastname.message);
                     $('#wishlistname-error').css('display','block');
					 $('#eventdate-error').append(result.eventdate.message);
                     $('#eventdate-error').css('display','block'); 
					 $('#eventenddate-error').append(result.eventenddate.message);
                     $('#eventenddate-error').css('display','block');
					  $('#addr-error').append(result.addr.message);
                     $('#addr-error').css('display','block'); 
					 $('#pcode-error').append(result.pcode.message);
                     $('#pcode-error').css('display','block');
                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					console.log(result)
					 $('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled " )
					$( "#step2" ).addClass( "active" )
				     $( "#tab2" ).removeClass( "disabled" )
					$( "#tab2" ).addClass( "active " )
					
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               
			}	


			
		});
		
		return false;
         }else{ 
         return false;
         }
     }
	
  });  
  
// Ajax User step2..................
$(document).on('click','.skipletter',function(){
	
	$( "#tab3" ).removeClass( "disabled" )
					$( "#tab3" ).addClass( "active" )
	$( "#step2" ).removeClass( "active" )
					$( "#complete" ).addClass( "active" )
					
});

$(document).on('click','.previous',function(){
	
	   var lastinsertid = $('#lastinsertid').val();
		$("#scontinue").css("display", "none");
		$("#updatecontinue").css("display", "block");
		var changeurl = base_url+'wishlist/edit_wishlist'
		$('#wishlistform').attr('action', changeurl);
		$( "#step1" ).removeClass( "disabled" )
		$( "#step1" ).addClass( "active" )
		$( "#step2" ).removeClass( "active" )
		$( "#step2" ).addClass( "disabled" )
					
});

// Ajax User step2..................
$(document).on('click','.personalize',function(event){
	
	
	
if($("#personalizeform").valid() == true)
		{
		event.preventDefault();
    var wsid = $('#wid').val();
    var rsvp_info = $('#guest_note').val();
    var eventdate = $('#eventpdate').val();
    var wishlist_address = $('#from').val();
	var cropImgdata = $(".imgreviewcrop").attr('src');
	 
	 var address= wishlist_address ;//address which you want Longitude and Latitude
	 if(address !='')
	 {
	 var arlene1 = [];
		jQuery.ajax({
		type: "GET",
		dataType: "json",
		url: "http://maps.googleapis.com/maps/api/geocode/json",
		data: {'address': address,'sensor':false},
		success: function(data){
			if(data.results.length){
				jQuery('#getlatid').val(data.results[0].geometry.location.lat);
			    jQuery('#getlongid').val(data.results[0].geometry.location.lng);
				var lat = (data.results[0].geometry.location.lat)? data.results[0].geometry.location.lat : '';
				var long = (data.results[0].geometry.location.lng)? data.results[0].geometry.location.lng : '';
				
				var dataString = 'rsvp_info='+ rsvp_info + '&wishlist_address='+ encodeURIComponent(wishlist_address) +'&lastwid='+ wsid +'&eventdate='+ eventdate +'&latitude='+ lat +'&longitute='+ long +'&wishlist_image='+ cropImgdata ;

       // $('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);

		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				url: base_url+'wishlist/create_personalize',
				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				console.log(result);	
				setTimeout(function(){
						 $('#loaderBg2').remove();
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    //$('#firstname-error').css('display','block');

                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					$('html, body').animate({ scrollTop: 0 }, 0);
					 $('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step2" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled" )
					$( "#step2" ).addClass( "disabled" )
					$( "#complete" ).addClass( "active" )
					$( "#tab3" ).removeClass( "disabled" )
					$( "#tab3" ).addClass( "active" )
					
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
			}	


			
		});
		
		return false;

				
			}else{
			
			
		   }
		}
		});	

	} else {
		
		
		var dataString = 'rsvp_info='+ rsvp_info + '&wishlist_address='+ encodeURIComponent(wishlist_address) +'&lastwid='+ wsid +'&eventdate='+ eventdate +'&wishlist_image='+ cropImgdata ;
		
		var data_string = 'wid='+ wsid ;

       // $('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);

		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				url: base_url+'wishlist/create_personalize',
				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
						 $('#loaderBg2').remove();
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    //$('#firstname-error').css('display','block');

                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					$('html, body').animate({ scrollTop: 0 }, 0);
					 $('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step2" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled" )
					$( "#step2" ).addClass( "disabled" )
					$( "#complete" ).addClass( "active" )
					$( "#tab3" ).removeClass( "disabled" )
					$( "#tab3" ).addClass( "active" )
					
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
                $.ajax({
					type: "POST",
					url: base_url+'wishlist/getWishlistUrlById',
					data: data_string,
					cache: false,
					dataType: 'json',
					success: function(res){
						$("#hidWishlistUrl").val(res);
					}
				});
			}	
		});
	}
		}else{ 
         return false;
         }
  });
$(document).on("change","#wisimage",function(){
/*<<<<<<< HEAD*/
	var cropImgdata = $(".imgreviewcrop").attr('src');
        if(readURL(this))
		{
			$.ajax({
				type:"POST",
				url:base_url+"wishlist/upload_image_personalize",
				data:{"wishlist_image":cropImgdata},
				dataType:"json",
				success: function(result){
						console.log(result);
					}
				});
		}
/*=======*/
	var lhtmt='<div class="imgwaitprocess" id="loaderBg2"><div class="imgloaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
	var wishlistId = $('#wid').val();
	$("#imageMain").append(lhtmt);
			if(readURL(this))
			{
				setTimeout(function(){
					$('#loaderBg2').remove();
					},2000);
				/*setTimeout(function(){
					var cropImgdata = $(".imgreviewcrop").attr('src');
				  	//alert(cropImgdata);
					$.ajax({
						type:"POST",
						url: base_url+"wishlist/upload_image_personalize",
						data:{"wishlist_image":cropImgdata, "lastwid":wishlistId},
						dataType:"json",
						success: function(result){
								console.log(result);
							}
						});
					},2000);*/
			}
/*>>>>>>> bbc40864a8fa3f455bc5af1b0a8d20e5536e2a06*/
    });
	
function readURL(input) {
	var fileTypes = ['jpg', 'jpeg', 'png','bmp'];  //acceptable file types
	var maxsize = "2097152";
	var filesize = input.files[0].size;
	
		if (input.files && input.files[0]) {
			 var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
			 isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types
			if (isSuccess) { //yes
			
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#uplode_img').attr('src', e.target.result);
					};
					reader.readAsDataURL(input.files[0]);
					return true;
				/*if(maxsize > filesize)
				{	
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#uplode_img').attr('src', e.target.result);
					};
					reader.readAsDataURL(input.files[0]);
					return true;
				}
				else
				{
					$("#wisimage").notify('Image Size should not be more than 2MB. ');
					return false;
				}*/
				
			}else{
				$("#wisimage").notify('File Type Not Supported.');
				return false;
			}
		}
	
}

 
  
 // Ajax User step2edit..................
$(document).on('click','.editpersonalize',function(event){

if($("#personalizeform").valid() == true)
		{

		event.preventDefault();
		
		var wid = $('#wishid').val();
		var rsvp_info = $('#guest_note').val();
		var eventdate = $('#eventpdate').val();
		var wishlist_address = $('#from').val();
		var wishid = $('#wishid').val();
		var cropImgdata = $(".imgreviewcrop").attr('src');
		
			 var address= wishlist_address ;//address which you want Longitude and Latitude
	 if(address !='')
	 {
	 var arlene1 = [];
		jQuery.ajax({
		type: "GET",
		dataType: "json",
		url: "http://maps.googleapis.com/maps/api/geocode/json",
		data: {'address': address,'sensor':false},
		success: function(data){
			if(data.results.length){
				jQuery('#getlatid').val(data.results[0].geometry.location.lat);
			    jQuery('#getlongid').val(data.results[0].geometry.location.lng);
				var lat = (data.results[0].geometry.location.lat)? data.results[0].geometry.location.lat : '';
				var long = (data.results[0].geometry.location.lng)? data.results[0].geometry.location.lng : '';
				
				var dataString = 'rsvp_info='+ rsvp_info + '&wishlist_address='+ encodeURIComponent(wishlist_address) +'&lastwid='+ wid +'&eventdate='+ eventdate +'&latitude='+ lat +'&longitute='+ long +'&wishlist_image='+ cropImgdata ;

       // $('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);

		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				url: base_url+'wishlist/edit_personalize',
				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
					
				setTimeout(function(){
						 $('#loaderBg2').remove();
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    //$('#firstname-error').css('display','block');

                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					$('html, body').animate({ scrollTop: 0 }, 0);
					 $('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step2" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled" )
					$( "#step2" ).addClass( "disabled" )
					$( "#complete" ).addClass( "active" )
					$( "#tab3" ).removeClass( "disabled" )
					$( "#tab3" ).addClass( "active" )
					
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               
			}	


			
		});
		
		return false;

				
			}else{
			
			
		   }
		}
		});	

	} else {
		
	    
var dataString = 'rsvp_info='+ rsvp_info + '&wishlist_address='+ encodeURIComponent(wishlist_address) +'&lastwid='+ wid +'&eventdate='+ eventdate +'&wishid='+ wishid +'&wishlist_image='+ cropImgdata;

       // $('#load').css('display','block');
		var lhtmt='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
 $("body").append(lhtmt);

		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				
				url: base_url+'wishlist/edit_personalize',
				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
						 $('#loaderBg2').remove();
                         if(result.msg == 'unique'){
							 
							 console.log('fail');
                    
                    //$('#firstname-error').css('display','block');

                   }
                if(result.msg == 'success'){
                    //window.location.href = base_url+'wishlist/create';
					$('html, body').animate({ scrollTop: 0 }, 0);
					 $('#wid').val(result.getwishid);
					$( "#step1" ).removeClass( "active" )
					$( "#step2" ).removeClass( "active" )
					$( "#step1" ).addClass( "disabled" )
					$( "#step2" ).addClass( "disabled" )
					$( "#complete" ).addClass( "active" )
					$( "#tab3" ).removeClass( "disabled" )
					$( "#tab3" ).addClass( "active" )
					
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               
			}	


			
		});
		
		return false;
		
	}
	
		}else{ 
         return false;
         }

  });


// Ajax User Registration..................
$(document).on('click','.wishlist',function(event){

    if($("#registrationform").valid() == true)
		{
		event.preventDefault();

    var regemail = $('#regemail').val();
    var regpass = $('#regpassword').val();
    var regfname = $('#fname').val();
    var reglname = $('#lname').val();
	var type= $(this).attr('dir');

var dataString = 'regemail='+ regemail + '&regpass='+ regpass + '&regfname='+ regfname + '&reglname='+ reglname;

        $('.img1loader').css('display','block');
		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				url: base_url+"user/signup",
				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
						 $('.img1loader').css('display','none');
                if(result.msg == 'unique'){
                     //$('#regemail-error').append(result.email.message);
                     $('#regemail-error12').css('display','block');

                   }
                if(result.msg == 'success'){
                    window.location.href = base_url+'wishlist/create';
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               
			}	


			
		});
		
		return false;
}else{ 
 return false;
}
  });

// Ajax gift Registration..................
$(document).on('click','.buygifts',function(event){

    if($("#registrationform").valid() == true)
		{
		event.preventDefault();

    var regemail = $('#regemail').val();
    var regpass = $('#regpassword').val();
    var regfname = $('#fname').val();
    var reglname = $('#lname').val();
	var type= $(this).attr('dir');

var dataString = 'regemail='+ regemail + '&regpass='+ regpass + '&regfname='+ regfname + '&reglname='+ reglname;

        $('.img1loader').css('display','block');
		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				url: base_url+"user/signup",
				data: dataString,
				cache: false,
                dataType: 'json',
				success: function(result){
				setTimeout(function(){
						 $('.img1loader').css('display','none');
                 if(result.msg == 'unique'){
                    // $('#regemail-error').append(result.email.message);
                     $('#regemail-error12').css('display','block');

                   }
                if(result.msg == 'success'){
                    window.location.href = base_url+'search';
                    
                 }
                 if(result.msg == 'error'){
                    // $('#regemail-error').text('Already Exists');
                    // $('#regemail-error').css('display','block');

                   }
				}, 1000);
               
			}	


			
		});
		
		return false;
}else{ 
 return false;
}
  });


	$('.link-btn').click(function(){
	var getval = $(this).parent().data('ctname');
	$("#setcat").val(getval);
	});

// Ajax User Login..................

$(document).on('click','.loginBtn',function(event){
	
	var gethidurl = $("#setcat").val();
 
		if($("#loginform").valid() == true)
		{
		event.preventDefault();
		var logemail = $('#logemail').val();
		var logpass = $('#logpass').val();
		
		var dataString = 'logemail='+ logemail + '&logpass='+ logpass;
		var lhtmt='<div class="waitprocess" id="loaderBg"><div class="loaderwait"><div></div><div style="background:#fffff;width:102px;height:100px"><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
		$("body").append(lhtmt);
		//$('.img1loader').css('display','block');
		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				url: base_url+"user/login",
				data: dataString,
				cache: false,
				dataType: 'json',
				success: function(result){
					$('#loaderBg').remove();
							setTimeout(function(){
							//$('.img1loader').css('display','none');
							
		if(result.msg == 'alreadylogedin'){
			//window.location.href = base_url;
		}

		if(result.msg == 'success'){
         		//window.location.href = base_url+'dashboard';
				window.location.href=window.location.href;
		}
		if(result.msg == 'success' && gethidurl !=''){
         		window.location.href = base_url+'create-a-wishlist?cat='+gethidurl;
		}
		if(result.msg == 'blocked'){
			
            $('.autherror').css('display','block');
         	$('.autherror').text('Access Denied!');
			 setTimeout(function(){
								  $('.autherror').css('display','none');
								}, 3000);
          	
		}
		if(result.msg == 'error'){
					
				 $('.autherror').css({'display':'block','border':'none'});
         	     $('.autherror').text('Username or Password is incorrect');
				 setTimeout(function(){
								  $('.autherror').css('display','none');
								}, 3000);
		}   
		}, 1000);
		}
			});
      
			return false;
		}
		
		else{

		return false;
	}
   
	


});


$(document).on('click','.modallogin',function(event){
      

		if($("#loginformmodal").valid() == true)
		{
		event.preventDefault();
		var logemail = $('#emailmodal').val();
		var logpass = $('#passwordmodal').val();
		
		var dataString = 'logemail='+ logemail + '&logpass='+ logpass;
		
		
		$('.loader').css('display','block');
		// AJAX Code To Submit Form.
		$.ajax({
				type: "POST",
				url: base_url+"user/login",
				data: dataString,
				cache: false,
				dataType: 'json',
				success: function(result){
							setTimeout(function(){
							$('.loader').css('display','none');
							
		if(result.msg == 'alreadylogedin'){
			//window.location.href = base_url;
		}

		if(result.msg == 'success'){
         		window.location.href = base_url+'my-wishlist';
		}
		if(result.msg == 'blocked'){
            $('.autherrormodal').css('display','block');
         	$('.autherrormodal').text('Access Denied!');
			 setTimeout(function(){
								  $('.autherrormodal').css('display','none');
								}, 3000);
          	
		}
		if(result.msg == 'error'){
				 $('.autherrormodal').css('display','block');
         	     $('.autherrormodal').text('You are an unauthorized user');
				 setTimeout(function(){
								  $('.autherrormodal').css('display','none');
								}, 3000);
		}   
		}, 1000);
		}
			});
			return false;
		}
		
		else{

		return false;
	}
   
		


//--------------------------------user update details-------------------------------------------

   
		



});
 var timeout = "";
 $('#file').bind('change', function() {
  //this.files[0].size gets the size of your file.
  var timeout = ((Math.round(this.files[0].size/1000))/60)/60;
	$(".timeouthidden").val(timeout);



});

$(document).on('click','.userupdateBtn',function(event){
		
		if($("#edituserprofile").valid() == true)
		{
				
		$("#firstname").removeClass("error");
		$("#lastname").removeClass("error");
		$("#email").removeClass("error");
      	$(".msg").hide();
		$(".fnameerror").empty();
		$(".lnameerror").empty();


		var fname 		= $('#firstname').val();
		var lname 		= $('#lastname').val();
		var email 		= $('#email').val();
		//var user_id	  = $('#user_id').val();
		var mobile	   = $('#mobile').val();
		
		var dob			= $('#dob').val();
		var fb 			= $('#facebook').val();
		var ig			= $('#instagram').val();
		var tw 			= $('#tw').val();
		
		var address1 	= $('#address1').val();
		var address2	= $('#address2').val();
		var kecamatan 	= $('#kecamatan').val();
		var city 		= $('#city').val();
		
		var postcode 	= $('#postcode').val();
		var image 		= $('.imagehidden').val();


		if(fname == ""){
						$(".fnameerror").text("Please Put First Name");
						$("#firstname").addClass("error");
						$("html, body").animate({ scrollTop: 0 }, "slow");
					   }
		else if(lname == ""){
						$(".lnameerror").text("Please Put Last Name");
						$("#lastname").addClass("error");
						$("html, body").animate({ scrollTop: 0 }, "slow");
					   		}
		/*else if(user_id == ""){
						$(".lnameerror").text("Please Put Last Name");
						$("#user_id").addClass("error");
						$("html, body").animate({ scrollTop: 0 }, "slow");
					   	}*/
		else if(email == ""){
						$(".lnameerror").text("Please Put Last Name");
						$("#email").addClass("error");
						$("html, body").animate({ scrollTop: 0 }, "slow");
					   	}
		else{
				var dataString = 'fname='+ fname + '&lname='+ lname +'&email='+ email + '&mobile='+ mobile + 
								'&dob='+ dob + '&fb='+ fb + '&ig='+ ig + '&tw='+ tw
								+'&address1='+address1+'&address2='+address2+'&kecamatan='+kecamatan+'&city='+city+
								'&postcode='+postcode+'&image='+image;
		
		
		
			// AJAX Code To Submit Form.
			var start_time = new Date().getTime();
			$.ajax({
					type: "POST",
					url: base_url+"user/updateprofile",
					data: dataString,
					cache: false,
					dataType: 'json',
			beforeSend: function() {
					// setting a timeout
					var timeout = $(".timeouthidden").val();
					$('.imgloader').css('display','block');
					$(".timecount").text("Approximatelly"+timeout+"sec");
			},
			success: function(result){
			var request_time = new Date().getTime() - start_time;
			setTimeout(function(){
				$('.imgloader').css('display','none');
				if(result.msg == 'alreadylogedin'){
				
				}
				if(result.msg == 'success'){
					$(".img-circle").attr('src',result.imagepath);
					$(".msg").show();
					$("html, body").animate({ scrollTop: 0 }, "slow");
				}
				});
				}	 
			 });
			return false;
		}
}
else{
		$("html, body").animate({ scrollTop: 150 }, "slow");
	}
	  	 
		
	});

$(document).on('click','.btnuserupdate',function(event){
		$("#user_id").removeClass("error");
		var user_id	  = $('#user_id').val();
		if(user_id == ""){
						$(".lnameerror").text("Please Put Last Name");
						$("#user_id").addClass("error");
						//$("html, body").animate({ scrollTop: 0 }, "slow");
					   	}
		
		else{
				var dataString = 'user_id='+user_id;
		
			$.ajax({
					type: "POST",
					url: base_url+"user/updateuserid",
					data: dataString,
					cache: false,
					dataType: 'json',
			beforeSend: function() {
					// setting a timeout
					//var timeout = $(".timeouthidden").val();
					//$('.imgloader').css('display','block');
					//$(".timecount").text("Approximatelly"+timeout+"sec");
			},
			success: function(result){
			
				if(result.msg == 'unique'){
				$(".errorusername").text('Please Use Unique Name');
				}
			
				if(result.msg == 'success'){
				$('.user_name').text($("#user_id").val());
				$('#useridModal').modal('hide');
				
				}
			
			
				}	
			 });
			return false;
		
		}
	  	 
		
	});

//---------------------------------------------Start User Profile Update----------------------------
/*$("#firstname").blur(function(){
		var fname = $('#firstname').val();
		var lname = $('#lastname').val();
		var address = $('#address').val();
		var city = $('#city').val();
		var state = $('#state').val();
		var zipcode = $('#zipcode').val();
		var country = $('#country').val();
		
		var start_time = new Date().getTime();
		var dataString = 'fname='+ fname + '&lname='+ lname + '&address='+ address + '&city='+ city + '&state='+ state + '&zipcode='+ zipcode + '&country='+ country;

		$.ajax({
				type: "POST",
				url: base_url+"user/updateprofile",
				data: dataString,
				cache: false,
				dataType: 'json',
				beforeSend: function() {
					// setting a timeout
                     var timeout = $(".timeouthidden").val();
					//$('.imgloader').css('display','block');
					//$(".timecount").text("Approximatelly"+timeout+"sec");
				},
				success: function(result){
				var request_time = new Date().getTime() - start_time;
				
						
							setTimeout(function(){
							$('.imgloader').css('display','none');
							
		if(result.msg == 'alreadylogedin'){
			//window.location.href = base_url;
		}

		if(result.msg == 'success'){
				 //$(".img-circle").attr('src',result.imagepath);
         		// $(".msg").show();
				 //$("html, body").animate({ scrollTop: 0 }, "slow");
		}
		
			});
}	

			});
			});

$("#lastname").blur(function(){
   		var fname = $('#firstname').val();
		var lname = $('#lastname').val();
		var address = $('#address').val();
		var city = $('#city').val();
		var state = $('#state').val();
		var zipcode = $('#zipcode').val();
		var country = $('#country').val();
		
		var start_time = new Date().getTime();
		var dataString = 'fname='+ fname + '&lname='+ lname + '&address='+ address + '&city='+ city + '&state='+ state + '&zipcode='+ zipcode + '&country='+ country;

		$.ajax({
				type: "POST",
				url: base_url+"user/updateprofile",
				data: dataString,
				cache: false,
				dataType: 'json',
				beforeSend: function() {
					// setting a timeout
                   var timeout = $(".timeouthidden").val();
					//$('.imgloader').css('display','block');
					//$(".timecount").text("Approximatelly"+timeout+"sec");
				},
				success: function(result){
				var request_time = new Date().getTime() - start_time;
				
						
							setTimeout(function(){
							$('.imgloader').css('display','none');
							
		if(result.msg == 'alreadylogedin'){
			//window.location.href = base_url;
		}

		if(result.msg == 'success'){
				// $(".img-circle").attr('src',result.imagepath);
         		 //$(".msg").show();
				 //$("html, body").animate({ scrollTop: 0 }, "slow");
		}
		
			});
}	

			});
			

});*/


//-----------------------------------AUTO FILLED VALIDATION CHECK-------------------------------------
   

$('.menu-list ul li').click(function(){
    $('.menu-list ul li').removeClass("active");
    $(this).addClass("active");
});

$('#logemail').on('change', function() {
  $("#loginform").valid();  // trigger validation test
  setTimeout(function(){
							$("#loginform").valid();  // trigger validation test
				},2000);// trigger validation test
});
$('#emailmodal').on('change', function() {
  $("#loginformmodal").valid();
  setTimeout(function(){
							$("#loginformmodal").valid();
				},2000);// trigger validation test
});

$("#emailmodal").hover(function(){
  
setTimeout(function(){
							$("#loginformmodal").valid();
				},2000);// trigger validation test
});

$("#logemail").hover(function(){
  
setTimeout(function(){
							$("#loginform").valid();
				},1000);// trigger validation test
});



//----for image crop----------


//-----------------------User Change Password--------------
$(document).on('click','.changepwd',function(event){

			$(".msg").hide();
			$(".fnameerror").empty();
			//$(".lnameerror").empty();
			var currentpassword = $('#cpasssword').val();
			var password 		= $('#newpassword').val();
			
			if($("#changepasswordform").valid() == true)
			{
				event.preventDefault();
				$('.loader').css('display','block');
				// AJAX Code To Submit Form.
				var dataString = 'password='+ password +'&curentpassword=' + currentpassword;
				$.ajax({
					type: "POST",
					url: base_url+"user/ajaxchangepassword",
					data: dataString,
					cache: false,
					dataType: 'json',
					success: function(result){
							
							setTimeout(function(){
							$('.loader').css('display','none');
								
							
							if(result.msg == 'notmatched'){
								$(".fnameerror").text('Password not matched');
							}
							
							if(result.msg == 'success'){
								$(".msg").show();
								$("html, body").animate({ scrollTop: 0 }, "slow");
								$("#cpasssword").val('');
							}
							
							});

						}	
	
				});
							return false;
						}
						else{
							return false;
						}
		
	});

//-----------------------User Forget Password--------------
$(document).on('click','.forgetpwdbtn',function(event){

			//$(".msg").hide();
			//$(".fnameerror").empty();
			//$(".lnameerror").empty();
			var email = $('#forgetemail').val();
			if($("#forgetpwdform").valid() == true)
			{
				event.preventDefault();
				$('.loader').css('display','block');
				// AJAX Code To Submit Form.
				var dataString = 'email='+ email;
				$.ajax({
					type: "POST",
					url: base_url+"user/forgetpassword",
					data: dataString,
					cache: false,
					dataType: 'json',
					success: function(result){
								
							
							
							$('.loader').css('display','none');
							if(result.msg == 'notmatched'){
								$(".fnameerror").text('Password not matched');
							}
							
							if(result.msg == 'success'){
								$(".forgetpwdmsg").show();
								$( ".forgetpwddiv" ).slideUp( "slow" );
                                $( ".registrationdiv" ).slideDown( "slow" );
								setTimeout(function(){
									$(".forgetpwdmsg").hide();
								},4000);// trigger validation test

								
								
							}
							
						

						}	
	
				});
							return false;
						}
						else{
							return false;
						}
		
	});

 //----------------------------forget Pwd slide-----------------
$(".forgetPwdlink").click(function(){
$( ".forgetpwddiv" ).slideToggle( "slow" );
$( ".registrationdiv" ).slideToggle( "slow" );


});


$( "#wishlistname" ).change(function() {
 
   //var wishname = $("#wishlistname").val();
    
	var str = $("#wishlistname").val();
	wishname = str.replace(/\s+/g, '-').toLowerCase();

	$("#wishlistname").keyup(function () {
    var name = $("#wishlistname").val();
    //var dname_without_space = $("#wishlistname").val().replace(/ /g, "");
    var name_without_special_char = name.replace(/[^a-zA-Z 0-9]+/g, "");
    $(this).val(name_without_special_char);
});
	
	 $('#seturl').val(wishname);
	 var userid = $("#userid").val();
	 var usetotwish = $("#usetotwish").val();
	var dataString = 'wishname='+ wishname;
	
  
  $('#loding').css({'display':'block'});
  $('#checkloding').css({'display':'none'});
  $('#crossloding').css({'display':'none'});
  $.ajax({
			type: "POST",
			url: base_url+"wishlist/get_url",
			data: dataString,
			cache: false,
			dataType: 'json',
			success: function(result){
				setTimeout(function(){
				
				
				if(result.msg == 'success'){
					
					$('#loding').css({'display':'none'});
					$('#checkloding').css({'display':'block'});
					
				}
				if(result.msg == 'error'){
					$('#loding').css({'display':'none'});
					$('#checkloding').css({'display':'block'});
					//$('#crossloding').css({'display':'block'});
					$('#seturl').val(wishname+'-'+userid+usetotwish );
					
					
				}
				
			}, 1000);
           }
		});
  });

var timer;
$( "#seturl" ).keyup(function() {
	
	var str = $('#seturl').val();
	  var pattern=/^[a-zA-Z0-9- ]*$/;
	  var check = pattern.test(str);
          if (check == false) {
				var dd = $("#seturl").val();
				//var dname_without_space = $("#wishlistname").val().replace(/ /g, "");
				var name_without_special_char = dd.replace(/[^a-zA-Z0-9-]+/g,"");
				$(this).val(name_without_special_char);
				
				return false;
          }else {
	
			clearTimeout(timer);
			timer = setTimeout(function() {
			//var wishname = $("#wishlistname").val();
			
			
			var str = $("#seturl").val();
			var wishname = str.replace(/\s+/g, '-').toLowerCase();
			
			$('#seturl').val(wishname);
			var userid = $("#userid").val();
			var usetotwish = $("#usetotwish").val();
			
			
			var dataString = 'wishname='+ wishname;
		
		
			$('#loding').css({'display':'block'});
			$('#checkloding').css({'display':'none'});
			$('#crossloding').css({'display':'none'});
			$( '#seturl' ).removeClass( "error" );
  
  
  $.ajax({
			type: "POST",
			url: base_url+"wishlist/get_url",
			data: dataString,
			cache: false,
			dataType: 'json',
			success: function(result){
				setTimeout(function(){
				
				
				if(result.msg == 'success'){
					
					$('#loding').css({'display':'none'});
					$('#checkloding').css({'display':'block'});
					$('#crossloding').css({'display':'none'});
					
				}
				if(result.msg == 'error'){
					$('#loding').css({'display':'none'});
					$('#checkloding').css({'display':'none'});
					$('#crossloding').css({'display':'block'});
					$( '#seturl' ).addClass( "error" );
					$("#seturl").val('');
					
				}
				
			}, 1000);
           }
		});
		
	}, 500);	
	
	        
	
		  }
		
  });	
